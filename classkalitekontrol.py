#!/usr/bin/env python
# -*- coding:utf -8-*-
from mysql.connector import (connection)
from PyQt5 import QtWidgets
import kalitekontrol

class Kalitekontrol(kalitekontrol.Ui_Dialog, QtWidgets.QDialog):
    def __init__(self, parent=None):
        super(Kalitekontrol, self).__init__(parent)
        self.setupUi(self)
        self.xlikisemirleri.currentIndexChanged.connect(self.isemirlerilistesiguncelle)
        self.dfibaslat.clicked.connect(self.dfiekrani)
        try:
            self.conn = connection.MySQLConnection(user='root', password='13579', host='localhost', database='AKMILL')
            self.cursor = self.conn.cursor()
        except:
            QtWidgets.QMessageBox.warning(self, "BAĞLANTI SORUNU",
                                          "VERİTABANINA BAĞLANTI GERÇEKLEŞTİRİLEMEDİ \n BAĞLANTILARINIZI KONTROL EDİNİZ")
            return
        self.isemirlerilistesiguncelle()
        self.isemirlerilistesi.currentCellChanged.connect(self.isemriuretimdetaylarilistesiguncelle)
        self.uretimdetaylari.currentCellChanged.connect(self.isemritakibipreview)
        self.kalitekontrolkaydet.clicked.connect(self.kaydet)

    def dfiekrani(self):
        dfi = Duzelticifaaliyet()
        dfi.show()
        dfi.exec_()

    def kaydet(self):
        try:

            isemrisatir = self.isemirlerilistesi.currentRow()
            uretimdetaysatir = self.uretimdetaylari.currentRow()
            now = datetime.datetime.now()
            date = now.strftime("%Y-%m-%d")
            timer = now.strftime("%H:%M")
            zaman = date + " " + timer
            if isemrisatir == -1 or uretimdetaysatir == -1:
                QtWidgets.QMessageBox.warning(self, "HATA", "İŞ EMRİ VE ÜRETİMDETAYI SEÇİMİNİ YAPMANIZ GEREKMEKTEDİR")
                return
            else:
                pass
            isemriid = self.isemirlerilistesi.item(isemrisatir, 0).text()
            uretimdetayid = self.uretimdetaylari.item(uretimdetaysatir, 5).text()
            # if self.sartlionay.isChecked() :
            #     aciklamalar = self.sartlionayaciklamalar.toPlainText()
            # elif self.ret.isChecked() :
            #     aciklamalar = self.retaciklamalar.toPlainText()
            # elif self.onay.isChecked():
            #     aciklamalar = "ONAYLANDI"
            try:
                self.cursor.execute("start transaction")
                self.cursor.execute(
                    "update isemirlerikalite set fai = 'ONAYLANDI', faitarih = '" + zaman + "' where isemirleri_isemri_id = '" + str(
                        isemriid) + "'")
                self.cursor.execute(
                    "update isemirleri set isdurumu = 'ONAYLANDI' where isemirleri.isemri_id = '" + str(isemriid) + "'")
                QtWidgets.QMessageBox.information(self, "BİLGİLENDİRME",
                                                  "SEÇMİŞ OLDUĞUNUZ İŞ EMRİNE FAI ONAYI VERİLMİŞTİR...")

                # todo personel id si gerekli olduğu için personelin iş emrini alma kısmına geçildi


            except:
                QtWidgets.QMessageBox.warning(self, "HATA",
                                              "BEKLENMNEDİK BİR HATA İLE KARŞILAŞILDI İŞLEMLER GERİ ALINYIOR...")
                self.cursor.execute("rollback")
                import traceback
                traceback.print_exc()
                return
            self.conn.commit()
            self.isemirlerilistesiguncelle()
        except:
            import traceback
            traceback.print_exc()
        pass

    def isemirlerilistesiguncelle(self):
        try:
            x = self.xlikisemirleri.currentText()
            self.cursor.execute(
                "select isemri_id, isemri_no, tahmini_ayar_zamani, uretimdetaylandirma_uretim_detaylandirma_id, aciklamalar, fai "
                "from isemirleri join isemirlerikalite on isemirlerikalite.isemirleri_isemri_id = isemirleri.isemri_id  order by isemirleri.isemri_id desc limit " + str(
                    x) + " ")
            datalar = self.cursor.fetchall()
            self.isemirlerilistesi.setRowCount(len(datalar))
            for satir, data in enumerate(datalar):
                self.isemirlerilistesi.setItem(satir, 0, QtWidgets.QTableWidgetItem(str(data[0])))
                self.isemirlerilistesi.setItem(satir, 1, QtWidgets.QTableWidgetItem(data[1]))
                self.isemirlerilistesi.setItem(satir, 2, QtWidgets.QTableWidgetItem(str(data[2])))
                self.isemirlerilistesi.setItem(satir, 3, QtWidgets.QTableWidgetItem(str(data[3])))
                self.isemirlerilistesi.setItem(satir, 4, QtWidgets.QTableWidgetItem(data[4]))
                self.isemirlerilistesi.setItem(satir, 5, QtWidgets.QTableWidgetItem(data[5]))

        except:
            import traceback
            traceback.print_exc()
        self.isemirlerilistesi.setColumnHidden(0, True)
        self.isemirlerilistesi.setColumnHidden(3, True)

        pass

    def isemriuretimdetaylarilistesiguncelle(self):
        satir = self.isemirlerilistesi.currentRow()
        if satir == -1:
            return
        else:
            uretimdetayid = self.isemirlerilistesi.item(satir, 3).text()
        try:

            self.cursor.execute(
                "select akmill_teknik_resim_no , musteri_teknik_resim_no, uretimdetaylandirma.adet,  operasyon_sayisi, malzeme_adi ,uretim_detaylandirma_id, akmillteknikresimleri.dosya_yolu,"
                "akmillteknikresimleri.dosya_yolu_onizleme, musteriteknikresimleri.dosya_yolu  , musteriteknikresimleri.dosya_yolu_onizleme, malzemetalepleri.olcux, malzemetalepleri.olcuy, malzemetalepleri.olcuz from uretimdetaylandirma "
                "left join akmillteknikresimleri on akmillteknikresimleri.akmill_teknikresim_id =uretimdetaylandirma.akmillteknikresimleri_akmill_teknikresim_id "
                "left join musteriteknikresimleri on musteriteknikresimleri.musteri_teknikresim_id = uretimdetaylandirma.musteriteknikresimleri_musteri_teknikresim_id"
                " left join malzemetalepleri on malzemetalepleri.uretimdetaylandirma_uretim_detaylandirma_id = uretimdetaylandirma.uretim_detaylandirma_id "
                "left join  malzemelistesi on malzemelistesi.malzeme_id = uretimdetaylandirma.malzemelistesi_malzeme_id where uretimdetaylandirma.uretim_detaylandirma_id = '" + str(
                    uretimdetayid) + "'")

            uretimdetaylarilistesi = self.cursor.fetchall()
            self.uretimdetaylari.setRowCount(len(uretimdetaylarilistesi))
            for row, value in enumerate(uretimdetaylarilistesi):
                self.uretimdetaylari.setItem(row, 0, QtWidgets.QTableWidgetItem(value[0]))  # AKMILL RESIM NO
                try:
                    self.cursor.execute(
                        "select parca_adi from akmillteknikresimleri where akmill_teknik_resim_no = '" + value[0] + "'")

                    parcaadi = self.cursor.fetchone()[0]
                    print parcaadi
                    self.uretimdetaylari.setItem(row, 10, QtWidgets.QTableWidgetItem(parcaadi))  # PARCA ADI

                except:
                    pass

                self.uretimdetaylari.setItem(row, 1, QtWidgets.QTableWidgetItem(value[1]))  # MUSTERI RESIM NO
                self.uretimdetaylari.setItem(row, 2, QtWidgets.QTableWidgetItem(str(value[2])))  # ADET
                self.uretimdetaylari.setItem(row, 3, QtWidgets.QTableWidgetItem(str(value[3])))  # OP SAYISI
                self.uretimdetaylari.setItem(row, 4, QtWidgets.QTableWidgetItem(value[4]))  # MALZEME ADI
                self.uretimdetaylari.setItem(row, 5, QtWidgets.QTableWidgetItem(str(value[5])))  # URETIMDETAYID
                self.uretimdetaylari.setItem(row, 6, QtWidgets.QTableWidgetItem(value[6]))  # MALZEME ADI
                self.uretimdetaylari.setItem(row, 7, QtWidgets.QTableWidgetItem(value[7]))  # MALZEME ADI
                self.uretimdetaylari.setItem(row, 8, QtWidgets.QTableWidgetItem(value[8]))  # MALZEME ADI
                self.uretimdetaylari.setItem(row, 9, QtWidgets.QTableWidgetItem(value[9]))  # MALZEME ADI
                # self.uretimdetaylari.setItem(row, 10, QtWidgets.QTableWidgetItem(str(value[10]))) # OLCUX
                # self.uretimdetaylari.setItem(row, 11, QtWidgets.QTableWidgetItem(str(value[11]))) # OLCUY
                # self.uretimdetaylari.setItem(row, 12, QtWidgets.QTableWidgetItem(str(value[12]))) # OLCUZ

            self.uretimdetaylari.setColumnHidden(3, True)

            self.uretimdetaylari.setColumnHidden(5, True)
            self.uretimdetaylari.setColumnHidden(6, True)
            self.uretimdetaylari.setColumnHidden(7, True)
            self.uretimdetaylari.setColumnHidden(8, True)
            self.uretimdetaylari.setColumnHidden(9, True)
            # self.uretimdetaylari.setColumnHidden(10, False)
            # self.uretimdetaylari.setColumnHidden(11, False)
            # self.uretimdetaylari.setColumnHidden(12, False)



        except:
            import traceback
            traceback.print_exc()
            return

    def isemritakibipreview(self):

        try:
            satir = self.uretimdetaylari.currentRow()
            sutun = self.uretimdetaylari.currentColumn()
            if sutun == 0:
                path = self.uretimdetaylari.item(satir, 7).text()
                if len(path) == 0:
                    self.preview.setText("PREVIEW")
                else:
                    pixmap = QPixmap(path)
                    # pixmap= pixmap.scaledToHeight(600)
                    # pixmap= pixmap.scaledToWidth(400)
                    self.preview.setScaledContents(True)
                    self.preview.setPixmap(pixmap)
                    self.preview.setFixedSize(580, 580)
            elif sutun == 1:
                path = self.uretimdetaylari.item(satir, 9).text()
                pixmap = QPixmap(path)
                if len(path) == 0:
                    self.preview.setText("PREVIEW")
                else:
                    pixmap = QPixmap(path)
                    # pixmap= pixmap.scaledToHeight(600)
                    # pixmap= pixmap.scaledToWidth(400)
                    self.preview.setScaledContents(True)
                    self.preview.setPixmap(pixmap)
                    self.preview.setFixedSize(580, 580)
            else:
                pass

        except:
            import traceback
            traceback.print_exc()
            return

        pass
    #
    # def kontrollistesiguncelle(self):
    #     pass
    #     self.kalitekontrolisemirleri.clearContents()
    #     self.kalitekontrolisemirleri.setRowCount(0)
    #     self.cursor.execute("select * from isemirlerilistesi where kalitekontrol = 'KONTROL EDİLMEDİ'")
    #     liste = self.cursor.fetchall()
    #     self.kalitekontrolisemirleri.setRowCount(len(liste))
    #
    #     for satir, data in enumerate(liste):
    #         self.kalitekontrolisemirleri.setItem(satir, 0, QtWidgets.QTableWidgetItem(data[0]))
    #         self.kalitekontrolisemirleri.setItem(satir, 1, QtWidgets.QTableWidgetItem(data[1]))
    #         self.kalitekontrolisemirleri.setItem(satir, 2, QtWidgets.QTableWidgetItem(data[2]))
    #         self.kalitekontrolisemirleri.setItem(satir, 3, QtWidgets.QTableWidgetItem(data[3]))
    #         self.kalitekontrolisemirleri.setItem(satir, 4, QtWidgets.QTableWidgetItem(data[4]))
    #         self.kalitekontrolisemirleri.setItem(satir, 5, QtWidgets.QTableWidgetItem(data[5]))
    #         self.kalitekontrolisemirleri.setItem(satir, 6, QtWidgets.QTableWidgetItem(data[6]+" "+data[7]))
    #         self.kalitekontrolisemirleri.setItem(satir, 7, QtWidgets.QTableWidgetItem(data[10]))
    # def kontrolkaydet(self):
    #     pass
    #     satir = self.kalitekontrolisemirleri.currentRow()
    #     if satir == -1:
    #         QtWidgets.QMessageBox.warning(self, "HATA", "LÜTFEN SEÇİM YAPINIZ")
    #         return
    #     else:
    #         col = 0
    #         kontrol = self.kontroldurumu.currentText()
    #         isemrinumarasi = self.kalitekontrolisemirleri.item(satir, col).text()
    #         try :
    #             self.cursor.execute("start transaction")
    #             if kontrol == "RET":
    #                 try:
    #                     self.cursor.execute("update isemirleri set kalitekontrol= 'RET' where isemrino = '"+str(isemrinumarasi)+"'")
    #                     self.cursor.execute("update isemirleri set kalitekontrol= 'RET' where isemrino = '"+str(isemrinumarasi)+"'")
    #                     QtWidgets.QMessageBox.warning(self, "BAŞARILI", "YAPILAN İŞE ONAY VERİLMEDİ \n ÜRETİM İÇİN TEKRAR İŞEMRİ OLUŞTURULMASI GEREKMEKTEDİR")
    #                 #     todo
    #                 except:
    #                     QtWidgets.QMessageBox.warning(self, "HATA" , "İŞLEM GERÇEKLEŞTİRİLEMEDİ")
    #                     self.cursor.execute("rollback")
    #             else:
    #                 self.cursor.execute("start transaction")
    #                 self.cursor.execute("update isemirleri set kalitekontrol = '"+kontrol+"' where isemrino = '"+isemrinumarasi+"'")
    #                 QtWidgets.QMessageBox.warning(self, "BAŞARILI", "KAYIT DEĞİŞTİRİLDİ")
    #         except :
    #             self.cursor.execute("rollback")
    #             QtWidgets.QMessageBox.warning(self, "HATA", "İŞLEM GERÇEKLEŞTİRİLEMEDİ")
    #         self.conn.commit()
    #     pass