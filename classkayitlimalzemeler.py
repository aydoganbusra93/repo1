#!/usr/bin/env python
# -*- coding:utf -8-*-
import malzemelistesi
from mysql.connector import (connection)
from PyQt5 import QtWidgets

class Kayitlimalzemeler(malzemelistesi.Ui_kayitlimalzemelistesi, QtWidgets.QDialog):
    def __init__(self, parent=None):
        super(Kayitlimalzemeler, self).__init__(parent)
        self.setupUi(self)
        try:
            self.conn = connection.MySQLConnection(user='root', password='13579', host='localhost', database='AKMILL')
            self.cursor = self.conn.cursor()
        except:
            QtWidgets.QMessageBox.warning(self, "BAĞLANTI SORUNU",
                                          "VERİTABANINA BAĞLANTI GERÇEKLEŞTİRİLEMEDİ \n BAĞLANTILARINIZI KONTROL EDİNİZ")
            return
        self.malzemeekle.clicked.connect(self.malzemekaydet)
        self.malzemelistesiguncelle()
        self.malzemesil.clicked.connect(self.malzemekaldir)

        pass

    def malzemekaldir(self):
        satir = self.malzemelistesi.currentRow()
        if satir == -1:
            QtWidgets.QMessageBox.warning(self, "HATA", "SEÇİM YAPMANIZ GEREKMEKTEDİR")
        else:
            malzemeid = self.malzemelistesi.item(satir, 4).text()
            try:
                self.cursor.execute("start transaction")
                self.cursor.execute("delete from malzemelistesi where malzeme_id = '" + malzemeid + "'")
            except:
                import traceback
                traceback.print_exc()
                self.cursor.execute("rollback")
                QtWidgets.QMessageBox.warning(self, "HATA", "İŞLEM GERÇEKLEŞTİRİLEMEDİ")
                return
            self.conn.commit()
            self.malzemelistesiguncelle()

    def malzemelistesiguncelle(self):
        try:
            self.cursor.execute("select * from malzemelistesi")
            malzemeler = self.cursor.fetchall()
            self.malzemelistesi.setRowCount(len(malzemeler))
            for satir, data in enumerate(malzemeler):
                self.malzemelistesi.setItem(satir, 0, QtWidgets.QTableWidgetItem(data[1]))
                self.malzemelistesi.setItem(satir, 1, QtWidgets.QTableWidgetItem(str(data[2])))
                self.malzemelistesi.setItem(satir, 2, QtWidgets.QTableWidgetItem(str(data[3])))
                self.malzemelistesi.setItem(satir, 3, QtWidgets.QTableWidgetItem(data[4]))
                self.malzemelistesi.setItem(satir, 4, QtWidgets.QTableWidgetItem(str(data[0])))
            self.malzemelistesi.setColumnHidden(4, True)
        except:
            import traceback
            traceback.print_exc()

    def malzemekaydet(self):
        malzemeadi = self.malzemecinsi.text()
        try:
            malzemeyogunlugu = float(self.malzemeyogunlugu.text())
            malzemesertligi = float(self.malzemesertlik.text())
            malzemecekme = float(self.malzemecekme.text())
        except:
            QtWidgets.QMessageBox.warning(self, "HATA", "LÜTFEN DEĞERLERİ REEL SAYI OLARKA GİRİNİZ")
            return
        try:
            self.cursor.execute("start transaction")
            self.cursor.execute(
                "insert into malzemelistesi (malzeme_adi , malzeme_yogunluk, cekme_dayanimi , sertlik ) values (%s, %s, %s, %s)",
                (malzemeadi, malzemeyogunlugu, malzemesertligi, malzemecekme))
            QtWidgets.QMessageBox.information(self, "BİLDİRİM", "KAYITLAR BAŞARI İLE EKLENDİ")
        except:
            QtWidgets.QMessageBox.warning(self, "HATA",
                                          "BEKLENMEDİK BİR HATA İLE KARŞILAŞILDI İŞLEMLER GERİ ALINIYOR \n DAHA SONRA TEKRAR DENEYİNİZ")
            return
        self.conn.commit()
        self.malzemelistesiguncelle()
