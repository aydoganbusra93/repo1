#!/usr/bin/env python
# -*- coding:utf -8-*-

from PyQt5 import QtWidgets, QtCore, QtGui
from mysql.connector import (connection)
import uretimemridetaylandirma
from PyQt5.QtGui import QPixmap
import subprocess

class UretimDetaylandirmaEkrani(QtWidgets.QDialog, uretimemridetaylandirma.Ui_Dialog):
    def __init__(self, parent=None):
        super(UretimDetaylandirmaEkrani, self).__init__(parent)
        self.setupUi(self)
        self.conn = connection.MySQLConnection(user='root', password='13579', host='localhost', database='AKMILL')
        self.cursor = self.conn.cursor()
        self.cursor.execute("select firma_adi from firmalar")
        firmalar = self.cursor.fetchall()
        for i in firmalar:
            self.firmalar.addItem(i[0])
        self.projeguncelle()
        # self.parcalarguncelle()
        self.firmalar.currentIndexChanged.connect(self.projeguncelle)
        self.uretimemrilistele.clicked.connect(self.uretimemirleriguncelle)

        self.projeler.currentIndexChanged.connect(self.uretimnumaralarinisifirla)
        self.resimlistesi.currentCellChanged.connect(self.preview)
        self.resim_kaynagi.currentIndexChanged.connect(self.resimlistesiguncelle)
        self.resimnoekle.clicked.connect(self.resimnoal)
        self.malzemelistelerinigetir()
        self.uretimdetayiekle.clicked.connect(self.uretimdetayiekleme)
        self.siparisdetaylari.currentCellChanged.connect(self.uretimdetayiguncelle)
        self.uretimdetaylari.currentCellChanged.connect(self.previewdetay)
        self.uretimdetaylari.doubleClicked.connect(self.detayduzenle)
        self.uretimdetaysil.clicked.connect(self.uretimdetayikaldir)
        self.aranacak.textChanged.connect(self.aramayap)
        self.siparisdetaylari.setColumnHidden(5, True)
        self.uretimdetaylari.setColumnHidden(5, True)
        self.uretimdetaylari.setColumnHidden(6, True)
        self.uretimdetaylari.setColumnHidden(7, True)
        self.uretimdetaylari.setColumnHidden(8, True)
        self.uretimdetaylari.setColumnHidden(9, True)

        # self.ekle.clicked.connect(self.uretimdetayiekle)
        # self.uretimdetaylistele.clicked.connect(self.uretimemirleriguncelle)
        # self.teknikresimler.currentIndexChanged.connect(self.resimnoveparcaesitle)
        # self.parcalar.currentIndexChanged.connect(self.parcaveresimnoesitle)
        # self.guncelle.clicked.connect(self.tablewidgetguncelle)
        # self.tableWidget.setSelectionBehavior(1)
        # self.tableWidget.setSelectionMode(1)
        # self.uretimdetayisil.clicked.connect(self.uretimdetayikaldir)
        # self.uretimemrinumaralari.currentIndexChanged.connect(self.tablewidgetguncelle)

    def malzemelistelerinigetir(self):
        try:
            self.cursor.execute("select malzeme_adi from malzemelistesi")
            malzemeler = self.cursor.fetchall()
            for i in malzemeler:
                self.malzeme_cinsi.addItems(i)
        except:
            QtWidgets.QMessageBox.information(self, "HATA", "MALZEME LİSTELERİNİ GÜNCELLENEMEDİ")
        pass

    def resimnoal(self):
        satir = self.resimlistesi.currentRow()
        if satir == -1:

            QtWidgets.QMessageBox.warning(self, "HATA", "LÜTFEN BİR SEÇİM YAPINIZ")
        elif self.resim_kaynagi.currentText() == 'AKMILL':
            self.akmill_resim_numarasi.setText(self.resimlistesi.item(satir, 2).text())
            QtWidgets.QMessageBox.information(self, "BİLDİRİM",
                                              "SEÇMİŞ OLDUĞUNUZ RESİMNUMARASI AKMILL RESİM NUMARASI BÖLÜMÜNE KOPYALANMIŞTIR")

        else:
            self.musteri_resim_numarasi.setText(self.resimlistesi.item(satir, 2).text())
            QtWidgets.QMessageBox.information(self, "BİLDİRİM",
                                              "SEÇMİŞ OLDUĞUNUZ RESİMNUMARASI MUSTERİ RESİM NUMARASI BÖLÜMÜNE KOPYALANMIŞTIR")

    def preview(self):
        try:
            satir = self.resimlistesi.currentRow()
            path = self.resimlistesi.item(satir, 4).text()
            if len(path) == 0:
                self.onizleme.setText("PREVIEW")
            else:
                pixmap = QPixmap(path)
                pixmap = QPixmap(path)
                pixmap = pixmap.scaledToHeight(600)

                self.onizleme.setScaledContents(True)

                self.onizleme.setPixmap(pixmap)
                # self.onizleme.setFixedSize(580, 580)

        except:
            import traceback
            traceback.print_exc()
            return

    def uretimnumaralarinisifirla(self):
        self.uretimemrinumaralari.clear()

    def uretimdetayikaldir(self):
        pass
        satir = self.tableWidget.currentRow()
        if satir == -1:
            QtWidgets.QMessageBox.warning(self, "HATA", "BİR SEÇİM YAPINIZ")
            return
        uretimno = self.tableWidget.item(satir, 0).text()
        firma = self.tableWidget.item(satir, 1).text()
        proje = self.tableWidget.item(satir, 2).text()
        teknikresimno = self.tableWidget.item(satir, 4).text()
        self.cursor.execute("select id from uretimemirleri where uretimEmirNo= '" + uretimno + "'")
        uretimnoid = self.cursor.fetchone()[0]
        self.cursor.execute("select firma_id from firmalar where firma_adi = '" + firma + "'")
        firmaid = self.cursor.fetchone()[0]
        self.cursor.execute("select id from projeler where projeadi = '" + proje + "'")
        projeid = self.cursor.fetchone()[0]
        self.cursor.execute("select id from teknikresimler where teknikresimno = '" + teknikresimno + "'")
        teknikresimid = self.cursor.fetchone()[0]
        try:
            self.cursor.execute("delete from uretimdetaylari where "
                                "uretimemrino_id = '" + str(uretimnoid) + "' and firma_id = '" + str(
                firmaid) + "' and proje_id = '" + str(projeid) + "' and teknikresim_id = '" + str(teknikresimid) + "'")
            self.conn.commit()
            QtWidgets.QMessageBox.information(self, "BAŞARILI", "SİLME İŞLEMİ GERÇEKLEŞTİRİLDİ")
        except:
            QtWidgets.QMessageBox.warning(self, "HATA", "BİR HATA İLE KARŞILAŞILDI")
        self.tablewidgetguncelle()

    def projeguncelle(self):
        try:
            self.aktiffirma = self.firmalar.currentText()
            self.cursor.execute("select firma_id from firmalar where firma_adi ='" + self.aktiffirma + "'")
            self.aktiffirmaid = self.cursor.fetchone()[0]
            self.cursor.execute(
                "SELECT proje_adi from projeler where FIRMALAR_firma_id = '" + str(self.aktiffirmaid) + "'")
            projeler = self.cursor.fetchall()
            self.projeler.clear()
            for i in projeler:
                self.projeler.addItem(i[0])
        except:
            QtWidgets.QMessageBox.information(self, "!!!", "BEKLENMEDIK BIR HATA ILE KARSILASILDI")
            self.uretimemrinumaralari.clear()

    def uretimemirleriguncelle(self):
        teknik_resim_kaynagi = self.resim_kaynagi.currentText()
        firmaadi = self.firmalar.currentText()
        try:
            self.cursor.execute("select proje_id from projeler where proje_adi = '" + self.projeler.currentText() + "'")
            fetch = self.cursor.fetchone()
            aktifprojeid = str(fetch[0])

            self.cursor.execute("select uretim_emir_no from uretimemirleri where FIRMALAR_firma_id = '" + str(
                self.aktiffirmaid) + "' and PROJELER_proje_id  = '" + aktifprojeid + "'")
            uretimemirleri = self.cursor.fetchall()
            self.uretimemrinumaralari.clear()
            if len(uretimemirleri) == 0:
                QtWidgets.QMessageBox.warning(self, "!!!", "Herhangi bir üretim emri bulunamamıştır")
                self.resimlistesi.clearContents()
                self.siparisdetaylari.clearContents()
                self.onizleme.setText("PREVIEW")
                return
            for i in uretimemirleri:
                self.uretimemrinumaralari.addItems(i)
            # self.parcalarguncelle()
            # self.parcalar.clear()
            # self.teknikresimler.clear()
            # self.cursor.execute("SELECT parcaadi,  teknikresimno from projedetaylarigorunumu where firma_adi = '" + str(self.firmalar.currentText().encode('utf-8')) + "' and projeadi ='" + self.projeler.currentText().encode('utf-8') + "'")
            # parcalar = self.cursor.fetchall()
            # self.parcaveresimno = {}
            # for i in parcalar:
            #     self.parcaveresimno[i[0]] = [i[1]]
            #     self.parcalar.addItem(i[0])
            #     self.teknikresimler.addItem(i[1])
        except:
            import traceback
            traceback.print_exc()
            QtWidgets.QMessageBox.information(self, "!!!", "aaaaTANIMLI EN AZ BIR PROJE BULUNMASI GEREKMEKTEDIR")
            self.uretimemrinumaralari.clear()
            return
        self.resimlistesiguncelle()
        self.siparisdetaylariguncelle()
        self.akmill_resim_numarasi.clear()
        self.musteri_resim_numarasi.clear()

    def aramayap(self):
        self.resimlistesi.clearContents()
        self.onizleme.setText("PREVIEW")
        if self.resim_kaynagi.currentText() == 'AKMILL':

            self.cursor.execute(
                "select parca_adi, revizyon_no, akmill_teknik_resim_no, dosya_yolu, dosya_yolu_onizleme, akmill_teknikresim_id from akmillteknikresimleri where firmalar_firma_id = '" + str(
                    self.aktiffirmaid) + "'and parca_adi like '%" + self.aranacak.text() + "%'")

            # self.cursor.execute("select firma_adi , parcaadi , teknikresimno, revno from teknikresimgorunumu where firma_adi = '" + self.firmaadi.currentText() + "' and parcaadi like '%" + self.parcaadi.text() + "%'")

            teknikresimler = self.cursor.fetchall()
            self.resimlistesi.setRowCount(len(teknikresimler))
            for row, value in enumerate(teknikresimler):
                self.resimlistesi.setItem(row, 0, QtWidgets.QTableWidgetItem(value[0]))
                self.resimlistesi.setItem(row, 1, QtWidgets.QTableWidgetItem(value[1]))
                self.resimlistesi.setItem(row, 2, QtWidgets.QTableWidgetItem(value[2]))
                self.resimlistesi.setItem(row, 3, QtWidgets.QTableWidgetItem(value[3]))
                self.resimlistesi.setItem(row, 4, QtWidgets.QTableWidgetItem(value[4]))
                self.resimlistesi.setItem(row, 5, QtWidgets.QTableWidgetItem(str(value[5])))
            self.resimlistesi.setColumnHidden(3, True)
            self.resimlistesi.setColumnHidden(4, True)
            self.resimlistesi.setColumnHidden(5, True)
        else:
            self.cursor.execute(
                "select parca_adi, revizyon_no, musteri_teknik_resim_no, dosya_yolu, dosya_yolu_onizleme, musteri_teknikresim_id from musteriteknikresimleri where firmalar_firma_id = '" + str(
                    self.aktiffirmaid) + "'and parca_adi like '%" + self.aranacak.text() + "%'")
            teknikresimler = self.cursor.fetchall()
            self.resimlistesi.setRowCount(len(teknikresimler))
            for row, value in enumerate(teknikresimler):
                self.resimlistesi.setItem(row, 0, QtWidgets.QTableWidgetItem(value[0]))
                self.resimlistesi.setItem(row, 1, QtWidgets.QTableWidgetItem(value[1]))
                self.resimlistesi.setItem(row, 2, QtWidgets.QTableWidgetItem(value[2]))
                self.resimlistesi.setItem(row, 3, QtWidgets.QTableWidgetItem(value[3]))
                self.resimlistesi.setItem(row, 4, QtWidgets.QTableWidgetItem(value[4]))
                self.resimlistesi.setItem(row, 5, QtWidgets.QTableWidgetItem(str(value[5])))
            self.resimlistesi.setColumnHidden(3, True)
            self.resimlistesi.setColumnHidden(4, True)
            self.resimlistesi.setColumnHidden(5, True)

        pass

    def siparisdetaylariguncelle(self):
        self.siparisdetaylari.clearContents()
        try:
            uretimemirnumarasi = self.uretimemrinumaralari.currentText()
            self.cursor.execute(
                "select uretim_emir_id,FIRMALAR_firma_id, PROJELER_proje_id from uretimemirleri where uretim_emir_no = '" + uretimemirnumarasi + "'")
            uretimfirmaprojeid = self.cursor.fetchall()
            uretimid = str(uretimfirmaprojeid[0][0])
            firmaid = str(uretimfirmaprojeid[0][1])
            projeid = str(uretimfirmaprojeid[0][2])
            self.cursor.execute(
                "select uretimemirleri.uretim_emir_no, firmalar.firma_adi , projeler.proje_adi, malzeme_tanimi, miktar, siparis_detay_id from siparisdetaylari  "
                "join uretimemirleri on  uretim_emir_id= '" + uretimid + "'"
                                                                         "join projeler on projeler.proje_id = '" + projeid + "' join firmalar on  firmalar.firma_id = '" + firmaid + "' where siparisdetaylari.URETIMEMIRLERI_uretim_emir_id = '" + uretimid + "'")
            # todo burada kaldin
            siparisdetaylari = self.cursor.fetchall()
            self.siparisdetaylari.setRowCount(len(siparisdetaylari))
            for row, value in enumerate(siparisdetaylari):
                self.siparisdetaylari.setItem(row, 0, QtWidgets.QTableWidgetItem(value[0]))
                self.siparisdetaylari.setItem(row, 1, QtWidgets.QTableWidgetItem(value[1]))
                self.siparisdetaylari.setItem(row, 2, QtWidgets.QTableWidgetItem(value[2]))
                self.siparisdetaylari.setItem(row, 3, QtWidgets.QTableWidgetItem(value[3]))
                self.siparisdetaylari.setItem(row, 4, QtWidgets.QTableWidgetItem(str(value[4])))
                self.siparisdetaylari.setItem(row, 5, QtWidgets.QTableWidgetItem(str(value[5])))
            self.siparisdetaylari.setColumnHidden(5, True)

        except:
            import traceback
            traceback.print_exc()

        pass

    def resimlistesiguncelle(self):
        self.resimlistesi.clearContents()
        self.onizleme.setText("PREVIEW")

        try:
            if self.resim_kaynagi.currentText() == 'AKMILL':

                self.cursor.execute(
                    "select parca_adi, revizyon_no, akmill_teknik_resim_no, dosya_yolu, dosya_yolu_onizleme, akmill_teknikresim_id from akmillteknikresimleri where firmalar_firma_id = '" + str(
                        self.aktiffirmaid) + "'")
                teknikresimler = self.cursor.fetchall()
                self.resimlistesi.setRowCount(len(teknikresimler))
                for row, value in enumerate(teknikresimler):
                    self.resimlistesi.setItem(row, 0, QtWidgets.QTableWidgetItem(value[0]))
                    self.resimlistesi.setItem(row, 1, QtWidgets.QTableWidgetItem(value[1]))
                    self.resimlistesi.setItem(row, 2, QtWidgets.QTableWidgetItem(value[2]))
                    self.resimlistesi.setItem(row, 3, QtWidgets.QTableWidgetItem(value[3]))
                    self.resimlistesi.setItem(row, 4, QtWidgets.QTableWidgetItem(value[4]))
                    self.resimlistesi.setItem(row, 5, QtWidgets.QTableWidgetItem(str(value[5])))
                self.resimlistesi.setColumnHidden(3, True)
                self.resimlistesi.setColumnHidden(4, True)
                self.resimlistesi.setColumnHidden(5, True)
            else:
                self.cursor.execute(
                    "select parca_adi, revizyon_no, musteri_teknik_resim_no, dosya_yolu, dosya_yolu_onizleme, musteri_teknikresim_id from musteriteknikresimleri where firmalar_firma_id = '" + str(
                        self.aktiffirmaid) + "'")
                teknikresimler = self.cursor.fetchall()
                self.resimlistesi.setRowCount(len(teknikresimler))
                for row, value in enumerate(teknikresimler):
                    self.resimlistesi.setItem(row, 0, QtWidgets.QTableWidgetItem(value[0]))
                    self.resimlistesi.setItem(row, 1, QtWidgets.QTableWidgetItem(value[1]))
                    self.resimlistesi.setItem(row, 2, QtWidgets.QTableWidgetItem(value[2]))
                    self.resimlistesi.setItem(row, 3, QtWidgets.QTableWidgetItem(value[3]))
                    self.resimlistesi.setItem(row, 4, QtWidgets.QTableWidgetItem(value[4]))
                    self.resimlistesi.setItem(row, 5, QtWidgets.QTableWidgetItem(str(value[5])))
                self.resimlistesi.setColumnHidden(3, True)
                self.resimlistesi.setColumnHidden(4, True)
                self.resimlistesi.setColumnHidden(5, True)

        except:
            import traceback
            traceback.print_exc()

    def parcalarguncelle(self):
        pass
        self.parcalar.clear()
        self.teknikresimler.clear()
        self.cursor.execute(
            "SELECT parcaadi,  teknikresimno from projedetaylarigorunumu where firma_adi = '" + self.firmalar.currentText() + "' and projeadi ='" + self.projeler.currentText() + "'")
        parcalar = self.cursor.fetchall()
        self.parcaveresimno = {}
        for i in parcalar:
            self.parcaveresimno[i[0]] = [i[1]]
            self.parcalar.addItem(i[0])
            self.teknikresimler.addItem(i[1])
        # self.parcaveresimnoesitle()

    def parcaveresimnoesitle(self):
        pass
        self.teknikresimler.setCurrentIndex(self.parcalar.currentIndex())

    def resimnoveparcaesitle(self):
        pass
        self.parcalar.setCurrentIndex(self.teknikresimler.currentIndex())

    def uretimdetayiekleme(self):
        pass
        if len(self.uretimemrinumaralari.currentText()) == 0:
            QtWidgets.QMessageBox.warning(self, "!!!", "ÜRETİM EMRİ NUMARASI SEÇİNİZ")
            return
        elif self.siparisdetaylari.currentRow() == -1:
            QtWidgets.QMessageBox.warning(self, "HATA",
                                          "SEÇİLMİŞ HERHANGİ BİR SİPARİŞ DETAYI BULUNAMAMIŞTIR \n LÜTFEN İLGİLİ SİPARİŞ DETAYINI SEÇİNİZ")
            return
        elif self.adet.value() == 0:
            QtWidgets.QMessageBox.warning(self, "HATA", "ADET SAYISINI BELİRTMENİZ GEREKMEKTEDİR")
            return
        elif self.opsayisi.value() == 0:
            QtWidgets.QMessageBox.warning(self, "HATA", "OPERASYON SAYISINI BELİRTMENİZ GEREKMEKTEDİR")
            return
        else:
            try:
                self.cursor.execute(
                    "select uretim_emir_id from uretimemirleri where uretim_emir_no = '" + self.uretimemrinumaralari.currentText() + "'")
                uretimid = self.cursor.fetchone()[0]
                satir = self.siparisdetaylari.currentRow()
                siparisdetayid = self.siparisdetaylari.item(satir, 5).text()
                if len(self.akmill_resim_numarasi.text()) == 0:
                    akmillresimid = None
                    pass
                else:
                    satir = self.siparisdetaylari.currentRow()
                    siparisdetayid = self.siparisdetaylari.item(satir, 5).text()

                    self.cursor.execute(
                        "select akmill_teknikresim_id from akmillteknikresimleri where akmill_teknik_resim_no = '" + self.akmill_resim_numarasi.text() + "'")
                    akmillresimid = str(self.cursor.fetchone()[0])
                    query = "select uretim_detaylandirma_id from uretimdetaylandirma where akmillteknikresimleri_akmill_teknikresim_id = '" + akmillresimid + "' and SIPARISDETAYLARI_siparis_detay_id = '" + siparisdetayid + "'"
                    try:
                        self.cursor.execute(query)
                    except:
                        import traceback
                        traceback.print_exc()
                    mevcutmu = self.cursor.fetchall()

                    if len(mevcutmu) != 0:
                        QtWidgets.QMessageBox.warning(self, "HATA",
                                                      "BU AKMILL TEKNİK RESİM NUMARASI İLE DAHA ÖNCE KAYIT GERÇEKLEŞTİRİLMİŞ OLUP AYNI KAYIT TEKRAR EKLENEMEZ \n"
                                                      "LÜTFEN ÖNCEKİ KAYITLARI KONTROL EDİNİZ")
                        return
                    else:
                        pass
                if len(self.musteri_resim_numarasi.text()) == 0:
                    musteriresimid = None
                    pass
                else:
                    satir = self.siparisdetaylari.currentRow()
                    siparisdetayid = self.siparisdetaylari.item(satir, 5).text()
                    self.cursor.execute(
                        "select musteri_teknikresim_id from musteriteknikresimleri where musteri_teknik_resim_no = '" + self.musteri_resim_numarasi.text() + "'")
                    musteriresimid = str(self.cursor.fetchone()[0])
                    self.cursor.execute(
                        "select uretim_detaylandirma_id from uretimdetaylandirma where musteriteknikresimleri_musteri_teknikresim_id = '" + musteriresimid + "' and SIPARISDETAYLARI_siparis_detay_id = '" + siparisdetayid + "'")
                    mevcutmu = self.cursor.fetchall()
                    if len(mevcutmu) != 0:
                        QtWidgets.QMessageBox.warning(self, "HATA",
                                                      "BU MÜŞTERİ TEKNİK RESİM NUMARASI İLE DAHA ÖNCE KAYIT GERÇEKLEŞTİRİLMİŞ OLUP AYNI KAYIT TEKRAR EKLENEMEZ \n"
                                                      "LÜTFEN ÖNCEKİ KAYITLARI KONTROL EDİNİZ")
                        return
                    else:
                        pass
                self.cursor.execute(
                    "select malzeme_id from malzemelistesi where malzeme_adi = '" + self.malzeme_cinsi.currentText() + "'")
                malzemeid = str(self.cursor.fetchone()[0])
                opsayisi = self.opsayisi.value()
                adet = self.adet.value()
                self.cursor.execute("select count(*) from uretimdetaylandirma where URETIMEMIRLERI_uretim_emir_id = '"+str(uretimid)+"'")
                toplamkayitsayisi = self.cursor.fetchone()[0]
                if toplamkayitsayisi == 0 :
                    siranumarasi = 1
                else :
                    self.cursor.execute("select numaralandirma from uretimdetaylandirma where URETIMEMIRLERI_uretim_emir_id= '" +str(uretimid)+"' order by numaralandirma desc")
                    sonkayitnumarasi = self.cursor.fetchall()[0][0]
                    siranumarasi = sonkayitnumarasi+ 1


                self.cursor.execute("start transaction")
                self.cursor.execute(
                    "insert into uretimdetaylandirma (URETIMEMIRLERI_uretim_emir_id, SIPARISDETAYLARI_siparis_detay_id, akmillteknikresimleri_akmill_teknikresim_id, musteriteknikresimleri_musteri_teknikresim_id,"
                    " malzemelistesi_malzeme_id, operasyon_sayisi,  adet, numaralandirma) values (%s, %s, %s, %s, %s, %s, %s, %s)",
                    (uretimid, siparisdetayid, akmillresimid, musteriresimid, malzemeid, opsayisi, adet, siranumarasi))
                QtWidgets.QMessageBox.information(self, "BİLDİRİM", "İŞLEM GERÇEKLEŞTİRİLDİ")
            except:
                import traceback
                traceback.print_exc()
                QtWidgets.QMessageBox.warning(self, "HATA", "BİR HATA İLE KARŞILAŞILDI")
            self.conn.commit()
            self.uretimdetayiguncelle()

    def uretimdetayiguncelle(self):
        satir = self.siparisdetaylari.currentRow()
        if satir == -1:
            return
        else:
            siparisdetayid = self.siparisdetaylari.item(satir, 5).text()
        try:

            self.cursor.execute(
                "select akmill_teknik_resim_no , musteri_teknik_resim_no, adet,  operasyon_sayisi, malzeme_adi ,uretim_detaylandirma_id, akmillteknikresimleri.dosya_yolu,"
                "akmillteknikresimleri.dosya_yolu_onizleme, musteriteknikresimleri.dosya_yolu  , musteriteknikresimleri.dosya_yolu_onizleme from uretimdetaylandirma "
                "left join akmillteknikresimleri on akmillteknikresimleri.akmill_teknikresim_id =uretimdetaylandirma.akmillteknikresimleri_akmill_teknikresim_id "
                "left join musteriteknikresimleri on musteriteknikresimleri.musteri_teknikresim_id = uretimdetaylandirma.musteriteknikresimleri_musteri_teknikresim_id "
                "left join  malzemelistesi on malzemelistesi.malzeme_id = uretimdetaylandirma.malzemelistesi_malzeme_id where uretimdetaylandirma.siparisdetaylari_siparis_detay_id = '" + str(
                    siparisdetayid) + "'")
            uretimdetaylarilistesi = self.cursor.fetchall()
            self.uretimdetaylari.setRowCount(len(uretimdetaylarilistesi))
            for row, value in enumerate(uretimdetaylarilistesi):
                self.uretimdetaylari.setItem(row, 0, QtWidgets.QTableWidgetItem(value[0]))  # AKMILL RESIM NO
                self.uretimdetaylari.setItem(row, 1, QtWidgets.QTableWidgetItem(value[1]))  # MUSTERI RESIM NO
                self.uretimdetaylari.setItem(row, 2, QtWidgets.QTableWidgetItem(str(value[2])))  # ADET
                self.uretimdetaylari.setItem(row, 3, QtWidgets.QTableWidgetItem(str(value[3])))  # OP SAYISI
                self.uretimdetaylari.setItem(row, 4, QtWidgets.QTableWidgetItem(value[4]))  # MALZEME ADI
                self.uretimdetaylari.setItem(row, 5, QtWidgets.QTableWidgetItem(str(value[5])))  # URETIMDETAYID
                self.uretimdetaylari.setItem(row, 6, QtWidgets.QTableWidgetItem(value[6]))  # MALZEME ADI
                self.uretimdetaylari.setItem(row, 7, QtWidgets.QTableWidgetItem(value[7]))  # MALZEME ADI
                self.uretimdetaylari.setItem(row, 8, QtWidgets.QTableWidgetItem(value[8]))  # MALZEME ADI
                self.uretimdetaylari.setItem(row, 9, QtWidgets.QTableWidgetItem(value[9]))  # MALZEME ADI

                self.cursor.execute(
                    "select parca_adi from akmillteknikresimleri where akmill_teknik_resim_no = '" + str(
                        value[0]) + "'")
                parcaadi = self.cursor.fetchone()[0]
                self.uretimdetaylari.setItem(row, 10, QtWidgets.QTableWidgetItem(parcaadi))
            self.uretimdetaylari.setColumnHidden(5, True)
            self.uretimdetaylari.setColumnHidden(6, True)
            self.uretimdetaylari.setColumnHidden(7, True)
            self.uretimdetaylari.setColumnHidden(8, True)
            self.uretimdetaylari.setColumnHidden(9, True)

        except:
            import traceback
            traceback.print_exc()
            return

    def previewdetay(self):
        try:
            satir = self.uretimdetaylari.currentRow()
            sutun = self.uretimdetaylari.currentColumn()
            if sutun == 0:
                path = self.uretimdetaylari.item(satir, 7).text()
                if len(path) == 0:
                    self.onizleme.setText("PREVIEW")
                else:
                    pixmap = QPixmap(path)
                    pixmap= pixmap.scaledToHeight(600)
                    # pixmap= pixmap.scaledToWidth(400)
                    self.onizleme.setScaledContents(True)
                    self.onizleme.setPixmap(pixmap)
                    # self.onizleme.setFixedSize(580, 580)
            elif sutun == 1:
                path = self.uretimdetaylari.item(satir, 9).text()
                pixmap = QPixmap(path)
                if len(path) == 0:
                    self.onizleme.setText("PREVIEW")
                else:
                    pixmap = QPixmap(path)
                    pixmap= pixmap.scaledToHeight(600)
                    # pixmap= pixmap.scaledToWidth(400)
                    self.onizleme.setScaledContents(True)
                    self.onizleme.setPixmap(pixmap)
                    # self.onizleme.setFixedSize(580, 580)
            else:
                pass

        except:
            import traceback
            traceback.print_exc()
            return

    def detayduzenle(self):
        sutun = self.uretimdetaylari.currentColumn()
        satir = self.uretimdetaylari.currentRow()
        if sutun == 0 or sutun == 1:
            self.detaydanresimac()
        elif sutun == 2:
            inp = QtWidgets.QInputDialog()
            inp.setInputMode(QtWidgets.QInputDialog.TextInput)
            text, ok = inp.getText(self, 'ADET SAYISINI DEĞİŞTİR', 'ADET SAYISI')
            if ok:
                if len(text) == 0:
                    QtWidgets.QMessageBox.warning(self, "HATA", "ALAN BOŞ BIRAKILAMAZ")
                    return
                else:
                    try:
                        self.cursor.execute("start transaction")
                        self.cursor.execute(
                            "update uretimdetaylandirma set adet = '" + text + "' where uretim_detaylandirma_id = '" + self.uretimdetaylari.item(
                                satir, 5).text() + "'")
                        QtWidgets.QMessageBox.warning(self, "BİLDİRİM", "MİKTAR DEĞİŞTİRİLDİ")
                    except:
                        import traceback
                        traceback.print_exc()
                        self.cursor.execute("rollback")
                        QtWidgets.QMessageBox.warning(self, "HATA", "LÜTFEN SAYISAL BİR DEĞER GİRİŞ YAPINIZ")
        elif sutun == 3:
            inp = QtWidgets.QInputDialog()
            inp.setInputMode(QtWidgets.QInputDialog.TextInput)
            text, ok = inp.getText(self, 'OPERASYON SAYISINI DEĞİŞTİR', 'OPERASYON SAYISI')
            if ok:
                if len(text) == 0:
                    QtWidgets.QMessageBox.warning(self, "HATA", "ALAN BOŞ BIRAKILAMAZ")
                    return
                else:
                    try:
                        self.cursor.execute("start transaction")
                        self.cursor.execute(
                            "update uretimdetaylandirma set operasyon_sayisi = '" + text + "' where uretim_detaylandirma_id = '" + self.uretimdetaylari.item(
                                satir, 5).text() + "'")
                        QtWidgets.QMessageBox.warning(self, "BİLDİRİM", "OPERASYON SAYISI DEĞİŞTİRİLDİ")
                    except:
                        import traceback
                        traceback.print_exc()
                        self.cursor.execute("rollback")
                        QtWidgets.QMessageBox.warning(self, "HATA", "LÜTFEN SAYISAL BİR DEĞER GİRİŞ YAPINIZ")
                self.conn.commit()

        else:
            QtWidgets.QMessageBox.warning(self, "HATA",
                                          "BU ALANLARDA DEĞİŞİKLİK YAPMANIZ İÇİN SİLİP TEKRAR EKLEME YAPMANIZ GEREKMEKTEDİR")
            return
        self.uretimdetayiguncelle()

    def detaydanresimac(self):
        satir = self.uretimdetaylari.currentRow()
        sutun = self.uretimdetaylari.currentColumn()
        if sutun == 0:
            path = self.uretimdetaylari.item(satir, 6).text()
            subprocess.Popen(path, shell=True)
        elif sutun == 1:
            path = self.uretimdetaylari.item(satir, 8).text()
            subprocess.Popen(path, shell=True)
        else:
            pass

    def uretimdetayikaldir(self):
        satir = self.uretimdetaylari.currentRow()
        if satir == -1:
            QtWidgets.QMessageBox.warning(self, "HATA", "BİR SEÇİM YAPMANIZ GEREKMEKTEDİR")
        else:
            uretimdetayid = self.uretimdetaylari.item(satir, 5).text()
            try:
                self.cursor.execute("start transaction")
                self.cursor.execute(
                    "delete from uretimdetaylandirma where uretim_detaylandirma_id = '" + uretimdetayid + "'")
                QtWidgets.QMessageBox.information(self, "BİLDİRİM", "KAYIT SİLME İŞLEMİ GERÇEKLEŞTİRİLDİ")
            except:
                self.cursor.execute("rollback")
                QtWidgets.QMessageBox.warning(self, "BAŞARILI",
                                              "BİR PROBLEMLE KARŞILAŞILDI İŞLEMLER GERİ ALINIYOR... OLASI SEBEPLER: \n "
                                              "İLGİLİ KAYIT İLE DAHA ÖNCE DOKÜMAN OLUŞTURULMUŞ OLABİLİR \n"
                                              "SUNUCU BAĞLANTISIYLA İLGİLİ BİR PROBLEM OLMUŞ OLABİLİR")
                return
        self.conn.commit()
        self.uretimdetayiguncelle()
        pass

    def tablewidgetguncelle(self):
        pass
        self.cursor.execute(
            "select * from uretimdetaylandirma where uretimEmirNo = '" + self.uretimemrinumaralari.currentText() + "'")
        uretimdetaylandirma = self.cursor.fetchall()
        self.tableWidget.setRowCount(len(uretimdetaylandirma))
        for row, value in enumerate(uretimdetaylandirma):
            self.tableWidget.setItem(row, 0, QtWidgets.QTableWidgetItem(value[0]))
            self.tableWidget.setItem(row, 1, QtWidgets.QTableWidgetItem(value[1]))
            self.tableWidget.setItem(row, 2, QtWidgets.QTableWidgetItem(value[2]))
            self.tableWidget.setItem(row, 3, QtWidgets.QTableWidgetItem(value[3]))
            self.tableWidget.setItem(row, 4, QtWidgets.QTableWidgetItem(value[4]))
            self.tableWidget.setItem(row, 5, QtWidgets.QTableWidgetItem(value[5]))
            self.tableWidget.setItem(row, 6, QtWidgets.QTableWidgetItem(str(value[7])))
