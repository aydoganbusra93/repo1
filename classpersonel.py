#!/usr/bin/env python
# -*- coding:utf -8-*-
from mysql.connector import (connection)
from PyQt5 import QtWidgets
import personeltakip
import datetime


class Personel(personeltakip.Ui_personeltakip, QtWidgets.QDialog):  # personel takib formunu cağıran class
    def __init__(self, parent=None):
        super(Personel, self).__init__(parent)
        self.setupUi(self)
        try:
            self.conn = connection.MySQLConnection(user='root', password='13579', host='localhost', database='AKMILL')
            self.cursor = self.conn.cursor()
        except:
            QtWidgets.QMessageBox.warning(self, "BAĞLANTI SORUNU",
                                          "VERİTABANINA BAĞLANTI GERÇEKLEŞTİRİLEMEDİ \n BAĞLANTILARINIZI KONTROL EDİNİZ")
            return
        self.personelEkle.clicked.connect(self.personelEklemesi)
        self.personellistesiguncelle()
        self.personelSil.clicked.connect(self.personelSilmesi)
        try:
            self.personellistesi.currentCellChanged.connect(self.fotogoster)
        except:
            import traceback
            traceback.print_exc()
        self.karttanimla.clicked.connect(self.kartnumarasinioku)
        # self.kart = KartOkuma()
        # self.kart.kartsinyali.connect(self.kartnumarasi)
        self.resimyukle.clicked.connect(self.personelfotoyukle)
        now = datetime.datetime.now()
        # date = now.strftime("%yyyy-%mm-%dd")
        self.personelbaslangictarihi.setDate(now)
        # todo tarih formatının database ile uyumlu yyyy-mm-dd formatında kaydedilmesi sağlanacak

    def personelfotoyukle(self):
        fileName = QtWidgets.QFileDialog.getOpenFileName(self, ("PERSONEL FOTOĞRAF SEÇİMİ"),
                                                         "C:/Users/Bilgisayar2/Desktop/akmill",
                                                         ("Image Files (*.png *.pdf  *.jpg)"))
        dosyayolu = QtCore.QFileInfo(fileName[0])
        dosyaadi = dosyayolu.baseName()
        anadosya = dosyayolu.filePath()
        if len(anadosya) == 0:
            return
        else:
            pass
        onizlemekayit = anadosya[:-(len(dosyaadi) + 4)]
        try:
            self.personelresimyolu.setText(fileName[0])
            if fileName[0][-3:] == 'pdf':
                QtWidgets.QMessageBox.warning(self, "BİLGİ",
                                              "SEÇMİŞ OLDUĞUNUZ PDF DOKÜMANININ İLK SAYFASI ÖNİZLEME OLARAK HAZIRLANIYOR")
                with Image(filename=fileName[0]) as img:
                    extractedimg = img.sequence[0]
                    extractedimg.resize(294, 347)
                    # img.sample(588, 731)
                    first_image = Image(image=extractedimg)
                    first_image.format = 'png'
                    first_image.save(filename=onizlemekayit + dosyaadi + '-preview.png')
                QtWidgets.QMessageBox.information(self, "BİLDİRİM", "ÖNİZLEME HAZIRLANDI ")
                self.personelonizleme.setText(onizlemekayit + dosyaadi + '-preview.png')
            elif fileName[0][-3:] == 'jpg':
                QtWidgets.QMessageBox.warning(self, "BİLGİ",
                                              "SEÇMİŞ OLDUĞUNUZ RESİM ÖNİZLEME İÇİN YENİDEN BOYUTLANDIRILIYOR")
                with Image(filename=fileName[0]) as img:
                    # extractedimg = img.sequence[0]
                    # extractedimg.resize(588,731)
                    img.resize(588, 731)
                    first_image = Image(image=img)
                    first_image.format = 'png'
                    first_image.save(filename=onizlemekayit + dosyaadi + '-preview.png')
                QtWidgets.QMessageBox.information(self, "BİLDİRİM", "RESİM YENİDEN BOYUTLANDIRILDI ")
                self.personelonizleme.setText(onizlemekayit + dosyaadi + '-preview.png')

            elif fileName[0][-3:] == 'png':
                QtWidgets.QMessageBox.warning(self, "BİLGİ",
                                              "SEÇMİŞ OLDUĞUNUZ RESİM ÖNİZLEME İÇİN YENİDEN BOYUTLANDIRILIYOR")
                with Image(filename=fileName[0]) as img:
                    # extractedimg = img.sequence[0]
                    # extractedimg.resize(588,731)
                    img.resize(588, 731)
                    first_image = Image(image=img)
                    first_image.format = 'png'
                    first_image.save(filename=onizlemekayit + dosyaadi + '-preview.png')
                QtWidgets.QMessageBox.information(self, "BİLDİRİM", "DOKÜMAN YENİDEN BOYUTLANDIRILDI ")
                self.personelonizleme.setText(onizlemekayit + dosyaadi + '-preview.png')
            else:
                pass
            path = self.personelonizleme.text()
            pixmap = QPixmap(path)
            self.label_5.setScaledContents(True)
            self.label_5.setPixmap(pixmap)
            self.label_5.setFixedSize(294, 397)
        except:
            import traceback
            traceback.print_exc()
        # self.teknikresimlistesi.currentCellChanged.connect(self.onizleme)

        pass

    def closeEvent(self, event):
        try:
            self.kart.ser.close()
            return
        except:
            return

    def kartnumarasinioku(self):
        try:
            self.kart.start()
        except:
            import traceback
            traceback.print_exc()
            QtWidgets.QMessageBox.warning(self, "HATA",
                                          "KART OKUYUCUDA VEYA DATABASE BAGLANTINIZDA PROBLEM OLABILIR \n KONTROL EDINIZ")
            return

        pass

    def kartnumarasi(self, kartno):
        try:
            self.personelkartid.setText(kartno)

        except:
            import traceback
            traceback.print_exc()

        pass

    def fotogoster(self):
        try:
            satir = self.personellistesi.currentRow()
            path = self.personellistesi.item(satir, 7).text()
            pixmap = QPixmap(path)
            pixmap = pixmap.scaledToHeight(150)
            self.label_5.setPixmap(pixmap)
            self.label_5.setScaledContents(True)
        except:
            QtWidgets.QMessageBox.warning(self, "HATA", "BÜTÜN ÇALIŞANLAR SİLİNMİŞ")

    def personellistesiguncelle(self):
        try:
            self.cursor.execute("select * from personel where aktif_pasif  = 1  ")
            personeldatasi = self.cursor.fetchall()
            self.personellistesi.setRowCount(len(personeldatasi))
            for satir, data in enumerate(personeldatasi):
                self.personellistesi.setItem(satir, 0, QtWidgets.QTableWidgetItem(data[1]))
                self.personellistesi.setItem(satir, 1, QtWidgets.QTableWidgetItem(data[2]))
                self.personellistesi.setItem(satir, 2, QtWidgets.QTableWidgetItem(data[3]))
                self.personellistesi.setItem(satir, 3, QtWidgets.QTableWidgetItem(data[5]))
                self.personellistesi.setItem(satir, 4, QtWidgets.QTableWidgetItem(data[6]))
                self.personellistesi.setItem(satir, 5, QtWidgets.QTableWidgetItem(str(data[7])))
                self.personellistesi.setItem(satir, 6, QtWidgets.QTableWidgetItem(str(data[0])))
                self.personellistesi.setItem(satir, 7, QtWidgets.QTableWidgetItem(data[11]))
            self.personellistesi.setColumnHidden(7, True)


        except:
            QtWidgets.QMessageBox.information(self, "HATA", "BEKLENMEDİK BİR HATA İLE KARŞILAŞILDI")
            return

    def personelEklemesi(self):
        personeladi = self.personel_adi.text()
        personelsoyadi = self.personel_soyadi.text()
        personelgorevi = self.personel_gorev.text()
        personeltc = self.personeltc.text()
        personelcepno = self.personelcep.text()
        personelbaslangic = self.personelbaslangictarihi.text()
        personelresim = self.personelresimyolu.text()
        personelonizleme = self.personelonizleme.text()
        personelkartid = self.personelkartid.text()

        self.cursor.execute("start transaction")
        try:
            self.cursor.execute(
                "select count(*) from personel where personel_adi = '" + personeladi + "' and personel_soyadi = '" + personelsoyadi + "' and aktif_pasif =1")
            mevcutmu = self.cursor.fetchone()[0]
            if len(personeladi) == 0 or len(personelsoyadi) == 0 or len(personelgorevi) == 0:
                QtWidgets.QMessageBox.warning(self, "HATA", "AD - SOYAD - GOREV TANIMLARI BOŞ BIRAKILAMAZ")
                return
            elif mevcutmu != 0:
                QtWidgets.QMessageBox.warning(self, "HATA",
                                              "BU AD VE SOYAD İLE DAHA ONCE KAYIT GEÇEKLEŞTİRİLDMİŞTİR TEKRAR EKLENEMEZ")
            else:
                self.cursor.execute(
                    "insert into personel (personel_adi, personel_soyadi , gorev , aktif_pasif, tc_no, cep_tel, personel_baslangic_tarihi, personelkartid, personelresimyolu,personelresimonizleme ) values (%s, %s, %s ,1, %s, %s, %s, %s, %s, %s)",
                    (personeladi, personelsoyadi, personelgorevi, personeltc, personelcepno, personelbaslangic,
                     personelkartid, personelresim, personelonizleme))
                self.conn.commit()
                QtWidgets.QMessageBox.information(self, "BAŞARILI", "KAYIT BAŞARI İLE GERÇEKLEŞTİRİLDİ")
                self.personellistesiguncelle()
                self.personel_adi.clear()
                self.personel_soyadi.clear()
                self.personel_gorev.clear()
                self.personeltc.clear()
                self.personelcep.clear()
                self.personelresimyolu.clear()
                self.personelonizleme.clear()
                self.personelkartid.clear()
        except:
            import traceback
            traceback.print_exc()
            QtWidgets.QMessageBox.warning(self, "HATA",
                                          "BEKLENMEDİK BİR HATA İLE KARŞILAŞILDI DAHA SONRA TEKRAR DENEYEBİLİRİSNİZ")
            return

    def personelSilmesi(self):
        try:
            satir = self.personellistesi.currentRow()
            ad = self.personellistesi.item(satir, 0).text()
            soyad = self.personellistesi.item(satir, 1).text()
        except:
            QtWidgets.QMessageBox.warning(self, "HATA", "LÜTFEN GEÇERLİ BİR SEÇİM YAPIINIZ")

        try:
            self.cursor.execute(
                "update personel set aktif_pasif  = False where personel_adi = '" + ad + "' and personel_soyadi = '" + soyad + "'")
            self.conn.commit()
            QtWidgets.QMessageBox.information(self, "BAŞARILI", "KAYIT BAŞARI İLE SİLİNDİ")
            self.personellistesiguncelle()

        except:
            import traceback
            traceback.print_exc()
            QtWidgets.QMessageBox.warning(self, "HATA", "BEKLENMEDİK BİR HATA İLE KARŞILAŞILDI")
