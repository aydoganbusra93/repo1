#!/usr/bin/env python
# -*- coding:utf -8-*-
import datetime

from PyQt5 import QtWidgets, QtCore, QtGui
from mysql.connector import (connection)
import malzemetalebi
from PyQt5.QtGui import QPixmap
import os
import pdfkit
import exeltablecreator




class Malzemetalebi(QtWidgets.QDialog, malzemetalebi.Ui_Dialog):
    def __init__(self, parent=None):
        super(Malzemetalebi, self).__init__(parent)
        try:
            self.setupUi(self)
        except:
            import traceback
            traceback.print_exc()
        self.conn = connection.MySQLConnection(user='root', password='13579', host='localhost', database='AKMILL')
        self.cursor = self.conn.cursor()
        self.cursor.execute("SELECT firma_adi  FROM firmalar")
        firmalar = self.cursor.fetchall()
        for i in firmalar:
            self.firmaadi.addItems(i)
        # self.projeguncelle()
        self.firmaadi.currentIndexChanged.connect(self.projelistesiguncelle)
        self.projeadi.currentIndexChanged.connect(self.malzemetalebisiparisdetaylariguncelle)
        self.siparisdetaylari.currentCellChanged.connect(self.malzemetalebiuretimdetaylariguncelle)
        self.uretimdetaylari.currentCellChanged.connect(self.malzemetalebipreview)
        self.malzemelistesiguncelle()
        durum = self.borumalzeme.checkState()
        self.borumalzeme.stateChanged.connect(self.malzemegirislerinilimitle)
        self.capmalzeme.stateChanged.connect(self.malzemegirislerinilimitle)
        self.profil.stateChanged.connect(self.malzemegirislerinilimitle)
        self.projelistesiguncelle()
        self.talepnumarasial.clicked.connect(self.talepnumarasiolustur)
        now = datetime.datetime.now()
        date = now.strftime("%Y-%m-%d")
        self.termintarihi.setDate(now)
        self.ekle.clicked.connect(self.listeyeekle)
        self.uretimdetaylari.currentCellChanged.connect(self.malzemeuretimdetaysecimesitleme)
        self.malzemelistesi.doubleClicked.connect(self.malzemeduzeltme)
        self.talepsil.clicked.connect(self.talepsilme)
        self.malzemelistesi.currentCellChanged.connect(self.uretimdetaymalzemeesitleme)
        # self.siparistalepformlistesi.currentCellChanged.connect(self.listegorunumu)
        self.pushButton.clicked.connect(self.siparisformuhazirla)
        # self.talepekrani.clicked.connect(self.talepgorunumu)
        # self.ara.clicked.connect(self.arama)
        # self.malzemelistesi.currentCellChanged.connect(self.getselection)
        # self.sil.clicked.connect(self.silme)
        self.kaydet.clicked.connect(self.formlarihazirla)
        # self.siparistalepformlistesigorunumu()
        # self.formac.clicked.connect(self.formuac)
        # self.formsil.clicked.connect(self.formusil)
        # self.parcalistesiguncelle()

    def uretimdetaymalzemeesitleme(self):
        malzemesatir = self.malzemelistesi.currentRow()
        uretimdetaysatir = self.uretimdetaylari.currentRow()
        if malzemesatir == -1 or uretimdetaysatir == -1:
            pass
        else:
            malzemeuretimdetayid = self.malzemelistesi.item(malzemesatir, 7).text()
            uretimdetayid = self.uretimdetaylari.item(uretimdetaysatir, 5).text()
            print uretimdetayid, malzemeuretimdetayid
            if malzemeuretimdetayid == uretimdetayid:
                print("esitlik mevcut")
                return
            else:
                pass
        uretimdetaylari = self.uretimdetaylari.rowCount()
        malzemedetaylari = self.malzemelistesi.rowCount()
        if uretimdetaylari == 0:
            return
        else:
            try:
                malzemeuretimdetayid = self.malzemelistesi.item(malzemesatir, 7).text()
                print ("malzeme uretimdetayid :: " + malzemeuretimdetayid)
                for i in range(0, uretimdetaylari):
                    if self.uretimdetaylari.item(i, 5).text() == malzemeuretimdetayid:
                        self.uretimdetaylari.selectRow(i)
                        print i
                        self.uretimdetaylari.selectRow(i)

                        break
                    else:
                        continue
                # self.malzemelistesi.clearSelection()

                # self.malzemelistesi.clearSelection()
            except:
                import traceback
                traceback.print_exc()
        pass

    def talepsilme(self):

        satir = self.malzemelistesi.currentRow()
        if satir == -1:
            QtWidgets.QMessageBox.warning(self, "HATA", "SEÇİM YAPMANIZ GEREKMEKTEDİR")
        else:
            pass
            try:
                self.cursor.execute("start transaction")
                self.cursor.execute(
                    "delete from malzemetalepleri where idmalzemetalepleri = '" + self.malzemelistesi.item(satir,
                                                                                                           9).text() + "'")
                QtWidgets.QMessageBox.information(self, " BİLDİRİM", "SİLME İŞLEMİ GERÇEKLEŞTİRİLDİ")

            except:
                QtWidgets.QMessageBox.warning(self, "HATA",
                                              "BEKLENMEDİK BİR HATA İLE KARŞILAŞILDI İŞLEMLER GERİ ALINIYOR...")
                self.cursor.execute("rollback")

        self.conn.commit()
        self.malzemetalepleriguncelle()

    def malzemeduzeltme(self):
        QtWidgets.QMessageBox.warning(self, "HATA",
                                      "DEĞİŞİKLİK YAPMANIZ GEREKİYORSA İLGİLİ TALEBİ SİLİP YENİDEN EKLEMENİZ GEREKMEKTEDİR")
        return

    def malzemeuretimdetaysecimesitleme(self):
        malzemesatir = self.malzemelistesi.currentRow()
        uretimdetaysatir = self.uretimdetaylari.currentRow()
        if malzemesatir == -1 or uretimdetaysatir == -1:
            pass
        else:
            malzemeuretimdetayid = self.malzemelistesi.item(malzemesatir, 7).text()
            uretimdetayid = self.uretimdetaylari.item(uretimdetaysatir, 5).text()
            if malzemeuretimdetayid == uretimdetayid:
                print("esitlik mevcut")
                return
            else:
                pass
        try:
            malzemetalepleri = self.malzemelistesi.rowCount()
            if malzemetalepleri == 0:
                return
            else:
                try:
                    uretimdetaysatir = self.uretimdetaylari.currentRow()
                    uretimdetayid = self.uretimdetaylari.item(uretimdetaysatir, 5).text()
                    for i in range(0, malzemetalepleri):
                        if self.malzemelistesi.item(i, 7).text() == uretimdetayid:
                            self.malzemelistesi.selectRow(i)
                            return
                        else:
                            self.malzemelistesi.clearSelection()
                            continue
                    # self.malzemelistesi.clearSelection()

                    # self.malzemelistesi.clearSelection()
                except:
                    import traceback
                    traceback.print_exc()

            pass
        except:
            import traceback
            traceback.print_exc()

    # def talepnumarasiolustur(self):
    #     satirdetay = self.siparisdetaylari.currentRow()
    #     satirsiparis = self.uretimdetaylari.currentRow()
    #     try:
    #         if satirdetay == -1:
    #             QtWidgets.QMessageBox.warning(self, "HATA", "ÜRETİM DETAYI SEÇMENİZ GEREKMEKTEDİR")
    #         else:
    #             siparisdetayid = self.siparisdetaylari.item(satirdetay, 5).text()
    #             uretimemirno = self.siparisdetaylari.item(satirdetay, 0).text()
    #             self.cursor.execute("select numaralandirma from siparisdetaylari where siparis_detay_id = '"+str(siparisdetayid)+"'")
    #             siranumarasi = self.cursor.fetchone()[0]
    #
    #             self.malzemetalepnumarasi.setText("PR-" + uretimemirno + "-" + str(siranumarasi))
    #             pass
    #     except:
    #         import traceback
    #         traceback.print_exc()




    def talepnumarasiolustur(self):
        satirdetay = self.siparisdetaylari.currentRow()
        satirsiparis = self.uretimdetaylari.currentRow()
        try:
            if satirdetay == -1:
                QtWidgets.QMessageBox.warning(self, "HATA", "ÜRETİM DETAYI SEÇMENİZ GEREKMEKTEDİR")
            else:
                siparisdetayid = self.siparisdetaylari.item(satirdetay, 5).text()
                uretimemirno = self.siparisdetaylari.item(satirdetay, 0).text()
                self.cursor.execute("select uretim_emir_id from uretimemirleri where uretim_emir_no = '"+uretimemirno+"'")
                uretimemirnoid = self.cursor.fetchone()[0]

                self.cursor.execute("select numaralandirma from siparisdetaylari where siparis_detay_id = '"+str(siparisdetayid)+"'")
                siranumarasi = self.cursor.fetchone()[0]
                ayristirilmis = uretimemirno.split("-")
                yil = ayristirilmis[1]
                numara = ayristirilmis[3]
                yeninumara = yil+ "-"+ numara
                self.cursor.execute("select malzemetalepformnumarasi from malzemetalepformlari where uretimemirid = '"+str(uretimemirnoid)+"'")
                malzemetalepnumarasi = self.cursor.fetchall()
                if len(malzemetalepnumarasi)== 0 :
                    siranumarasi = 1
                else :
                    siranumarasi = malzemetalepnumarasi[0]
                print siranumarasi

                self.malzemetalepnumarasi.setText("PR-" + yeninumara + "-" + str(siranumarasi))
                pass
        except:
            import traceback
            traceback.print_exc()

    def projelistesiguncelle(self):
        self.siparisdetaylari.setRowCount(0)
        self.uretimdetaylari.setRowCount(0)

        self.projeadi.clear()
        # self.uretimemrino.clear()
        try:
            firmaadi = self.firmaadi.currentText()
            self.cursor.execute("select firma_id from firmalar where firma_adi = '" + firmaadi + "'")
            firmaid = self.cursor.fetchone()[0]
            self.cursor.execute("SELECT proje_adi from projeler where FIRMALAR_firma_id = '" + str(firmaid) + "'")
            projeler = self.cursor.fetchall()
            for i in projeler:
                self.projeadi.addItems(i)
            # self.cursor.execute("select id , firmaid from firma_proje where "
            #                     "projeadi ='" + self.projeadi.currentText() + "' and firma_adi = '" + self.firma_adi.currentText() + "'")
            # projeid = self.cursor.fetchone()[0]
            # self.cursor.execute("select uretimEmirNo from uretimemirleri where "
            #                     "proje_id = '" + str(projeid) + "' and firma_id = '" + str(firmaid) + "'")
            # uretimemrinumaralari = self.cursor.fetchall()
            # for i in uretimemrinumaralari:
            #     self.uretimemrino.addItems(i)
            # self.cursor.execute ("select count(*) from isemirleri" )
            # isemriadedi = int(self.cursor.fetchone()[0])
            # self.cursor.execute("select firma_id from firmalar where firma_adi = '" + self.firma_adi.currentText()+ "'")
            # self.aktiffirmaid = (self.cursor.fetchone()[0])
            # self.year = datetime.datetime.now().strftime('%Y')
            # self.setisemrinumarasi()
            # self.isemrino.setText(str("I-") + str(self.year[2:]) + "-00" + str(self.aktiffirmaid) + "-" + str(isemriadedi + 1))

        except:
            QtWidgets.QMessageBox.information(self, "BİR SORUNLA KARŞILAŞILDI", "TANIMLI HERHANGİ BİR "
                                                                                "PROJE VEYA FİRMA BULUNMAMAKTADIR \n LÜTFEN FİRMA VEYA PROJE TANIMLAMASINI GERÇEKLEŞTİRİNİZ")
            import traceback
            traceback.print_exc

    def malzemetalebisiparisdetaylariguncelle(self):
        self.siparisdetaylari.setRowCount(0)
        self.uretimdetaylari.setRowCount(0)
        if len(self.projeadi.currentText()) == 0:
            return
        else:
            pass

        self.siparisdetaylari.clearContents()
        try:
            self.cursor.execute("select proje_id from projeler where proje_adi = '" + self.projeadi.currentText() + "'")
            projeid = self.cursor.fetchone()[0]

            self.cursor.execute(
                "select uretim_emir_no from uretimemirleri where PROJELER_proje_id = '" + str(projeid) + "'")
            try:
                uretimemirnumarasi = self.cursor.fetchone()[0]
            except TypeError:
                QtWidgets.QMessageBox.warning(self, "HATA",
                                              "İLGİLİ PROJE İLGİLİ HERHANGİ BİR ÜRETİM EMRİ KAYDI BULUNAMAMŞITR")
                return

            self.cursor.execute(
                "select uretim_emir_id,FIRMALAR_firma_id, PROJELER_proje_id from uretimemirleri where uretim_emir_no = '" + uretimemirnumarasi + "'")
            uretimfirmaprojeid = self.cursor.fetchall()
            uretimid = str(uretimfirmaprojeid[0][0])
            firmaid = str(uretimfirmaprojeid[0][1])
            projeid = str(uretimfirmaprojeid[0][2])
            self.cursor.execute(
                "select uretimemirleri.uretim_emir_no, firmalar.firma_adi , projeler.proje_adi, malzeme_tanimi, miktar, siparis_detay_id from siparisdetaylari  "
                "join uretimemirleri on  uretim_emir_id= '" + uretimid + "'"
                                                                         "join projeler on projeler.proje_id = '" + projeid + "' join firmalar on  firmalar.firma_id = '" + firmaid + "' where siparisdetaylari.URETIMEMIRLERI_uretim_emir_id = '" + uretimid + "'")
            # todo burada kaldin
            siparisdetaylari = self.cursor.fetchall()
            self.siparisdetaylari.setRowCount(len(siparisdetaylari))
            for row, value in enumerate(siparisdetaylari):
                self.siparisdetaylari.setItem(row, 0, QtWidgets.QTableWidgetItem(value[0]))
                self.siparisdetaylari.setItem(row, 1, QtWidgets.QTableWidgetItem(value[1]))
                self.siparisdetaylari.setItem(row, 2, QtWidgets.QTableWidgetItem(value[2]))
                self.siparisdetaylari.setItem(row, 3, QtWidgets.QTableWidgetItem(value[3]))
                self.siparisdetaylari.setItem(row, 4, QtWidgets.QTableWidgetItem(str(value[4])))
                self.siparisdetaylari.setItem(row, 5, QtWidgets.QTableWidgetItem(str(value[5])))
            self.siparisdetaylari.setColumnHidden(5, False)

        except:
            import traceback
            traceback.print_exc()

    def malzemetalebiuretimdetaylariguncelle(self):
        satir = self.siparisdetaylari.currentRow()
        if satir == -1:
            return
        else:
            siparisdetayid = self.siparisdetaylari.item(satir, 5).text()
        try:

            self.cursor.execute(
                "select akmill_teknik_resim_no , musteri_teknik_resim_no, adet,  operasyon_sayisi, malzeme_adi ,uretim_detaylandirma_id, akmillteknikresimleri.dosya_yolu,"
                "akmillteknikresimleri.dosya_yolu_onizleme, musteriteknikresimleri.dosya_yolu  , musteriteknikresimleri.dosya_yolu_onizleme from uretimdetaylandirma "
                "left join akmillteknikresimleri on akmillteknikresimleri.akmill_teknikresim_id =uretimdetaylandirma.akmillteknikresimleri_akmill_teknikresim_id "
                "left join musteriteknikresimleri on musteriteknikresimleri.musteri_teknikresim_id = uretimdetaylandirma.musteriteknikresimleri_musteri_teknikresim_id "
                "left join  malzemelistesi on malzemelistesi.malzeme_id = uretimdetaylandirma.malzemelistesi_malzeme_id where uretimdetaylandirma.siparisdetaylari_siparis_detay_id = '" + str(
                    siparisdetayid) + "'")
            uretimdetaylarilistesi = self.cursor.fetchall()
            self.uretimdetaylari.setRowCount(len(uretimdetaylarilistesi))
            for row, value in enumerate(uretimdetaylarilistesi):
                self.uretimdetaylari.setItem(row, 0, QtWidgets.QTableWidgetItem(value[0]))  # AKMILL RESIM NO
                self.uretimdetaylari.setItem(row, 1, QtWidgets.QTableWidgetItem(value[1]))  # MUSTERI RESIM NO
                self.uretimdetaylari.setItem(row, 2, QtWidgets.QTableWidgetItem(str(value[2])))  # ADET
                self.uretimdetaylari.setItem(row, 3, QtWidgets.QTableWidgetItem(str(value[3])))  # OP SAYISI
                self.uretimdetaylari.setItem(row, 4, QtWidgets.QTableWidgetItem(value[4]))  # MALZEME ADI
                self.uretimdetaylari.setItem(row, 5, QtWidgets.QTableWidgetItem(str(value[5])))  # URETIMDETAYID
                self.uretimdetaylari.setItem(row, 6, QtWidgets.QTableWidgetItem(value[6]))  # MALZEME ADI
                self.uretimdetaylari.setItem(row, 7, QtWidgets.QTableWidgetItem(value[7]))  # MALZEME ADI
                self.uretimdetaylari.setItem(row, 8, QtWidgets.QTableWidgetItem(value[8]))  # MALZEME ADI
                self.uretimdetaylari.setItem(row, 9, QtWidgets.QTableWidgetItem(value[9]))  # MALZEME ADI

                self.cursor.execute(
                    "select parca_adi from akmillteknikresimleri where akmill_teknik_resim_no = '" + str(
                        value[0]) + "'")
                parcaadi = self.cursor.fetchone()[0]
                self.uretimdetaylari.setItem(row, 10, QtWidgets.QTableWidgetItem(parcaadi))
            self.uretimdetaylari.setColumnHidden(5, True)
            self.uretimdetaylari.setColumnHidden(6, True)
            self.uretimdetaylari.setColumnHidden(7, True)
            self.uretimdetaylari.setColumnHidden(8, True)
            self.uretimdetaylari.setColumnHidden(9, True)
            self.cursor.execute(
                "select distinct termintarihi from malzemetalepleri where siparisdetaylari_siparis_detay_id= '" + str(
                    siparisdetayid) + "'")
            termintarihi = self.cursor.fetchall()[0][0]
            self.termintarihi.setDate(termintarihi)

        except:
            import traceback
            traceback.print_exc()
            return
        self.malzemetalepleriguncelle()

        pass

    def malzemetalebipreview(self):
        try:
            satir = self.uretimdetaylari.currentRow()
            sutun = self.uretimdetaylari.currentColumn()
            if sutun == 0:
                path = self.uretimdetaylari.item(satir, 7).text()
                if len(path) == 0:
                    self.preview.setText("PREVIEW")
                else:
                    pixmap = QPixmap(path)
                    pixmap= pixmap.scaledToHeight(600)
                    # pixmap= pixmap.scaledToWidth(400)
                    self.preview.setScaledContents(True)
                    self.preview.setPixmap(pixmap)
                    print path
                    # self.preview.setFixedSize(580, 580)
            elif sutun == 1:
                path = self.uretimdetaylari.item(satir, 9).text()
                pixmap = QPixmap(path)
                if len(path) == 0:
                    self.preview.setText("PREVIEW")
                else:
                    pixmap = QPixmap(path)
                    pixmap= pixmap.scaledToHeight(600)
                    # pixmap= pixmap.scaledToWidth(400)
                    self.preview.setScaledContents(True)
                    self.preview.setPixmap(pixmap)
                    # self.preview.setFixedSize(580, 580)
            else:
                pass

        except:
            import traceback
            traceback.print_exc()
            return

        pass

    def malzemetalepleriguncelle(self):

        satir = self.siparisdetaylari.currentRow()
        if satir == -1:
            QtWidgets.QMessageBox.warning(self, "HATA", "SİPARİŞ DETAYI SEÇİMİ YAPMANIZ GEREKMEKTEDİR")
        else:
            try:
                siparisdetayid = self.siparisdetaylari.item(satir, 5).text()
                self.cursor.execute(
                    "select * from malzemetalepleri join malzemelistesi on malzemelistesi.malzeme_id = malzemetalepleri.malzemelistesi_malzeme_id where siparisdetaylari_siparis_detay_id = '" + siparisdetayid + "'")
                malzemelisteleri = self.cursor.fetchall()
                self.malzemelistesi.setRowCount(len(malzemelisteleri))
                for satir, data in enumerate(malzemelisteleri):
                    self.malzemelistesi.setItem(satir, 0, QtWidgets.QTableWidgetItem(str(data[1])))
                    self.malzemelistesi.setItem(satir, 1, QtWidgets.QTableWidgetItem(str(data[2])))
                    self.malzemelistesi.setItem(satir, 2, QtWidgets.QTableWidgetItem(str(data[3])))
                    self.malzemelistesi.setItem(satir, 3, QtWidgets.QTableWidgetItem(str(data[4])))
                    self.malzemelistesi.setItem(satir, 4, QtWidgets.QTableWidgetItem(data[10]))
                    self.malzemelistesi.setItem(satir, 5, QtWidgets.QTableWidgetItem(data[11]))
                    self.malzemelistesi.setItem(satir, 6, QtWidgets.QTableWidgetItem(str(data[12])))
                    self.malzemelistesi.setItem(satir, 7, QtWidgets.QTableWidgetItem(str(data[8])))
                    self.malzemelistesi.setItem(satir, 8, QtWidgets.QTableWidgetItem(data[14]))
                    self.malzemelistesi.setItem(satir, 9, QtWidgets.QTableWidgetItem(str(data[0])))

                    # self.malzemelistesi.setItem(satir, 6, QtWidgets.QTableWidgetItem(str(data[6])))
                    # self.malzemelistesi.setItem(satir, 7, QtWidgets.QTableWidgetItem(str(data[7])))
                    # self.malzemelistesi.setItem(satir, 8, QtWidgets.QTableWidgetItem(str(data[8].encode('utf-8'))))
                    # self.malzemelistesi.setItem(satir, 9, QtWidgets.QTableWidgetItem(str(data[9].encode('utf-8'))))
                    # self.malzemelistesi.setItem(satir, 10, QtWidgets.QTableWidgetItem(str(data[10])))
            except:
                import traceback
                traceback.print_exc()

    def formusil(self):
        pass
        satir = self.siparistalepformlistesi.currentRow()
        if satir == -1:
            QtWidgets.QMessageBox.warning(self, "HATA", " SEÇİM YAPMANIZ GEREKMEKTEDİR")
            return
        else:
            value = self.siparistalepformlistesi.item(satir, 0).text()
            path = "//AKMILL2/Users/Akmill2/Desktop/AKmill/MRP/MalzemeTalepleri/" + value
            self.cursor.execute("select count(*) from malzemetalebigorunumu where uretimEmirNo = '" + value[3:] + "'")
            altsiparisvarmi = self.cursor.fetchone()[0]
            if altsiparisvarmi != 0:
                QtWidgets.QMessageBox.warning(self, "HATA",
                                              "ÖNCELİKLE ALT SİPARİS TALEPLERİNİ KALDIRMANIZ GEREKMEKTEDİR")
                return
            else:
                try:
                    self.cursor.execute("start transaction")
                    self.cursor.execute("delete from malzemetalepformlistesi where siparistalepno = '" + value + "'")
                    if os.path.exists(path + ".pdf"):
                        QtWidgets.QMessageBox.warning(self, "BAsarili", "DOSYA MEVCUT")
                        try:
                            os.remove(path + ".pdf")
                            os.remove(path + ".html")
                            QtWidgets.QMessageBox.information(self, "BAŞARILI",
                                                              "SİPARİŞ TALEBİNE AİT AİT PDF ve HTML DOSAYSI SİSTEMDEN KALDIRILDI")
                        except:
                            QtWidgets.QMessageBox.warning(self, "HATA",
                                                          "PDF SİSTEMDEN KALDIRILMAADI DOSYA AÇIK VEYA BŞAKA BİR PC TARAFINDAN KULLANILIYOR OLABİLİR ")
                            self.cursor.execute("rollback")
                    else:
                        QtWidgets.QMessageBox.warning(self, "HATA",
                                                      "İLGİLİ DOSYA SİLİNMİŞ VEYA HENÜZ OLUŞTURULMAMIŞ VERİİTABANINDAN SİLME İŞLEMİ YİNEDE GERÇEKLEŞTİRİLECEK")

                except:
                    QtWidgets.QMessageBox.warning(self, "HATA ", "BEKLENMEDİK BİR HATA İLE KARŞILAŞILDI")
                self.cursor.execute("commit")
                self.siparistalepformlistesigorunumu()
                pass

    def formuac(self):
        pass
        satir = self.siparistalepformlistesi.currentRow()
        value = self.siparistalepformlistesi.item(satir, 0).text()
        path = "//AKMILL2/Users/Akmill2/Desktop/AKmill/MRP/MalzemeTalepleri/" + value + ".pdf"
        subprocess.Popen(path, shell=True)
        pass

    def formlarihazirla(self):

        siparisdetaysatir = self.siparisdetaylari.currentRow()
        if siparisdetaysatir == -1:
            QtWidgets.QMessageBox.warning(self, "HATA", "SEÇİM YAPMANIZ GEREKMEKTEDİR")
            return
        else:
            uretimemirno = self.siparisdetaylari.item(siparisdetaysatir, 0).text()
            siparisdetayid = self.siparisdetaylari.item(siparisdetaysatir, 5).text()
            firmaadi = self.siparisdetaylari.item(siparisdetaysatir, 1).text()
            projeadi = self.siparisdetaylari.item(siparisdetaysatir, 2).text()
        numara = uretimemirno + '-' + siparisdetayid
        dosyayolu = "//AYAS/Users/AYAS/Desktop/MRP/URETIM/MALZEMETALEPLERI/PR-" + numara + '.html'

        if os.path.exists(dosyayolu):
            QtWidgets.QMessageBox.warning(self, "HATA",
                                          "SİPARİS TALEP FORMU DAHA ONCE HAZIRLANMIS\n DOSYANIN SİLİNMESİ GEREKMEKTEDİR")
            return
        else:
            yazilacak = """<!DOCTYPE html>
            <!-- saved from url=(0071)http://localhost:50246/akmill/SSDA.html?_ijt=ij9ub9111m8taomk4lflgpc95v -->
            <html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><style>
                html ,body {
            height : 210mm;
            width:297mm;
            }
            table, th ,td {
            border : 1px solid black;
            border-collapse : collapse;
            }
            th ,td {
            padding : 2px;
            }
            p.normal {
                font-style : normal ;
                font-size : 1px;
                font-family : "Calibri";
            }

            p.small {
            line-height:0.5;
            font-weight : bold;

            }
            </style>"""
            asil = """ <title>Title</title>
            </head>
            <body>
            <table style="width : 100%">
                <tbody><tr>
                    <th rowspan="2" colspan="3"><img src="//AYAS/Users/AYAS/Desktop/MRP/akmillogo.png" width="200" height="64"></th>
                    <th rowspan="2" colspan="7" style="white-space:pre-wrap; word-wrap:break-word"> MALZEME TALEP FORMU <p>HAMMEDDE</p></th>
                    <th rowspan="1" colspan="1">TARİH</th>
                    <th rowspan="1" colspan="1">{0}</th>
                    <th rowspan="1" colspan="1">TERMIN TARIHI</th>

                </tr>
                <tr>
                    <th rowspan="1" colspan="1">MALZEME TALEP NO</th>
                    <th rowspan="1" colspan="1"> {1}</th>
                    <th rowspan="1" colspan="1"> {2}</th>

                </tr>
                <tr>
                    <th rowspan="2" colspan="1">NO</th>
                    <th rowspan="2" colspan="1">AK-PARCA ADI</th>
                    <th rowspan="2" colspan="1">MU-PARCA ADI</th>
                    <th rowspan="1" colspan="3">ÖLÇÜLER</th>
                    <th rowspan="2" colspan="1">ADET</th>
                    <th rowspan="2" colspan="1">MALZEME CİNSİ</th>
                    <th rowspan="2" colspan="1">TEORİK AĞIRLIK</th>
                    <th rowspan="2" colspan="1">EK AÇIKLAMA</th>
                    <th rowspan="2" colspan="1">FİRMA ADI</th>
                    <th rowspan="2" colspan="1">PROJE ADI</th>
                    <th rowspan="2" colspan="1">TALEP DURUMU</th>

                </tr>
                <tr>
                    <th rowspan="1" colspan="1">Z</th>
                    <th rowspan="1" colspan="1"> X</th>
                    <th rowspan="1" colspan="1">Y</th>
                </tr>"""
            file = open(dosyayolu, 'w')
            # operatoradi = operator[0] + " " + operator[1]
            talepno = "PR-" + numara
            now = datetime.datetime.now()
            date = now.strftime("%Y-%m-%d")
            termin = self.termintarihi.text()
            mystring = asil.format(date, talepno, termin)
            file.write(yazilacak + mystring)
            file.close()
            query = """select t1.olcux, t1.olcuy, t1.olcuz, t1.adet, t1.aciklamalar, t1.siparisdetayid, t1.uretimdetayid, t1.malzemetipi, t1.siparismistokmu, t1.malzemeagirlik,
                                   t1.akresimid, t1.muresimid, akmillteknikresimleri.parca_adi as akparcaadi,  musteriteknikresimleri.parca_adi as muparcaadi, t1.malzemecinsi from
                                    (select olcux, olcuy, olcuz, malzemetalepleri.adet as adet, aciklamalar, termintarihi, malzemetalepleri.siparisdetaylari_siparis_detay_id as siparisdetayid,
                                    uretimdetaylandirma_uretim_detaylandirma_id as uretimdetayid, malzemetipi, siparismistokmu,
                                    malzemeagirlik, akmillteknikresimleri_akmill_teknikresim_id as akresimid, musteriteknikresimleri_musteri_teknikresim_id as muresimid,
                                    malzemelistesi.malzeme_adi as malzemecinsi from malzemetalepleri
                                    join uretimdetaylandirma on malzemetalepleri.uretimdetaylandirma_uretim_detaylandirma_id = uretim_detaylandirma_id
                                    join malzemelistesi on malzemelistesi.malzeme_id = malzemetalepleri.malzemelistesi_malzeme_id) as t1
                                    left join akmillteknikresimleri on akmillteknikresimleri.akmill_teknikresim_id = t1.akresimid
                                    left join musteriteknikresimleri on musteriteknikresimleri.musteri_teknikresim_id = t1.muresimid  where siparisdetayid = {0}"""
            newquery = query.format(siparisdetayid)
            try:
                self.cursor.execute(newquery)
                print newquery
                # self.cursor.execute("select * from malzemetalepleri where siparisdetaylari_siparis_detay_id  = '"+siparisdetayid+"'")
                listi = self.cursor.fetchall()
                print listi
            except:
                import traceback
                traceback.print_exc()
            a = 1
            firmaadi = self.siparisdetaylari.item(siparisdetaysatir, 1).text()
            projeadi = self.siparisdetaylari.item(siparisdetaysatir, 2).text()
            print firmaadi, projeadi
            # firmadi = listi[0][8].encode('utf-8')
            # projeadi = listi[0][9].encode('utf-8')
            try:
                print("asdasdasdasdasdasd")
                with open(dosyayolu, 'ab') as f:
                    for i in listi:

                        if i[7] == "CAP MALZEME":
                            f.write(
                                "\n<tr>"
                                "\n<td>" + str(a) + " </td>"
                                                    "\n<td>" + str(i[12]) + " </td>"  # AKMİLL PARÇA ADI
                                                                            "\n<td>" + str(
                                    i[13]) + " </td>"  # MUSTERI PARÇA ADI
                                             "\n<td>" + "-" + " </td>"  # PARÇA OLCULERI
                                                              "\n<td> &#x2205 " + str(i[0]) + "</td>"  # PARÇA OLCULERI
                                                                                              "\n<td>" + str(
                                    i[1]) + " </td>"  # PARÇA OLCULERI
                                            "\n<td>" + str(i[3]) + " </td>"  # ADET
                                                                   "\n<td>" + i[14].encode(
                                    "utf-8") + " </td>"  # MALZEME CİNSİ
                                               "\n<td>" + str(i[9]) + " </td>"  # MALZEME AGIRLIK
                                                                      "\n<td>" + str(i[4]) + " </td>"  # ACIKLAMALAR
                                                                                             "\n<td>" + firmaadi + " </td>"  # firma adi            
                                                                                                                   "\n<td>" + projeadi + " </td>"  # proje adi
                                                                                                                                         "\n<td>" + str(
                                    i[8]) + " </td>"  # termin tarihi

                                            "\n</tr>")
                            a = a + 1
                        elif i[7] == "BORU MALZEME":
                            f.write(
                                "\n<tr>"
                                "\n<td>" + str(a) + " </td>"
                                                    "\n<td>" + str(i[12]) + " </td>"  # AKMİLL PARÇA ADI
                                                                            "\n<td>" + str(
                                    i[13]) + " </td>"  # MUSTERI PARÇA ADI
                                             "\n<td> &#x2205o " + str(i[0]) + "</td>"  # #PARCA OLCULERI DIS CAP
                                                                              "\n<td> &#x2205i " + str(
                                    i[1]) + "</td>"  # PARCA OLCULERI IC CAP
                                            "\n<td>" + str(i[2]) + " </td>"  # PARCA OLCULERI UZUNLUK
                                                                   "\n<td>" + str(i[3]) + " </td>"  # ADET
                                                                                          "\n<td>" + i[14].encode(
                                    "utf-8") + " </td>"  # MALZEME CİNSİ
                                               "\n<td>" + str(i[9]) + " </td>"  # MALZEME AGIRLIK
                                                                      "\n<td>" + str(i[4]) + " </td>"  # ACIKLAMALAR
                                                                                             "\n<td>" + firmaadi + " </td>"  # firma adi            
                                                                                                                   "\n<td>" + projeadi + " </td>"  # proje adi
                                                                                                                                         "\n<td>" + str(
                                    i[8]) + " </td>"  # termin tarihi

                                            "\n</tr>")
                            a = a + 1
                        elif i[7] == "PROFIL":
                            f.write(
                                "\n<tr>"
                                "\n<td>" + str(a) + " </td>"
                                                    "\n<td>" + str(i[12]) + " </td>"  # AKMİLL PARÇA ADI
                                                                            "\n<td>" + str(
                                    i[13]) + " </td>"  # MUSTERI PARÇA ADI
                                             "\n<td>" + str(i[0]) + "</td>"  # PARCA OLCULERI
                                                                    "\n<td>" + str(i[1]) + " </td>"  # PARCA OLCULERI
                                                                                           "\n<td>" + str(
                                    i[2]) + " </td>"  # PARCA OLCULERI
                                            "\n<td>" + str(i[3]) + " </td>"  # ADET
                                                                   "\n<td>" + i[14].encode(
                                    "utf-8") + " </td>"  # MALZEME CİNSİ
                                               "\n<td>" + str(i[9]) + " </td>"  # MALZEME AGIRLIK
                                                                      "\n<td>" + str(i[4]) + " </td>"  # ACIKLAMALAR
                                                                                             "\n<td>" + firmaadi + " </td>"  # firma adi            
                                                                                                                   "\n<td>" + projeadi + " </td>"  # proje adi
                                                                                                                                         "\n<td>" + str(
                                    i[8]) + " </td>"  # termin tarihi

                                            "\n</tr>")
                            a = a + 1
                    f.write(
                        """\n</tbody></table>
                        <p class="small"><br>&#x2205 sembolü malzemenin silindirik kesitli olduğunu göstermekte olup çap ölçüsünü temsil etmektedir.</br><br> 
                        &#x2205i ve &#x2205o sembolleri malzemenin boru profilinde olduğunu göstermektedir. &#x2205o: dış çap ölçüsünü, &#x2205i: iç çap ölçüsünü temsil etmektedir.  </br><br> Bütün ölçü birimleri mm cinsindendir. </br> <br>Anlaşılmayan veya eksik olan yerlerde lütfen iletişime geçiniz... </br> 
                        <br>Tel : 0312 395 57 22</th></br></p>"""
                        "</body></html>"
                    )
                    f.close()
            except:
                import traceback
                traceback.print_exc()
            config = pdfkit.configuration(wkhtmltopdf="C:\\Program Files\\wkhtmltopdf\\bin\\wkhtmltopdf.exe")
            optionss = {'page-size': 'A4',
                        'orientation': 'Landscape',
                        'margin-top': '10mm',
                        'margin-left': '25mm',
                        'margin-right': '10mm',
                        'margin-bottom': '10mm'}
            pdfkit.from_file(dosyayolu, dosyayolu + '.pdf', options=optionss, configuration=config)
            # self.cursor.execute("insert into malzemetalepformlistesi (siparistalepno , firmadi , projeadi , tarih) values (%s , %s, %s,%s)", ('PR-'+uretimemrino, firmadi, projeadi, "tar"))
            # self.conn.commit()
            # self.siparistalepformlistesigorunumu()
            QtWidgets.QMessageBox.information(self, "BASARILI", "MALZEME TALEP FORMU BASARI ILE OLUSTURULDU")

    def malzemegirislerinilimitle(self):

        if self.borumalzeme.checkState() == 2:
            self.capmalzeme.setCheckState(0)
            self.profil.setCheckState(0)
            self.cap.setDisabled(True)
            self.uzunlukcap.setDisabled(True)
            self.kalinlik.setDisabled(True)
            self.genislik.setDisabled(True)
            self.uzunluk.setDisabled(True)
            self.capmalzeme.setDisabled(True)
            self.profil.setDisabled(True)
            self.discap.setDisabled(False)
            self.iccap.setDisabled(False)
            self.uzunlukboru.setDisabled(False)
        elif self.capmalzeme.checkState() == 2:
            self.discap.setDisabled(True)
            self.iccap.setDisabled(True)
            self.uzunlukboru.setDisabled(True)
            self.kalinlik.setDisabled(True)
            self.genislik.setDisabled(True)
            self.uzunluk.setDisabled(True)
            self.profil.setDisabled(True)
            self.borumalzeme.setDisabled(True)
            self.cap.setDisabled(False)
            self.uzunlukcap.setDisabled(False)
        elif self.profil.checkState() == 2:
            self.kalinlik.setDisabled(False)
            self.genislik.setDisabled(False)
            self.uzunluk.setDisabled(False)
            self.discap.setDisabled(True)
            self.iccap.setDisabled(True)
            self.uzunlukboru.setDisabled(True)
            self.cap.setDisabled(True)
            self.uzunlukcap.setDisabled(True)
            self.capmalzeme.setDisabled(True)
            self.borumalzeme.setDisabled(True)
        else:
            self.capmalzeme.setDisabled(False)
            self.profil.setDisabled(False)
            self.borumalzeme.setDisabled(False)

    # def projeguncelle(self):
    #     pass
    #     self.projeadi.clear()
    #     firmaadi = self.firmaadi.currentText()
    #     self.cursor.execute("select projeadi from firma_proje where firma_adi = '"+firmaadi+"'")
    #     projeler = self.cursor.fetchall()
    #     for i in projeler :
    #         self.projeadi.addItems(i)
    #     self.uretimnoguncelle()
    # def uretimnoguncelle(self):
    #     pass
    #     self.uretimno.clear()
    #     projeadi = self.projeadi.currentText()
    #     self.cursor.execute("select distinct uretimEmirNo from uretimdetaylandirma where projeadi = '"+projeadi+"'")
    #     uretimemirleri = self.cursor.fetchall()
    #     for i in uretimemirleri:
    #         self.uretimno.addItems(i)
    def malzemelistesiguncelle(self):

        self.cursor.execute("select malzeme_adi from malzemelistesi")
        malzemeler = self.cursor.fetchall()
        for i in malzemeler:
            self.malzeme.addItems(i)

    # def parcalistesiguncelle(self):
    #     pass
    #     # try :
    #     durum  = "False"
    #     self.cursor.execute("select parcaadi from uretimdetaylandirma where uretimEmirNo = '"+self.uretimno.currentText()+"' and  siparisdurumu = '"+durum+"'")
    #     parcalistesi = self.cursor.fetchall()
    #     self.parcaadi.clear()
    #     for i in parcalistesi :
    #         self.parcaadi.addItems(i)
    #     # except :
    #     #     QtWidgets.QMessageBox.warning(self, " HATA " , "BİR PROBLEMLE KARŞILAŞILDI")
    #     #     return
    def listeyeekle(self):
        try:
            uretimdetaysatir = self.uretimdetaylari.currentRow()
            if uretimdetaysatir == -1:
                QtWidgets.QMessageBox.warning(self, "HATA",
                                              "TALEP OLUŞTURMAK İSTEDİĞİNİZ URETİMDETAYINI SEÇMENİZ GEREKMEKTEDİR \n ÜRETİM DETAYLARI BÖLÜMÜNDEN SEÇİMİ GERÇEKLEŞTİRİLEBİLİRSİNİZ..")
                return
            else:
                pass
            if self.agirlikhesapla() == 1:
                QtWidgets.QMessageBox.warning(self, "HATA", "BİR HATA İLE KARŞILAIŞDI")
                import traceback
                traceback.print_exc()
                return
            elif len(self.malzemetalepnumarasi.text()) == 0:
                QtWidgets.QMessageBox.warning(self, "HATA", "MALZEME TALEP NUMARASI ALMANIZ GEREKMEKTEDİR")
                return
            else:
                # uretimemrino = self.uretimno.currentText()
                # firmadi = self.firmaadi.currentText()
                # projeadi = self.projeadi.currentText()
                satir = self.uretimdetaylari.currentRow()
                uretimdetayid = self.uretimdetaylari.item(satir, 5).text()

                now = datetime.datetime.now()
                date = now.strftime("%Y-%m-%d")
                # ad = 'PR-' + uretimemrino
                self.cursor.execute(
                    "select count(*) from malzemetalepleri where uretimdetaylandirma_uretim_detaylandirma_id = '" + uretimdetayid + "'")
                mevcutmu = self.cursor.fetchone()[0]
                if mevcutmu != 0:
                    QtWidgets.QMessageBox.warning(self, "HATA",
                                                  "DAHA ÖNCE BU ÜRETİM DETAYI İÇİN TALEP OLUŞTURULMUŞ LÜTFEN KONTROL EDİNİZ")
                    return
                    # self.cursor.execute(
                    #     "insert into malzemetalepleri (siparistalepno , firmadi , projeadi , tarih) values (%s , %s, %s,%s)",
                    #     ('PR-' + uretimemrino, firmadi, projeadi, date))
                else:
                    try:
                        siparisdetaysatir = self.siparisdetaylari.currentRow()
                        siparisdetayid = self.siparisdetaylari.item(siparisdetaysatir, 5).text()
                        uretimdetaysatir = self.uretimdetaylari.currentRow()
                        uretimdetayid = self.uretimdetaylari.item(uretimdetaysatir, 5).text()
                        temindurumu = self.siparismistokmu.currentText()
                        self.cursor.execute(
                            "select malzeme_id from malzemelistesi where malzeme_adi= '" + self.malzeme.currentText() + "'")
                        malzemeid = self.cursor.fetchone()[0]
                        # parcaid = self.cursor.fetchone()[0]
                        ekaciklama = self.aciklamalar.text()
                        adet = int(self.adet.text())
                        termintarihi = self.termintarihi.text()
                        # malzemetipi = self.malzeme.currentText()
                        paket = self.olcux, self.olcuy, self.olcuz, adet, ekaciklama, termintarihi, siparisdetayid, uretimdetayid, malzemeid, self.malzemetipi, temindurumu, self.teorikagirlik
                        self.cursor.execute(
                            "insert into  malzemetalepleri (olcux, olcuy, olcuz, adet, aciklamalar, termintarihi,siparisdetaylari_siparis_detay_id,"
                            "uretimdetaylandirma_uretim_detaylandirma_id, malzemelistesi_malzeme_id, malzemetipi, siparismistokmu, malzemeagirlik) values (%s ,%s ,%s,%s ,%s,%s ,%s,%s ,%s,%s ,%s ,%s)",
                            (paket))
                        self.conn.commit()
                        # self.cursor.execute("update uretimdetaylari set siparisdurumu  = True where teknikresim_id = '"+str(parcaid)+"' and uretimemrino_id = '"+str(uretimnoid)+"'")
                        self.conn.commit()
                        QtWidgets.QMessageBox.information(self, "BAŞARILI", "TALEP BAŞARIYLA EKLENDİ")
                        self.malzemetalepleriguncelle()

                    except:
                        import traceback
                        traceback.print_exc()
                        QtWidgets.QMessageBox.warning(self, "hata", "BİR HATA İLE KARŞILAŞILDI")
                        return
        except:
            import traceback
            traceback.print_exc()
            # self.parcalistesiguncelle()
            # self.siparistalepformlistesigorunumu()

    def agirlikhesapla(self):

        if self.adet.text() == "0":
            QtWidgets.QMessageBox.warning(self, "hata", "ADET SAYISI '0' OLAMAZ")
            return 1
        else:

            if self.borumalzeme.checkState() == 2:
                try:
                    adet = int(self.adet.text())
                    discap = int(self.discap.text())
                    iccap = int(self.iccap.text())
                    if iccap >= discap:
                        QtWidgets.QMessageBox.warning(self, "HATA",
                                                      "İÇ ÇAP DIŞ ÇAPA KÜÇÜK VEYA EŞİT OLAMAZ LÜTFEN KONTROL EDİNİZ")
                        return
                    else:
                        pass
                    uzunluk = int(self.uzunlukboru.text())
                    hacim = (discap ** 2 - iccap ** 2) * math.pi * uzunluk / 4
                    self.cursor.execute(
                        "select malzeme_yogunluk from malzemelistesi where malzeme_adi = '" + self.malzeme.currentText() + "'")
                    yogunluk = float(self.cursor.fetchone()[0])
                    self.teorikagirlik = yogunluk * adet * hacim / 1000000000
                    self.olcux = discap
                    self.olcuy = iccap
                    self.olcuz = uzunluk
                    self.malzemetipi = "BORU MALZEME"
                except:
                    QtWidgets.QMessageBox.warning(self, "HATA",
                                                  "LUTFEN DEGERLERI GERÇEK SAYI OLARAK GIRINIZ VE ALANLARI BOŞ BIRAKMAYINIZ")
                    return 1
            elif self.capmalzeme.checkState() == 2:
                try:
                    cap = int(self.cap.text())
                    uzunluk = int(self.uzunlukcap.text())
                    adet = int(self.adet.text())
                    hacim = (cap ** 2 * math.pi * uzunluk / 4)
                    self.cursor.execute(
                        "select malzeme_yogunluk from malzemelistesi where malzeme_adi = '" + self.malzeme.currentText() + "'")
                    yogunluk = float(self.cursor.fetchone()[0])
                    self.teorikagirlik = yogunluk * hacim * adet / 1000000000
                    self.olcux = cap
                    self.olcuy = uzunluk
                    self.olcuz = 0
                    self.malzemetipi = "CAP MALZEME"
                except:
                    QtWidgets.QMessageBox.warning(self, "HATA",
                                                  "LUTFEN DEGERLERI RAKAM OLARAK GIRINIZ VE ALANLARI BOŞ BIRAKMAYINIZ")
                    return 1
            elif self.profil.checkState() == 2:
                try:
                    kalinlik = int(self.kalinlik.text())
                    uzunluk = int(self.uzunluk.text())
                    genislik = int(self.genislik.text())
                    adet = int(self.adet.text())
                    hacim = kalinlik * genislik * uzunluk
                    self.cursor.execute(
                        "select malzeme_yogunluk from malzemelistesi where malzeme_adi = '" + self.malzeme.currentText() + "'")
                    yogunluk = float(self.cursor.fetchone()[0])
                    self.teorikagirlik = hacim * yogunluk * adet / 1000000000
                    self.olcux = kalinlik
                    self.olcuy = genislik
                    self.olcuz = uzunluk
                    self.malzemetipi = "PROFIL"
                except:
                    import traceback
                    traceback.print_exc()
                    QtWidgets.QMessageBox.warning(self, "HATA",
                                                  "LUTFEN DEGERLERI RAKAM OLARAK GIRINIZ VE ALANLARI BOŞ BIRAKMAYINIZ")
                    return 1
            else:
                QtWidgets.QMessageBox.warning(self, "HATA",
                                              "MALZEME TİPİNİ BELİRLEYİP GEREKLİ ÖLÇÜLERİ DOLDURMANIZ GEREKMEKTEDİR")
                return

    # def listegorunumu(self):
    #     pass
    #     satir = self.siparistalepformlistesi.currentRow()
    #     if satir == -1 :
    #         QtWidgets.QMessageBox.warning(self, "HATA" ,"HERHANGİ BİR KAYIT BULUNAMADI")
    #     else :
    #
    #         value = self.siparistalepformlistesi.item(satir, 0).text()[3:]
    #     # try :
    #         self.cursor.execute("select * from  malzemetalebigorunumu where uretimEmirNo = '"+value+"'")
    #         malzemelisteleri = self.cursor.fetchall()
    #         self.malzemelistesi.setRowCount(len(malzemelisteleri))
    #         for satir, data in enumerate(malzemelisteleri):
    #             self.malzemelistesi.setItem(satir, 0, QtWidgets.QTableWidgetItem(data[0].encode('utf-8')))
    #             self.malzemelistesi.setItem(satir, 1, QtWidgets.QTableWidgetItem(data[1].encode('utf-8')))
    #             self.malzemelistesi.setItem(satir, 2, QtWidgets.QTableWidgetItem(str(data[2].encode('utf-8'))))
    #             self.malzemelistesi.setItem(satir, 3, QtWidgets.QTableWidgetItem(str(data[3].encode('utf-8'))))
    #             self.malzemelistesi.setItem(satir, 4, QtWidgets.QTableWidgetItem(str(data[4])))
    #             self.malzemelistesi.setItem(satir, 5, QtWidgets.QTableWidgetItem(str(data[5])))
    #             self.malzemelistesi.setItem(satir, 6, QtWidgets.QTableWidgetItem(str(data[6])))
    #             self.malzemelistesi.setItem(satir, 7, QtWidgets.QTableWidgetItem(str(data[7])))
    #             self.malzemelistesi.setItem(satir, 8, QtWidgets.QTableWidgetItem(str(data[8].encode('utf-8'))))
    #             self.malzemelistesi.setItem(satir, 9, QtWidgets.QTableWidgetItem(str(data[9].encode('utf-8'))))
    #             self.malzemelistesi.setItem(satir, 10, QtWidgets.QTableWidgetItem(str(data[10])))
    #
    #     # except :
    #     #     QtWidgets.QMessageBox.warning(self, "HATA" , "BEKLENMEDIK BIR HATA İLE KARŞILAŞILDI")
    #     #     return
    #         self.stackedWidget.setCurrentIndex(1)
    # def siparistalepformlistesigorunumu(self):
    #     pass
    #     self.cursor.execute("select * from malzemetalepformlistesi")
    #     siparislisteleri  = self.cursor.fetchall()
    #     self.siparistalepformlistesi.setRowCount(len(siparislisteleri))
    #     for satir, data in enumerate(siparislisteleri):
    #         self.siparistalepformlistesi.setItem(satir, 0, QtWidgets.QTableWidgetItem(data[1]))
    #         self.siparistalepformlistesi.setItem(satir, 1, QtWidgets.QTableWidgetItem(data[2]))
    #         self.siparistalepformlistesi.setItem(satir, 2, QtWidgets.QTableWidgetItem(data[3]))
    #         self.siparistalepformlistesi.setItem(satir, 3, QtWidgets.QTableWidgetItem(data[4]))
    #
    #
    #     pass
    # def talepgorunumu(self):
    #     pass
    #     self.stackedWidget.setCurrentIndex(0)
    #     # self.cursor.execute("select ")
    def siparisformuhazirla(self):
        satir = self.siparisdetaylari.currentRow()
        if satir == -1:
            QtWidgets.QMessageBox.warning(self, "HATA", "SEÇİM YAPMANIZ GEREKMEKTEDİR")
            return
        else:
            siparisdetayid = self.siparisdetaylari.item(satir, 5).text()
            uretimemrinumarasi = self.siparisdetaylari.item(satir, 0).text()
            dosyaadi = "PO-" + uretimemrinumarasi + "-" + siparisdetayid
            output = "//AYAS/Users/AYAS/Desktop/MRP/URETIM/SIPARIS FORMLARI/" + dosyaadi + ".xls"

        try:
            if os.path.exists(output):
                QtWidgets.QMessageBox.warning(self, "HATA",
                                              "SİPARİS TALEP FORMU DAHA ONCE HAZIRLANMIS\n DOSYANIN SİLİNMESİ GEREKMEKTEDİR")
                return
            else:
                pass

            query = """select t1.olcux, t1.olcuy, t1.olcuz, t1.adet, t1.aciklamalar, t1.malzemetipi, t1.siparismistokmu, 
                                    t1.malzemecinsi, t1.siparisdetayid from
                                    (select olcux, olcuy, olcuz, malzemetalepleri.adet as adet, aciklamalar, termintarihi, malzemetipi, siparismistokmu,
                                    malzemelistesi.malzeme_adi as malzemecinsi, siparisdetaylari_siparis_detay_id as siparisdetayid from malzemetalepleri
                                    join malzemelistesi on malzemelistesi.malzeme_id = malzemetalepleri.malzemelistesi_malzeme_id where siparismistokmu = 'SİPARİŞ') as t1 where t1.siparisdetayid = {0}"""
            newquery = query.format(siparisdetayid)
            self.cursor.execute(newquery)
            datalar = self.cursor.fetchall()
            try:
                deneme = exeltablecreator.SiparisTalepFormu()
                deneme.form_olustur("MALZEME SIPARIS FORMU.xlsx", output, datalar, dosyaadi)
                QtWidgets.QMessageBox.information(self, "TAMAMLANDI", "SİPARİŞ FORM TASLAĞI HAZIRLANDI")
            except:
                import traceback
                traceback.print_exc()
            import subprocess
            subprocess.Popen(output, shell=True)
        except:
            import traceback
            traceback.print_exc()

        pass
    # def arama(self):
    #     pass
    #     aranacak = self.lineEdit.text()
    #     self.cursor.execute("select * from malzemetalebigorunumu where parcaadi like '%"+aranacak+"%' or firma_adi like  '%"+aranacak+"%' or malzemeadi like '%"+aranacak+"%'")
    #     sonuclistesi = self.cursor.fetchall()
    #     self.malzemelistesi.setRowCount(len(sonuclistesi))
    #     for satir, data in enumerate(sonuclistesi):
    #         self.malzemelistesi.setItem(satir, 0, QtWidgets.QTableWidgetItem(data[0]))
    #         self.malzemelistesi.setItem(satir, 1, QtWidgets.QTableWidgetItem(data[1]))
    #         self.malzemelistesi.setItem(satir, 2, QtWidgets.QTableWidgetItem(data[2]))
    #         self.malzemelistesi.setItem(satir, 3, QtWidgets.QTableWidgetItem(str(data[3])))
    #         self.malzemelistesi.setItem(satir, 4, QtWidgets.QTableWidgetItem(str(data[4])))
    #         self.malzemelistesi.setItem(satir, 5, QtWidgets.QTableWidgetItem(str(data[5])))
    #         self.malzemelistesi.setItem(satir, 6, QtWidgets.QTableWidgetItem(str(data[6])))
    #         self.malzemelistesi.setItem(satir, 7, QtWidgets.QTableWidgetItem(str(data[7])))
    #         self.malzemelistesi.setItem(satir, 8, QtWidgets.QTableWidgetItem(str(data[8])))
    #         self.malzemelistesi.setItem(satir, 9, QtWidgets.QTableWidgetItem(str(data[9])))
    #         self.malzemelistesi.setItem(satir, 10, QtWidgets.QTableWidgetItem(str(data[10])))
    # def getselection(self):
    #     pass
    #     try:
    #         satir = self.malzemelistesi.currentRow()
    #         if satir == -1 :
    #             return 1
    #         else:
    #             uretimno = self.malzemelistesi.item(satir, 1).text()
    #             resimno = self.malzemelistesi.item(satir ,3).text()
    #             self.cursor.execute("select id from uretimemirleri where uretimEmirNo = '"+uretimno+"'")
    #             self.uretimnoid = self.cursor.fetchone()[0]
    #             self.cursor.execute("select id from teknikresimler where teknikresimno = '"+resimno+"'")
    #             self.teknikresimnumarasiid = self.cursor.fetchone()[0]
    #     except:
    #         QtWidgets.QMessageBox.warning(self, "HATA", "BÜTÜN MALZEME TALEPLERİ SİLİNDİ")
    # def silme(self):
    #     pass
    #     if self.getselection() == 1 :
    #         QtWidgets.QMessageBox.warning(self, "HATA", "SEÇİM YAPMANIZ GEREKMEKTEDİR")
    #     else:
    #         self.cursor.execute(
    #             "delete from malzemetalebi where parcaid = '" + str(self.teknikresimnumarasiid )+ "' and uretimnoid = '" + str(self.uretimnoid )+ "'")
    #         self.cursor.execute(
    #             "update uretimdetaylari set siparisdurumu  = False where teknikresim_id = '" +str(self.teknikresimnumarasiid )+ "' and uretimemrino_id = '" + str(self.uretimnoid )+ "'")
    #         self.conn.commit()
    #         self.parcalistesiguncelle()
    #         self.siparistalepformlistesigorunumu()
    #         self.listegorunumu()
    #         # self.listegorunumu()
