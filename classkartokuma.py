#!/usr/bin/env python
# -*- coding:utf -8-*-
from PyQt5.QtCore import pyqtSignal
from PyQt5 import QtCore
import serial


class KartOkuma(QtCore.QThread):
    kartsinyali = pyqtSignal(str)

    def __init__(self, parent=None):
        super(KartOkuma, self).__init__(parent)
        self.ser = serial.Serial(port="COM3",
                                 baudrate=9600,
                                 parity=serial.PARITY_ODD,
                                 stopbits=serial.STOPBITS_TWO,
                                 bytesize=serial.SEVENBITS
                                 )
        self.frequency = 1000  # Set Frequency To 2500 Hertz
        self.duration = 300  # Set Duration To 1000 ms == 1 second
    def run(self):
        while 1:
            time.sleep(0.25)
            try:
                deger = self.ser.inWaiting()
            except:
                self.exit()
                return
            if deger != 0:
                serial = self.ser.read(self.ser.inWaiting())
                if serial[0] != "F":
                    time.sleep(0.25)
                    self.kartsinyali.emit(serial)
                else:
                    pass
            else:
                continue