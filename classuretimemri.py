#!/usr/bin/env python
# -*- coding:utf -8-*-
from mysql.connector import (connection)
from PyQt5 import QtWidgets
import uretimEmriFormu
from PyQt5.QtGui import QPixmap
import subprocess
import datetime
import pdfuretimemri
import os



pathuretimemri = "\\\\Desktop-qc87hvj\\\Akmill\\MRP\\URETIM\\"


class UretimEmri(uretimEmriFormu.Ui_uretimEmriFormu, QtWidgets.QDialog):  # üretim emri oluşturulan formu çağıran class
    def __init__(self, parent=None):
        super(UretimEmri, self).__init__(parent)
        self.setupUi(self)
        try:
            self.conn = connection.MySQLConnection(user='root', password='13579', host='localhost', database='AKMILL')
            self.cursor = self.conn.cursor(buffered=True)
        except:
            QtWidgets.QMessageBox.warning(self, "BAĞLANTI SORUNU",
                                          "VERİTABANINA BAĞLANTI GERÇEKLEŞTİRİLEMEDİ \n BAĞLANTILARINIZI KONTROL EDİNİZ")
            return
        self.cursor.execute("SELECT firma_adi FROM  firmalar")
        firmalar = self.cursor.fetchall()
        for i in firmalar:
            self.firma_adi.addItems(i)
            self.proje_ekleme_firma_adi.addItems(i)
        aktiffirma = self.firma_adi.currentText()
        try:
            self.cursor.execute("select firma_id from firmalar where firma_adi = '" + aktiffirma + "'")
            aktiffirmaid = (self.cursor.fetchone()[0])
            self.cursor.execute("SELECT proje_adi from projeler where FIRMALAR_firma_id  = '" + str(aktiffirmaid) + "'")
            projeler = self.cursor.fetchall()
            for i in projeler:
                self.proje_adi.addItems(i)
        except TypeError:
            QtWidgets.QMessageBox.warning(self, "HATA",
                                          "TANIMLI BİR FİRMA BULUNAMADIĞI İÇİN İŞLEM GERÇEKLEŞTİRİLEMİYOR")
            return
        self.projelistesiguncelle()
        self.firma_adi.currentIndexChanged.connect(self.projelistesiguncelle)
        self.uretimdurumu.setValue(0)
        self.tableWidget.currentCellChanged.connect(self.progressbarguncelle)
        self.tableWidget.setSelectionBehavior(1)
        self.tableWidget.setSelectionMode(1)
        self.projelistesiguncelle()
        self.proje_ekle.clicked.connect(self.projeekle)
        self.proje_ekleme_firma_adi.currentIndexChanged.connect(self.projelistesiguncelle)
        self.proje_sil.clicked.connect(self.proje_silme)
        self.siparis_detayi_ekle.clicked.connect(self.siparisdetayiekle)
        self.yeni_uretim_emir_numarasi.clicked.connect(self.numaralandir)
        self.uretimemirlerilistesiguncelle()
        self.uretim_emirleri_listesi.currentCellChanged.connect(self.siparisdetaylarilistesiguncelle)
        self.teslim.selectionChanged.connect(self.sevktarihi)
        self.siparis_detayi_sil.clicked.connect(self.siparisdetaysil)
        self.uretim_emiri_sil.clicked.connect(self.uretimemrisil)
        self.uretim_emirleri_listesi.doubleClicked.connect(self.uretimemriduzenle)
        self.siparis_detaylari_listesi.doubleClicked.connect(self.siparisdetayiduzenle)
        self.uretim_emrine_tasi.clicked.connect(self.siparisdetaytasima)
    def siparisdetaytasima(self):
        try:

            secilisatir = self.siparis_detaylari_listesi.currentRow()
            if secilisatir == -1:
                QtWidgets.QMessageBox.warning(self, "HATA", "TAŞINACAK SİPARİŞ DETAYI SEÇİMİ YAPMANIZ GEREKMEKTEDİR")
                return
            tasinilansiparisdetayid = self.siparis_detaylari_listesi.item(secilisatir, 5).text()

            inp = QtWidgets.QInputDialog()
            inp.setOption(QtWidgets.QInputDialog.UseListViewForComboBoxItems)
            inp.setStyleSheet("""/*
      aqua.qss (version 0.1)
      released under the MIT license (https://opensource.org/licenses/MIT)
      www.poketcode.com - March 2016
    */
    /**** QWidget (enabled) ****/
    QWidget
    {
      background-color: rgb(80,80,80);
      color: rgb(220,220,220);
      font-size: 11px;
      outline: none;
    }
    /**** QWidget (disabled) ****/
    QWidget:disabled
    {
      color: rgb(40,40,40);
    }
    /*********************************************************************************************************/

    /**** QPushButton (enabled) ****/
    QPushButton
    {
      background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                                        stop: 0 rgb(120,120,120), stop: 1 rgb(80,80,80));
      border: 1px solid rgb(20,20,20);
      color: rgb(230,230,230);
      padding: 4px 8px;
    }

    QPushButton:hover
    {
      background-color: rgb(70,110,130);
    }
    QPushButton:pressed
    {
      border-color: rgb(90,200,255);
      padding: 1px -1px -1px 1px;
    }
    /**** QPushButton (checkable) ****/
    QPushButton:checked
    {
      background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                                        stop: 0 rgb(40,150,200), stop: 1 rgb(90,200,255));
      color: rgb(20,20,20);
    }

    QPushButton:checked:hover
    {
      background-color: rgb(70,110,130);
    }
    /**** QPushButton (disabled) ****/
    QPushButton:disabled
    {
      background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                                        stop: 0 rgb(160,160,160), stop: 1 rgb(120,120,120));
      border-color: rgb(60,60,60);
      color: rgb(40,40,40);
    }
    /**** QListView (enabled) ****/
    QListView
    {
      alternate-background-color: rgb(110,110,110);
      background-color: rgb(100,100,100);
      border: 1px solid rgb(20,20,20);
      color: rgb(220,220,220);
      selection-background-color: rgb(70,110,130);
      selection-color: white;
    }

    QListView QLineEdit
    {
      padding: -1 0 0 0;
    }
    QListView::item:hover,
    QListView::item:selected:hover
    {
      background-color: rgb(70,110,130);
      color: white;
    }

    QListView::item:selected
    {
      background-color: rgb(90,200,255);
      color: rgb(20,20,20);
    }

    /**** QListView (disabled) ****/
    QListView:disabled
    {
      alternate-background-color: rgb(130,130,130);
      background-color: rgb(120,120,120);
      border-color: rgb(60,60,60);
      color: rgb(40,40,40);
    }

    QListView::item:selected:disabled
    {
      background-color: transparent;
    }

    /*********************************************************************************************************/

    /**** QColumnView (enabled) ****/
    QColumnView
    {
      alternate-background-color: rgb(110,110,110);
      background-color: rgb(100,100,100);
      border: 1px solid rgb(20,20,20);
      color: rgb(220,220,220);
      selection-background-color: rgb(70,110,130);
      selection-color: white;
    }

    QColumnView QLineEdit
    {
      padding: -1 0 0 0;
    }

    QColumnView::item:hover,
    QColumnView::item:selected:hover
    {
      background-color: rgb(70,110,130);
      color: white;
    }

    QColumnView::item:selected
    {
      background-color: rgb(90,200,255);
      color: rgb(20,20,20);
    }

    /**** QColumnView (disabled) ****/
    QColumnView:disabled
    {
      alternate-background-color: rgb(130,130,130);
      background-color: rgb(120,120,120);
      border-color: rgb(60,60,60);
      color: rgb(40,40,40);
    }

    QColumnView::item:selected:disabled
    {
      background-color: transparent;
    }

    /*********************************************************************************************************/

    /**** QScrollArea (enabled) ****/
    QScrollArea
    {
      background-color: rgb(100,100,100);
      border: 1px solid rgb(20,20,20);
    }

    /**** QScrollArea (disabled) ****/
    QScrollArea:disabled
    {
      background-color: rgb(120,120,120);
      border-color: rgb(60,60,60);
    }

    /*********************************************************************************************************/


    /**** QComboBox (enabled) ****/
    QComboBox
    {
      background-color: rgb(80,80,80);
      border: 1px solid rgb(20,20,20);
      color: rgb(220,220,220);
      padding: 4px 8px;
      selection-background-color: rgb(70,110,130);
      selection-color: white;
    }

    QComboBox:hover
    {
      background-color: rgb(70,110,130);
    }

    QComboBox::drop-down
    {
      image: url(images/dropdown.png);
      margin: 4px;
      width: 14px;
      height: 14px;
    }

    QComboBox::drop-down:on
    {
      image: url(images/dropdown_on.png);
    }

    QComboBox QAbstractItemView
    {
      background-color: rgb(80,80,80);
      padding: 4px;
    }

    /**** QComboBox (editable) ****/
    QComboBox:editable
    {
      background-color: rgb(80,80,80);
      padding: 4px;
    }

    QComboBox:editable:focus
    {
      border-color: rgb(90,200,255);
    }

    /**** QComboBox (disabled) ****/
    QComboBox:disabled
    {
      background-color: rgb(120,120,120);
      border-color: rgb(60,60,60);
      color: rgb(40,40,40);
    }

    QComboBox::drop-down:disabled
    {
      image: url(images/dropdown_disabled.png);
    }

    /**** QScrollBar (enabled) ****/
    QScrollBar
    {
      background-color: transparent;
      border: 1px solid rgb(10,10,10);
      border-radius: 5px;
    }

    QScrollBar:hover
    {
      background-color: rgb(70,110,130);
    }

    QScrollBar::handle
    {
      background-color: rgb(90,200,255);
      border-radius: 3px;
      min-width: 16px;
      min-height: 16px;
      margin: 1px;
    }

    QScrollBar::add-line,
    QScrollBar::sub-line
    {
      border: 1px solid transparent;
      border-radius: 10px;
      subcontrol-origin: margin;
    }

    QScrollBar::add-line:hover,
    QScrollBar::add-line:pressed,
    QScrollBar::sub-line:hover,
    QScrollBar::sub-line:pressed
    {
      background-color: rgb(70,110,130);
    }

    QScrollBar::add-page,
    QScrollBar::sub-page
    {
      background-color: transparent;
    }

    /**** QScrollBar (horizontal) ****/
    QScrollBar:horizontal
    {
      height: 20px;
      margin: 5px 23px 5px 23px;
    }

    QScrollBar::handle:horizontal
    {
      min-width: 30px;
    }

    QScrollBar::add-line:horizontal
    {
      width: 20px;
      subcontrol-position: right;
    }

    QScrollBar::sub-line:horizontal
    {
      width: 20px;
      subcontrol-position: left;
    }

    QScrollBar::left-arrow:horizontal
    {
      image: url(images/scrollbar_left_arrow.png);
    }

    QScrollBar::left-arrow:horizontal:pressed
    {
      image: url(images/scrollbar_left_arrow_pressed.png);
    }

    QScrollBar::right-arrow:horizontal
    {
      image: url(images/scrollbar_right_arrow.png);
    }

    QScrollBar::right-arrow:horizontal:pressed
    {
      image: url(images/scrollbar_right_arrow_pressed.png);
    }

    /**** QScrollBar (vertical) ****/
    QScrollBar:vertical
    {
      width: 20px;
      margin: 23px 5px 23px 5px;
    }

    QScrollBar::handle:vertical
    {
      min-height: 30px;
    }

    QScrollBar::add-line:vertical
    {
      height: 20px;
      subcontrol-position: bottom;
    }

    QScrollBar::sub-line:vertical
    {
      height: 20px;
      subcontrol-position: top;
    }

    QScrollBar::up-arrow:vertical
    {
      image: url(images/scrollbar_up_arrow.png);
    }

    QScrollBar::up-arrow:vertical:pressed
    {
      image: url(images/scrollbar_up_arrow_pressed.png);
    }

    QScrollBar::down-arrow:vertical
    {
      image: url(images/scrollbar_down_arrow.png);
    }

    QScrollBar::down-arrow:vertical:pressed
    {
      image: url(images/scrollbar_down_arrow_pressed.png);
    }

    /**** QScrollBar (disabled) ****/
    QScrollBar:disabled
    {
      background-color: rgb(120,120,120);
      border-color: rgb(60,60,60);
    }

    QScrollBar::handle:disabled
    {
      background-color: rgb(80,80,80);
    }

    QScrollBar::left-arrow:horizontal:disabled
    {
      image: url(images/scrollbar_left_arrow_disabled.png);
    }

    QScrollBar::right-arrow:horizontal:disabled
    {
      image: url(images/scrollbar_right_arrow_disabled.png);
    }

    QScrollBar::up-arrow:vertical:disabled
    {
      image: url(images/scrollbar_up_arrow_disabled.png);
    }

    QScrollBar::down-arrow:vertical:disabled
    {
      image: url(images/scrollbar_down_arrow_disabled.png);
    }

    /*********************************************************************************************************/

    /**** QSlider (enabled) ****/
    QSlider
    {
      background-color: transparent;
    }

    QSlider::groove
    {
      border-radius: 2px;
    }

    QSlider::handle
    {
      background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                                        stop: 0 rgb(40,150,200), stop: 1 rgb(90,200,255));
      border-radius: 8px;
    }

    /**** QSlider (horizontal) ****/
    QSlider::groove:horizontal
    {
      background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                                        stop: 0 rgb(160,160,160), stop: 1 rgb(80,80,80));
      height: 4px;
    }

    QSlider::handle:horizontal
    {
      margin: -6px 0;
      width: 16px;
    }

    QSlider::groove:horizontal:hover
    {
      background-color: rgb(70,110,130);
    }

    /**** QSlider (vertical) ****/
    QSlider::groove:vertical
    {
      background-color: qlineargradient(x1: 0, y1: 0, x2: 1, y2: 0,
                                        stop: 0 rgb(160,160,160), stop: 1 rgb(80,80,80));
      width: 4px;
    }

    QSlider::handle:vertical
    {
      margin: 0 -6px;
      height: 16px;
    }

    QSlider::groove:vertical:hover
    {
      background-color: rgb(70,110,130);
    }

    /**** QSlider (disabled) ****/
    QSlider::handle:disabled
    {
      background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                                        stop: 0 rgb(160,160,160), stop: 1 rgb(120,120,120));
    }

    /*********************************************************************************************************/


    """)
            inp.setWindowTitle("SİPARİŞ DETAYI TAŞIMA")

            try:
                self.cursor.execute(
                    "select firma_id from firmalar where firma_adi = '" + self.firma_adi.currentText() + "'")
                firmaid = self.cursor.fetchall()[0][0]
                self.cursor.execute(
                    "select uretim_emir_no from uretimemirleri where FIRMALAR_firma_id = '" + str(firmaid) + "'")
                uretimemirnumaralari = self.cursor.fetchall()
                print uretimemirnumaralari
                liste = []
                for i in uretimemirnumaralari:
                    liste.append(i[0])

                inp.setComboBoxItems(liste)


            except:
                import traceback
                traceback.print_exc()
            try:

                if inp.exec_() == QtWidgets.QDialog.Accepted:
                    self.cursor.execute("start transaction")
                    tasinacakuretimemirnumarasi = inp.textValue()
                    self.cursor.execute(
                        "select uretim_emir_id from uretimemirleri where uretim_emir_no = '" + tasinacakuretimemirnumarasi + "'")
                    tasinacakuretimemirid = str(self.cursor.fetchall()[0][0])
                    self.cursor.execute(
                        "update siparisdetaylari set  URETIMEMIRLERI_uretim_emir_id = '" + tasinacakuretimemirid + "' where siparis_detay_id"
                                                                                                                   "= '" + tasinilansiparisdetayid + "'")

                    self.cursor.execute(
                        "update uretimdetaylandirma set  URETIMEMIRLERI_uretim_emir_id = '" + tasinacakuretimemirid + "' where SIPARISDETAYLARI_siparis_detay_id"
                                                                                                                      "= '" + tasinilansiparisdetayid + "'")
                    QtWidgets.QMessageBox.warning(self, "BİLDİRİM", "TAŞIMA İŞLEMİ BAŞARI İLE GERÇEKLEŞTİRİLDİ")


                else:
                    print ("onaylanmadi")

            except:
                self.cursor.execute('rollback')

                import traceback
                traceback.print_exc()
        except:
            import traceback
            traceback.print_exc()
        self.conn.commit()
    def uretimemriduzenle(self):
        satir = self.uretim_emirleri_listesi.currentRow()
        sutun = self.uretim_emirleri_listesi.currentColumn()
        if sutun == 1 or sutun == 2:
            QtWidgets.QMessageBox.warning(self, "HATA",
                                          "OLUŞTURULMUŞ ÜRETİM EMİRLERİNDE SADECE MÜŞTERİ SİPARİŞ NUMARASINA AİT DEĞİŞİKLİK YAPILABİLİR \n"
                                          "DİĞER ALANARLA İLGİLİ DEĞİŞİKLİK YAPMAK İSTİYORSANIZ ÜRETİM EMRİNİ SİLİP YENİDEN OLUŞTURMANIZ GEREKMEKTEDİR")
            return
        elif sutun == 0:
            import subprocess
            # pathuretimemri = "\\\\DESKTOP-QC87HVJ\\Users\\PC1\\Desktop\\AKMILL\\MRP\\URETIM\\"
            dosyaadi = self.uretim_emirleri_listesi.item(satir, 2).text()
            acilacak = pathuretimemri + dosyaadi
            try:
                subprocess.Popen(r'explorer /open, ' + acilacak + '')
                # return
            except:
                import traceback
                traceback.print_exc()


        else:
            inp = QtWidgets.QInputDialog()
            inp.setInputMode(QtWidgets.QInputDialog.TextInput)
            text, ok = inp.getText(self, 'SİPAİRŞ NUMARASI DEĞİŞTİR', 'MÜŞTERİ SİPARİŞ NUMARASI')
            if ok:
                if len(text) == 0:
                    QtWidgets.QMessageBox.warning(self, "HATA", "ALAN BOŞ BIRAKILAMAZ")
                    return
                else:
                    try:
                        self.cursor.execute("start transaction")
                        self.cursor.execute(
                            "update uretimemirleri set musteri_siparis_no = '" + text + "' where uretim_emir_id = '" + self.uretim_emirleri_listesi.item(
                                satir, 4).text() + "'")
                        QtWidgets.QMessageBox.warning(self, "BİLDİRİM", "MÜŞTERİ SİPARİŞ NUMARASI GÜNCELLENDİ")
                    except:
                        self.cursor.execute("rollback")
                        QtWidgets.QMessageBox.warning(self, "HATA",
                                                      "BİR HATA İLE KARŞILAŞILDI İŞLEMLER GERİ ALINIYOR...")

                self.conn.commit()
                self.uretimemirlerilistesiguncelle()
            else:
                return
    def siparisdetayiduzenle(self):
        satir = self.siparis_detaylari_listesi.currentRow()
        sutun = self.siparis_detaylari_listesi.currentColumn()
        if sutun == 0:
            inp = QtWidgets.QInputDialog()
            inp.setInputMode(QtWidgets.QInputDialog.TextInput)
            text, ok = inp.getText(self, 'PARÇA NUMARASI DEĞİŞTİR', 'MÜŞTERİ PARÇA NUMARASI')
            if ok:
                if len(text) == 0:
                    QtWidgets.QMessageBox.warning(self, "HATA", "ALAN BOŞ BIRAKILAMAZ")
                    return
                else:
                    try:
                        self.cursor.execute("start transaction")
                        self.cursor.execute(
                            "update siparisdetaylari set musteri_parca_numarasi = '" + text + "'  where siparis_detay_id = '" + self.siparis_detaylari_listesi.item(
                                satir, 5).text() + "'")
                        QtWidgets.QMessageBox.warning(self, "BİLDİRİM", "MÜŞTERİ PARÇA NUMARASI GÜNCELLENDİ")
                        self.siparisdetaylarilistesiguncelle()
                    except:
                        QtWidgets.QMessageBox.warning(self, "HATA", "BEKLENMEDİK BİR HATA İLE KARŞILAŞILDI ")
                        return
                self.conn.commit()
        elif sutun == 1:
            inp = QtWidgets.QInputDialog()
            inp.setInputMode(QtWidgets.QInputDialog.TextInput)
            text, ok = inp.getText(self, 'MALZEME TANIMI DEĞİŞTİR', 'MALZEME TANIMI')
            if ok:
                if len(text) == 0:
                    QtWidgets.QMessageBox.warning(self, "HATA", "ALAN BOŞ BIRAKILAMAZ")
                    return
                else:
                    self.cursor.execute("start transaction")
                    self.cursor.execute(
                        "update siparisdetaylari set malzeme_tanimi = '" + text + "'  where siparis_detay_id = '" + self.siparis_detaylari_listesi.item(
                            satir, 5).text() + "'")
                    QtWidgets.QMessageBox.warning(self, "BİLDİRİM", "MALZEME TANIMI GÜNCELLENDİ")
                self.conn.commit()
                self.siparisdetaylarilistesiguncelle()
        elif sutun == 2:
            inp = QtWidgets.QInputDialog()
            inp.setInputMode(QtWidgets.QInputDialog.TextInput)
            text, ok = inp.getText(self, 'SEVKİYAT TARİHİNİ DEĞİŞTİR', 'SEVK TARİHİ YYYY/MM/DD FORMATINDA GİRİŞ ')
            if ok:
                if len(text) == 0:
                    QtWidgets.QMessageBox.warning(self, "HATA", "ALAN BOŞ BIRAKILAMAZ")
                    return
                else:
                    try:
                        self.cursor.execute("start transaction")
                        self.cursor.execute(
                            "update siparisdetaylari set sevk_tarihi = '" + text + "'  where siparis_detay_id = '" + self.siparis_detaylari_listesi.item(
                                satir, 5).text() + "'")
                        QtWidgets.QMessageBox.warning(self, "BİLDİRİM", "MALZEME TANIMI GÜNCELLENDİ")
                    except:
                        self.cursor.execute("rollback")
                        QtWidgets.QMessageBox.warning(self, " HATA", "GİRDİĞİNİZ TARİH FORMATINI KONTROL EDİNİZ")
                        return
                self.conn.commit()
                self.siparisdetaylarilistesiguncelle()
        elif sutun == 3:
            inp = QtWidgets.QInputDialog()
            inp.setInputMode(QtWidgets.QInputDialog.TextInput)
            text, ok = inp.getText(self, 'SİPARİŞ MİKTARI', 'SİPARİŞ MİKTARINI TAM SAYI OLARAK GİRİNİZ ')
            if ok:
                if len(text) == 0:
                    QtWidgets.QMessageBox.warning(self, "HATA", "ALAN BOŞ BIRAKILAMAZ")
                    return
                else:
                    try:
                        text = int(text)
                        self.cursor.execute("start transaction")
                        self.cursor.execute("update siparisdetaylari set miktar = '" + str(
                            text) + "'  where siparis_detay_id = '" + self.siparis_detaylari_listesi.item(satir,
                                                                                                          5).text() + "'")
                        QtWidgets.QMessageBox.warning(self, "BİLDİRİM", "SİPARİŞ MİKTARI GÜNCELLENDİ")
                    except:
                        self.cursor.execute("rollback")
                        QtWidgets.QMessageBox.warning(self, " HATA", "SİPARİŞ MİKTARI TAN SAYI OLARAK GİRİNİZ")
                        return
                self.conn.commit()
                self.siparisdetaylarilistesiguncelle()
        else:
            inp = QtWidgets.QInputDialog()
            inp.setInputMode(QtWidgets.QInputDialog.TextInput)
            text, ok = inp.getText(self, 'SİPARİŞ DETAYI AÇIKLAMALARI', ' AÇIIKLAMALAR...')
            if ok:
                if len(text) == 0:
                    QtWidgets.QMessageBox.warning(self, "HATA", "ALAN BOŞ BIRAKILAMAZ")
                    return
                else:
                    try:
                        self.cursor.execute("start transaction")
                        self.cursor.execute(
                            "update siparisdetaylari set aciklamalar = '" + text + "'  where siparis_detay_id = '" + self.siparis_detaylari_listesi.item(
                                satir, 5).text() + "'")
                        QtWidgets.QMessageBox.warning(self, "BİLDİRİM", "SİPARİŞ DETAYU AÇIKLAMASI GÜNCELLENDİ")
                    except:
                        self.cursor.execute("rollback")
                        QtWidgets.QMessageBox.warning(self, " HATA", "BİR HATA İLE KARŞILAŞILDI İŞLEMLER GERİ ALINIYOR")
                        return
                self.conn.commit()
                self.siparisdetaylarilistesiguncelle()
        pass
    def projelistesiguncelle(self):
        self.siparis_detaylari_listesi.clearContents()
        self.siparis_detaylari_listesi.setRowCount(0)
        self.uretim_emirleri_listesi.clearContents()
        self.uretim_emirleri_listesi.setRowCount(0)
        self.proje_adi.clear()
        self.aktiffirma = self.firma_adi.currentText()
        self.cursor.execute("select firma_id from firmalar where firma_adi = '" + self.aktiffirma + "'")
        self.aktiffirmaid = (self.cursor.fetchone()[0])
        self.cursor.execute(
            "select firma_numaralandirma from firmalar where firma_id = '" + str(self.aktiffirmaid) + "'")
        self.firmanumarasi = self.cursor.fetchone()[0]
        self.cursor.execute(
            "SELECT proje_adi from projeler where FIRMALAR_firma_id  = '" + str(self.aktiffirmaid) + "'")
        projeler = self.cursor.fetchall()
        for i in projeler:
            self.proje_adi.addItems(i)
        self.sevk_tarihi.setText(self.teslim.selectedDate().toString('yyyy/MM/dd'))
        # self.teslim.selectionChanged.connect(self.numaralandir)
        # self.firma_adi.currentIndexChanged.connect(self.numaralandir)
        self.uretimemirlerilistesiguncelle()
        self.numaralandir()
    # def numaralandir(self):
    #     try:
    #         print self.firma_adi.currentText()
    #         self.cursor.execute(
    #             "select firma_id from firmalar where firma_adi = '" + self.firma_adi.currentText() + "'")
    #         firmaid = self.cursor.fetchall()
    #         print firmaid
    #         firmaid = firmaid[0][0]
    #
    #     except:
    #         import traceback
    #         traceback.print_exc()
    #
    #     try:
    #         self.year = datetime.datetime.now().strftime('%Y')
    #         self.cursor.execute("select count(*) from uretimemirleri where FIRMALAR_firma_id = '" + str(
    #             firmaid) + "' and yil = '" + str(self.year[2:]) + "'")
    #         sifirlanmismi = self.cursor.fetchall()[0][0]
    #         if sifirlanmismi == 0:
    #             self.toplamemirsayisi = 0
    #             self.uretim_emir_numarasi.setText(str("U-") +
    #                                               str(self.year[2:]) + "-00" + str(self.firmanumarasi) + "-" + str(
    #                 self.toplamemirsayisi + 1))
    #             self.uretimemirnumaralandirma = self.toplamemirsayisi
    #         else:
    #
    #             self.cursor.execute(
    #                 "select uretimemrinumaralandirma from uretimemirleri where FIRMALAR_firma_id = '" + str(
    #                     firmaid) + "' and yil = '" + str(self.year[2:]) + "' order by uretimemrinumaralandirma desc")
    #             self.uretimemirnumaralandirma = self.cursor.fetchone()[0]
    #             print self.uretimemirnumaralandirma
    #             self.uretim_emir_numarasi.setText(
    #                 str("U-") + str(self.year[2:]) + "-00" + str(self.firmanumarasi) + "-" + str(
    #                     self.uretimemirnumaralandirma + 1))
    #     except:
    #         import traceback
    #         traceback.print_exc()






    def numaralandir(self):
        try:
            print self.firma_adi.currentText()
            self.cursor.execute(
                "select firma_id from firmalar where firma_adi = '" + self.firma_adi.currentText() + "'")
            firmaid = self.cursor.fetchall()
            print firmaid
            firmaid = firmaid[0][0]

        except:
            import traceback
            traceback.print_exc()

        try:
            self.year = datetime.datetime.now().strftime('%Y')
            self.cursor.execute("select count(*) from uretimemirleri where FIRMALAR_firma_id = '" + str(
                firmaid) + "' and yil = '" + str(self.year[2:]) + "'")
            sifirlanmismi = self.cursor.fetchall()[0][0]
            if sifirlanmismi == 0:
                self.toplamemirsayisi = 0
                self.uretim_emir_numarasi.setText(str("U-") +
                                                  str(self.year[2:]) + "-00" + str(self.firmanumarasi) + "-" + str(
                    self.toplamemirsayisi + 1))
                self.uretimemirnumaralandirma = self.toplamemirsayisi
            else:

                self.cursor.execute("select uretimemrinumaralandirma from uretimemirleri where yil = '" + str(self.year[2:]) + "' order by uretimemrinumaralandirma desc")
                self.uretimemirnumaralandirma = self.cursor.fetchone()[0]
                print self.uretimemirnumaralandirma
                self.uretim_emir_numarasi.setText(
                    str("U-") + str(self.year[2:]) + "-00" + str(self.firmanumarasi) + "-" + str(
                        self.uretimemirnumaralandirma + 1))
        except:
            import traceback
            traceback.print_exc()






    def progressbarguncelle(self):
        satir = self.tableWidget.currentRow()
        col = 6
        value = self.tableWidget.item(satir, col).text()

        self.cursor.execute(
            "select count(*) from isemirlerilistesi where uretimEmirNo = '" + value + "' and kalitekontrol= 'ONAY' ")
        olusturulanisemrisayisi = float(self.cursor.fetchone()[0])
        self.cursor.execute("select count(*) from uretimdetaylandirma where uretimEmirNo = '" + value + "'")
        mevcutmu = self.cursor.fetchone()[0]
        if mevcutmu == 0:
            QtWidgets.QMessageBox.warning(self, "HATA", "ÜRETİM EMRİ DETAYLANDIRMA TANIMLAMALARI YAPILMAMIŞTIR")
            return
        else:
            self.cursor.execute(
                "select sum(operasyonsayisi) from uretimdetaylandirma where uretimEmirNo = '" + value + "'")
            olusturulmasigerekenisemrisayisi = int(self.cursor.fetchone()[0])
            percentage = 100 * (float((olusturulanisemrisayisi) / olusturulmasigerekenisemrisayisi))
            self.cikartilan.setText(str(olusturulanisemrisayisi))
            self.kalan.setText(str(olusturulmasigerekenisemrisayisi - olusturulanisemrisayisi))
            self.toplam.setText(str(olusturulmasigerekenisemrisayisi))
            self.label_4.setText(str(value) + " NUMARALI URETIMIN DURUMU")
            i = 0
            while i <= (percentage + 0.1):
                self.uretimdurumu.setValue(i)
                time.sleep(0.01)
                i = i + 1
            self.uretimdurumu.setValue(percentage)
    def projeekle(self):
        self.cursor.execute(
            "select firma_id from firmalar where firma_adi = '" + self.proje_ekleme_firma_adi.currentText() + "'")
        firmaid = (self.cursor.fetchone()[0])
        projeadi = self.proje_ekleme_proje_adi.text()
        try:
            self.cursor.execute("select count(*) from projeler where proje_adi = '" + projeadi + "'")
            if self.cursor.fetchone()[0] != 0:
                QtWidgets.QMessageBox.warning(self, "HATA",
                                              "BU PROJE ADI DAHA ÖNCE KULLANILMIŞ \n AYNI PROJE ADI İLE DEVAM ETMEK İSTİYORSANIZ SİPARİŞ FORMUNUZDA YER ALAN TARİHİDE PROJE ADINA EKLEYEREK DEVAM EDINIZ")
                return
            else:
                self.cursor.execute("start transaction")
                self.cursor.execute("INSERT INTO projeler (FIRMALAR_firma_id, proje_adi ) values (%s, %s)",
                                    (firmaid, projeadi))
                QtWidgets.QMessageBox.warning(self, "BAŞARILI", "PROJE ADI BAŞARI İLE EKLENDİ")
        except:
            QtWidgets.QMessageBox.warning(self, "HATA", "BİR HATA İLE KARŞILAŞILDI İŞLEMLER GERİ ALINIYOR")
            self.cursor.execute("rollback")
        self.conn.commit()
        self.projelistesiguncelle()
    def proje_silme(self):
        satir = self.proje_listesi.currentRow()
        if satir == -1:
            QtWidgets.QMessageBox.warning(self, "HATA", "LÜTFEN SEÇİM YAPINIZ")
            return
        else:
            col = 2
            projeid = self.proje_listesi.item(satir, col).text()
            try:
                self.cursor.execute("start transaction")
                self.cursor.execute("delete from projeler where proje_id = '" + projeid + "'")
                QtWidgets.QMessageBox.warning(self, "BAŞARILI", "KAYIT BAŞARI İLE SİLİNDİ")
            except:
                import traceback
                traceback.print_exc()
                QtWidgets.QMessageBox.warning(self, "HATA", "BİR HATA İLE KARŞILAŞILDI")
                self.cursor.execute("rollback")
                return
            self.conn.commit()
            self.projelistesiguncelle()
    def uretimemriekle(self, uretimemirnumarasi, adduretimemri, data_uretimemri, pathuretimemri):
        try:
            QtWidgets.QMessageBox.information(self, "BİLDİRİM",
                                              " '" + uretimemirnumarasi + "' numaralı üretim emri oluşturuluyor ve belirtmiş olduğunuz sipariş detayı ekleniyor")
            self.cursor.execute("start transaction")
            self.cursor.execute(adduretimemri, data_uretimemri)
            QtWidgets.QMessageBox.warning(self, "BAŞARILI", "KAYITLAR BAŞARI İLE EKLENDİ")
            dirName = pathuretimemri + uretimemirnumarasi
            dirName2 = pathuretimemri + uretimemirnumarasi + "/FIRMA DOKUMANLARI"
            dirName3 = pathuretimemri + uretimemirnumarasi + "/URETIM DATALARI"
            dirName4 = pathuretimemri + uretimemirnumarasi + "/KALITE DOKUMANLARI"
            if not os.path.exists(dirName):
                os.mkdir(dirName)
                os.mkdir(dirName2)
                os.mkdir(dirName3)
                os.mkdir(dirName4)
                QtWidgets.QMessageBox.information(self, "BİLGİ",
                                                  "OLUŞTURULAN KLASÖRLER : \n '" + dirName2 + "' \n '" + dirName3 + "' \n '" + dirName4 + "'")
            else:
                QtWidgets.QMessageBox.warning(self, "HATA", "İLGİLİ KLASÖR ZATEN OLUŞTURULMUŞ")
            self.siparisdetayiekle()
        except:
            import traceback
            traceback.print_exc()
    def siparisdetayiekle(self):
        if self.siparis_miktari.text() == "0":
            QtWidgets.QMessageBox.information(self, "HATA", "SİPARİŞ MİKTARI BELİRTİLMESİ ZORUNLUDUR")
            return
        try:
            firmaadi = self.firma_adi.currentText().encode('utf-8')
            self.cursor.execute("select firma_id from firmalar where firma_adi = '" + firmaadi + "'")
            firmaid = self.cursor.fetchone()[0]
            projeadi = self.proje_adi.currentText().encode('utf-8')
            self.cursor.execute(
                "SELECT proje_id from projeler where proje_adi = '" + projeadi + "' and FIRMALAR_firma_id = '" + str(
                    firmaid) + "'")
            projeid = self.cursor.fetchone()[0]

        except:
            QtWidgets.QMessageBox.information(self, "HATA",
                                              "" + firmaadi + " FIRMASINA AIT TANIMLI HERHANGI BIR PROJE BULUNMAMAKTADIR \n"
                                                              "GEREKLI TANIMLAMALARI GERCEKLESTIRINIZ")
            return
        now = datetime.datetime.now()
        date = now.strftime('%Y')
        musterisiparisno = self.musteri_siparis_numarasi.text().encode('utf-8')
        malzemetanimi = self.malzeme_tanimi.text().encode('utf-8')
        musteriparcanumarasi = self.musteri_parca_numarasi.text().encode('utf-8')
        sevktarihi = self.sevk_tarihi.text().encode('utf-8')
        miktar = self.siparis_miktari.text().encode('utf-8')
        uretimemirnumarasi = self.uretim_emir_numarasi.text().encode('utf-8')
        aciklamalar = self.notlar.toPlainText().encode('utf-8')
        # aciklamalar1 = self.notlar.toPlainText().encode('utf-8')
        # aciklamalar = aciklamalar1.replace("\n", "<br>")

        self.uretimemirnumaralandirma = int(uretimemirnumarasi.split("-")[3])
        adduretimemri = ("INSERT INTO uretimemirleri " \
                         "(uretim_emir_no,PROJELER_proje_id,FIRMALAR_firma_id, musteri_siparis_no, uretimemrinumaralandirma, yil)" \
                         "VALUES (%s, %s, %s, %s, %s, %s)")
        data_uretimemri = (uretimemirnumarasi, projeid, firmaid, musterisiparisno, self.uretimemirnumaralandirma, (self.year[2:]))
        try:
            self.cursor.execute("select count(*) from uretimemirleri where uretim_emir_no = '" + uretimemirnumarasi + "'")
            if self.cursor.fetchone()[0] != 0:
                QtWidgets.QMessageBox.information(self, "BİLDİRİM",
                                                  "OLUŞTURMUS OLDUĞUNUZ '" + uretimemirnumarasi + "' numaralı uretim emrine siparis detayi ekleniyor")
                self.cursor.execute("select uretim_emir_id from uretimemirleri where uretim_emir_no = '" + uretimemirnumarasi + "'")
                uretimemirid = self.cursor.fetchall()[0][0]
                self.cursor.execute("select count(*) from siparisdetaylari where URETIMEMIRLERI_uretim_emir_id = '"+str(uretimemirid)+"'")
                toplamkayitsayisi = self.cursor.fetchone()[0]
                if toplamkayitsayisi == 0 :
                    siranumarasi = 1
                else :
                    self.cursor.execute("select numaralandirma from siparisdetaylari where URETIMEMIRLERI_uretim_emir_id= '" +str(uretimemirid)+"' order by numaralandirma desc")
                    sonkayitnumarasi = self.cursor.fetchall()[0][0]
                    siranumarasi = sonkayitnumarasi+ 1
                self.cursor.execute("start transaction")
                self.cursor.execute(
                    "insert into siparisdetaylari (URETIMEMIRLERI_uretim_emir_id, musteri_parca_numarasi, malzeme_tanimi, sevk_tarihi,  miktar, aciklamalar, yil, numaralandirma) values "
                    "(%s, %s, %s, %s, %s, %s, %s, %s)",
                    (uretimemirid, musteriparcanumarasi, malzemetanimi, sevktarihi, miktar, aciklamalar, str(date[2:]), siranumarasi))
                QtWidgets.QMessageBox.warning(self, "BİLDİRİM", "SİPARİŞ DETAYU EKLENDİ")

            else:

                self.uretimemriekle(uretimemirnumarasi, adduretimemri, data_uretimemri, pathuretimemri)


        except:
            import traceback
            traceback.print_exc()
            self.cursor.execute("rollback")
            QtWidgets.QMessageBox.warning(self, "!!!", "ÜRETİM EMRİ OLUŞTURULURKEN BİR PROBLEMLE KARŞILAŞILDI")
        self.conn.commit()
        self.uretimemirlerilistesiguncelle()
    def uretimemirlerilistesiguncelle(self):
        self.uretim_emirleri_listesi.clearContents()
        self.uretim_emirleri_listesi.setRowCount(0)
        self.cursor.execute(
            "select FIRMALAR.firma_adi , PROJELER.proje_adi, uretim_emir_no  , musteri_siparis_no, uretim_emir_id  from firmalar "
            "join uretimemirleri on uretimemirleri.FIRMALAR_firma_id = firmalar.firma_id "
            "join projeler on uretimemirleri.PROJELER_proje_id = projeler.proje_id where firma_adi = '" + self.firma_adi.currentText() + "'")
        uretimemirleri = self.cursor.fetchall()
        self.uretim_emirleri_listesi.setRowCount(len(uretimemirleri))
        for satir, deger in enumerate(uretimemirleri):
            self.uretim_emirleri_listesi.setItem(satir, 0, QtWidgets.QTableWidgetItem(deger[0]))
            self.uretim_emirleri_listesi.setItem(satir, 1, QtWidgets.QTableWidgetItem(deger[1]))
            self.uretim_emirleri_listesi.setItem(satir, 2, QtWidgets.QTableWidgetItem(deger[2]))
            self.uretim_emirleri_listesi.setItem(satir, 3, QtWidgets.QTableWidgetItem(deger[3]))
            self.uretim_emirleri_listesi.setItem(satir, 4, QtWidgets.QTableWidgetItem(str(deger[4])))
            self.uretim_emirleri_listesi.setColumnHidden(4, True)
    def siparisdetaylarilistesiguncelle(self):
        satir = self.uretim_emirleri_listesi.currentRow()
        if satir == -1:
            QtWidgets.QMessageBox.warning(self, "HATA", "LÜTFEN SEÇİM YAPINIZ")
            return
        else:
            col = 4
            uretimemirnoid = self.uretim_emirleri_listesi.item(satir, col).text()
            uretimemirno = self.uretim_emirleri_listesi.item(satir, 2).text()
            musterisiparisno = self.uretim_emirleri_listesi.item(satir, 3).text()
            self.uretim_emir_numarasi.setText(uretimemirno)
            self.musteri_siparis_numarasi.setText(musterisiparisno)
        try:
            self.cursor.execute(
                "select * from siparisdetaylari where URETIMEMIRLERI_uretim_emir_id = '" + uretimemirnoid + "'")
            siparisdetaylari = self.cursor.fetchall()
            self.siparis_detaylari_listesi.setRowCount(len(siparisdetaylari))
            for satir, deger in enumerate(siparisdetaylari):
                self.siparis_detaylari_listesi.setItem(satir, 0, QtWidgets.QTableWidgetItem((deger[2])))
                self.siparis_detaylari_listesi.setItem(satir, 1, QtWidgets.QTableWidgetItem(deger[3]))
                self.siparis_detaylari_listesi.setItem(satir, 2, QtWidgets.QTableWidgetItem(str(deger[4])))
                self.siparis_detaylari_listesi.setItem(satir, 3, QtWidgets.QTableWidgetItem(str(deger[5])))
                self.siparis_detaylari_listesi.setItem(satir, 4, QtWidgets.QTableWidgetItem(str(deger[6])))
                self.siparis_detaylari_listesi.setItem(satir, 5, QtWidgets.QTableWidgetItem(str(deger[0])))
            self.siparis_detaylari_listesi.setColumnHidden(5, True)
            # todo buraya preview dosya yolu eklenecek
            dosyaadi = uretimemirno + "\\Dokumantasyon\\" + uretimemirno + "-Dokumantasyonu.pdf-preview.png"
            path = pathuretimemri + dosyaadi
            pixmap = QPixmap(path)
            # pixmap= pixmap.scaledToHeight(600)
            # pixmap= pixmap.scaledToWidth(400)
            self.label.setScaledContents(True)
            self.label.setPixmap(pixmap)
            # self.preview.setFixedSize(580, 580)
        except:
            import traceback
            traceback.print_exc()
    def sevktarihi(self):
        self.sevk_tarihi.setText(self.teslim.selectedDate().toString('yyyy/MM/dd'))
    def siparisdetaysil(self):
        satir = self.siparis_detaylari_listesi.currentRow()
        if satir == -1:
            QtWidgets.QMessageBox.warning(self, "HATA", "LÜTFEN SEÇİM YAPINIZ")
            return
        else:
            col = 5
            siparisdetayid = self.siparis_detaylari_listesi.item(satir, col).text()
            try:
                self.cursor.execute("start transaction")
                self.cursor.execute("delete from siparisdetaylari where siparis_detay_id = '" + siparisdetayid + "'")
                QtWidgets.QMessageBox.warning(self, "BİLGİ", "SİLME İŞLEMİ BAŞARI İLE GERÇEKLEŞTİRİLDİ")
            except:
                QtWidgets.QMessageBox.warning(self, "HATA",
                                              "SİLME İŞLEMİ GERÇEKLEŞTİRİLEMEDİ \n BU SİPARİŞ DETAYI KULLANILARAK MALZEMETALEBİ, İŞ EMRİ VEYA URETİM DETAYI OLUŞTURULMUŞ OLABİLİR \n VERİ TABANI İLE OLAN BAĞLANTI KOPMUŞ OLABİLİR")
                self.cursor.execute("rollback")
                return
            self.conn.commit()
            self.siparisdetaylarilistesiguncelle()
    def uretimemrisil(self):
        satir = self.uretim_emirleri_listesi.currentRow()
        if satir == -1:
            QtWidgets.QMessageBox.warning(self, "HATA", "LÜTFEN SEÇİM YAPINIZ")
            return
        else:
            col = 4
            uretimemriid = self.uretim_emirleri_listesi.item(satir, col).text()
            try:
                self.cursor.execute("start transaction")
                self.cursor.execute("delete from uretimemirleri where uretim_emir_id = '" + uretimemriid + "'")
                QtWidgets.QMessageBox.warning(self, "BİLGİ", "SİLME İŞLEMİ BAŞARI İLE GERÇEKLEŞTİRİLDİ")
            except:
                QtWidgets.QMessageBox.warning(self, "HATA", "BİR HATA İLE KARŞILAŞILDI İŞLEMLER GERİ ALINIYOR")
                self.cursor.execute("rollback")
                return
            self.conn.commit()
            self.uretimemirlerilistesiguncelle()