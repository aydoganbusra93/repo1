#!/usr/bin/env python
# -*- coding:utf -8-*-
import uretimraporlama
from PyQt5 import QtWidgets, QtCore, QtGui
from mysql.connector import (connection)
import pdfkit
import os
import subprocess


class UretimRaporlama(uretimraporlama.Ui_Dialog, QtWidgets.QDialog):
    def __init__(self, parent=None):
        super(UretimRaporlama, self).__init__(parent)
        self.setupUi(self)
        try:
            self.conn = connection.MySQLConnection(user='root', password='13579', host='localhost',
                                                   database='AKMILL')
            self.cursor = self.conn.cursor()
        except:
            QtWidgets.QMessageBox.warning(self, "BAĞLANTI SORUNU",
                                          "VERİTABANINA BAĞLANTI GERÇEKLEŞTİRİLEMEDİ \n BAĞLANTILARINIZI KONTROL EDİNİZ")
            return
        self.firmalarigetir()
        self.firma_adi.currentTextChanged.connect(self.projelerigetir)
        self.projelerigetir()
        self.proje_adi.currentIndexChanged.connect(self.uretimnumaralarigetir)
        self.listele.clicked.connect(self.listeyidoldur)
        self.createpdf.clicked.connect(self.raporpdfolustur)

    def firmalarigetir(self):
        try:
            self.cursor.execute("select  firma_adi from firmalar")
            firmalar = self.cursor.fetchall()
            for i in firmalar:
                self.firma_adi.addItems(i)
        except:
            import traceback
            traceback.print_exc()

    def projelerigetir(self):
        self.proje_adi.clear()
        try:
            self.cursor.execute(
                "select firma_id from firmalar where firma_adi = '" + self.firma_adi.currentText() + "'")
            firmaid = self.cursor.fetchone()[0]
            self.cursor.execute("select proje_adi from projeler where FIRMALAR_firma_id= '" + str(firmaid) + "'")
            projeler = self.cursor.fetchall()
            if len(projeler) == 0:
                QtWidgets.QMessageBox.warning(self, "HATA", "HERHANGİ BİR PROJE KAYDI BULUNMAMAKTADIR")
                return
            else:
                pass
            for i in projeler:
                self.proje_adi.addItems(i)
        except:
            import traceback
            traceback.print_exc()

    def uretimnumaralarigetir(self):
        self.uretim_numarasi.clear()
        try:
            projeadi = self.proje_adi.currentText()

            firmaadi = self.firma_adi.currentText()
            self.cursor.execute("select firma_id from firmalar where firma_adi = '" + firmaadi + "'")
            firmaid = self.cursor.fetchone()[0]
            self.cursor.execute("select proje_id from projeler where proje_adi = '" + projeadi + "'")
            projeid = self.cursor.fetchone()[0]
            self.cursor.execute("select uretim_emir_no from uretimemirleri where FIRMALAR_firma_id = '" + str(
                firmaid) + "' and PROJELER_proje_id = '" + str(projeid) + "'")
            uretimemrinumaralari = self.cursor.fetchall()
            for i in uretimemrinumaralari:
                self.uretim_numarasi.addItems(i)
        except:
            import traceback
            traceback.print_exc()

        pass

    def listeyidoldur(self):
        self.raporlistesi.clearContents()
        try:
            uretimno = self.uretim_numarasi.currentText()
            print (len(uretimno))
            if len(uretimno) == 0:
                QtWidgets.QMessageBox.warning(self, "HATA", "HERHANGİ BİR URETİM EMRİ BULUNMAMAKTADIR BULUNAMAMIŞTIR")
                return
            else:
                pass
            self.cursor.execute("select uretim_emir_id from uretimemirleri where uretim_emir_no = '" + uretimno + "'")
            uretimid = self.cursor.fetchone()[0]
            query = """select t12345.firma_adi as "FİRMA ADI", t12345.proje_adi as "PROJE ADI", t12345.uretim_emir_no as "URETİM EMİR NUMARASI", t12345.akparca AS "AKMILL PARÇA ADI", t12345.muparca AS "MÜŞTERİ PARÇA ADI",
 t12345.adet AS "ADET", t12345.isemri_no AS "İŞ EMRİ NUMARASI", personel.personel_adi AS "OPERATÖR ADI", personel.personel_soyadi AS "OPERATÖR SOYADI",
t12345.ayarbasla AS "AYAR BAŞLANGIÇ SAATİ", t12345.ayarbitis AS "AYAR BİTİŞ SAATİ", t12345.CEVRIMZAMANI AS "ÇEVRİM ZAMANI", t12345.isbitis AS "İŞ BİTİŞ SAATİ",
 t12345.kaliteonay AS "KALİTE ONAYI", makinalar.makina_adi AS "TEZGAH ADI" from
(select t1234.firma_adi, t1234.proje_adi, t1234.uretim_emir_no, t1234.akparca, t1234.muparca, t1234.adet, t1234.isemri_no, isemridurumu.personel_personel_id, isemridurumu.ayarbasla, isemridurumu.ayarbitis,
isemridurumu.CEVRIMZAMANI, isemridurumu.isbitis, isemridurumu.kaliteonay, isemridurumu.makinalar_makina_id from 
(select t123.firma_adi, t123.proje_adi, t123.uretim_emir_no, t123.akparca, t123.muparca, t123.adet, isemirleri.isemri_id, isemirleri.isemri_no from
(select t12.firma_adi, t12.proje_adi, t12.uretim_emir_no, uretimdetaylandirma.uretim_detaylandirma_id, akmillteknikresimleri.parca_adi as akparca,
musteriteknikresimleri.parca_adi as muparca, uretimdetaylandirma.adet from
(select uretimemirleri.uretim_emir_id, firmalar.firma_adi, projeler.proje_adi, uretimemirleri.uretim_emir_no from uretimemirleri 
left join firmalar on firmalar.firma_id = uretimemirleri.FIRMALAR_firma_id
left join projeler on projeler.proje_id = uretimemirleri.PROJELER_proje_id where uretim_emir_id= {0}) as t12 
left join uretimdetaylandirma on t12.uretim_emir_id = uretimdetaylandirma.URETIMEMIRLERI_uretim_emir_id
left join musteriteknikresimleri on uretimdetaylandirma.musteriteknikresimleri_musteri_teknikresim_id = musteriteknikresimleri.musteri_teknikresim_id
left join akmillteknikresimleri on uretimdetaylandirma.akmillteknikresimleri_akmill_teknikresim_id = akmillteknikresimleri.akmill_teknikresim_id) as t123
left join isemirleri on t123.uretim_detaylandirma_id = isemirleri.uretimdetaylandirma_uretim_detaylandirma_id) as t1234
left join isemridurumu on isemridurumu.isemirleri_isemri_id = t1234.isemri_id) as t12345
left join makinalar on makinalar.makina_id = t12345.makinalar_makina_id
left join personel on personel.personel_id = t12345.personel_personel_id"""
            query1 = query.format("'" + str(uretimid) + "'")
            self.cursor.execute(query1)
            datalar = self.cursor.fetchall()
            self.pdficerikleri = datalar
            print datalar
            self.raporlistesi.setRowCount(len(datalar))
            for satir, data in enumerate(datalar):
                self.raporlistesi.setItem(satir, 0, QtWidgets.QTableWidgetItem(str(data[0])))
                self.raporlistesi.setItem(satir, 1, QtWidgets.QTableWidgetItem(data[1]))
                self.raporlistesi.setItem(satir, 2, QtWidgets.QTableWidgetItem(str(data[2])))
                self.raporlistesi.setItem(satir, 3, QtWidgets.QTableWidgetItem(str(data[3])))
                self.raporlistesi.setItem(satir, 4, QtWidgets.QTableWidgetItem(str(data[4])))
                self.raporlistesi.setItem(satir, 5, QtWidgets.QTableWidgetItem(str(data[5])))
                self.raporlistesi.setItem(satir, 6, QtWidgets.QTableWidgetItem(data[6]))
                self.raporlistesi.setItem(satir, 7, QtWidgets.QTableWidgetItem(data[7]))
                self.raporlistesi.setItem(satir, 8, QtWidgets.QTableWidgetItem(data[8]))
                self.raporlistesi.setItem(satir, 9, QtWidgets.QTableWidgetItem(str(data[9])))
                self.raporlistesi.setItem(satir, 10, QtWidgets.QTableWidgetItem(str(data[10])))
                self.raporlistesi.setItem(satir, 11, QtWidgets.QTableWidgetItem(str(data[11])))
                self.raporlistesi.setItem(satir, 12, QtWidgets.QTableWidgetItem(str(data[12])))
                self.raporlistesi.setItem(satir, 13, QtWidgets.QTableWidgetItem(str(data[13])))
                self.raporlistesi.setItem(satir, 14, QtWidgets.QTableWidgetItem(str(data[14])))









        except:
            import traceback
            traceback.print_exc()

    def raporpdfolustur(self):
        taslak = """<html>
                    <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>Data</title>
                    </head>
                    <body>
                    <table border=1>
                    <tr>
                    <th bgcolor=silver class='medium'>Firma Adi</th>
                    <th bgcolor=silver class='medium'>Proje Adi</th>
                    <th bgcolor=silver class='medium'>Uretim Emir Numarasi</th>
                    <th bgcolor=silver class='medium'>Akmill Parca Adi</th>
                    <th bgcolor=silver class='medium'>Musteri Parca Adi</th>
                    <th bgcolor=silver class='medium'>Adet</th>
                    <th bgcolor=silver class='medium'>Is Emri Numarasi</th>
                    <th bgcolor=silver class='medium'>Operator Adi</th>
                    <th bgcolor=silver class='medium'>Operator Soyadi</th>
                    <th bgcolor=silver class='medium'>Ayar Baslangici</th>
                    <th bgcolor=silver class='medium'>Ayar Bitisi</th>
                    <th bgcolor=silver class='medium'>Cevrim Suresi</th>
                    <th bgcolor=silver class='medium'>Is Bitisi</th>
                    <th bgcolor=silver class='medium'>Kalite Onayi</th>
                    <th bgcolor=silver class='medium'>Tezgah Adi</th>
                    </tr>
                    """
        uretimno = self.uretim_numarasi.currentText()
        path = "//DESKTOP-QC87HVJ/Users/PC1/Desktop/AKMILL/MRP/Raporlar/UretimRaporlari/Rapor-" + uretimno + '.html'
        if os.path.exists(
                "//DESKTOP-QC87HVJ/Users/PC1/Desktop/AKMILL/MRP/Raporlar/UretimRaporlari/Rapor-" + uretimno + '.html'):
            import subprocess
            subprocess.Popen(path + ".pdf", shell=True)
            print "dosyamevcut"
            return
        else:
            try:
                file = open(path, 'w')
                file.write(taslak)
                file.close()
                a = 1
                with open(path, 'ab') as f:
                    for i in self.pdficerikleri:
                        f.write(
                            "\n<tr>"
                            "\n<td><font size= '3px'> " + str(i[0]) + " </font></td>"
                                                                      "\n<td style='white-space:pre-wrap; wprd-wrap:break-word'><font size= '3px'>" +
                            i[1].encode("utf-8") + "</font> </td>"
                                                   "\n<td><font size= '3px'>" + str(i[2]) + " </font></td>"
                                                                                            "\n<td><font size= '3px'>" + str(
                                i[3]) + " </font></td>"
                                        "\n<td><font size= '3px'>" + str(i[4]) + "  </font></td>"
                                                                                 "\n<td><font size= '3px'>" + str(
                                i[5]) + "  </font></td>"
                                        "\n<td><font size= '3px'>" + str(i[6]) + " </font></td>"
                                                                                 "\n<td><font size= '3px'>" + str(
                                i[7]) + "</font> </td>"
                                        "\n<td><font size= '3px'>" + str(i[8]) + " </font></td>"
                                                                                 "\n<td><font size= '3px'>" + str(
                                i[9]) + " </font></td>"
                                        "\n<td><font size= '3px'>" + str(i[10]) + " </font></td>"
                                                                                  "\n<td><font size= '3px'>" + str(
                                i[11]) + " </font></td>"
                                         "\n<td><font size= '3px'>" + str(i[12]) + " </font></td>"
                                                                                   "\n<td><font size= '3px'>" + str(
                                i[13]) + " </font></td>"
                                         "\n<td><font size= '3px'>" + str(i[14]) + " </font></td>"

                                                                                   "\n</tr>")
                        a = a + 1
                    f.write("</table> </body></html>")
                    f.close()
                    config = pdfkit.configuration(
                        wkhtmltopdf="C:\\Program Files\\wkhtmltopdf\\bin\\wkhtmltopdf.exe")
                    optionss = {'page-size': 'A4',
                                'orientation': 'Landscape',
                                'margin-top': '5mm',
                                'margin-left': '15mm',
                                'margin-right': '5mm',
                                'margin-bottom': '5mm'}
                    pdfkit.from_file(path, path + '.pdf',
                                     options=optionss, configuration=config)
                import subprocess
                subprocess.Popen(path + ".pdf", shell=True)


            except:
                import traceback
                traceback.print_exc()

        pass