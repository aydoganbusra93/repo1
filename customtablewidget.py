#!/usr/bin/env python
# -*- coding:utf -8-*-

from PyQt5 import QtWidgets, QtCore, QtGui


class TableWidget(QtWidgets.QTableWidget):
    def __init__(self, parent=None):
        super(TableWidget, self).__init__(parent)
        self.dragDropMode()
    def dragMoveEvent(self, e):
