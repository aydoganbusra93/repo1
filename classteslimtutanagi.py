#!/usr/bin/env python
# -*- coding:utf -8-*-

import jpgtoexcelxlsxwriter
from mysql.connector import (connection)
from PyQt5 import QtWidgets
from PyQt5.QtGui import QPixmap
import teslimtutanagi
import classwarnings

class TeslimatTuanagi(teslimtutanagi.Ui_Dialog, QtWidgets.QDialog):
    def __init__(self, parent=None):
        super(TeslimatTuanagi, self).__init__(parent)
        self.setupUi(self)
        try:
            self.conn = connection.MySQLConnection(user='root', password='13579', host='localhost',
                                                   database='AKMILL')
            self.cursor = self.conn.cursor()
        except:
            self.err = classwarnings.Warnings()
            msg= self.err.databaseconnectionerror()
            QtWidgets.QMessageBox.warning(self,msg[0], msg[1])
            return
        self.active=True
        self.cursor.execute("select firma_adi from firmalar")
        firmalistesi = self.cursor.fetchall()
        for i in firmalistesi:
            self.firmalar.addItems(i)
        self.firmalar.currentIndexChanged.connect(self.projelerigetir)
        self.uretimemirlerinilistele.clicked.connect(self.uretimnumaralarinigetir)
        self.listele.clicked.connect(self.urunlistesidoldur)
        self.ekle.clicked.connect(self.checkedboxeslist)
        self.hepsinisec.clicked.connect(self.selectallcheckboxes)
        self.hepsinikaldir.clicked.connect(self.deselectallcheckboxes)
        self.olustur.clicked.connect(self.dokumanolustur)
        self.urunlistesi.currentItemChanged.connect(self.preview)
        self.projelerigetir()
        # self.urunlistesi.currentCellChanged.connect(self.preview)
        self.hazirlanacaklar.currentItemChanged.connect(self.previewhazirlanacak)
        # self.hazirlanacaklar.currentCellChanged.connect(self.previewhazirlanacak)

    def preview(self):
        pass
        try:
            uretimnumarasi = self.urunlistesi.item(0, 1).text()
            dosyayolu = "C:/Users/PC1/Desktop/AKMILL/MRP/URETIM/" + uretimnumarasi + "/Dokumantasyon"
            satir= self.urunlistesi.currentRow()
            if satir != -1 :
                akparcaadi = self.urunlistesi.item(satir,9).text()
                onizleme = dosyayolu + "/"+ akparcaadi +".bmp"
                pixmap = QPixmap(onizleme)
                # pixmap= pixmap.scaledToHeight(600)
                # pixmap= pixmap.scaledToWidth(400)
                self.onizleme.setScaledContents(True)
                self.onizleme.setPixmap(pixmap)
                self.onizleme.setFixedSize(600, 300)
                pass
        except:
            import traceback
            traceback.print_exc()

    def previewhazirlanacak(self):
        pass
        try:
            uretimnumarasi = self.urunlistesi.item(0, 1).text()
            dosyayolu = "C:/Users/PC1/Desktop/AKMILL/MRP/URETIM/" + uretimnumarasi + "/Dokumantasyon"
            satir= self.hazirlanacaklar.currentRow()
            if satir != -1 :
                akresimid = self.hazirlanacaklar.item(satir,3).text()
                self.cursor.execute("select parca_adi from akmillteknikresimleri where akmill_teknikresim_id = '"+akresimid+"'")
                akparcaadi = self.cursor.fetchone()[0]
                onizleme = dosyayolu + "/"+ akparcaadi +".bmp"
                print onizleme
                print akresimid
                pixmap = QPixmap(onizleme)
                # pixmap= pixmap.scaledToHeight(600)
                # pixmap= pixmap.scaledToWidth(400)
                self.onizleme.setScaledContents(True)
                self.onizleme.setPixmap(pixmap)
                self.onizleme.setFixedSize(600, 300)
                pass
        except:
            import traceback
            traceback.print_exc()




    def closeEvent(self, QCloseEvent):
        self.active = False

    def projelerigetir(self):
        self.projeler.clear()
        self.urunlistesi.setRowCount(0)
        self.hazirlanacaklar.setRowCount(0)
        try:
            firmaadi = self.firmalar.currentText()
            self.cursor.execute("select firma_id from firmalar where firma_adi = '"+firmaadi+"'")
            firmaid = self.cursor.fetchone()[0]
            self.cursor.execute("select proje_adi from projeler where FIRMALAR_firma_id = '"+str(firmaid)+"'")
            projelistesi = self.cursor.fetchall()
            for i in projelistesi :

                self.projeler.addItems(i)
        except:
            traceback.print_exc()
    def uretimnumaralarinigetir(self):
        self.uretimnumaralari.clear()

        projeadi = self.projeler.currentText()

        if projeadi == None or len(projeadi) == 0:
            QtWidgets.QMessageBox.warning(self, "HATA", "PROJE ADI SEÇMENİZ GEREKMEKTEDİR")
            return
        else:
            pass
        self.cursor.execute("select proje_id, FIRMALAR_firma_id from projeler where proje_adi = '"+projeadi+"'")
        data = self.cursor.fetchall()
        projeid = data[0][0]
        firmaid = data[0][1]
        self.cursor.execute("select uretim_emir_no from uretimemirleri where PROJELER_proje_id = '"+str(projeid)+"' and FIRMALAR_firma_id = '"+str(firmaid)+"'" )
        uretimnumaralari = self.cursor.fetchall()
        for i in uretimnumaralari :
            self.uretimnumaralari.addItems(i)
    def urunlistesidoldur(self):

        # numberofrows= self.urunlistesi.rowCount()
        # print ("satir sayisi : "+ str(numberofrows))
        # try:
        # for i in range (0,numberofrows+1):
        #     self.urunlistesi.removeRow(i)
        self.urunlistesi.setRowCount(0)
        uretimno = self.uretimnumaralari.currentText()
        if uretimno == None or len(uretimno)== 0 :
            QtWidgets.QMessageBox.warning(self, "HATA","URETİM NUMARASI SEÇMENİZ GEREKMEKTEDİR")
            return
        else :
            try:
                self.cursor.execute("select uretim_emir_id from uretimemirleri where uretim_emir_no = '"+uretimno+"'")
                uretimid = self.cursor.fetchone()[0]
                self.cursor.execute("select * from uretimdetaylandirma where URETIMEMIRLERI_uretim_emir_id = '"+str(uretimid)+"'")
                datalar  = self.cursor.fetchall()
                self.urunlistesi.setRowCount(len(datalar))
                self.cblist = []
                for i in range(0,len(datalar)):
                    self.cblist.append("cb" +str(i))
                for satir, data in enumerate(datalar):
                    self.cblist[satir]  = QtWidgets.QCheckBox(parent=self.urunlistesi)
                    self.urunlistesi.setCellWidget(satir,0,self.cblist[satir])

                    self.urunlistesi.setItem(satir, 1, QtWidgets.QTableWidgetItem(str(uretimno)))
                    self.urunlistesi.setItem(satir, 2, QtWidgets.QTableWidgetItem(str(data[2])))
                    self.urunlistesi.setItem(satir, 3, QtWidgets.QTableWidgetItem(str(data[3])))
                    self.cursor.execute("select parca_adi from akmillteknikresimleri where akmill_teknikresim_id = '"+str(data[3])+"'")
                    akparcaadi = self.cursor.fetchone()[0]
                    self.urunlistesi.setItem(satir, 9, QtWidgets.QTableWidgetItem(str(akparcaadi)))
                    self.urunlistesi.setItem(satir, 4, QtWidgets.QTableWidgetItem(str(data[4])))
                    print ("asdasdasdasdasdasdasdasdasdasdasdasd")
                    print data[2]
                    self.cursor.execute("select miktar from siparisdetaylari where siparis_detay_id= '"+str(data[2])+"'")
                    print ("asdasdasdasdasdasdasdasdasdasdasdasd")
                    miktar = self.cursor.fetchone()[0]
                    toplamadet = miktar * data[9]
                    self.urunlistesi.setItem(satir, 5, QtWidgets.QTableWidgetItem(str(toplamadet)))
                    self.urunlistesi.setItem(satir, 7, QtWidgets.QTableWidgetItem(str(data[0])))
            except:
                traceback.print_exc()
    def checkedboxeslist(self):
        try:
            secililer = []
            uretimdetayidlistesi = []

            satirsayisi = self.urunlistesi.rowCount()
        except:
            traceback.print_exc()
        if satirsayisi == None or satirsayisi == 0 :
            QtWidgets.QMessageBox.warning(self, "HATA", "HERHANGİ BİR ÜRETİMDETAYI BULUNAMAMIŞTIR")
        else :
            for i in range (0,satirsayisi) :
                if self.cblist[i].isChecked() :
                    secililer.append(i)


        adet = len(secililer)
        mevcutsatirsayisi = self.hazirlanacaklar.rowCount()

        try:

            if mevcutsatirsayisi > 0:
                for i in secililer:
                    for a in range(0, mevcutsatirsayisi):
                        if self.hazirlanacaklar.item(a, 7).text() == self.urunlistesi.item(i, 7).text():
                            QtWidgets.QMessageBox.warning(self, "HATA",
                                                          "SEÇMİŞ OLDUĞUNUZ ÜRÜN ZATEN LİSTEYE EKLENMİŞ BULUNMAKTADIR LÜTFEN KONTROL EDİNİZ")
                            return
                        else:
                            pass
            else:
                pass
            self.hazirlanacaklar.setRowCount(mevcutsatirsayisi + adet)
            for satir , i in enumerate(secililer):

                    self.hazirlanacaklar.setItem((mevcutsatirsayisi+satir),1,QtWidgets.QTableWidgetItem(self.urunlistesi.item(i,1).text()))
                    self.hazirlanacaklar.setItem((mevcutsatirsayisi+satir),2,QtWidgets.QTableWidgetItem(self.urunlistesi.item(i,2).text()))
                    self.hazirlanacaklar.setItem((mevcutsatirsayisi+satir),3,QtWidgets.QTableWidgetItem(self.urunlistesi.item(i,3).text()))

                    print "select parca_adi from akmillteknikresimleri where akmill_teknikresim_id = '" + self.urunlistesi.item(i, 3).text() + "'"
                    self.cursor.execute("select parca_adi from akmillteknikresimleri where akmill_teknikresim_id = '"+self.urunlistesi.item(i,3).text()+"'")

                    parcaadi = self.cursor.fetchone()[0]

                    self.hazirlanacaklar.setItem((mevcutsatirsayisi+satir),4,QtWidgets.QTableWidgetItem(self.urunlistesi.item(i,4).text()))
                    self.hazirlanacaklar.setItem((mevcutsatirsayisi+satir),5,QtWidgets.QTableWidgetItem(self.urunlistesi.item(i,5).text()))
                    self.hazirlanacaklar.setItem((mevcutsatirsayisi+satir),7,QtWidgets.QTableWidgetItem(self.urunlistesi.item(i,7).text()))
                    self.hazirlanacaklar.setItem((mevcutsatirsayisi+satir),9,QtWidgets.QTableWidgetItem(parcaadi))

                    uretimdetayidlistesi.append(self.urunlistesi.item(i, 7).text())
            self.hazirlanacaklar.setColumnHidden(0, True)
            self.hazirlanacaklar.setColumnHidden(2, True)
            self.hazirlanacaklar.setColumnHidden(3, True)
            self.hazirlanacaklar.setColumnHidden(4, True)
            self.hazirlanacaklar.setColumnHidden(6, True)
            self.hazirlanacaklar.setColumnHidden(7, True)
            self.hazirlanacaklar.setColumnHidden(8, True)




            # for i in secililer:
            #     print ("secililerin listesi:" + str(secililer))
            try:
                yenisatirsayisi = self.urunlistesi.rowCount()
                for a in range (0,yenisatirsayisi):
                    if self.urunlistesi.item(a,7).text() in uretimdetayidlistesi :
                        self.cblist[a].setChecked(False)
                        self.urunlistesi.hideRow(a)
            except:
                traceback.print_exc()
        except:
            traceback.print_exc()
    def selectallcheckboxes(self):
        satirsayisi = self.urunlistesi.rowCount()
        if satirsayisi == None or satirsayisi == 0 :
            QtWidgets.QMessageBox.warning(self, "HATA", "HERHANGİ BİR ÜRETİMDETAYI BULUNAMAMIŞTIR")
        else :
            for i in range (0,satirsayisi) :
                if self.urunlistesi.isRowHidden(i):
                    pass
                else:
                    self.cblist[i].setChecked(True)
    def deselectallcheckboxes(self):
        satirsayisi = self.urunlistesi.rowCount()
        if satirsayisi == None or satirsayisi == 0 :
            QtWidgets.QMessageBox.warning(self, "HATA", "HERHANGİ BİR ÜRETİMDETAYI BULUNAMAMIŞTIR")
        else :
            for i in range (0,satirsayisi) :
                self.cblist[i].setChecked(False)
    def dokumanolustur(self):
        try:
            dosya = jpgtoexcelxlsxwriter.TeslimatTuanagi()
            uretimnumarasi = self.hazirlanacaklar.item(0,1).text()
            projeadi = self.projeler.currentText()
            onizlemedosyayolu = "C:/Users/PC1/Desktop/AKMILL/MRP/URETIM/"+uretimnumarasi+"/Dokumantasyon"
            dosya.dosyaolustur(onizlemedosyayolu+"/"+uretimnumarasi+ "-Teslimat-Tutanagi.xlsx", uretimnumarasi,projeadi)
            adet = self.hazirlanacaklar.rowCount()
            if adet == None or adet == 0 :
                QtWidgets.QMessageBox.warning(self, "HATA", "HERHANGİ BİR PARÇA LİSTESİ BULUNAMAMISTIR LUTFEN KONTROL EDİNİZ")
            else :
                for i in range(0, adet) :
                    akresimid = self.hazirlanacaklar.item(i,3).text()
                    self.cursor.execute("select parca_adi from akmillteknikresimleri where akmill_teknikresim_id = '"+akresimid+"'")
                    parcaadi = self.cursor.fetchone()[0]
                    parcaadedi = self.hazirlanacaklar.item(i,5).text()
                    onizlemedosyayolu1 = onizlemedosyayolu + "/" + parcaadi+ ".bmp"
                    dosya.teslimat_tutanagi_olustur(i,parcaadi,onizlemedosyayolu1,parcaadedi)
                dosya.dosyakapat()
        except:
            traceback.print_exc()


        pass

    #todo burada kaldin malzeme teslim tutanagi hazirlama kısmı




