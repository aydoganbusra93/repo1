#!/usr/bin/env python
# -*- coding:utf -8-*-

from PyQt5 import QtWidgets, QtCore, QtGui
from mysql.connector import (connection)
import yenifirma

class Yenifirma(QtWidgets.QDialog, yenifirma.Ui_Dialog):  # Yeni firma ekranını oluşturan class
    def __init__(self, parent=None):
    # def __init__(self, conn, parent=None):
        super(Yenifirma, self).__init__(parent)
        self.setupUi(self)
        self.buttonBox.accepted.connect(self.firmaekle)
        # self.conn = conn

        self.conn =connection.MySQLConnection(user='root', password='13579', host='localhost', database='AKMILL')
        self.cursor = self.conn.cursor()
        self.firma_listesi_guncelle()
        self.firma_sil.clicked.connect(self.firmasil)
    def firma_listesi_guncelle(self):
        self.cursor.execute("select * from firmalar")
        firmalar = self.cursor.fetchall()
        self.firma_listesi.setRowCount(len(firmalar))

        for satir, data in enumerate(firmalar):
            self.firma_listesi.setItem(satir, 0, QtWidgets.QTableWidgetItem(data[1]))
            self.firma_listesi.setItem(satir, 1, QtWidgets.QTableWidgetItem(data[4]))
            self.firma_listesi.setItem(satir, 2, QtWidgets.QTableWidgetItem(data[5]))
            self.firma_listesi.setItem(satir, 3, QtWidgets.QTableWidgetItem(data[3]))
            self.firma_listesi.setItem(satir, 4, QtWidgets.QTableWidgetItem(data[6]))
            self.firma_listesi.setItem(satir, 5, QtWidgets.QTableWidgetItem(str(data[7])))
            self.firma_listesi.setItem(satir, 6, QtWidgets.QTableWidgetItem(str(data[0])))

        self.firma_listesi.setColumnHidden(6, True)


        # QtWidgets.QMessageBox.information(self, "BAŞARILI", "TABLO GÜNCELLENDİ ")
        pass
    def firmaekle(self):
        # self.conn =connection.MySQLConnection(user='root', password='13579', host='192.168.0.11', database='AKMILL')
        # self.cursor = self.conn.cursor()
        self.cursor.execute("select count(*) from firmalar")
        mevcutmu = self.cursor.fetchone()[0]
        if mevcutmu == 0 :
            firmanumarasi = 0
        else :
            self.cursor.execute("select firma_numaralandirma from firmalar order by firma_id desc")
            firmanumarasi= self.cursor.fetchall()[0][0]
        firmaadi = self.lineEdit.text()
        mail1 = self.lineEdit_2.text()
        mail2 = self.lineEdit_3.text()
        telno = self.lineEdit_4.text()
        vergino = self.lineEdit_5.text()
        adres = self.firma_adresi.text()

        try:
            self.cursor.execute("select count(*) from firmalar where firma_adi = '"+firmaadi+"'")
            existance = self.cursor.fetchone()[0]
            print existance
            if existance != 0 :
                QtWidgets.QMessageBox.warning(self, "HATA" , "DAHA ÖNCE BU FİRMA ADI İLE KAYIT OLUŞTURULMUŞTUR LÜTFEN KONTROL EDİNİZ")
                return
            else :
                self.cursor.execute(
                    "INSERT INTO firmalar (firma_adi ,firma_adres, firma_tel, firma_mail1, firma_mail2, firma_vergi_no,firma_numaralandirma ) values (%s, %s , %s, %s, %s, %s, %s)",
                    (firmaadi, adres, telno, mail1, mail2, vergino,firmanumarasi+1))
                self.conn.commit()

            # query = "INSERT INTO firmalar (firma_adi ,firma_adres, firma_tel, firma_mail1, firma_mail2, firma_vergi_no) values (%s, %s , %s, %s, %s, %s)"
            # params = (firmaadi, adres, telno, mail1, mail2, vergino,)
            # self.conn.ekle(query, params)

            QtWidgets.QMessageBox.information(self, "BAŞARILI" , "FİRMA BAŞARIYLA EKLENDİ")
            self.firma_listesi_guncelle()
        except :
            QtWidgets.QMessageBox.Warning(self , "HATA" , "FİRMA EKLENEMEDİ")
            return
    def firmasil(self):
        satir = self.firma_listesi.currentRow()
        if satir == -1:
            QtWidgets.QMessageBox.warning(self, "HATA", "LÜTFEN SEÇİM YAPINIZ")
            return
        else:
            col = 6
            firmaid = self.firma_listesi.item(satir, col).text()
            try:
                self.cursor.execute("delete from firmalar where firma_id = '" + firmaid + "'")
                self.conn.commit()
                self.firma_listesi_guncelle()
                QtWidgets.QMessageBox.warning(self, "BİLGİ", "FİRMA SİLME İŞLEMİ BAŞARI İLE GERÇEKLEŞTİRİLDİ")
            except:
                QtWidgets.QMessageBox.warning(self, "HATA", "SİLME İŞLEMİ TAMAMLANAMADI LÜTFEN İŞLEMLERİNİ KONTROL EDİNİZ")