#!/usr/bin/env python
# -*- coding:utf -8-*-

import pymysql
from PyQt5 import QtWidgets
import operatorekrani
import win10toast
import threading
import datetime
from PyQt5.QtGui import QPixmap
import winsound
import classkartokuma
import subprocess
import os
import yazici

class Operator(operatorekrani.Ui_Dialog, QtWidgets.QDialog):
    def __init__(self, parent=None):
        super(Operator, self).__init__(parent)
        self.setupUi(self)
        self.takimlistesi.clicked.connect(self.takimlistesibastir)
        try:

            self.conn = pymysql.connect(user='root', password='13579', host='localhost', database='AKMILL')
            self.cursor = self.conn.cursor()
        except:
            QtWidgets.QMessageBox.warning(self, "BAĞLANTI SORUNU",
                                          "VERİTABANINA BAĞLANTI GERÇEKLEŞTİRİLEMEDİ \n BAĞLANTILARINIZI KONTROL EDİNİZ")
            return
        try:
            self.cursor.execute(
                "select personel_adi, personel_soyadi from personel where aktif_pasif = 1 and gorev = 'CNC OPERATÖRÜ' or gorev = 'MONTAJ ELEMANI'")
            adsoyad = self.cursor.fetchall()
            for kisi in adsoyad:
                self.personel.addItem(kisi[0] + " " + kisi[1])
            self.cursor.execute("select makina_adi from makinalar")
            tezgahlar = self.cursor.fetchall()
            for i in tezgahlar:
                self.makinalistesi.addItems(i)
            self.personelonizlemegetir()
            self.personel.currentIndexChanged.connect(self.personelonizlemegetir)
            self.isemirlerilistesiguncelle()
            self.isemirlerilistesi_2.currentCellChanged.connect(self.operatoruretimdetaylarilistesiguncelle)
            self.uretimdetaylari_2.currentCellChanged.connect(self.operatortakibipreview)
            self.uretimdetaylari_2.doubleClicked.connect(self.operatorresimac)
            self.isdurumukart = classkartokuma.KartOkuma(self)
            self.isdurumukaydet.clicked.connect(self.isdurumukartokumabaslat)
            self.isdurumukart.kartsinyali.connect(self.kartoku)

        except:
            import traceback
            traceback.print_exc()
        self.cursor.execute("select count(*) from isemirleri")
        self.sayiiki = self.cursor.fetchone()[0]
        self.listeyenile.clicked.connect(self.isemirlerilistesiguncelle)
        self.bildirim = win10toast.ToastNotifier()
        self.dongu = threading.Timer(30.0, self.refresh)
        self.refresh()
        pixmap = QPixmap("C:\Users\PC1\Desktop\AKMILL\MRP\Makineler\dm1.jpg")
        pixmap = pixmap.scaledToWidth(350)
        pixmap = pixmap.scaledToHeight(200)
        # self.makinapreview.setPixmap(pixmap)
        # self.makinapreview.setScaledContents(True)
        # self.makinapreview.setFixedSize(287, 260)

    def refresh(self):
        try:
            print('startlandi')
            self.dongu.start()
        except:
            import traceback
            traceback.print_exc()
        try:
            self.conn.close()
            self.conn.connect()
            self.cursor.execute("select count(*) from isemirleri")
            sayi = self.cursor.fetchone()[0]
            if self.sayiiki != sayi:
                self.bildirim.show_toast(title="IS EMRI", msg="YENI IS EMRI OLUSTURULDU", threaded=True)
                winsound.Beep(1000, 2000)
            else:
                pass
            self.sayiiki = sayi
            self.isemirlerilistesiguncelle()
            winsound.Beep(1000, 500)


        except:
            import traceback
            traceback.print_exc()

    def operatorresimac(self):
        satir = self.uretimdetaylari_2.currentRow()
        sutun = self.uretimdetaylari_2.currentColumn()
        if satir == -1:
            QtWidgets.QMessageBox.warning(self, "HATA", "SEÇİM YAPMANIZ GEREKMEKTEDİR")
        else:
            try:
                if sutun == 0:

                    akdosyayolu = self.uretimdetaylari_2.item(satir, 6).text()
                    if len(akdosyayolu) == 0:
                        QtWidgets.QMessageBox.warning(self, "HATA",
                                                      "AKMILL TARAFINDAN HAZIRLANMIŞ HERHANGİ BİR TEKNİK RESİM BULUNMAMAKTADIR")
                        return
                    else:
                        try:
                            subprocess.Popen(akdosyayolu, shell=True)
                        except:
                            import traceback
                            traceback.print_exc()
                elif sutun == 1:
                    mudosyayolu = self.uretimdetaylari_2.item(satir, 8).text()
                    if len(mudosyayolu) == 0:
                        QtWidgets.QMessageBox.warning(self, "HATA",
                                                      "MÜŞTERİ TARAFINDAN TANIMLANMIŞ HERHANGİ BİR TEKNİK RESİM BULUNMAMAKTADIR")
                        return
                    else:
                        try:
                            subprocess.Popen(mudosyayolu, shell=True)
                        except:
                            import traceback
                            traceback.print_exc()
                else:
                    return
            except:
                QtWidgets.QMessageBox.warning(self, "HATA", "BEKLENMEDİK BİR HATA İLE KARŞILAŞILDI")

    def takimlistesibastir(self):
        satir = self.isemirlerilistesi_2.currentRow()
        if satir == -1:
            return
        else:
            uretimdetayid = self.isemirlerilistesi_2.item(satir, 3).text()
            try:
                self.cursor.execute(
                    "select olcux, olcuy,olcuz, malzemetipi from malzemetalepleri where uretimdetaylandirma_uretim_detaylandirma_id= '" + str(
                        uretimdetayid) + "'")
                olculer = self.cursor.fetchone()
                if len(olculer) == 0:
                    x = '-'
                    y = '-'
                    z = '-'
                    malzemetipi = "BELİRTİLMEMİŞ"
                elif len(olculer) == 1:
                    x = '-'
                    y = '-'
                    z = '-'
                    malzemetipi = "BELİRTİLMEMİŞ"

                elif len(olculer) == 2:
                    x = olculer[0][0]
                    y = olculer[0][1]
                    z = '-'
                    malzemetipi = olculer[3]

                elif len(olculer) == 4:
                    x = olculer[0]
                    y = olculer[1]
                    z = olculer[2]
                    malzemetipi = olculer[3]

                elif len(olculer) == 3:
                    x = olculer[0]
                    y = olculer[1]
                    z = olculer[2]
                    malzemetipi = olculer[3]
                self.cursor.execute(
                    "select  count(*) from malzemelistesi join malzemetalepleri on malzemelistesi.malzeme_id = malzemetalepleri.malzemelistesi_malzeme_id  where  uretimdetaylandirma_uretim_detaylandirma_id= '" + str(
                        uretimdetayid) + "'")
                adet = self.cursor.fetchone()[0]
                if adet == 0:
                    malzemecinsi = "BELİRTİLMEMİŞ"
                else:
                    self.cursor.execute(
                        "select  malzemelistesi.malzeme_adi, uretimdetaylandirma_uretim_detaylandirma_id from malzemelistesi join "
                        "malzemetalepleri on malzemelistesi.malzeme_id = malzemetalepleri.malzemelistesi_malzeme_id  where  uretimdetaylandirma_uretim_detaylandirma_id= '" + str(
                            uretimdetayid) + "'")
                    malzemecinsi = self.cursor.fetchone()[0]

            except:
                import traceback
                traceback.print_exc()
        isemrisatir = self.isemirlerilistesi_2.currentRow()
        if isemrisatir == -1:
            QtWidgets.QMessageBox.warning(self, "HATA", "SEÇİM YAPMANIZ GEREKMEKTEDİR")
        else:
            isemriid = self.isemirlerilistesi_2.item(isemrisatir, 0).text()
            isemrino = self.isemirlerilistesi_2.item(isemrisatir, 1).text()
            if os.path.exists("//DESKTOP-QC87HVJ/Users/PC1/Desktop/AKMILL/TakimListeleri/TOOLS-" + isemrino + '.html'):
                path1 = "//DESKTOP-QC87HVJ/Users/PC1/Desktop/AKMILL/TakimListeleri/TOOLS-" + isemrino + '.html'
                self.takimlistesiciktial(path1)
                # QtWidgets.QMessageBox.warning(self, "HATA","TAKIM LİSTESİ DAHA ÖNCE HAZIRLANMIS\n DOSYANIN SİLİNMESİ GEREKMEKTEDİR")

                return
            else:
                yazilacak = """
                    <html lang="en">
                    <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                        <style>          
                            table, th ,td {
                            border : 1px solid black;

                            }             
                    """
                asil = """               
                        </style>
                            <title>Title</title>                 
                                </head>
                                    <body>
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <th rowspan="1" colspan="3" style="white-space:pre-wrap; wprd-wrap:break-word"><font size="3px">IS EMRI</font></th>
                                                    <th rowspan="1" colspan="3" style="white-space:pre-wrap; wprd-wrap:break-word"><font size="3px"> {0}</font> </th>
                                                    <th rowspan="1" colspan="3" style="white-space:pre-wrap; wprd-wrap:break-word"><font size="3px"> {1}</font> </th>
                                                </tr>
                                                <tr>                                             
                                                    <th rowspan="1" colspan="3" style="white-space:pre-wrap; wprd-wrap:break-word"><font size="3px">OLCULER</font></th>
                                                    <th rowspan="1" colspan="2" style="white-space:pre-wrap; wprd-wrap:break-word"><font size="3px"> {2}</font> </th>
                                                    <th rowspan="1" colspan="2" style="white-space:pre-wrap; wprd-wrap:break-word"><font size="3px"> {3}</font> </th>
                                                    <th rowspan="1" colspan="2" style="white-space:pre-wrap; wprd-wrap:break-word"><font size="3px"> {4}</font> </th>
                                                </tr>
                                                <tr>                                             
                                                    <th rowspan="1" colspan="3" style="white-space:pre-wrap; wprd-wrap:break-word"><font size="3px">MALZEME CINSI</font></th>
                                                    <th rowspan="1" colspan="3" style="white-space:pre-wrap; wprd-wrap:break-word"><font size="3px"> {5}</font> </th>
                                                    <th rowspan="1" colspan="3" style="white-space:pre-wrap; wprd-wrap:break-word"><font size="3px"> {6}</font> </th>
                                                </tr>
                                                <tr>
                                                    <th rowspan="1" colspan="1" style="white-space:pre-wrap; wprd-wrap:break-word"><font size="3px">NO</font></th>
                                                    <th rowspan="1" colspan="1" style="white-space:pre-wrap; wprd-wrap:break-word"><font size="3px">TKM</font></th>
                                                    <th rowspan="1" colspan="1" style="white-space:pre-wrap; wprd-wrap:break-word"><font size="3px">CAP</font></th>
                                                    <th rowspan="1" colspan="1" style="white-space:pre-wrap; wprd-wrap:break-word"><font size="3px">RAD</font></th>
                                                    <th rowspan="1" colspan="1" style="white-space:pre-wrap; wprd-wrap:break-word"><font size="3px">BOY</font></th>
                                                    <th rowspan="1" colspan="1" style="white-space:pre-wrap; wprd-wrap:break-word"><font size="3px">ACI</font></th>
                                                    <th rowspan="1" colspan="1" style="white-space:pre-wrap; wprd-wrap:break-word"><font size="3px">KALN</font></th>
                                                    <th rowspan="1" colspan="1" style="white-space:pre-wrap; wprd-wrap:break-word"><font size="3px">MALZM</font></th>
                                                    <th rowspan="1" colspan="1" style="white-space:pre-wrap; wprd-wrap:break-word"><font size="3px">FINSH</font></th>
                                                </tr>          
                """
                try:
                    file = open("//AYAS/Users/AYAS/Desktop/MRP/TakimListeleri/TOOLS-" + isemrino + '.html',
                                'w')

                    mystring = asil.format(isemrino, self.personel.currentText(), x, y, z, malzemecinsi, malzemetipi)
                    file.write(yazilacak + mystring)
                    file.close()
                except:
                    import traceback
                    traceback.print_exc()

                self.cursor.execute(
                    "select takimno,takimtipi,takimcapi,takimradusu,takimboyu,takimaci,takimkalinlik,takimmalzemesi,kabafinish from takimlisteleri where isemirleri_isemri_id = '" + isemriid + "'")
                listi = self.cursor.fetchall()
                a = 1

                with open("//AYAS/Users/AYAS/Desktop/MRP/TakimListeleri/TOOLS-" + isemrino + '.html',
                          'ab') as f:
                    for i in listi:
                        f.write(
                            "\n<tr>"
                            "\n<td><font size= '3px'> T" + str(i[0]) + " </font></td>"
                                                                       "\n<td style='white-space:pre-wrap; wprd-wrap:break-word'><font size= '3px'>" +
                            i[1].encode("utf-8") + "</font> </td>"
                                                   "\n<td><font size= '3px'>" + str(i[2]) + " </font></td>"
                                                                                            "\n<td><font size= '3px'>" + str(
                                i[3]) + " </font></td>"
                                        "\n<td><font size= '3px'>" + str(i[4]) + "  </font></td>"
                                                                                 "\n<td><font size= '3px'>" + str(
                                i[5]) + "  </font></td>"
                                        "\n<td><font size= '3px'>" + str(i[6]) + " </font></td>"
                                                                                 "\n<td><font size= '3px'>" + i[
                                7].encode("utf-8") + "</font> </td>"
                                                     "\n<td><font size= '3px'>" + i[8].encode("utf-8") + " </font></td>"
                                                                                                         "\n</tr>")
                        a = a + 1
            try:
                path1 = "//AYAS/Users/AYAS/Desktop/MRP/TakimListeleri/TOOLS-" + isemrino + '.html'
                self.takimlistesiciktial(path1)
                # config = pdfkit.configuration(wkhtmltopdf="C:\\Program Files\\wkhtmltopdf\\bin\\wkhtmltopdf.exe")
                # optionss = {'page-width': '100mm',
                #             'page-height': '58mm',
                #             'orientation': 'Portrait',
                #             'margin-top': '0mm',
                #             'margin-left': '0mm',
                #             'margin-right': '0mm',
                #             'margin-bottom': '0mm'}
                # pdfkit.from_file("//DESKTOP-QC87HVJ/Users/PC1/Desktop/AKMILL/TakimListeleri/TOOLS-" + isemrino + '.html', "//DESKTOP-QC87HVJ/Users/PC1/Desktop/AKMILL/TakimListeleri/TOOLS-" + isemrino + '.pdf', options=optionss, configuration=config)
                # takimyazdirma = yazici.appFormPrintWebviev()
                # path = "//DESKTOP-QC87HVJ/Users/PC1/Desktop/AKMILL/TakimListeleri/TOOLS-" + isemrino + '.html'
                # takimyazdirma.yazdirma(path)

            except:
                import traceback
                traceback.print_exc()

        pass

    def takimlistesiciktial(self, path1):
        takimyazdirma = yazici.appFormPrintWebviev()
        # path = "//DESKTOP-QC87HVJ/Users/PC1/Desktop/AKMILL/TakimListeleri/TOOLS-" + isemrino + '.html'
        takimyazdirma.yazdirma(path1)

    def isdurumukartokumabaslat(self):
        self.isdurumukart.start()

        # self.baslat.clicked.connect(self.isebasliyorum)
        # try:
        #     self.islema = Dogrulama()
        #     self.islema.start()
        # except :
        #     import traceback
        #
        #     traceback.print_exc()
        #     QtWidgets.QMessageBox.warning(self , "HATA" ,"BAGLANTILARINIZ KONTROL EDINIZ")
        #     return
        # self.islema.sinyal.connect(self.isebasliyorum)
        # self.pushButton.clicked.connect(self.isemrigoster)
        # self.isemirlerilistesigetir()

    def kartoku(self, val1):
        try:
            isemirlerilistesisatir = self.isemirlerilistesi_2.currentRow()
            self.cursor.execute("select personelkartid,personel_id from personel where personel_adi = '" +
                                self.personel.currentText().split()[0] + "' and personel_soyadi = '" +
                                self.personel.currentText().split()[1] + "'")
            datalar = self.cursor.fetchall()
            personelkartno = datalar[0][0]
            personelid = datalar[0][1]

            if isemirlerilistesisatir == -1:
                QtWidgets.QMessageBox.warning(self, "HATA",
                                              "SEÇMİŞ OLDUĞUNUZ BİR İŞ EMRİ BULUNMAMAKTADIR\n LÜTFEN SEÇİM YAPINIZ")
                return
            else:
                isemriid = self.isemirlerilistesi_2.item(isemirlerilistesisatir, 0).text()
                pass
            if val1 == personelkartno:
                QtWidgets.QMessageBox.information(self, "BİLGİ", "DOĞRU KİŞİSİN")
                now = datetime.datetime.now()
                date = now.strftime("%Y-%m-%d")
                timer = now.strftime("%H:%M")
                zaman = date + " " + timer
                try:
                    if self.isdurumu.currentText() == "AYAR":
                        self.cursor.execute(
                            "select count(*) from isemridurumu where isemirleri_isemri_id = '" + isemriid + "'")
                        mevcutmu = self.cursor.fetchone()[0]
                        try:
                            if mevcutmu == 0:
                                self.cursor.execute("start transaction")
                                print (self.makinalistesi.currentText())
                                self.cursor.execute(
                                    "select makina_id from makinalar where makina_adi = '" + self.makinalistesi.currentText() + "'")
                                makinaid = self.cursor.fetchone()[0]
                                print makinaid
                                self.cursor.execute(
                                    "insert into isemridurumu (isemirleri_isemri_id, personel_personel_id, ayarbasla, makinalar_makina_id) values (%s, %s, %s, %s)",
                                    (isemriid, personelid, zaman, makinaid))
                                self.cursor.execute(
                                    "update isemirleri set isdurumu = 'AYARLANIYOR' where isemri_id = '" + isemriid + "'")
                                QtWidgets.QMessageBox.information(self, "BİLGİ",
                                                                  "SEÇMİŞ OLDUĞUNUZ İŞ EMRİ NUMARASI AYAR BAŞLANGICI OLARAK KAYDEDİLMİŞTİR\n KOLAY GELSİN")
                            else:
                                QtWidgets.QMessageBox.warning(self, "HATA",
                                                              "DAHA ÖNCE BU İŞ EMRİ NUMARASI İLE İLGİLİ AYAR İŞLEMİ BAŞLATILMIŞ LÜTFEN KALDIĞINIZ İŞLEME DİKKAT EDEREK GİRİŞ YAPMAYA ÇALIŞINIZ...")
                                return
                        except:
                            QtWidgets.QMessageBox.warning(self, "HATA",
                                                          "BİR HATA İLE KARŞILAŞILDI İŞLEMLER GERİ ALINIYIOR")
                            self.cursor.execute("rollback")
                            import traceback
                            traceback.print_exc()
                        self.conn.commit()

                    elif self.isdurumu.currentText() == "İŞLEME BAŞLANGICI":
                        self.cursor.execute(
                            "select count(*) from isemridurumu where isemirleri_isemri_id = '" + str(isemriid) + "'")
                        ismevcutmu = self.cursor.fetchone()[0]
                        if ismevcutmu == 0:
                            QtWidgets.QMessageBox.warning(self, "HATA",
                                                          "DAHA ÖNCE BU İŞ EMRİ NUMARASI İLE İLGİLİ AYAR BAŞLANGICI KAYDI GERÇEKLEŞTİRİLMEMİŞTİR.\n DOĞRUDAN AYAR BİTİŞİNE GEÇİŞ YAPAMAZSINIZ")
                            return
                        else:
                            self.cursor.execute(
                                "select ayarbitis from isemridurumu where isemirleri_isemri_id = '" + str(
                                    isemriid) + "'")
                            mevcutmu = self.cursor.fetchone()[0]

                            try:
                                if mevcutmu == None:
                                    self.cursor.execute("start transaction")
                                    self.cursor.execute(
                                        "update isemridurumu set ayarbitis = '" + zaman + "' where isemirleri_isemri_id = '" + isemriid + "'")
                                    self.cursor.execute(
                                        "insert into isemirlerikalite (isemirleri_isemri_id, personel_personel_id, fai) values (%s, %s, %s)",
                                        (str(isemriid), str(personelid), "ONAY BEKLENIYOR"))
                                    self.cursor.execute(
                                        "update isemirleri set isdurumu = 'FAI BEKLENIYOR' where isemri_id = '" + isemriid + "'")

                                    QtWidgets.QMessageBox.information(self, "BİLGİ",
                                                                      "SEÇMİŞ OLDUĞUNUZ İŞ EMRİ NUMARASI AYAR BİTİŞİ OLARAK GÜNCELLENMİŞTİR\n KOLAY GELSİN")
                                else:
                                    QtWidgets.QMessageBox.warning(self, "HATA",
                                                                  "DAHA ÖNCE BU İŞ EMRİ NUMARASI İLE İLGİLİ AYAR İŞLEMİ BAŞLATILMIŞ LÜTFEN KALDIĞINIZ İŞLEME DİKKAT EDEREK GİRİŞ YAPMAYA ÇALIŞINIZ...")
                                    return
                            except:
                                QtWidgets.QMessageBox.warning(self,
                                                              "BİR HATA İLE KARŞILAŞILDI İŞLEMLER GERİ ALINIYIOR...")
                                self.cursor.execute("rollback")
                                import traceback
                                traceback.print_exc()
                            self.conn.commit()

                    elif self.isdurumu.currentText() == "ÇEVRİM ZAMANI":
                        self.cursor.execute(
                            "select count(*) from isemridurumu where isemirleri_isemri_id = '" + str(isemriid) + "'")
                        ismevcutmu = self.cursor.fetchone()[0]
                        if ismevcutmu == 0:
                            QtWidgets.QMessageBox.warning(self, "HATA",
                                                          "DAHA ÖNCE BU İŞ EMRİ NUMARASI İLE İLGİLİ AYAR BAŞLANGICI KAYDI GERÇEKLEŞTİRİLMEMİŞTİR.\n DOĞRUDAN ÇEVRİM ZAMANI İLE İLGİLİ GİRİŞ YAPAMAZSINIZ")
                            return
                        else:
                            self.cursor.execute(
                                "select ayarbitis from isemridurumu where isemirleri_isemri_id = '" + str(
                                    isemriid) + "'")
                            ayarbitisimevcutmu = self.cursor.fetchone()[0]
                            self.cursor.execute(
                                "select CEVRIMZAMANI from isemridurumu where isemirleri_isemri_id = '" + str(
                                    isemriid) + "'")
                            cevrimzamanimevcutmu = self.cursor.fetchone()[0]
                            self.cursor.execute(
                                "select fai from isemirlerikalite where isemirleri_isemri_id = '" + str(isemriid) + "'")
                            faidurumu = self.cursor.fetchone()[0]
                            if faidurumu != "ONAYLANDI":
                                QtWidgets.QMessageBox.warning(self, "HATA",
                                                              "YAPILAN İŞE ONAY VERİLMESİ GEREKMEKTEDİR.\n FAI ONAYLANMADIĞI SÜRECE ÇEVRİM ZAMANI GİRİŞİ YAPAMAZSINIZ")
                                return
                            else:
                                pass
                            if ayarbitisimevcutmu != None and cevrimzamanimevcutmu == None:
                                inp = QtWidgets.QInputDialog()
                                inp.setInputMode(QtWidgets.QInputDialog.TextInput)
                                text, ok = inp.getText(self, 'title', 'ÇEVRİM ZAMANINI GİRİNİZ')
                                if ok:
                                    if len(text) == 0:
                                        QtWidgets.QMessageBox.warning(self, "HATA", "ALAN BOŞ BIRAKILAMAAZ")
                                        return
                                    else:
                                        try:
                                            self.cursor.execute("start transaction")
                                            self.cursor.execute("update isemridurumu set CEVRIMZAMANI = '" + str(
                                                text) + "' where isemirleri_isemri_id ='" + str(isemriid) + "'")
                                            QtWidgets.QMessageBox.information(self, "BİLGİLENDİRME",
                                                                              "ÇEVRİM ZAMANI GÜNCELLENDİ \n KOLAY GELSİN....")
                                            self.cursor.execute(
                                                "update isemirleri set isdurumu = 'DEVAM EDİYOR' where isemri_id = '" + isemriid + "'")

                                            # self.cursor.execute("update isemirleri set durum  = '3' where id  = '" + str(isemriid) + "'")
                                        except:
                                            import traceback
                                            traceback.print_exc()
                                            QtWidgets.QMessageBox.warning(self, "HATA",
                                                                          "BEKLENMEDİK BİR HATA İLE KARŞILAŞILDI")
                                            self.cursor.execute("rollback")
                                        self.conn.commit()

                                else:
                                    QtWidgets.QMessageBox.warning(self, "HATA",
                                                                  "DAHA ÖNCE BU İŞ EMRİ NUMARASI İLE İLGİLİ AYAR İŞLEMİ BAŞLATILMIŞ LÜTFEN KALDIĞINIZ İŞLEME DİKKAT EDEREK GİRİŞ YAPMAYA ÇALIŞINIZ...")
                                    return


                    elif self.isdurumu.currentText() == "İŞ BİTİŞİ":
                        self.cursor.execute(
                            "select count(*) from isemridurumu where isemirleri_isemri_id = '" + str(isemriid) + "'")
                        ismevcutmu = self.cursor.fetchone()[0]
                        if ismevcutmu == 0:
                            QtWidgets.QMessageBox.warning(self, "HATA",
                                                          "DAHA ÖNCE BU İŞ EMRİ NUMARASI İLE İLGİLİ AYAR BAŞLANGICI KAYDI GERÇEKLEŞTİRİLMEMİŞTİR.\n DOĞRUDAN AYAR BİTİŞİNE GEÇİŞ YAPAMAZSINIZ")
                            return
                        else:
                            self.cursor.execute(
                                "select ayarbasla, ayarbitis, CEVRIMZAMANI from isemridurumu where isemirleri_isemri_id = '" + str(
                                    isemriid) + "'")
                            datalar = self.cursor.fetchall()
                            ayarbasla = datalar[0][0]
                            ayarbitis = datalar[0][1]
                            cevrimzamani = datalar[0][2]
                            try:
                                if ayarbasla != None and ayarbitis != None and cevrimzamani != None:
                                    self.cursor.execute("start transaction")
                                    self.cursor.execute(
                                        "update isemridurumu set isbitis = '" + zaman + "' where isemirleri_isemri_id = '" + isemriid + "'")
                                    self.cursor.execute(
                                        "update isemirleri set isdurumu = 'TAMAMLANDI' where isemri_id = '" + isemriid + "'")
                                    QtWidgets.QMessageBox.information(self, "BİLGİ",
                                                                      "SEÇMİŞ OLDUĞUNUZ İŞEMRİ TAMAMLANDI OLARAK GÜNCELLENMİŞTİR\n KOLAY GELSİN")
                                else:
                                    QtWidgets.QMessageBox.warning(self, "HATA",
                                                                  "DAHA ÖNCE BU İŞ EMRİ NUMARASI EKSİK KAYIT BULUNMAKTADIR LÜTFEN KALDIĞINIZ İŞLEME DİKKAT EDEREK GİRİŞ YAPMAYA ÇALIŞINIZ...")
                                    return
                            except:
                                QtWidgets.QMessageBox.warning(self, "HATA",
                                                              "BEKLENMEDİK BİR HATA İLE KARŞILAIŞDI İŞLEMLER GERİ ALINYIOR")
                                self.cursor.execute("rollback")
                                import traceback
                                traceback.print_exc()
                            self.conn.commit()
                    else:
                        pass
                except:
                    import traceback
                    traceback.print_exc()
            else:
                QtWidgets.QMessageBox.warning(self, "HATA", "BAŞKASI ADINA İŞ YAPMAMALISIN")
                return
        except:
            import traceback
            traceback.print_exc()
            return

    def personelgirisinikisitla(self):
        try:
            satir = self.isemirlerilistesi_2.currentRow()
            if satir == -1:
                return
            else:
                try:
                    isemriid = self.isemirlerilistesi_2.item(satir, 0).text()
                    self.cursor.execute(
                        "select ayarbasla, ayarbitis, CEVRIMZAMANI, isbitis, personel_personel_id from isemridurumu where isemirleri_isemri_id = '" + isemriid + "'")
                    datalar = self.cursor.fetchall()
                    print datalar
                    if len(datalar) == 0:
                        self.isdurumu.setCurrentIndex(0)
                        self.isdurumu.setEnabled(False)
                        self.personel.setEnabled(True)

                        return
                except:
                    import traceback
                    traceback.print_exc()

                else:
                    personelid = str(datalar[0][4])
                    ayarbasi = datalar[0][0]
                    ayarbitisi = datalar[0][1]
                    cevrim = datalar[0][2]
                    isbitisi = datalar[0][3]

                self.cursor.execute(
                    "select personel_adi, personel_soyadi from personel where personel_id = '" + personelid + "'")
                personelbilgisi = self.cursor.fetchall()
                ad = personelbilgisi[0][0]
                soyad = personelbilgisi[0][1]
                self.personel.setCurrentText(ad + " " + soyad)
                if ayarbitisi == None and cevrim == None and isbitisi == None:
                    self.isdurumu.setCurrentIndex(1)
                    self.isdurumu.setEnabled(False)
                    self.cursor.execute(
                        "select personel_adi, personel_soyadi from personel where personel_id = '" + personelid + "'")
                    personelbilgisi = self.cursor.fetchall()
                    ad = personelbilgisi[0][0]
                    soyad = personelbilgisi[0][1]
                    self.personel.setCurrentText(ad + " " + soyad)
                    self.personel.setEnabled(False)

                elif cevrim == None and isbitisi == None:
                    self.isdurumu.setCurrentIndex(2)
                    self.isdurumu.setEnabled(False)
                    self.cursor.execute(
                        "select personel_adi, personel_soyadi from personel where personel_id = '" + personelid + "'")
                    personelbilgisi = self.cursor.fetchall()
                    ad = personelbilgisi[0][0]
                    soyad = personelbilgisi[0][1]
                    self.personel.setCurrentText(ad + " " + soyad)
                    self.personel.setEnabled(False)

                elif isbitisi == None:
                    self.isdurumu.setCurrentIndex(3)
                    self.isdurumu.setEnabled(False)
                    self.cursor.execute(
                        "select personel_adi, personel_soyadi from personel where personel_id = '" + personelid + "'")
                    personelbilgisi = self.cursor.fetchall()
                    ad = personelbilgisi[0][0]
                    soyad = personelbilgisi[0][1]
                    self.personel.setCurrentText(ad + " " + soyad)
                    self.personel.setEnabled(False)

                else:
                    pass

        except:
            import traceback
            traceback.print_exc()

        pass

    def operatortakibipreview(self):

        try:
            satir = self.uretimdetaylari_2.currentRow()
            sutun = self.uretimdetaylari_2.currentColumn()
            if sutun == 0:
                path = self.uretimdetaylari_2.item(satir, 7).text()
                if len(path) == 0:
                    self.preview.setText("PREVIEW")
                else:
                    pixmap = QPixmap(path)
                    pixmap= pixmap.scaledToHeight(600)
                    # pixmap= pixmap.scaledToWidth(400)
                    self.preview.setScaledContents(True)
                    self.preview.setPixmap(pixmap)
                    # self.preview.setFixedSize(607, 565)
            elif sutun == 1:
                path = self.uretimdetaylari_2.item(satir, 9).text()
                pixmap = QPixmap(path)
                if len(path) == 0:
                    self.preview.setText("PREVIEW")
                else:
                    pixmap = QPixmap(path)
                    pixmap= pixmap.scaledToHeight(600)
                    # pixmap= pixmap.scaledToWidth(400)
                    self.preview.setScaledContents(True)
                    self.preview.setPixmap(pixmap)
                    # self.preview.setFixedSize(580, 580)
            else:
                pass

        except:
            import traceback
            traceback.print_exc()
            return
        pass

    def operatoruretimdetaylarilistesiguncelle(self):
        satir = self.isemirlerilistesi_2.currentRow()
        if satir == -1:
            return
        else:
            uretimdetayid = self.isemirlerilistesi_2.item(satir, 3).text()
        try:

            self.cursor.execute(
                "select akmill_teknik_resim_no , musteri_teknik_resim_no, uretimdetaylandirma.adet,  operasyon_sayisi, malzeme_adi ,uretim_detaylandirma_id, akmillteknikresimleri.dosya_yolu,"
                "akmillteknikresimleri.dosya_yolu_onizleme, musteriteknikresimleri.dosya_yolu  , musteriteknikresimleri.dosya_yolu_onizleme, malzemetalepleri.olcux, malzemetalepleri.olcuy, malzemetalepleri.olcuz from uretimdetaylandirma "
                "left join akmillteknikresimleri on akmillteknikresimleri.akmill_teknikresim_id =uretimdetaylandirma.akmillteknikresimleri_akmill_teknikresim_id "
                "left join musteriteknikresimleri on musteriteknikresimleri.musteri_teknikresim_id = uretimdetaylandirma.musteriteknikresimleri_musteri_teknikresim_id"
                " left join malzemetalepleri on malzemetalepleri.uretimdetaylandirma_uretim_detaylandirma_id = uretimdetaylandirma.uretim_detaylandirma_id "
                "left join  malzemelistesi on malzemelistesi.malzeme_id = uretimdetaylandirma.malzemelistesi_malzeme_id where uretimdetaylandirma.uretim_detaylandirma_id = '" + str(
                    uretimdetayid) + "'")

            uretimdetaylarilistesi = self.cursor.fetchall()
            self.uretimdetaylari_2.setRowCount(len(uretimdetaylarilistesi))
            for row, value in enumerate(uretimdetaylarilistesi):
                self.uretimdetaylari_2.setItem(row, 0, QtWidgets.QTableWidgetItem(value[0]))  # AKMILL RESIM NO
                self.cursor.execute(
                    "select parca_adi from akmillteknikresimleri where akmill_teknik_resim_no = '" + str(
                        value[0]) + "'")
                parcaadi = self.cursor.fetchone()[0]
                self.uretimdetaylari_2.setItem(row, 1, QtWidgets.QTableWidgetItem(value[1]))  # MUSTERI RESIM NO

                self.uretimdetaylari_2.setItem(row, 1, QtWidgets.QTableWidgetItem(value[1]))  # MUSTERI RESIM NO

                self.cursor.execute(
                    "select SIPARISDETAYLARI_siparis_detay_id from uretimdetaylandirma where uretim_detaylandirma_id = '" + uretimdetayid + "'")
                siparisdetayid = self.cursor.fetchone()[0]
                self.cursor.execute(
                    "select miktar from siparisdetaylari where siparis_detay_id = '" + str(siparisdetayid) + "'")
                takimadedi = self.cursor.fetchone()[0]
                self.uretimdetaylari_2.setItem(row, 2, QtWidgets.QTableWidgetItem(str(takimadedi)))  # ADET
                self.uretimdetaylari_2.setItem(row, 3, QtWidgets.QTableWidgetItem(str(value[3])))  # OP SAYISI
                self.uretimdetaylari_2.setItem(row, 4, QtWidgets.QTableWidgetItem(value[4]))  # MALZEME ADI
                self.uretimdetaylari_2.setItem(row, 5, QtWidgets.QTableWidgetItem(str(value[5])))  # URETIMDETAYID
                self.uretimdetaylari_2.setItem(row, 6, QtWidgets.QTableWidgetItem(value[6]))  # MALZEME ADI
                self.uretimdetaylari_2.setItem(row, 7, QtWidgets.QTableWidgetItem(value[7]))  # MALZEME ADI
                self.uretimdetaylari_2.setItem(row, 8, QtWidgets.QTableWidgetItem(value[8]))  # MALZEME ADI
                self.uretimdetaylari_2.setItem(row, 9, QtWidgets.QTableWidgetItem(value[9]))  # MALZEME ADI
                self.uretimdetaylari_2.setItem(row, 10, QtWidgets.QTableWidgetItem(parcaadi))  # PARCA ADI

                self.cursor.execute(
                    "select count(*) from malzemetalepleri where uretimdetaylandirma_uretim_detaylandirma_id = '" + str(
                        value[5]) + "'")
                kayitmevcutmu = self.cursor.fetchone()[0]
                print kayitmevcutmu
                if kayitmevcutmu == 0:
                    continue
                else:
                    self.cursor.execute(
                        "select adet from malzemetalepleri where uretimdetaylandirma_uretim_detaylandirma_id = '" + str(
                            value[5]) + "'")
                    malzemeadedi = self.cursor.fetchone()[0]
                    self.uretimdetaylari_2.setItem(row, 11, QtWidgets.QTableWidgetItem(
                        str(malzemeadedi)))  # SİPARİŞ VERİLEN MALZEME ADEDİ

                # self.uretimdetaylari_2.setItem(row, 10, QtWidgets.QTableWidgetItem(str(value[10]))) # OLCUX
                # self.uretimdetaylari_2.setItem(row, 11, QtWidgets.QTableWidgetItem(str(value[11]))) # OLCUY
                # self.uretimdetaylari_2.setItem(row, 12, QtWidgets.QTableWidgetItem(str(value[12]))) # OLCUZ

            self.uretimdetaylari_2.setColumnHidden(1, True)
            self.uretimdetaylari_2.setColumnHidden(3, True)

            self.uretimdetaylari_2.setColumnHidden(5, True)
            self.uretimdetaylari_2.setColumnHidden(6, True)
            self.uretimdetaylari_2.setColumnHidden(7, True)
            self.uretimdetaylari_2.setColumnHidden(8, True)
            self.uretimdetaylari_2.setColumnHidden(9, True)
            # self.uretimdetaylari_2.setColumnHidden(10, False)
            # self.uretimdetaylari_2.setColumnHidden(11, False)
            # self.uretimdetaylari_2.setColumnHidden(12, False)
        except:
            import traceback
            traceback.print_exc()
            return
        self.personelgirisinikisitla()

    def closeEvent(self, event):
        try:
            self.dongu.cancel()
            print ("dongu kapandi")

            self.isdurumukart.quit()
            print ("dongu kapandi")

            self.isdurumukart.ser.close()
            print ("dongu kapandi")




        except:
            import traceback
            traceback.print_exc()
            return

    def personelonizlemegetir(self):
        personel = self.personel.currentText()
        personeladsoyad = personel.split()
        personelad = personeladsoyad[0]
        personelsoyad = personeladsoyad[1]
        try:
            self.cursor.execute(
                "select personelresimonizleme from personel where personel_adi = '" + personelad + "' and personel_soyadi = '" + personelsoyad + "'")
            onizlemedosyayolu = self.cursor.fetchone()[0]
            print onizlemedosyayolu
            pixmap = QPixmap(onizlemedosyayolu)
            pixmap = pixmap.scaledToWidth(289)
            pixmap = pixmap.scaledToHeight(259)
            self.personelonizleme.setPixmap(pixmap)
            # self.personelonizleme.setFixedSize(288,259)
            # self.personelonizleme.setScaledContents(True)
        except:
            import traceback
            traceback.print_exc()

        pass

    def isemirlerilistesiguncelle(self):

        try:
            # self.cursor.close()
            # print("cursor kapandi")
            self.conn.close()
            print("connection kapandi")
            self.conn.connect()
            print ("connection acildi")
            # self.cursor = self.conn.cursor()
            # print("cursor acildi")
        except:
            import traceback
            traceback.print_exc()

        try:

            print ("baslangicindaaaaaaaa")
            self.cursor.execute("select * from isemirleri where isdurumu != 'TAMAMLANDI'")
            isemirleri = self.cursor.fetchall()
            self.isemirlerilistesi_2.setRowCount(len(isemirleri))
            print isemirleri
            for satir, data in enumerate(isemirleri):
                self.isemirlerilistesi_2.setItem(satir, 0, QtWidgets.QTableWidgetItem(str(data[0])))
                self.isemirlerilistesi_2.setItem(satir, 1, QtWidgets.QTableWidgetItem(data[1]))
                self.isemirlerilistesi_2.setItem(satir, 2, QtWidgets.QTableWidgetItem(str(data[2])))
                self.isemirlerilistesi_2.setItem(satir, 3, QtWidgets.QTableWidgetItem(str(data[3])))
                self.isemirlerilistesi_2.setItem(satir, 4, QtWidgets.QTableWidgetItem(data[4]))
                self.isemirlerilistesi_2.setItem(satir, 5, QtWidgets.QTableWidgetItem(data[5]))
            self.isemirlerilistesi_2.setColumnHidden(0, False)
            self.isemirlerilistesi_2.setColumnHidden(3, True)

        except:
            import traceback
            traceback.print_exc()
        pass

    # def isemrigoster(self):
    #     pass
    #     satir = self.tableWidget.currentRow()
    #     if satir == -1:
    #         QtWidgets.QMessageBox.warning(self, "HATA", "LÜTFEN SEÇİM YAPINIZ")
    #         return
    #     else:
    #         col = 0
    #         value = self.tableWidget.item(satir, col).text()
    #         path = "//AKMILL2/Users/Akmill2/Desktop/AKmill/MRP/IsEmirleri/" + value + ".pdf"
    #         subprocess.Popen(path, shell=True)
    def dogrula(self, val1):
        QtWidgets.QMessageBox.information(self, "basarili", "bravo" + "" + val1 + "")
        time.sleep(0.5)

    def isebasliyorum(self, val1):
        personelkartoku = KartOkuma()
        personelkartoku.start()
        try:
            QtWidgets.QMessageBox.information(self, "BİLGİ", "KARTINIZI OKUTUNUZ...")

        except:
            import traceback
            traceback.print_exc()
        pass

        if val1 == self.comboBox.currentText():
            self.cursor.execute(
                "select id, teknikresimnoid from isemirleri where isemrino = '" + self.isemrino.text() + "'")
            data = self.cursor.fetchall()
            isemriid = data[0][0]
            teknikresimid = data[0][1]
            adsoyad = self.comboBox.currentText().split(" ")
            self.cursor.execute(
                "select id from personel where ad = '" + adsoyad[0] + "' and soyad = '" + adsoyad[1] + "'")
            operatorid = self.cursor.fetchone()[0]
            self.cursor.execute("select id from tezgahlar where tezgahadi = '" + self.cnctezgah.currentText() + "'")
            tezgahid = self.cursor.fetchone()[0]
            now = datetime.datetime.now()
            date = now.strftime("%Y-%m-%d")
            timer = now.strftime("%H:%M")
            zaman = date + " " + timer

            if self.isadi.currentText() == "AYARLANIYOR":
                self.cursor.execute("select isemrino from isemirleri order by id desc limit 1")
                sonisemrino = self.cursor.fetchone()[0]
                sonisemrino = sonisemrino[:-4]
                self.cursor.execute(
                    "select count(*) from operatortakipgorunumu where isemrino = '" + self.isemrino.text() + "' and kalitekontrol <> 'RET'")
                mevcutmu = self.cursor.fetchone()[0]
                if mevcutmu != 0:
                    QtWidgets.QMessageBox.warning(self, "HATA",
                                                  "DAHA ÖNCE BU İŞ EMRİ NUMARASIYLA İŞLEME GERÇEKLEŞTİRİLMİŞTİR")
                else:
                    try:
                        self.cursor.execute("start transaction")
                        self.cursor.execute(
                            "insert into operatortakip (isemriid , teknikresimid, operatorid,tezgahid,ayar_baslangici) values(%s, %s, %s, %s, %s)",
                            (isemriid, teknikresimid, operatorid, tezgahid, zaman))
                        self.cursor.execute("update isemirleri set durum = '1' where id = '" + str(isemriid) + "'")
                        QtWidgets.QMessageBox.information(self, "İŞ EMRİ DURUMU",
                                                          "'" + self.isemrino.text() + "' NUMARALI IS EMRI AYARLANIYOR OLARAK GUNCELLENMISTIR KOLAY GELSIN")
                    except:
                        QtWidgets.QMessageBox.warning(self, "HATA", "BEKLENMEDİK BİR HATA İLE KARŞILAŞILDI")
                        self.cursor.execute("rollback")
                self.conn.commit()
            elif self.isadi.currentText() == "AYAR BITISI":
                try:
                    self.cursor.execute("start transaction")
                    self.cursor.execute(
                        "update operatortakip set isleme_baslangici ='" + str(zaman) + "' where isemriid = '" + str(
                            isemriid) + "'")
                    self.cursor.execute("update isemirleri set durum  = '2' where id  = '" + str(isemriid) + "'")
                    QtWidgets.QMessageBox.information(self, "IS EMRI DURUMU",
                                                      "'" + self.isemrino.text() + "' NUMARALI IS EMRI DURUMU AYAR BITISI OLARAK GUNCELLENDI")
                except:
                    QtWidgets.QMessageBox.warning(self, "HATA", "BEKLENMEDİK BİR HATA İLE KARŞILAŞILDI")
                    self.cursor.execute("rollback")
                self.conn.commit()
            elif self.isadi.currentText() == "TAMAMLANDI":
                try:
                    self.cursor.execute("start transaction")
                    self.cursor.execute(
                        "update operatortakip set isleme_bitisi ='" + str(zaman) + "' where isemriid = '" + str(
                            isemriid) + "'")
                    self.cursor.execute("update isemirleri set durum  = '4' where id  = '" + str(isemriid) + "'")
                    QtWidgets.QMessageBox.information(self, "IS EMRI DURUMU",
                                                      "'" + self.isemrino.text() + "' NUMARALI IS EMRI DURUMU TAMANLANDI OLARAK GUNCELLENDI")
                except:
                    QtWidgets.QMessageBox.warning(self, "HATA", "BEKLENMEDİK BİR HATA İLE KARŞILAŞILDI")
                    self.cursor.execute("rollback")
                self.conn.commit()
            elif self.isadi.currentText() == "CEVRIM ZAMANI":
                inp = QtWidgets.QInputDialog()
                inp.setInputMode(QtWidgets.QInputDialog.TextInput)
                text, ok = inp.getText(self, 'title', 'ÇEVRİM ZAMANINI GİRİNİZ')
                if ok:
                    if len(text) == 0:
                        QtWidgets.QMessageBox.warning(self, "HATA", "ALAN BOŞ BIRAKILAMAAZ")
                    else:
                        try:
                            self.cursor.execute("start transaction")
                            self.cursor.execute(
                                "update operatortakip set cevrim_zamani = '" + str(text) + "' where isemriid = '" + str(
                                    isemriid) + "'")
                            self.cursor.execute(
                                "update isemirleri set durum  = '3' where id  = '" + str(isemriid) + "'")
                            QtWidgets.QMessageBox.information(self, "IS EMRI DURUMU",
                                                              " '" + self.isemrino.text() + "'" + "  NUMARALI IS EMRI DURUMU DEVAM EDIYOR OLARAK GUNCELLENDI")
                        except:
                            QtWidgets.QMessageBox.warning(self, "HATA", "BEKLENMEDİK BİR HATA İLE KARŞILAŞILDI")
                            self.cursor.execute("rollback")
                        self.conn.commit()
                else:
                    return
            else:
                QtWidgets.QMessageBox.information(self, "GEÇERSİZ", "GEÇERLİ BİR İŞLEM YAPMADINIZ")
            self.isemirlerilistesigetir()
            pass
        else:
            QtWidgets.QMessageBox.warning(self, "PROBLEM", "YANLIS KISISIN")

    def parametreleritasi(self):
        satir = self.tableWidget_3.currentRow()
        if satir == -1:
            QtWidgets.QMessageBox.warning(self, "HATA", "LÜTFEN SEÇİM YAPINIZ")
            return
        else:
            self.isemrino.setText(self.tableWidget_3.item(satir, 0).text())
            self.adet.setText(self.tableWidget_3.item(satir, 3).text())

    def isemirlerilistesigetir(self):

        adsoyad = self.comboBox.currentText().split(" ")
        ad = adsoyad[0]
        soyad = adsoyad[1]
        path = "//AYAS/Users/AYAS/Desktop/MRP/Personel" + "/" + ad + " " + soyad + ".bmp"
        pixmap = QPixmap(path)
        pixmap = pixmap.scaledToHeight(272)
        self.foto.setPixmap(pixmap)
        self.foto.setScaledContents(True)
        # try:
        self.tableWidget_3.clearSelection()
        self.tableWidget_2.clearSelection()
        self.tableWidget.clearSelection()
        advesoyad = self.comboBox.currentText().split(" ")
        ad = advesoyad[0]
        soyad = advesoyad[1]
        self.cursor.execute(
            "select * from isemirlerilistesi where ad = '" + ad + "'  and soyad = '" + soyad + "' and durum <> 'TAMAMLANDI' and kalitekontrol <> 'RET'")
        isemirleri = self.cursor.fetchall()
        if len(isemirleri) == 0:
            QtWidgets.QMessageBox.warning(self, "HATA",
                                          "ÜZERİNİZE TANIMLI BEKLEYEN HERHANGİ BİR İŞ EMRİ BULUNAMAMIŞTIR")
            self.tableWidget_3.clearContents()
            self.tableWidget_3.setRowCount(0)
            self.tableWidget_2.clearContents()
            self.tableWidget_2.setRowCount(0)
            self.tableWidget.clearContents()
            self.tableWidget.setRowCount(0)
            return

        else:

            self.tableWidget.setRowCount(len(isemirleri))
            self.tableWidget_3.setRowCount(len(isemirleri))

            for satir, data in enumerate(isemirleri):
                self.tableWidget.setItem(satir, 0, QtWidgets.QTableWidgetItem(data[0]))
                self.tableWidget.setItem(satir, 1, QtWidgets.QTableWidgetItem(data[2]))
                self.tableWidget.setItem(satir, 2, QtWidgets.QTableWidgetItem(data[3]))
                self.tableWidget.setItem(satir, 3, QtWidgets.QTableWidgetItem(data[8]))
                self.tableWidget.setItem(satir, 4, QtWidgets.QTableWidgetItem(data[9]))
                self.tableWidget_3.setItem(satir, 0, QtWidgets.QTableWidgetItem(data[0]))
                self.tableWidget_3.setItem(satir, 1, QtWidgets.QTableWidgetItem(data[2]))
                self.tableWidget_3.setItem(satir, 2, QtWidgets.QTableWidgetItem(data[3]))
                self.tableWidget_3.setItem(satir, 3, QtWidgets.QTableWidgetItem(data[8]))
                self.tableWidget_3.setItem(satir, 4, QtWidgets.QTableWidgetItem(data[9]))
            self.cursor.execute(
                "select * from operatortakipgorunumu where ad = '" + ad + "' and soyad = '" + soyad + "' and kalitekontrol <> 'RET'")
            isemirleri = self.cursor.fetchall()
            self.tableWidget_2.setRowCount(len(isemirleri))
            for satir, data in enumerate(isemirleri):
                self.tableWidget_2.setItem(satir, 0, QtWidgets.QTableWidgetItem(data[0]))
                self.tableWidget_2.setItem(satir, 1, QtWidgets.QTableWidgetItem(data[2]))
                self.tableWidget_2.setItem(satir, 2, QtWidgets.QTableWidgetItem(data[3]))
                self.tableWidget_2.setItem(satir, 3, QtWidgets.QTableWidgetItem(data[6]))
                self.tableWidget_2.setItem(satir, 4, QtWidgets.QTableWidgetItem(str(data[7])))
                self.tableWidget_2.setItem(satir, 5, QtWidgets.QTableWidgetItem(str(data[8])))
                self.tableWidget_2.setItem(satir, 6, QtWidgets.QTableWidgetItem(str(data[9])))
                self.tableWidget_2.setItem(satir, 7, QtWidgets.QTableWidgetItem(str(data[10])))
