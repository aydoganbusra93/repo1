#!/usr/bin/env python
# -*- coding:utf -8-*-
import subprocess
from PyQt5 import QtWidgets, QtCore, QtGui
from mysql.connector import (connection)
import teknikresimekle
from PyQt5.QtGui import QPixmap

class TeknikResimYonetimEkrani(QtWidgets.QDialog, teknikresimekle.Ui_teknikresimekle):

    def __init__(self, parent=None):
        super(TeknikResimYonetimEkrani, self).__init__(parent)
        self.setupUi(self)
        # self.pushButton.clicked.connect(self.resimekle)
        # self.comboBox.currentIndexChanged.connect(self.arsivlistesiguncelle)
        # self.comboBox_2.currentIndexChanged.connect(self.arsivlistesiprojeilefiltrele)
        try:
            self.conn = connection.MySQLConnection(user='root', password='13579', host='localhost', database='AKMILL')
        except:
            import traceback
            traceback.print_exc()

        self.cursor = self.conn.cursor(buffered=True)
        self.cursor.execute("SELECT firma_adi from firmalar")
        firmalar = self.cursor.fetchall()
        for i in firmalar:
            self.firma_ak.addItems(i)
            self.firma_mu.addItems(i)
            self.firmaadi_arsiv.addItems(i)
        self.firma_ak.currentIndexChanged.connect(self.akmillresimlerlistesiguncelle)
        self.firma_mu.currentIndexChanged.connect(self.musteriresimlerlistesiguncelle)

        self.tarih_ak.setDateTime(QtCore.QDateTime.currentDateTime())
        self.tarih_mu.setDateTime(QtCore.QDateTime.currentDateTime())
        self.ototeknikresimnumarasi()
        self.akmillresimlerlistesiguncelle()
        self.musteriresimlerlistesiguncelle()
        # self.ekle_ak.clicked.connect(self.ototeknikresimnumarasi)
        self.ekle_ak.clicked.connect(self.akmillresimekle)
        self.akmill_resimler_dosya_yolu.clicked.connect(self.dosyayolunual)
        self.musteri_resimler_dosya_yolu.clicked.connect(self.musteridosyayolunual)
        self.akmill_resimler_listesi.currentCellChanged.connect(self.previewak)
        self.musteri_resimler_listesi.currentCellChanged.connect(self.previewmu)
        self.akmill_resimler_listesi.doubleClicked.connect(self.akteknikresimduzenle)
        self.musteri_resimler_listesi.doubleClicked.connect(self.muteknikresimduzenle)
        self.sil_ak.clicked.connect(self.akresimsil)
        self.sil_mu.clicked.connect(self.muresimsil)
        self.ekle_mu.clicked.connect(self.musteriresimekle)

    def akresimsil(self):
        satir = self.akmill_resimler_listesi.currentRow()
        if satir == -1:
            QtWidgets.QMessageBox.warning(self, "HATA", "LÜTFEN SEÇİM YAPINIZI")
            return
        else:
            onizlemedosyayolu = self.akmill_resimler_listesi.item(satir, 7).text()
            resimdosyayolu = self.akmill_resimler_listesi.item(satir, 5).text()
            try:
                reply3 = QtWidgets.QMessageBox.question(self, "CONTUNIE?",
                                                        " İLGİLİ KAYIT VERİTABANINDAN SİLMEK İSTEDİĞİNİZE EMİN MİSİNİZ?",
                                                        QtWidgets.QMessageBox.Yes, QtWidgets.QMessageBox.No)
                if reply3 == QtWidgets.QMessageBox.Yes:
                    try:
                        self.cursor.execute("start transaction")
                        self.cursor.execute(
                            "delete from akmillteknikresimleri where akmill_teknikresim_id = '" + self.akmill_resimler_listesi.item(
                                satir, 6).text() + "'")
                        QtWidgets.QMessageBox.information(self, "BAŞARILI", "DOSYA SİLİNDİ")
                    except:
                        QtWidgets.QMessageBox.warning(self, "HATA",
                                                      "BİR HATA İLE KARŞILAŞILDI İŞLEMLER GERİ ALINIYOR...")
                        self.cursor.execute("rollback")
                        return
                    self.conn.commit()
                else:
                    return
                reply = QtWidgets.QMessageBox.question(self, "CONTUNIE?", " HAZIRLANMIŞ ONİZLEME DOSYASI SİLİNSİN Mİ ?",
                                                       QtWidgets.QMessageBox.Yes, QtWidgets.QMessageBox.No)
                if reply == QtWidgets.QMessageBox.Yes:
                    if os.path.exists(onizlemedosyayolu):
                        os.remove(onizlemedosyayolu)
                    else:
                        QtWidgets.QMessageBox.warning(self, "HATA",
                                                      "İLGİLİ KAYDA AİT HERHANGİ BİR ONİZLEME DOSYASI BULUNAMAMIŞTIR")

                    QtWidgets.QMessageBox.information(self, "BAŞARILI", "ONIZLEME SİLME İŞLEMİ GERÇEKLEŞTİRİLDİ")
                else:
                    pass
                reply2 = QtWidgets.QMessageBox.question(self, "CONTUNIE?", " TEKNİK RESİM DOSYASI SİLİNSİN Mİ ?",
                                                        QtWidgets.QMessageBox.Yes, QtWidgets.QMessageBox.No)
                if reply2 == QtWidgets.QMessageBox.Yes:
                    if os.path.exists(resimdosyayolu):
                        os.remove(resimdosyayolu)
                    else:
                        QtWidgets.QMessageBox.warning(self, "HATA",
                                                      "İLGİLİ KAYDA AİT TEKNİKRESİM DOSYASI BULUNAMAMIŞTIR")
                    QtWidgets.QMessageBox.information(self, "BAŞARILI", "TEKNİKRESİM DOSYASI İŞLEMİ GERÇEKLEŞTİRİLDİ")
                else:
                    pass
            except:
                import traceback
                traceback.print_exc()
        self.akmillresimlerlistesiguncelle()

        pass

    def akteknikresimduzenle(self):
        satir = self.akmill_resimler_listesi.currentRow()
        sutun = self.akmill_resimler_listesi.currentColumn()
        if sutun == 0:
            self.openfile()
        elif sutun == 1:
            inp = QtWidgets.QInputDialog()
            inp.setInputMode(QtWidgets.QInputDialog.TextInput)
            text, ok = inp.getText(self, 'PARÇA ADI DEĞİŞTİR', 'PARÇA ADI')
            if ok:
                if len(text) == 0:
                    QtWidgets.QMessageBox.warning(self, "HATA", "ALAN BOŞ BIRAKILAMAZ")
                    return
                else:
                    try:
                        self.cursor.execute("start transaction")
                        self.cursor.execute(
                            "update akmillteknikresimleri set parca_adi = '" + text + "' where akmill_teknikresim_id = '" + self.akmill_resimler_listesi.item(
                                satir, 6).text() + "'")
                        QtWidgets.QMessageBox.warning(self, "BİLDİRİM", "MÜŞTERİ SİPARİŞ NUMARASI GÜNCELLENDİ")
                    except:
                        self.cursor.execute("rollback")
                        QtWidgets.QMessageBox.warning(self, "HATA",
                                                      "BİR HATA İLE KARŞILAŞILDI İŞLEMLER GERİ ALINIYOR...")
        elif sutun == 2:
            inp = QtWidgets.QInputDialog()
            inp.setInputMode(QtWidgets.QInputDialog.TextInput)
            text, ok = inp.getText(self, 'REVİZYON  NO DEĞİŞTİR', 'REVİZYON NO :')
            if ok:
                if len(text) == 0:
                    QtWidgets.QMessageBox.warning(self, "HATA", "ALAN BOŞ BIRAKILAMAZ")
                    return
                else:
                    try:
                        self.cursor.execute("start transaction")
                        self.cursor.execute(
                            "update akmillteknikresimleri set revizyon_no = '" + text + "' where akmill_teknikresim_id = '" + self.akmill_resimler_listesi.item(
                                satir, 6).text() + "'")
                        QtWidgets.QMessageBox.warning(self, "BİLDİRİM", "REVİZYON NUMARASI GÜNCELLENDİ")
                    except:
                        self.cursor.execute("rollback")
                        QtWidgets.QMessageBox.warning(self, "HATA",
                                                      "BİR HATA İLE KARŞILAŞILDI İŞLEMLER GERİ ALINIYOR...")
        elif sutun == 3:
            QtWidgets.QMessageBox.warning(self, "HATA",
                                          "OLUŞTURULMUŞ TEKNİKRESİM KAYITLARINDAN RESİM NUMARASINA AİT DEĞİŞİKLİK YAPILAMAZ \n"
                                          "RESİM NUMARASINDA DEĞİŞİKLİK YAPMAK İSTİYORSANIZ KAYDI SİLİP YENİDEN OLUŞTURMANIZ GEREKMEKTEDİR")
            return
        elif sutun == 4:
            inp = QtWidgets.QInputDialog()
            inp.setInputMode(QtWidgets.QInputDialog.TextInput)
            text, ok = inp.getText(self, 'TARİH DEĞİŞTİR', 'TARİH YYYY/MM/DD FORMATINDA GİRİŞ :')
            if ok:
                if len(text) == 0:
                    QtWidgets.QMessageBox.warning(self, "HATA", "ALAN BOŞ BIRAKILAMAZ")
                    return
                else:
                    try:
                        self.cursor.execute("start transaction")
                        self.cursor.execute(
                            "update akmillteknikresimleri set tarih = '" + text + "' where akmill_teknikresim_id = '" + self.akmill_resimler_listesi.item(
                                satir, 6).text() + "'")
                        QtWidgets.QMessageBox.warning(self, "BİLDİRİM", "REVİZYON NUMARASI GÜNCELLENDİ")
                    except:
                        self.cursor.execute("rollback")
                        QtWidgets.QMessageBox.warning(self, "HATA",
                                                      "GİRMİŞ OLDUĞUNUZ FORMATTA YANLIŞLIK BULUNMAKTADIR   \n  YYYY/MM/DD ŞEKLİNDE GİRİŞ YAPINIZ")
                        return
        elif sutun == 5:
            self.onizleme_ak.clear()
            self.dosya_yolu_ak.clear()
            self.dosyayolunual()
            if len(self.dosya_yolu_ak.text()) == 0 or len(self.onizleme_ak.text()) == 0:
                return
            else:
                pass
            try:
                dosyoniz = self.onizleme_ak.text().replace("\\", "\\\\")

                print ("yeni dosya  yolu :: " + self.onizleme_ak.text())

                self.cursor.execute("start transaction")
                self.cursor.execute(
                    "update akmillteknikresimleri set dosya_yolu = '" + self.dosya_yolu_ak.text() + "', dosya_yolu_onizleme = '" + dosyoniz + "'  where akmill_teknikresim_id = '" + self.akmill_resimler_listesi.item(
                        satir, 6).text() + "'")
                QtWidgets.QMessageBox.warning(self, "BİLDİRİM", "DOSYA YOLU VE ONİZLEME GÜNCELLENDİ")
            except:
                import traceback
                traceback.print_exc()
                self.cursor.execute("rollback")
                QtWidgets.QMessageBox.warning(self, "HATA", "BİR HATA İLE KARŞILAŞILDI İŞLEMLER GERİ ALINIYOR...")
        else:
            pass
        self.conn.commit()
        self.akmillresimlerlistesiguncelle()

    def openfile(self):
        satir = self.akmill_resimler_listesi.currentRow()
        # sutun = self.akmill_resimler_listesi.currentColumn()
        path = self.akmill_resimler_listesi.item(satir, 5).text()
        subprocess.Popen(path, shell=True)

    def previewak(self):
        try:

            satir = self.akmill_resimler_listesi.currentRow()
            if satir == -1:
                self.akmill_preview.setText('ÖNİZLEME')
            else:
                path = self.akmill_resimler_listesi.item(satir, 7).text()
                pixmap = QPixmap(path)
                # pixmap= pixmap.scaledToHeight(400)
                # pixmap= pixmap.scaledToWidth(400)
                self.akmill_preview.setScaledContents(False)
                self.akmill_preview.setPixmap(pixmap)
                self.akmill_preview.setFixedSize()
        except:
            return

    def dosyayolunual(self):
        fileName = QtWidgets.QFileDialog.getOpenFileName(self, ("TEKNİK RESİM SEÇİMİ"),
                                                         "//DESKTOP-QC87HVJ/Users/PC1/Desktop/AKMILL",
                                                         ("Image Files (*.png *.pdf  *.jpg)"))
        dosyayolu = QtCore.QFileInfo(fileName[0])
        dosyaadi = dosyayolu.baseName()
        anadosya = dosyayolu.filePath()
        anadosya = anadosya.replace('/', '\\')
        if len(anadosya) == 0:
            return
        else:
            pass
        onizlemekayit = anadosya[:-(len(dosyaadi) + 4)]
        try:
            self.dosya_yolu_ak.setText(fileName[0])
            if fileName[0][-3:] == 'pdf' or 'PDF':
                QtWidgets.QMessageBox.warning(self, "BİLGİ",
                                              "SEÇMİŞ OLDUĞUNUZ PDF DOKÜMANININ İLK SAYFASI ÖNİZLEME OLARAK HAZIRLANIYOR")
                with Image(filename=fileName[0]) as img:
                    extractedimg = img.sequence[0]
                    extractedimg.resize(588, 731)
                    # img.sample(588, 731)
                    first_image = Image(image=extractedimg)
                    first_image.format = 'png'
                    first_image.save(filename=onizlemekayit + dosyaadi + '-preview.png')
                QtWidgets.QMessageBox.information(self, "BİLDİRİM", "ÖNİZLEME HAZIRLANDI ")
                self.onizleme_ak.setText(onizlemekayit + dosyaadi + '-preview.png')
            elif fileName[0][-3:] == 'JPG' or 'jpg':
                QtWidgets.QMessageBox.warning(self, "BİLGİ",
                                              "SEÇMİŞ OLDUĞUNUZ DOKÜMANIN ÖNİZLEME İÇİN YENİDEN BOYUTLANDIRILIYOR")
                with Image(filename=fileName[0]) as img:
                    # extractedimg = img.sequence[0]
                    # extractedimg.resize(588,731)
                    img.resize(588, 731)
                    first_image = Image(image=img)
                    first_image.format = 'png'
                    first_image.save(filename=onizlemekayit + dosyaadi + '-preview.png')
                QtWidgets.QMessageBox.information(self, "BİLDİRİM", "DOKÜMAN YENİDEN BOYUTLANDIRILDI ")
                self.onizleme_ak.setText(onizlemekayit + dosyaadi + '-preview.png')


            elif fileName[0][-3:] == 'png' or 'PNG':
                QtWidgets.QMessageBox.warning(self, "BİLGİ",
                                              "SEÇMİŞ OLDUĞUNUZ DOKÜMANIN ÖNİZLEME İÇİN YENİDEN BOYUTLANDIRILIYOR")
                with Image(filename=fileName[0]) as img:
                    # extractedimg = img.sequence[0]
                    # extractedimg.resize(588,731)
                    img.resize(588, 731)
                    first_image = Image(image=img)
                    first_image.format = 'png'
                    first_image.save(filename=onizlemekayit + dosyaadi + '-preview.png')
                QtWidgets.QMessageBox.information(self, "BİLDİRİM", "DOKÜMAN YENİDEN BOYUTLANDIRILDI ")
                self.onizleme_ak.setText(onizlemekayit + dosyaadi + '-preview.png')

            else:
                pass

        except:
            import traceback
            traceback.print_exc()

        # self.teknikresimlistesi.currentCellChanged.connect(self.onizleme)

    def akmillresimlerlistesiguncelle(self):
        try:
            self.cursor.execute(
                "select  FIRMALAR.firma_adi , parca_adi, revizyon_no, akmill_teknik_resim_no, tarih , dosya_yolu, akmill_teknikresim_id ,dosya_yolu_onizleme from firmalar "
                "join akmillteknikresimleri on akmillteknikresimleri.firmalar_firma_id = firmalar.firma_id where firma_adi = '" + self.firma_ak.currentText() + "'")
        except:
            import traceback
            traceback.print_exc()
        akmillresimlistesi = self.cursor.fetchall()
        self.akmill_resimler_listesi.setRowCount(len(akmillresimlistesi))
        try:
            for satir, deger in enumerate(akmillresimlistesi):
                self.akmill_resimler_listesi.setItem(satir, 0, QtWidgets.QTableWidgetItem(deger[0]))
                self.akmill_resimler_listesi.setItem(satir, 1, QtWidgets.QTableWidgetItem(deger[1]))
                self.akmill_resimler_listesi.setItem(satir, 2, QtWidgets.QTableWidgetItem(deger[2]))
                self.akmill_resimler_listesi.setItem(satir, 3, QtWidgets.QTableWidgetItem(deger[3]))
                self.akmill_resimler_listesi.setItem(satir, 4, QtWidgets.QTableWidgetItem(str(deger[4])))
                self.akmill_resimler_listesi.setItem(satir, 5, QtWidgets.QTableWidgetItem(deger[5]))
                self.akmill_resimler_listesi.setItem(satir, 6, QtWidgets.QTableWidgetItem(str(deger[6])))
                self.akmill_resimler_listesi.setItem(satir, 7, QtWidgets.QTableWidgetItem(deger[7]))


        except:
            import traceback
            traceback.print_exc()
        self.akmill_resimler_listesi.setColumnHidden(6, True)
        self.akmill_resimler_listesi.setColumnHidden(7, True)
        self.akmill_resimler_listesi.selectRow(-1)
        self.previewak()
        self.ototeknikresimnumarasi()

    def akmillresimekle(self):
        try:
            firmaindex = self.firma_ak.currentIndex()
            ak_firmaadi = self.firma_ak.currentText()
            self.cursor.execute("select firma_id from firmalar where firma_adi = '" + ak_firmaadi + "'")
            firmaid = self.cursor.fetchone()[0]
            self.cursor.execute(
                "select count(*) from akmillteknikresimleri where firmalar_firma_id= '" + str(firmaid) + "'")
            mevcutmu = self.cursor.fetchone()[0]
            print(mevcutmu)
            if mevcutmu == 0:
                print("44444")
                resimnumarasi = 0
            else:
                self.cursor.execute(
                    "select resimnumaralandirma from akmillteknikresimleri where firmalar_firma_id = '" + str(
                        firmaid) + "' order by akmill_teknikresim_id desc")
                print("asdasdasdasdasdasfAQEWEQASDASD")
                resimnumarasi = self.cursor.fetchone()[0]
                print resimnumarasi
                print ("sorgudonusu")

            ak_parca = self.parcaadi_ak.text()
            ak_rev = self.revno_ak.text()
            ak_resimno = self.otomatiknumara_ak.text()
            ak_tarih = self.tarih_ak.text()
            ak_dosya_yolu = self.dosya_yolu_ak.text()
            print("ooooooooooooooooooo")

            if len(ak_rev) == 0 or len(ak_parca) == 0 or len(ak_resimno) == 0 or len(ak_tarih) == 0 or len(
                    ak_firmaadi) == 0:
                QtWidgets.QMessageBox.warning(self, "HATA",
                                              "ALANLAR BOŞ BIRAKILAMAZ \n HERHANGİ BİR REVİZYON NUMARASI MEVCUT DEĞİLSE '00' OLARAK GİRİŞ YAPABİLİRSİNİZ")
                return
            else:
                pass
            print("ooooooooooooooooooo")
            try:
                self.cursor.execute(
                    "select count(*) from akmillteknikresimleri where parca_adi = '" + ak_parca + "' and revizyon_no = '" + ak_rev + "'")
                varmi = self.cursor.fetchone()[0]
                print("ooooooooooooooooooo")
            except:
                import traceback
                traceback.print_exc()

            print("ooooooooooooooooooo")

            if varmi != 0:
                print("ooooooooooooooooooo")

                QtWidgets.QMessageBox.warning(self, "HATA",
                                              "DAHA ÖNCE BU PARÇA ADI İLE KAYIT GERÇEKLEŞTİRİLMİŞTİR \n MEVCUT PARÇADA DEĞİŞİKLİK VARSA "
                                              "LÜTFEN REVİZYON NUMALARINI KONTROL EDİNİZ")
                return
            else:
                pass
            try:
                print("rtry boluk")
                self.cursor.execute("start transaction")
                print("transaction")

                self.cursor.execute(
                    "insert into akmillteknikresimleri (firmalar_firma_id,akmill_teknik_resim_no,parca_adi,revizyon_no,tarih,dosya_yolu,resimnumaralandirma,dosya_yolu_onizleme )"
                    "values (%s, %s, %s, %s, %s,%s, %s, %s)", (
                    firmaid, ak_resimno, ak_parca, ak_rev, ak_tarih, ak_dosya_yolu, resimnumarasi + 1,
                    self.onizleme_ak.text()))
                print("insert")

                QtWidgets.QMessageBox.warning(self, "BİLDİRİM", "EKLEME İŞLEMLERİ GERÇEKLEŞTİRİLDİ")
            except:
                import traceback
                traceback.print_exc()
                QtWidgets.QMessageBox.warning(self, "HATA", "BİR HATA İLE KARŞILAŞILDI İŞLEMLER GERİ ALINIYOR")
                self.cursor.execute("rollback")
            self.conn.commit()
            self.akmillresimlerlistesiguncelle()
            # self.ototeknikresimnumarasi()
        except:
            traceback.print_exc()
        # self.firma_ak.setCurrentText(firmaindex)

    def ototeknikresimnumarasi(self):
        self.cursor.execute("select firma_id from firmalar where firma_adi = '" + self.firma_ak.currentText() + "'")
        firmaid = self.cursor.fetchone()[0]
        print firmaid
        try:
            print ("firma idsi : " + str(firmaid))
            self.cursor.execute(
                "select resimnumaralandirma from akmillteknikresimleri where firmalar_firma_id = '" + str(
                    firmaid) + "' order by resimnumaralandirma desc")
            resim = self.cursor.fetchall()
            print ("donen datalar : " + str(resim))
            # resimnumarasi = resim[0][0] + 1
            if len(resim) == 0:
                # QtWidgets.QMessageBox.warning(self, "HATA", "BU FİRMAYA KAYITLI HERHANGİ BİR RESİM BULUNAMAMIŞTIR")

                resimnumarasi = 1
            else:
                pass

                print resim
                resimnumarasi = resim[0][0] + 1

        except:
            import traceback
            traceback.print_exc()
        # resimnumarasi = self.cursor.fetchall()[0][0]
        # self.cursor.execute("select count(*) from akmillteknikresimleri where firmalar_firma_id = '"+str(firmaid)+"'")
        # if mevcutmu == 0:
        #     resimnumarasi = 1
        # else:
        #     self.cursor.execute("select resimnumaralandirma from akmillteknikresimleri order by akmill_teknikresim_id desc")
        # resimnumarasi =self.cursor.fetchall()[0][0]
        # resimnumarasi = resimnumarasi +1
        # firmaadi = self.firma_ak.currentText()
        # try:
        #     self.cursor.execute("select firma_id from firmalar where firma_adi = '"+firmaadi+"'")
        # except:
        #     import traceback
        #     traceback.print_exc()
        # firmaid = self.cursor.fetchall()[0][0]
        print ("resim numarasi : " + str(resimnumarasi))
        self.otomatiknumara_ak.setText("0" + str(firmaid) + "_" + str(resimnumarasi))
        pass

    def musteriresimlerlistesiguncelle(self):

        self.cursor.execute(
            "select  FIRMALAR.firma_adi , parca_adi, revizyon_no, musteri_teknik_resim_no, tarih , dosya_yolu, musteri_teknikresim_id, dosya_yolu_onizleme from firmalar "
            "join musteriteknikresimleri on musteriteknikresimleri.firmalar_firma_id = firmalar.firma_id where firma_adi = '" + self.firma_mu.currentText() + "'")
        musteriresimlistesi = self.cursor.fetchall()
        self.musteri_resimler_listesi.setRowCount(len(musteriresimlistesi))

        for satir, deger in enumerate(musteriresimlistesi):
            self.musteri_resimler_listesi.setItem(satir, 0, QtWidgets.QTableWidgetItem(deger[0]))
            self.musteri_resimler_listesi.setItem(satir, 1, QtWidgets.QTableWidgetItem(deger[1]))
            self.musteri_resimler_listesi.setItem(satir, 2, QtWidgets.QTableWidgetItem(deger[2]))
            self.musteri_resimler_listesi.setItem(satir, 3, QtWidgets.QTableWidgetItem(deger[3]))
            self.musteri_resimler_listesi.setItem(satir, 4, QtWidgets.QTableWidgetItem(str(deger[4])))
            self.musteri_resimler_listesi.setItem(satir, 5, QtWidgets.QTableWidgetItem(deger[5]))
            self.musteri_resimler_listesi.setItem(satir, 6, QtWidgets.QTableWidgetItem(str(deger[6])))
            self.musteri_resimler_listesi.setItem(satir, 7, QtWidgets.QTableWidgetItem(deger[7]))
        self.musteri_resimler_listesi.setColumnHidden(6, True)
        self.musteri_resimler_listesi.setColumnHidden(7, True)
        self.musteri_resimler_listesi.selectRow(-1)
        self.previewmu()

        pass

        pass

    def musteriresimekle(self):
        self.cursor.execute("select count(*) from musteriteknikresimleri")
        mevcutmu = self.cursor.fetchone()[0]
        if mevcutmu == 0:
            resimnumarasi = 1
        else:
            self.cursor.execute(
                "select resim_numaralandirma from musteriteknikresimleri order by musteri_teknikresim_id desc")
            resimnumarasi = self.cursor.fetchall()[0][0]

        firmaindex = self.firma_mu.currentIndex()
        mu_firmaadi = self.firma_mu.currentText()
        self.cursor.execute("select firma_id from firmalar where firma_adi = '" + mu_firmaadi + "'")
        firmaid = self.cursor.fetchone()[0]
        mu_parca = self.parcaadi_mu.text()
        mu_rev = self.revno_mu.text()
        mu_resimno = self.musteri_numara.text()
        mu_tarih = self.tarih_mu.text()
        mu_dosya_yolu = self.dosya_yolu_mu.text()
        if len(mu_rev) == 0 or len(mu_parca) == 0 or len(mu_resimno) == 0 or len(mu_tarih) == 0 or len(
                mu_firmaadi) == 0:
            QtWidgets.QMessageBox.warning(self, "HATA",
                                          "ALANLAR BOŞ BIRAKILAMAZ \n HERHANGİ BİR REVİZYON NUMARASI MEVCUT DEĞİLSE '00' OLARAK GİRİŞ YAPABİLİRSİNİZ")
            return
        else:
            pass

        self.cursor.execute(
            "select count(*) from musteriteknikresimleri where parca_adi = '" + mu_parca + "' and revizyon_no = '" + mu_rev + "'")
        varmi = self.cursor.fetchone()[0]
        if varmi != 0:
            QtWidgets.QMessageBox.warning(self, "HATA",
                                          "DAHA ÖNCE BU PARÇA ADI İLE KAYIT GERÇEKLEŞTİRİLMİŞTİR \n MEVCUT PARÇADA DEĞİŞİKLİK VARSA "
                                          "LÜTFEN REVİZYON NUMALARINI KONTROL EDİNİZ")
            return
        else:
            pass
        try:
            self.cursor.execute("start transaction")
            self.cursor.execute(
                "insert into musteriteknikresimleri (firmalar_firma_id,musteri_teknik_resim_no,parca_adi,revizyon_no,tarih,dosya_yolu,resim_numaralandirma,dosya_yolu_onizleme )"
                "values (%s, %s, %s, %s, %s,%s, %s, %s)", (
                firmaid, mu_resimno, mu_parca, mu_rev, mu_tarih, mu_dosya_yolu, resimnumarasi + 1,
                self.onizleme_mu.text()))
            QtWidgets.QMessageBox.warning(self, "BİLDİRİM", "EKLEME İŞLEMLERİ GERÇEKLEŞTİRİLDİ")
        except:
            import traceback
            traceback.print_exc()
            QtWidgets.QMessageBox.warning(self, "HATA", "BİR HATA İLE KARŞILAŞILDI İŞLEMLER GERİ ALINIYOR")
            self.cursor.execute("rollback")
        self.conn.commit()
        self.musteriresimlerlistesiguncelle()
        # self.firma_mu.setCurrentText(firmaindex)

    def musteridosyayolunual(self):
        fileName = QtWidgets.QFileDialog.getOpenFileName(self, ("MÜSTERİ TEKNİK RESİM SEÇİMİ"),
                                                         "//DESKTOP-QC87HVJ/Users/PC1/Desktop/AKMILL",
                                                         ("Image Files (*.png *.pdf  *.jpg)"))
        dosyayolu = QtCore.QFileInfo(fileName[0])
        dosyaadi = dosyayolu.baseName()
        anadosya = dosyayolu.filePath()
        if len(anadosya) == 0:
            return
        else:
            pass
        onizlemekayit = anadosya[:-(len(dosyaadi) + 4)]
        try:
            self.dosya_yolu_mu.setText(fileName[0])
            if fileName[0][-3:] == 'pdf' or 'PDF':
                QtWidgets.QMessageBox.warning(self, "BİLGİ",
                                              "SEÇMİŞ OLDUĞUNUZ PDF DOKÜMANININ İLK SAYFASI ÖNİZLEME OLARAK HAZIRLANIYOR")
                with Image(filename=fileName[0]) as img:
                    extractedimg = img.sequence[0]
                    extractedimg.resize(588, 731)
                    # img.sample(588, 731)
                    first_image = Image(image=extractedimg)
                    first_image.format = 'png'
                    first_image.save(filename=onizlemekayit + dosyaadi + '-preview.png')
                QtWidgets.QMessageBox.information(self, "BİLDİRİM", "ÖNİZLEME HAZIRLANDI ")
                self.onizleme_mu.setText(onizlemekayit + dosyaadi + '-preview.png')
            elif fileName[0][-3:] == 'jpg' or 'JPG':
                QtWidgets.QMessageBox.warning(self, "BİLGİ",
                                              "SEÇMİŞ OLDUĞUNUZ DOKÜMANIN ÖNİZLEME İÇİN YENİDEN BOYUTLANDIRILIYOR")
                with Image(filename=fileName[0]) as img:
                    # extractedimg = img.sequence[0]
                    # extractedimg.resize(588,731)
                    img.resize(588, 731)
                    first_image = Image(image=img)
                    first_image.format = 'png'
                    first_image.save(filename=onizlemekayit + dosyaadi + '-preview.png')
                QtWidgets.QMessageBox.information(self, "BİLDİRİM", "DOKÜMAN YENİDEN BOYUTLANDIRILDI ")
                self.onizleme_mu.setText(onizlemekayit + dosyaadi + '-preview.png')

            elif fileName[0][-3:] == 'png' or 'PNG':
                QtWidgets.QMessageBox.warning(self, "BİLGİ",
                                              "SEÇMİŞ OLDUĞUNUZ DOKÜMANIN ÖNİZLEME İÇİN YENİDEN BOYUTLANDIRILIYOR")
                with Image(filename=fileName[0]) as img:
                    # extractedimg = img.sequence[0]
                    # extractedimg.resize(588,731)
                    img.resize(588, 731)
                    first_image = Image(image=img)
                    first_image.format = 'png'
                    first_image.save(filename=onizlemekayit + dosyaadi + '-preview.png')
                QtWidgets.QMessageBox.information(self, "BİLDİRİM", "DOKÜMAN YENİDEN BOYUTLANDIRILDI ")
                self.onizleme_mu.setText(onizlemekayit + dosyaadi + '-preview.png')
            else:
                pass

        except:
            import traceback
            traceback.print_exc()

        # self.teknikresimlistesi.currentCellChanged.connect(self.onizleme)

    def previewmu(self):
        try:
            satir = self.musteri_resimler_listesi.currentRow()
            if satir == -1:
                self.musteri_preview.setText('ÖNİZLEME')
            else:
                satir = self.musteri_resimler_listesi.currentRow()
                path = self.musteri_resimler_listesi.item(satir, 7).text()
                pixmap = QPixmap(path)
                # pixmap= pixmap.scaledToHeight(400)
                # pixmap= pixmap.scaledToWidth(400)
                self.musteri_preview.setScaledContents(False)
                self.musteri_preview.setPixmap(pixmap)
                self.musteri_preview.setFixedSize()
        except:
            return

    def muteknikresimduzenle(self):
        satir = self.musteri_resimler_listesi.currentRow()
        sutun = self.musteri_resimler_listesi.currentColumn()
        if sutun == 0:
            self.openfilemu()
        elif sutun == 1:
            inp = QtWidgets.QInputDialog()
            inp.setInputMode(QtWidgets.QInputDialog.TextInput)
            text, ok = inp.getText(self, 'PARÇA ADI DEĞİŞTİR', 'PARÇA ADI')
            if ok:
                if len(text) == 0:
                    QtWidgets.QMessageBox.warning(self, "HATA", "ALAN BOŞ BIRAKILAMAZ")
                    return
                else:
                    try:
                        self.cursor.execute("start transaction")
                        self.cursor.execute(
                            "update musteriteknikresimleri set parca_adi = '" + text + "' where musteri_teknikresim_id = '" + self.musteri_resimler_listesi.item(
                                satir, 6).text() + "'")
                        QtWidgets.QMessageBox.warning(self, "BİLDİRİM", "MÜŞTERİ PARÇA ADI GÜNCELLENDİ")
                    except:
                        self.cursor.execute("rollback")
                        QtWidgets.QMessageBox.warning(self, "HATA",
                                                      "BİR HATA İLE KARŞILAŞILDI İŞLEMLER GERİ ALINIYOR...")
        elif sutun == 2:
            inp = QtWidgets.QInputDialog()
            inp.setInputMode(QtWidgets.QInputDialog.TextInput)
            text, ok = inp.getText(self, 'REVİZYON  NO DEĞİŞTİR', 'REVİZYON NO :')
            if ok:
                if len(text) == 0:
                    QtWidgets.QMessageBox.warning(self, "HATA", "ALAN BOŞ BIRAKILAMAZ")
                    return
                else:
                    try:
                        self.cursor.execute("start transaction")
                        self.cursor.execute(
                            "update musteriteknikresimleri set revizyon_no = '" + text + "' where musteri_teknikresim_id = '" + self.musteri_resimler_listesi.item(
                                satir, 6).text() + "'")
                        QtWidgets.QMessageBox.warning(self, "BİLDİRİM", "MÜŞTERİ REVİZYON NUMARASI GÜNCELLENDİ")
                    except:
                        self.cursor.execute("rollback")
                        QtWidgets.QMessageBox.warning(self, "HATA",
                                                      "BİR HATA İLE KARŞILAŞILDI İŞLEMLER GERİ ALINIYOR...")
        elif sutun == 3:
            inp = QtWidgets.QInputDialog()
            inp.setInputMode(QtWidgets.QInputDialog.TextInput)
            text, ok = inp.getText(self, 'MÜŞTERİ RESİM  NO DEĞİŞTİR', 'MÜŞTERİ RESİM NO :')
            if ok:
                if len(text) == 0:
                    QtWidgets.QMessageBox.warning(self, "HATA", "ALAN BOŞ BIRAKILAMAZ")
                    return
                else:
                    try:
                        self.cursor.execute("start transaction")
                        self.cursor.execute(
                            "update musteriteknikresimleri set musteri_teknik_resim_no = '" + text + "' where musteri_teknikresim_id = '" + self.musteri_resimler_listesi.item(
                                satir, 6).text() + "'")
                        QtWidgets.QMessageBox.warning(self, "BİLDİRİM", "MÜŞTERİ RESİM NUMARASI GÜNCELLENDİ")
                    except:
                        self.cursor.execute("rollback")
                        QtWidgets.QMessageBox.warning(self, "HATA",
                                                      "BİR HATA İLE KARŞILAŞILDI İŞLEMLER GERİ ALINIYOR...")
        elif sutun == 4:
            inp = QtWidgets.QInputDialog()
            inp.setInputMode(QtWidgets.QInputDialog.TextInput)
            text, ok = inp.getText(self, 'TARİH DEĞİŞTİR', 'TARİH YYYY/MM/DD FORMATINDA GİRİŞ :')
            if ok:
                if len(text) == 0:
                    QtWidgets.QMessageBox.warning(self, "HATA", "ALAN BOŞ BIRAKILAMAZ")
                    return
                else:
                    try:
                        self.cursor.execute("start transaction")
                        self.cursor.execute(
                            "update musteriteknikresimleri set tarih = '" + text + "' where musteri_teknikresim_id = '" + self.musteri_resimler_listesi.item(
                                satir, 6).text() + "'")
                        QtWidgets.QMessageBox.warning(self, "BİLDİRİM", "TARİH GÜNCELLENDİ")
                    except:
                        self.cursor.execute("rollback")
                        QtWidgets.QMessageBox.warning(self, "HATA",
                                                      "GİRMİŞ OLDUĞUNUZ FORMATTA YANLIŞLIK BULUNMAKTADIR   \n  YYYY/MM/DD ŞEKLİNDE GİRİŞ YAPINIZ")
                        return
        elif sutun == 5:
            self.onizleme_ak.clear()
            self.dosya_yolu_mu.clear()
            self.musteridosyayolunual()
            if len(self.dosya_yolu_mu.text()) == 0 or len(self.onizleme_mu.text()) == 0:
                return
            else:
                pass
            try:
                self.cursor.execute("start transaction")
                self.cursor.execute(
                    "update musteriteknikresimleri set dosya_yolu = '" + self.dosya_yolu_mu.text() + "', dosya_yolu_onizleme = '" + self.onizleme_mu.text() + "'  where musteri_teknikresim_id = '" + self.musteri_resimler_listesi.item(
                        satir, 6).text() + "'")
                QtWidgets.QMessageBox.warning(self, "BİLDİRİM", "DOSYA YOLU VE ONİZLEME GÜNCELLENDİ")
            except:
                self.cursor.execute("rollback")
                QtWidgets.QMessageBox.warning(self, "HATA", "BİR HATA İLE KARŞILAŞILDI İŞLEMLER GERİ ALINIYOR...")
        else:
            pass
        self.conn.commit()
        self.musteriresimlerlistesiguncelle()

    def openfilemu(self):
        satir = self.musteri_resimler_listesi.currentRow()
        # sutun = self.akmill_resimler_listesi.currentColumn()
        path = self.musteri_resimler_listesi.item(satir, 5).text()
        subprocess.Popen(path, shell=True)

    def muresimsil(self):
        satir = self.musteri_resimler_listesi.currentRow()
        if satir == -1:
            QtWidgets.QMessageBox.warning(self, "HATA", "LÜTFEN SEÇİM YAPINIZI")
            return
        else:
            onizlemedosyayolu = self.musteri_resimler_listesi.item(satir, 7).text()
            resimdosyayolu = self.musteri_resimler_listesi.item(satir, 5).text()
            try:
                reply3 = QtWidgets.QMessageBox.question(self, "CONTUNIE?",
                                                        " İLGİLİ KAYIT VERİTABANINDAN SİLMEK İSTEDİĞİNİZE EMİN MİSİNİZ?",
                                                        QtWidgets.QMessageBox.Yes, QtWidgets.QMessageBox.No)
                if reply3 == QtWidgets.QMessageBox.Yes:
                    try:
                        self.cursor.execute("start transaction")
                        self.cursor.execute(
                            "delete from musteriteknikresimleri where musteri_teknikresim_id = '" + self.musteri_resimler_listesi.item(
                                satir, 6).text() + "'")
                        QtWidgets.QMessageBox.information(self, "BAŞARILI", "DOSYA SİLİNDİ")
                    except:
                        QtWidgets.QMessageBox.warning(self, "HATA",
                                                      "BİR HATA İLE KARŞILAŞILDI İŞLEMLER GERİ ALINIYOR...")
                        self.cursor.execute("rollback")
                        return
                    self.conn.commit()
                else:
                    return
                reply = QtWidgets.QMessageBox.question(self, "CONTUNIE?", " HAZIRLANMIŞ ONİZLEME DOSYASI SİLİNSİN Mİ ?",
                                                       QtWidgets.QMessageBox.Yes, QtWidgets.QMessageBox.No)
                if reply == QtWidgets.QMessageBox.Yes:
                    if os.path.exists(onizlemedosyayolu):
                        os.remove(onizlemedosyayolu)
                    else:
                        QtWidgets.QMessageBox.warning(self, "HATA",
                                                      "İLGİLİ KAYDA AİT HERHANGİ BİR ONİZLEME DOSYASI BULUNAMAMIŞTIR")

                    QtWidgets.QMessageBox.information(self, "BAŞARILI", "ONIZLEME SİLME İŞLEMİ GERÇEKLEŞTİRİLDİ")
                else:
                    pass
                reply2 = QtWidgets.QMessageBox.question(self, "CONTUNIE?", " TEKNİK RESİM DOSYASI SİLİNSİN Mİ ?",
                                                        QtWidgets.QMessageBox.Yes, QtWidgets.QMessageBox.No)
                if reply2 == QtWidgets.QMessageBox.Yes:
                    if os.path.exists(resimdosyayolu):
                        os.remove(resimdosyayolu)
                    else:
                        QtWidgets.QMessageBox.warning(self, "HATA",
                                                      "İLGİLİ KAYDA AİT TEKNİKRESİM DOSYASI BULUNAMAMIŞTIR")
                    QtWidgets.QMessageBox.information(self, "BAŞARILI", "TEKNİKRESİM DOSYASI İŞLEMİ GERÇEKLEŞTİRİLDİ")
                else:
                    pass
            except:
                import traceback
                traceback.print_exc()
        self.musteriresimlerlistesiguncelle()

        pass

    def onizleme2(self):
        pass
        try:
            satir = self.arananlar.currentRow()
            parcaadi = self.arananlar.item(satir, 1).text()
            path = "//AKMILL2/Users/Akmill2/Desktop/AKmill/MRP/resimler" + "/" + parcaadi + ".bmp"
            pixmap = QPixmap(path)
            # pixmap= pixmap.scaledToHeight(150)
            self.preview_2.setPixmap(pixmap)
            self.preview_2.setScaledContents(True)
        except:
            return

    def onizleme(self):
        pass
        try:
            satir = self.teknikresimlistesi.currentRow()
            resimno = self.teknikresimlistesi.item(satir, 4).text()

            path = "//AKMILL2/Users/Akmill2/Desktop/AKmill/MRP/resimler" + "/" + resimno + ".bmp"
            pixmap = QPixmap(path)
            # pixmap= pixmap.scaledToHeight(150)
            self.preview.setPixmap(pixmap)
            self.preview.setScaledContents(True)
        except:
            return

    def manuelresimnogirisi(self):
        pass
        inp = QtWidgets.QInputDialog()
        inp.setInputMode(QtWidgets.QInputDialog.TextInput)
        text, ok = inp.getText(self, 'title', 'TEKNİK RESİM NUMARASI')
        if ok:
            if len(text) == 0:
                QtWidgets.QMessageBox.warning(self, "HATA", "ALAN BOŞ BIRAKILAMAAZ")
            else:
                self.otomatiknumara.setText(text)
        else:
            return

    def eslestirmeyiiptalet(self):
        pass
        satir = self.teknikresimlistesi.currentRow()
        if satir == -1:
            QtWidgets.QMessageBox.warning(self, "HATA", "LÜTFEN SEÇİM YAPINIZI")
            return
        else:
            firmaadi = self.teknikresimlistesi.item(satir, 0).text()
            teknikresimno = self.teknikresimlistesi.item(satir, 1).text()
            projeadi = self.comboBox_2.currentText()
            self.cursor.execute("select firma_id from firmalar where firma_adi = '" + firmaadi + "'")
            firmaid = self.cursor.fetchone()[0]
            self.cursor.execute("select id from teknikresimler where  teknikresimno = '" + teknikresimno + "'")
            teknikresimid = self.cursor.fetchone()[0]
            self.cursor.execute("select id from projeler where projeadi = '" + projeadi + "'")
            projeid = self.cursor.fetchone()[0]
            self.cursor.execute(
                "delete from projedetaylari where firmaid = '" + str(firmaid) + "' and projeid = '" + str(
                    projeid) + "' and teknikresimnoid = '" + str(teknikresimid) + "'")
            self.conn.commit()
            QtWidgets.QMessageBox.information(self, "BAŞARILI", "PROJE VE TEKNİK RESİM İLİŞKİLENDİRİLMESİ İPTAL EDİLDİ")
            self.arsivlistesiguncelle()

    def resimsilme(self):
        pass
        try:
            satir = self.arananlar.currentRow()
            parcaadi = self.arananlar.item(satir, 1).text()
            teknikresimno = self.arananlar.item(satir, 2).text()
            self.cursor.execute(
                "delete from teknikresimler where teknikresimno = '" + teknikresimno + "' and parcaadi = '" + parcaadi + "'")
            self.conn.commit()
            QtWidgets.QMessageBox.information(self, "BAŞARILI", "SİLME İŞLEMİ GERÇEKLEŞTİRİLDİ")
        except:
            QtWidgets.QMessageBox.warning(self, "HATA",
                                          "BU KAYIT SİLİNEMİYOR OLASI SEBEPLER :  \n ==>RESİM NUMARASIYLA DAHA ÖNCE İŞ EMRİ OLUŞTURULMUŞ OLABİLİR \n ==>ÜRETİM EMRİYLE İLİŞKİLENDİRİLMİŞ OLABİLİR \n ==> HERHANGİ BİR PROJE İLE "
                                          "İLİŞKİLENDİRİLMİŞ OLABİLİR \n LÜTFEN KONTROL EDİNİZ")
            return
        self.setotomatiknumara()
        self.numaradanarama()
        self.isimdenarama()

    def setotomatiknumara(self):
        pass
        self.cursor.execute(
            "select firma_id from firmalar where firma_adi = '" + self.eklenecekfirma.currentText() + "'")
        firmaid = self.cursor.fetchone()[0]
        self.cursor.execute(
            "select count(*) from teknikresimler where firmaid = '" + str(firmaid) + "'")
        toplamresimsayisi = int(self.cursor.fetchone()[0])
        self.otomatiknumara.setText("0" + str(firmaid) + "_" + str(toplamresimsayisi + 1))

    def genelarsiveresimekle(self):
        pass
        eklenecekparcaadi = self.eklenecekparca.text()
        eklenecekrevno = self.eklenecekrevno.text()
        if len(eklenecekrevno) == 0 or len(eklenecekparcaadi) == 0:
            QtWidgets.QMessageBox.warning(self, "HATA ", "PARCA ADI VEYA REVNO BOŞ BIRAKILMAMALDIRI")
            return
        else:
            self.cursor.execute(
                "select count(*) from teknikresimler  where teknikresimno = '" + self.otomatiknumara.text() + "'")
            bulunanresimno = (self.cursor.fetchone()[0])
            self.cursor.execute(
                "select count(*) from teknikresimler where parcaadi = '" + self.eklenecekparca.text() + "'")
            bulunanparcaadi = (self.cursor.fetchone()[0])
            if bulunanresimno != 0:
                QtWidgets.QMessageBox.warning(self, "HATA", "BU NUMARA İLE DAHA ÖNCE KAYIT GERÇEKLEŞTİRİLMİŞ")
                return
            elif bulunanparcaadi != 0:
                QtWidgets.QMessageBox.warning(self, "HAT", "BU PARCA ADI İLE DAHA ÖNCE KAYIT GERÇEKLEŞTİRİLMİŞ")
                return
            else:

                try:
                    self.cursor.execute(
                        "select id from firmalar where firma_adi = '" + self.eklenecekfirma.currentText() + "'")
                    firmaid = self.cursor.fetchone()[0]
                    tarih = self.setdate.text()
                    self.cursor.execute("start transaction")
                    self.cursor.execute(
                        "insert into teknikresimler (firmaid, teknikresimno , revno , parcaadi ,tarih) values (%s, %s ,%s ,%s, %s)",
                        (firmaid, self.otomatiknumara.text(), self.eklenecekrevno.text(), self.eklenecekparca.text(),
                         tarih))
                    QtWidgets.QMessageBox.information(self, "BAŞARILI", "TEKNİK RESİM GENEL ARŞİV LİSTESİNE EKLENDİ")
                except:
                    QtWidgets.QMessageBox.warning(self, "HATTA", "BEKLENMYEN BİR PROBLEM İLE KARŞI KARŞIYA KALINDI")
                    self.cursor.execute("rollback")
        self.conn.commit()
        self.setotomatiknumara()
        self.eklenecekparca.clear()
        self.eklenecekrevno.clear()

    def setresimparametreleri(self):
        pass
        satir = self.arananlar.currentRow()
        # firmaadi = self.arananlar.item(satir , 0).text()
        try:
            parcaadi = self.arananlar.item(satir, 1).text()
            teknikresimno = self.arananlar.item(satir, 2).text()
            revno = self.arananlar.item(satir, 3).text()
        except:
            QtWidgets.QMessageBox.warning(self, "HATA", "DATA BULUNAMADI")
            return
        self.teknikresimno_2.setText(teknikresimno)
        self.parcaadi_2.setText(parcaadi)
        self.revno.setText(revno)
        self.onizleme2()

    def numaradanarama(self):
        pass
        self.parcaadi.clear()
        self.cursor.execute(
            "select * from teknikresimgorunumu   where firma_adi = '" + self.firmaadi.currentText() + "' and teknikresimno like '%" + self.teknikresimno.text() + "%'")
        bulunanlar = self.cursor.fetchall()
        self.arananlar.setRowCount(len(bulunanlar))
        for satir, deger in enumerate(bulunanlar):
            self.arananlar.setItem(satir, 0, QtWidgets.QTableWidgetItem(deger[0]))
            self.arananlar.setItem(satir, 1, QtWidgets.QTableWidgetItem(deger[1]))
            self.arananlar.setItem(satir, 2, QtWidgets.QTableWidgetItem(str(deger[2])))
            self.arananlar.setItem(satir, 3, QtWidgets.QTableWidgetItem(deger[3]))
            self.arananlar.setItem(satir, 4, QtWidgets.QTableWidgetItem(str(deger[4])))

    def isimdenarama(self):
        pass
        self.teknikresimno.clear()
        self.cursor.execute(
            "select firma_adi , parcaadi , teknikresimno, revno from teknikresimgorunumu where firma_adi = '" + self.firmaadi.currentText() + "' and parcaadi like '%" + self.parcaadi.text() + "%'")
        bulunanlar = self.cursor.fetchall()
        self.arananlar.setRowCount(len(bulunanlar))
        for satir, deger in enumerate(bulunanlar):
            self.arananlar.setItem(satir, 0, QtWidgets.QTableWidgetItem(deger[0]))
            self.arananlar.setItem(satir, 1, QtWidgets.QTableWidgetItem(deger[1]))
            self.arananlar.setItem(satir, 2, QtWidgets.QTableWidgetItem(str(deger[2])))
            self.arananlar.setItem(satir, 3, QtWidgets.QTableWidgetItem(deger[3]))

    def projeekleme(self):
        pass
        firmaadi = self.firmaadi_2.currentText()
        projeadi = self.projeadi.text()
        self.cursor.execute("select firma_id from firmalar where firma_adi = '" + firmaadi + "'")
        fetch = self.cursor.fetchone()
        try:
            self.cursor.execute("SELECT COUNT(*) FROM projeler where projeadi = '" + projeadi + "'")
            sonuc = (self.cursor.fetchone()[0])
            if sonuc != 0:
                QtWidgets.QMessageBox.warning(self, "HATA", "PROJE ADI DAHA ONCE OLUŞTURULMUS FARKLI BİR AD KULLANIN")
                return
        except:
            QtWidgets.QMessageBox.warning(self, "hata", "BAĞLANTI PROBLEMİ")
        try:
            if len(projeadi) == 0:
                QtWidgets.QMessageBox.warning(self, "!!!!", "LÜTFEN PROJE ADI GİRİNİZ")
                return
            self.cursor.execute("INSERT INTO projeler (projeadi , firmaid) values (%s, %s)", (projeadi, fetch[0]))
            self.conn.commit()
            QtWidgets.QMessageBox.information(self, "BAŞARILI", "PROJE BAŞARIYLA ARŞİVE EKLENDİ ")
        except:
            QtWidgets.QMessageBox.information(self, "!!!", "BEKLENMEDIK BIR HATA ILE KARSILASILDI")
        self.projeadi.clear()

    def projefiltreleme(self):
        pass
        self.projeler.clear()
        projefiltre = self.firmaadi.currentText()
        self.cursor.execute("select firma_id from firmalar where firma_adi = '" + projefiltre + "'")
        fetch = self.cursor.fetchone()
        projefiltreid = str(fetch[0])
        self.cursor.execute("SELECT proje_adi from projeler where FIRMALAR_firma_id  = '" + projefiltreid + "'")
        projeler = self.cursor.fetchall()
        for i in projeler:
            self.projeler.addItems(i)
        self.isimdenarama()
        self.numaradanarama()

    def arsivlistesiguncelle(self):
        pass
        self.comboBox_2.clear()
        projefiltre = self.comboBox.currentText()
        self.cursor.execute("select firma_id from firmalar where firma_adi = '" + projefiltre + "'")
        fetch = self.cursor.fetchone()
        projefiltreid = str(fetch[0])
        self.cursor.execute("SELECT proje_adi from projeler where FIRMALAR_firma_id  = '" + projefiltreid + "'")
        projeler = self.cursor.fetchall()
        for i in projeler:
            self.comboBox_2.addItems(i)
        filtre = self.comboBox.currentText()
        filtre2 = self.comboBox_2.currentText()
        self.cursor.execute(
            "select * from projedetaylarigorunumu where firma_adi = '" + filtre + "' and projeadi = '" + filtre2 + "'")
        arsivlistesi = self.cursor.fetchall()
        self.teknikresimlistesi.setRowCount(len(arsivlistesi))
        # self.teknikresimlistesi.setEditTriggers(QtGui.Qabstrac)
        for satir, deger in enumerate(arsivlistesi):
            self.teknikresimlistesi.setItem(satir, 0, QtWidgets.QTableWidgetItem(deger[0]))
            self.teknikresimlistesi.setItem(satir, 1, QtWidgets.QTableWidgetItem(deger[1]))
            self.teknikresimlistesi.setItem(satir, 2, QtWidgets.QTableWidgetItem(str(deger[2])))
            self.teknikresimlistesi.setItem(satir, 3, QtWidgets.QTableWidgetItem(deger[3]))
            self.teknikresimlistesi.setItem(satir, 4, QtWidgets.QTableWidgetItem(deger[4]))
            self.teknikresimlistesi.setItem(satir, 5, QtWidgets.QTableWidgetItem(str(deger[5])))

    def arsivlistesiprojeilefiltrele(self):
        pass
        pass
        filtre = self.comboBox.currentText()
        filtre2 = self.comboBox_2.currentText()
        self.cursor.execute(
            "select * from projedetaylarigorunumu where firma_adi = '" + filtre + "' and projeadi = '" + filtre2 + "'")
        arsivlistesi = self.cursor.fetchall()
        self.teknikresimlistesi.setRowCount(len(arsivlistesi))
        for satir, deger in enumerate(arsivlistesi):
            self.teknikresimlistesi.setItem(satir, 0, QtWidgets.QTableWidgetItem(deger[0]))
            self.teknikresimlistesi.setItem(satir, 1, QtWidgets.QTableWidgetItem(deger[1]))
            self.teknikresimlistesi.setItem(satir, 2, QtWidgets.QTableWidgetItem(str(deger[2])))
            self.teknikresimlistesi.setItem(satir, 3, QtWidgets.QTableWidgetItem(deger[3]))
            self.teknikresimlistesi.setItem(satir, 4, QtWidgets.QTableWidgetItem(deger[4]))

    def resimekle(self):
        pass
        try:
            proje = self.projeler.currentText()
            self.cursor.execute("select id from projeler where projeadi = '" + proje + "'")
            projeid = self.cursor.fetchone()[0]
            teknikresimno = self.teknikresimno_2.text()
            self.cursor.execute("select id from teknikresimler where teknikresimno = '" + teknikresimno + "'")
            teknikresimnoid = self.cursor.fetchone()[0]
            revno = self.revno.text()
            firmaadi = self.firmaadi.currentText()
            parcaadi = self.parcaadi_2.text()
        except:
            QtWidgets.QMessageBox.warning(self, " HATA", "LÜTFEN SEÇİM YAPINIZ")
            return

        try:
            self.cursor.execute("select firma_id from firmalar where firma_adi = '" + firmaadi + "'")
            firmaid = self.cursor.fetchone()[0]

            if len(teknikresimno) == 0 or len(parcaadi) == 0 or len(revno) == 0:
                QtWidgets.QMessageBox.warning(self, "!!!!!",
                                              "TEKNİK RESİM NUMARASI, PARÇA ADI ve REVİZYON NUMARASI BOŞ BIRAKILAMAZ")
                return
            else:
                self.cursor.execute(
                    "select count(*) from projedetaylari where firmaid = '" + str(firmaid) + "' and projeid = '" + str(
                        projeid) + "' and teknikresimnoid = '" + str(teknikresimnoid) + "'")
                bulunansayi = self.cursor.fetchone()[0]
                if bulunansayi != 0:
                    QtWidgets.QMessageBox.warning(self, "HATA", "DAHA ÖNCE BUNA BENZER BİR KAYIT GERÇEKLEŞTİRİLMİŞ")
                    return
                else:
                    self.cursor.execute(
                        "INSERT INTO projedetaylari (firmaid , projeid ,teknikresimnoid) values (%s , %s, %s)",
                        (firmaid, projeid, teknikresimnoid))
                    self.conn.commit()
                    QtWidgets.QMessageBox.information(self, "BAŞARILI", "RESIM GEÇERLİ PROJE İLE İLİŞKİLENDİRİLDİ")
        except:
            QtWidgets.QMessageBox.information(self, "HATA", "BIR SORUNLA KARSILASILDI")
            return
