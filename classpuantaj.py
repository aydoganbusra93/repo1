#!/usr/bin/env python
# -*- coding:utf -8-*-

from PyQt5 import QtWidgets, QtCore, QtGui
from mysql.connector import (connection)
import puantaj
import classislemler

class Anaekran(QtWidgets.QDialog, puantaj.Ui_Dialog):  # ANA PENCEREYI CAGIRAN CLASS
    def __init__(self, parent=None):
        super(Anaekran, self).__init__(parent)
        self.setupUi(self)
        self.giris.clicked.connect(self.baglan)
        try:
            self.islem = classislemler.Islemler(self)
        except:
            QtWidgets.QMessageBox.warning(self, "HATA",
                                          "KART OKUYUCUDA VEYA DATABASE BAGLANTINIZDA PROBLEM OLABILIR \n KONTROL EDINIZ")
            return
        self.islem.sinyal.connect(self.kayit)
        self.guncelle.clicked.connect(self.tabwidgetguncelle)

    def closeEvent(self, event):
        try:
            self.islem.ser.close()
            return
        except:
            return

    def baglan(self):
        self.islem.start()

    def kayit(self, val1, val2, val3, val4):
        pixmap = QPixmap(val4)
        pixmap = pixmap.scaledToWidth(200)
        self.label_4.setPixmap(pixmap)
        self.label_4.setScaledContents(True)
        self.adsoyad.setText(val1)
        self.tarih.setText(val2)
        self.saat.setText(val3)
        time.sleep(0.5)

    def tabwidgetguncelle(self):
        # try :
        self.conn = connection.MySQLConnection(user='caner', password='13579', host='AKMILL2', database='puantaj')
        self.cursor = self.conn.cursor()
        self.isim = self.personeladi.currentText()
        self.nekadarzaman = self.sure.text()
        self.cursor.execute("Select giris, saat from takip where ad  ='" + self.isim + "'")
        datalar = self.cursor.fetchall()
        self.puantajwidget.setRowCount(len(datalar) / 2)
        x = 0
        a = 0
        while x + 1 < (len(datalar)):
            self.puantajwidget.setItem(a, 0, QtWidgets.QTableWidgetItem(str(datalar[x][0])))
            self.puantajwidget.setItem(a, 1, QtWidgets.QTableWidgetItem(str(datalar[x][1])))
            if x + 1 == len(datalar):
                self.puantajwidget.setItem(a, 2, QtWidgets.QTableWidgetItem(str(datalar[x][1])))
            else:
                self.puantajwidget.setItem(a, 2, QtWidgets.QTableWidgetItem(str(datalar[x + 1][1])))
                # mesai suresi hesaplama
                self.puantajwidget.setItem(a, 3, QtWidgets.QTableWidgetItem(str((datalar[x + 1][1]) - (datalar[x][1]))))
                x = x + 2
                a = a + 1
