#!/usr/bin/env python
# -*- coding:utf -8-*-
import datetime

from PyQt5 import QtWidgets, QtCore, QtGui
from mysql.connector import (connection)
import malzemetalebi
from PyQt5.QtGui import QPixmap
import os
import pdfkit
import exeltablecreator
from functools import partial
import subprocess
import math
import datetime

class Malzemetalebi(QtWidgets.QDialog, malzemetalebi.Ui_Dialog):
    def __init__(self, parent=None):
        super(Malzemetalebi, self).__init__(parent)
        self.setupUi(self)
        self.conn = connection.MySQLConnection(user='root', password='13579', host='localhost', database='AKMILL')
        self.cursor = self.conn.cursor(buffered=True)
        self.cursor.execute("SELECT firma_adi  FROM firmalar")
        firmalar = self.cursor.fetchall()
        for i in firmalar:
            self.firmaadi.addItems(i)
            self.firmaadi_3.addItems(i)

        self.firmaadi_3.currentIndexChanged.connect(partial (self.projelistesiguncelle, "firmaadi_3"))
        self.firmaadi.currentIndexChanged.connect(partial (self.projelistesiguncelle, "firmaadi"))

        self.projeadi.currentIndexChanged.connect(partial (self.malzemetalebisiparisdetaylariguncelle, "projeadi"))
        self.projeadi_3.currentIndexChanged.connect(partial (self.malzemetalebisiparisdetaylariguncelle, "projeadi_3"))

        self.siparisdetaylari.currentCellChanged.connect(partial(self.malzemetalebiuretimdetaylariguncelle ,"siparisdetaylari"))
        self.siparisdetaylari_3.currentCellChanged.connect(partial(self.malzemetalebiuretimdetaylariguncelle,  "siparisdetaylari_3"))

        self.uretimdetaylari.currentCellChanged.connect(self.malzemetalebipreview)
        self.uretimdetaylari_3.currentCellChanged.connect(self.malzemetalebipreviewform)

        self.malzemelistesi_3.currentCellChanged.connect(self.malzemelistesipreviewform)

        self.malzemelistesiguncelle()
        durum = self.borumalzeme.checkState()
        self.borumalzeme.stateChanged.connect(self.malzemegirislerinilimitle)
        self.capmalzeme.stateChanged.connect(self.malzemegirislerinilimitle)
        self.profil.stateChanged.connect(self.malzemegirislerinilimitle)
        self.projelistesiguncelle("firmaadi_3")
        self.projelistesiguncelle("firmaadi")



        self.talepnumarasial_3.clicked.connect(self.talepnumarasiolustur)




        now = datetime.datetime.now()
        date = now.strftime("%Y-%m-%d")
        self.termintarihi.setDate(now)
        self.termintarihi_3.setDate(now)

        self.ekle_3.clicked.connect(self.listeyeekle)
        self.uretimdetaylari.currentCellChanged.connect(self.malzemeuretimdetaysecimesitleme)
        self.malzemelistesi.doubleClicked.connect(self.malzemeduzeltme)
        self.talepsil.clicked.connect(self.talepsilme)
        self.talepsil_3.clicked.connect(self.talepsilme3)

        self.malzemelistesi.currentCellChanged.connect(self.uretimdetaymalzemeesitleme)
        # self.siparistalepformlistesi.currentCellChanged.connect(self.listegorunumu)
        self.pushButton.clicked.connect(self.siparisformuhazirla)
        # self.talepekrani.clicked.connect(self.talepgorunumu)
        # self.ara.clicked.connect(self.arama)
        # self.malzemelistesi.currentCellChanged.connect(self.getselection)
        # self.sil.clicked.connect(self.silme)
        self.kaydet.clicked.connect(self.formlarihazirla)
        # self.siparistalepformlistesigorunumu()
        # self.formac.clicked.connect(self.formuac)
        # self.formsil.clicked.connect(self.formusil)
        # self.parcalistesiguncelle()
        self.uretimdetaylari_3.setDragDropOverwriteMode(False)
        self.malzemelistesi_3.setDragDropOverwriteMode(False)

        self.installEventFilter(self.uretimdetaylari_3)
        self.talepekle.clicked.connect(self.checkedboxeslist)

        self.malzemelistesi_3.doubleClicked.connect(self.resmiac)
        self.kaydet_3.clicked.connect(self.talepkaydet)
        # self.kaydet_3.clicked.connect(self.formhazirla)



    def talepkaydet(self):
        satirsayisi = self.malzemelistesi_3.rowCount()
        if satirsayisi == 0 :
            QtWidgets.QMessageBox.warning(self, "HATA", "KAYIT OLUSTURMAK ICIN EN AZ BIR MALZEME SECILMIS OLMALIDIR")
            return
        elif len(self.malzemetalepnumarasi_3.text()) == 0 :
            QtWidgets.QMessageBox.warning(self, "HATA", "KAYIT OLUSTURMAK ICIN TALEP NUMARASI ALMANI GEREKMEKTEDIR")
            return

        else :
            try:
                for i in range(0, satirsayisi):
                    malzemetalepid = self.malzemelistesi_3.item(i,17).text()
                    seciliuretimemri = self.siparisdetaylari_3.currentRow()
                    uretimemrino = self.siparisdetaylari_3.item(seciliuretimemri,0).text()
                    self.cursor.execute("select uretim_emir_id from uretimemirleri where uretim_emir_no = '"+uretimemrino+"'")
                    uretimemriid = self.cursor.fetchone()[0]
                    uretimdetayid = int(self.malzemelistesi_3.item(i,6).text())
                    toplamagirlik = float(self.malzemelistesi_3.item(i,20).text())


                    malzemetalepformnumarasi = self.malzemetalepnumarasi_3.text().split("-")[3]
                    siparisadedi = int(self.malzemelistesi_3.item(i,3).text())

                    self.cursor.execute("start transaction")
                    self.cursor.execute("insert into malzemetalepformlari (malzemetalep_id,uretimemirid,malzemetalepformnumarasi,siparisadedi, uretimdetay_id, toplamaigrlik) values (%s, %s, %s, %s, %s, %s)",
                                        (malzemetalepid, uretimemriid, malzemetalepformnumarasi, siparisadedi, uretimdetayid, toplamagirlik))
            except:
                self.cursor.execute("rollback")
                QtWidgets.QMessageBox.warning(self, "HATA", "BİR HATA İLE KARSILASILDI ISLEMLER GERI ALINDI")
                import traceback
                traceback.print_exc()
            self.conn.commit()
            QtWidgets.QMessageBox.information(self, "BASARILI", "TALEP EKLEME ISLEMI TAMAMLANDI")
            self.formhazirla()

    def resmiac(self):
        malzemesatir = self.malzemelistesi_3.currentRow()
        malzemesutun = self.malzemelistesi_3.currentColumn()

        if malzemesatir == -1 :
            return
        elif malzemesutun == 1 : #AKMILL TEKNIK RESMI AC
            akresim =self.malzemelistesi_3.item(malzemesatir, 7).text()
            if len(akresim) == 0 :
                QtWidgets.QMessageBox.warning(self, "HATA", "KAYITLI RESIM BULUNMAMAKTADIR")
            else :
                subprocess.Popen(akresim, shell=True)
        elif malzemesutun == 2 : # MUSTERI RESIM AC
            musteriresim =self.malzemelistesi_3.item(malzemesatir, 9).text()
            if len(musteriresim) == 0 :
                QtWidgets.QMessageBox.warning(self, "HATA", "KAYITLI RESIM BULUNMAMAKTADIR")
            else :
                subprocess.Popen(musteriresim, shell=True)
        elif malzemesutun == 3 :



            inp = QtWidgets.QInputDialog()
            inp.setInputMode(QtWidgets.QInputDialog.TextInput)
            text, ok = inp.getText(self, 'PARÇA MİKTARI DEĞİŞTİR', 'YENİ ADET')
            if ok:
                if len(text) == 0:
                    QtWidgets.QMessageBox.warning(self, "HATA", "ALAN BOŞ BIRAKILAMAZ")
                    return
                else:
                    try:
                        yeniadet = int(text)
                        if yeniadet == 0 :
                            QtWidgets.QMessageBox.warning(self, "HATA", "MİKTAR SIFIR OLAMAZ")
                            return
                        self.malzemelistesi_3.setItem(malzemesatir, 3, QtWidgets.QTableWidgetItem(text))
                        QtWidgets.QMessageBox.information(self, "BİLDİRİM", "YENİ ADET BELİRLENDİ")
                        strbirimaigrlik = self.malzemelistesi_3.item(malzemesatir, 19).text()
                        floatbirimagirlik = float(strbirimaigrlik)
                        toplamagirlik = floatbirimagirlik * yeniadet
                        self.malzemelistesi_3.setItem(malzemesatir, 20, QtWidgets.QTableWidgetItem(str(toplamagirlik)))



                    except:
                        QtWidgets.QMessageBox.warning(self, "HATA","GEÇERLİ BİR SAYI GİRİNİZ")








        else:
            QtWidgets.QMessageBox.warning(self, "HATA" , "DUZENLEME ISLEMI GERCEKLESTIREMEZSINIZ")


    def uretimdetaymalzemeesitleme(self):
        try:
            malzemesatir = self.malzemelistesi.currentRow()
            uretimdetaysatir = self.uretimdetaylari.currentRow()
            if malzemesatir == -1 or uretimdetaysatir == -1:
                pass
            else:
                malzemeuretimdetayid = self.malzemelistesi.item(malzemesatir, 7).text()
                uretimdetayid = self.uretimdetaylari.item(uretimdetaysatir, 5).text()
                print uretimdetayid, malzemeuretimdetayid
                if malzemeuretimdetayid == uretimdetayid:
                    print("esitlik mevcut")
                    return
                else:
                    pass
            uretimdetaylari = self.uretimdetaylari.rowCount()
            malzemedetaylari = self.malzemelistesi.rowCount()
            if uretimdetaylari == 0:
                return
            else:
                try:
                    malzemeuretimdetayid = self.malzemelistesi.item(malzemesatir, 7).text()
                    print ("malzeme uretimdetayid :: " + malzemeuretimdetayid)
                    for i in range(0, uretimdetaylari):
                        if self.uretimdetaylari.item(i, 5).text() == malzemeuretimdetayid:
                            self.uretimdetaylari.selectRow(i)
                            print i
                            self.uretimdetaylari.selectRow(i)

                            break
                        else:
                            continue
                    # self.malzemelistesi.clearSelection()

                    # self.malzemelistesi.clearSelection()
                except:
                    import traceback
                    traceback.print_exc()
            pass
        except:
            import traceback
            traceback.print_exc()

    def talepsilme3(self):
        secilisatir = self.malzemelistesi_3.currentRow()
        if secilisatir == -1 :
            QtWidgets.QMessageBox.warning(self, "HATA", "SEÇİM YAPMANIZ GEREKMEKTEDİR")
            return
        else:
            self.malzemelistesi_3.removeRow(secilisatir)
            QtWidgets.QMessageBox.information(self, "BİLDİRİM", "SEÇMİŞ OLDUĞUNUZ MALZEME TALEBİ LİSTEDEN KALDIRILDI")

    def talepsilme(self):

        satir = self.malzemelistesi.currentRow()
        if satir == -1:
            QtWidgets.QMessageBox.warning(self, "HATA", "SEÇİM YAPMANIZ GEREKMEKTEDİR")
        else:
            pass
            try:
                self.cursor.execute("start transaction")
                self.cursor.execute(
                    "delete from malzemetalepleri where idmalzemetalepleri = '" + self.malzemelistesi.item(satir,
                                                                                                           9).text() + "'")
                QtWidgets.QMessageBox.information(self, " BİLDİRİM", "SİLME İŞLEMİ GERÇEKLEŞTİRİLDİ")

            except:
                QtWidgets.QMessageBox.warning(self, "HATA",
                                              "BEKLENMEDİK BİR HATA İLE KARŞILAŞILDI İŞLEMLER GERİ ALINIYOR...")
                self.cursor.execute("rollback")

        self.conn.commit()
        self.malzemetalepleriguncelle()

    def malzemeduzeltme(self):
        QtWidgets.QMessageBox.warning(self, "HATA",
                                      "DEĞİŞİKLİK YAPMANIZ GEREKİYORSA İLGİLİ TALEBİ SİLİP YENİDEN EKLEMENİZ GEREKMEKTEDİR")
        return

    def malzemeuretimdetaysecimesitleme(self):
        malzemesatir = self.malzemelistesi.currentRow()
        uretimdetaysatir = self.uretimdetaylari.currentRow()
        if malzemesatir == -1 or uretimdetaysatir == -1:
            pass
        else:
            malzemeuretimdetayid = self.malzemelistesi.item(malzemesatir, 7).text()
            uretimdetayid = self.uretimdetaylari.item(uretimdetaysatir, 5).text()
            if malzemeuretimdetayid == uretimdetayid:
                print("esitlik mevcut")
                return
            else:
                pass
        try:
            malzemetalepleri = self.malzemelistesi.rowCount()
            if malzemetalepleri == 0:
                return
            else:
                try:
                    uretimdetaysatir = self.uretimdetaylari.currentRow()
                    uretimdetayid = self.uretimdetaylari.item(uretimdetaysatir, 5).text()
                    for i in range(0, malzemetalepleri):
                        if self.malzemelistesi.item(i, 7).text() == uretimdetayid:
                            self.malzemelistesi.selectRow(i)
                            return
                        else:
                            self.malzemelistesi.clearSelection()
                            continue
                    # self.malzemelistesi.clearSelection()

                    # self.malzemelistesi.clearSelection()
                except:
                    import traceback
                    traceback.print_exc()

            pass
        except:
            import traceback
            traceback.print_exc()

    def talepnumarasiolustur(self):
        satir =  self.siparisdetaylari_3.currentRow()
        if satir == -1 :
            QtWidgets.QMessageBox.warning(self, "HATA", "URETIM SIPARIS DETAYI SECIMI YAPMANIZ GEREKMEKTEDIR")
            return
        else :
            uretimemirno = self.siparisdetaylari_3.item(satir, 0).text()
            self.cursor.execute("select uretim_emir_id from uretimemirleri where uretim_emir_no = '"+uretimemirno+"'")
            uretimid = self.cursor.fetchone()[0]
            print uretimid
            uretimnumarasi  = uretimemirno.split("-")[3]
            self.year = datetime.datetime.now().strftime('%Y')
            yil = str(self.year[2:])
            firmaadi = self.firmaadi_3.currentText()

        try:
            self.cursor.execute("select malzemetalepformnumarasi from malzemetalepformlari where uretimemirid = '"+str(uretimid)+"' order by id desc")

            sonnumara = self.cursor.fetchone()[0]
            print "son numara :::  " + str(sonnumara)
            siradakinumara = sonnumara +1
        except TypeError:
            import traceback
            traceback.print_exc()
            siradakinumara = 1

        try:

                self.malzemetalepnumarasi_3.setText("PR-" + yil+ "-"+ uretimnumarasi + "-" + str(siradakinumara))
                pass
        except:
            import traceback
            traceback.print_exc()

    def projelistesiguncelle(self,a):
        if a == "firmaadi":

            self.siparisdetaylari.setRowCount(0)
            self.uretimdetaylari.setRowCount(0)
            self.projeadi.clear()
            firmaadi = self.firmaadi.currentText()


        elif a == "firmaadi_3":
            self.uretimdetaylari_3.setRowCount(0)
            self.siparisdetaylari_3.setRowCount(0)
            self.malzemelistesi_3.setRowCount(0)
            self.projeadi_3.clear()
            firmaadi = self.firmaadi_3.currentText()


        try:
            self.cursor.execute("select firma_id from firmalar where firma_adi = '" + firmaadi + "'")
            firmaid = self.cursor.fetchone()[0]
            self.cursor.execute("SELECT proje_adi from projeler where FIRMALAR_firma_id = '" + str(firmaid) + "'")
            projeler = self.cursor.fetchall()
        except:
            import traceback
            traceback.print_exc()
            QtWidgets.QMessageBox.warning(self, "HATA", "BAGLANTI PROBLEMI OLABILIR KONTROL EDINIZ")
        try:

            if a == "firmaadi":
                for i in projeler:
                    self.projeadi.addItems(i)
            elif a == "firmaadi_3":
                for i in projeler:
                    self.projeadi_3.addItems(i)
        except:
            QtWidgets.QMessageBox.information(self, "BİR SORUNLA KARŞILAŞILDI", "TANIMLI HERHANGİ BİR "
                                                                                "PROJE VEYA FİRMA BULUNMAMAKTADIR \n LÜTFEN FİRMA VEYA PROJE TANIMLAMASINI GERÇEKLEŞTİRİNİZ")
            import traceback
            traceback.print_exc()

    def malzemetalebisiparisdetaylariguncelle(self,b):
        if b == "projeadi" :
            if len(self.projeadi.currentText()) == 0:
                return
            else:
                projeadi = self.projeadi.currentText()
                self.siparisdetaylari.setRowCount(0)
                self.uretimdetaylari.setRowCount(0)
                self.siparisdetaylari.clearContents()

        if b == "projeadi_3" :
            if len(self.projeadi_3.currentText()) == 0:
                return
            else:
                projeadi = self.projeadi_3.currentText()
                self.siparisdetaylari_3.setRowCount(0)
                self.uretimdetaylari_3.setRowCount(0)
                self.siparisdetaylari_3.clearContents()
                self.malzemelistesi_3.clearContents()
                self.malzemelistesi_3.setRowCount(0)

        try:
            self.cursor.execute("select proje_id from projeler where proje_adi = '" + projeadi + "'")
            projeid = self.cursor.fetchone()[0]
            self.cursor.execute("select uretim_emir_no from uretimemirleri where PROJELER_proje_id = '" + str(projeid) + "'")
            uretimemirnumarasi = self.cursor.fetchone()[0]
            self.cursor.execute("select uretim_emir_id,FIRMALAR_firma_id, PROJELER_proje_id from uretimemirleri where uretim_emir_no = '" + uretimemirnumarasi + "'")
            uretimfirmaprojeid = self.cursor.fetchall()
            uretimid = str(uretimfirmaprojeid[0][0])
            firmaid = str(uretimfirmaprojeid[0][1])
            projeid = str(uretimfirmaprojeid[0][2])
            self.cursor.execute(
                "select uretimemirleri.uretim_emir_no, firmalar.firma_adi , projeler.proje_adi, malzeme_tanimi, miktar, siparis_detay_id from siparisdetaylari  "
                "join uretimemirleri on  uretim_emir_id= '" + uretimid + "'"
                                                                         "join projeler on projeler.proje_id = '" + projeid + "' join firmalar on  firmalar.firma_id = '" + firmaid + "' where siparisdetaylari.URETIMEMIRLERI_uretim_emir_id = '" + uretimid + "'")
            siparisdetaylari = self.cursor.fetchall()

            # todo burada kaldin
        except:
            import traceback
            traceback.print_exc()
            QtWidgets.QMessageBox.warning(self, "HATA", "BAGLANTI PROBLEMI OLABILIR KONTROL EDINIZ")
            return
        try:
            if b == "projeadi":
                self.siparisdetaylari.setRowCount(len(siparisdetaylari))
                for row, value in enumerate(siparisdetaylari):
                    self.siparisdetaylari.setItem(row, 0, QtWidgets.QTableWidgetItem(value[0]))
                    self.siparisdetaylari.setItem(row, 1, QtWidgets.QTableWidgetItem(value[1]))
                    self.siparisdetaylari.setItem(row, 2, QtWidgets.QTableWidgetItem(value[2]))
                    self.siparisdetaylari.setItem(row, 3, QtWidgets.QTableWidgetItem(value[3]))
                    self.siparisdetaylari.setItem(row, 4, QtWidgets.QTableWidgetItem(str(value[4])))
                    self.siparisdetaylari.setItem(row, 5, QtWidgets.QTableWidgetItem(str(value[5])))
                    self.siparisdetaylari.setColumnHidden(5, False)
            elif b == "projeadi_3":
                self.siparisdetaylari_3.setRowCount(len(siparisdetaylari))
                for row, value in enumerate(siparisdetaylari):
                    self.siparisdetaylari_3.setItem(row, 0, QtWidgets.QTableWidgetItem(value[0]))
                    self.siparisdetaylari_3.setItem(row, 1, QtWidgets.QTableWidgetItem(value[1]))
                    self.siparisdetaylari_3.setItem(row, 2, QtWidgets.QTableWidgetItem(value[2]))
                    self.siparisdetaylari_3.setItem(row, 3, QtWidgets.QTableWidgetItem(value[3]))
                    self.siparisdetaylari_3.setItem(row, 4, QtWidgets.QTableWidgetItem(str(value[4])))
                    self.siparisdetaylari_3.setItem(row, 5, QtWidgets.QTableWidgetItem(str(value[5])))
                    self.siparisdetaylari_3.setColumnHidden(5, False)
        except:
            import traceback
            traceback.print_exc()

    def malzemetalebiuretimdetaylariguncelle(self,a):
        if a == "siparisdetaylari":
            satir = self.siparisdetaylari.currentRow()
            if satir == -1:
                return
            siparisdetayid = self.siparisdetaylari.item(satir, 5).text()
        elif a == "siparisdetaylari_3":
            satir = self.siparisdetaylari_3.currentRow()
            if satir == -1:
                return
            siparisdetayid = self.siparisdetaylari_3.item(satir, 5).text()
        try:
            self.cursor.execute(
                "select akmill_teknik_resim_no , musteri_teknik_resim_no, adet,  operasyon_sayisi, malzeme_adi ,uretim_detaylandirma_id, akmillteknikresimleri.dosya_yolu,"
                "akmillteknikresimleri.dosya_yolu_onizleme, musteriteknikresimleri.dosya_yolu  , musteriteknikresimleri.dosya_yolu_onizleme from uretimdetaylandirma "
                "left join akmillteknikresimleri on akmillteknikresimleri.akmill_teknikresim_id =uretimdetaylandirma.akmillteknikresimleri_akmill_teknikresim_id "
                "left join musteriteknikresimleri on musteriteknikresimleri.musteri_teknikresim_id = uretimdetaylandirma.musteriteknikresimleri_musteri_teknikresim_id "
                "left join  malzemelistesi on malzemelistesi.malzeme_id = uretimdetaylandirma.malzemelistesi_malzeme_id where uretimdetaylandirma.siparisdetaylari_siparis_detay_id = '" + str(
                    siparisdetayid) + "'")
            uretimdetaylarilistesi = self.cursor.fetchall()
        except:
            import traceback
            traceback.print_exc()
            QtWidgets.QMessageBox.warning(self, "HATA", "BAGLANTI PROBLEMI OLABILIR KONTROL EDINIZ")
        try:
            if a == "siparisdetaylari":
                self.uretimdetaylari.setRowCount(len(uretimdetaylarilistesi))
                for row, value in enumerate(uretimdetaylarilistesi):
                    self.uretimdetaylari.setItem(row, 0, QtWidgets.QTableWidgetItem(value[0]))  # AKMILL RESIM NO
                    self.uretimdetaylari.setItem(row, 1, QtWidgets.QTableWidgetItem(value[1]))  # MUSTERI RESIM NO
                    self.uretimdetaylari.setItem(row, 2, QtWidgets.QTableWidgetItem(str(value[2])))  # ADET
                    self.uretimdetaylari.setItem(row, 3, QtWidgets.QTableWidgetItem(str(value[3])))  # OP SAYISI
                    self.uretimdetaylari.setItem(row, 4, QtWidgets.QTableWidgetItem(value[4]))  # MALZEME ADI
                    self.uretimdetaylari.setItem(row, 5, QtWidgets.QTableWidgetItem(str(value[5])))  # URETIMDETAYID
                    self.uretimdetaylari.setItem(row, 6, QtWidgets.QTableWidgetItem(value[6]))  # MALZEME ADI
                    self.uretimdetaylari.setItem(row, 7, QtWidgets.QTableWidgetItem(value[7]))  # MALZEME ADI
                    self.uretimdetaylari.setItem(row, 8, QtWidgets.QTableWidgetItem(value[8]))  # MALZEME ADI
                    self.uretimdetaylari.setItem(row, 9, QtWidgets.QTableWidgetItem(value[9]))  # MALZEME ADI
                    self.cursor.execute("select parca_adi from akmillteknikresimleri where akmill_teknik_resim_no = '" + str(value[0]) + "'")
                    parcaadi = self.cursor.fetchone()[0]
                    self.uretimdetaylari.setItem(row, 10, QtWidgets.QTableWidgetItem(parcaadi))
                    self.uretimdetaylari.setColumnHidden(5, True)
                    self.uretimdetaylari.setColumnHidden(6, True)
                    self.uretimdetaylari.setColumnHidden(7, True)
                    self.uretimdetaylari.setColumnHidden(8, True)
                    self.uretimdetaylari.setColumnHidden(9, True)
                    # self.cursor.execute("select distinct termintarihi from malzemetalepleri where siparisdetaylari_siparis_detay_id= '" + str(siparisdetayid) + "'")
                    # termintarihi = self.cursor.fetchall()[0][0]
                    # self.termintarihi.setDate(termintarihi)
                self.malzemetalepleriguncelle()

            elif a == "siparisdetaylari_3":
                self.uretimdetaylari_3.setRowCount(len(uretimdetaylarilistesi))
                self.cblist = []
                for i in range(0, len(uretimdetaylarilistesi)):
                    self.cblist.append("cb" + str(i))
                for row, value in enumerate(uretimdetaylarilistesi):
                    self.cblist[row] = QtWidgets.QCheckBox(parent=self.uretimdetaylari_3)
                    self.uretimdetaylari_3.setCellWidget(row, 0, self.cblist[row])
                    self.uretimdetaylari_3.setItem(row, 1, QtWidgets.QTableWidgetItem(value[0]))  # AKMILL RESIM NO
                    self.uretimdetaylari_3.setItem(row, 2, QtWidgets.QTableWidgetItem(value[1]))  # MUSTERI RESIM NO
                    self.uretimdetaylari_3.setItem(row, 3, QtWidgets.QTableWidgetItem(str(value[2])))  # ADET
                    self.uretimdetaylari_3.setItem(row, 4, QtWidgets.QTableWidgetItem(str(value[3])))  # OP SAYISI
                    self.uretimdetaylari_3.setItem(row, 5, QtWidgets.QTableWidgetItem(value[4]))  # MALZEME ADI
                    self.uretimdetaylari_3.setItem(row, 6,QtWidgets.QTableWidgetItem(str(value[5])))  # URETIMDETAYID
                    self.uretimdetaylari_3.setItem(row, 7, QtWidgets.QTableWidgetItem(value[6]))  # MALZEME ADI
                    self.uretimdetaylari_3.setItem(row, 8, QtWidgets.QTableWidgetItem(value[7]))  # MALZEME ADI
                    self.uretimdetaylari_3.setItem(row, 9, QtWidgets.QTableWidgetItem(value[8]))  # MALZEME ADI
                    self.uretimdetaylari_3.setItem(row, 10, QtWidgets.QTableWidgetItem(value[9]))  # MALZEME ADI

        except:
            import traceback
            traceback.print_exc()
            return

        pass

    def malzemetalebipreviewform(self):
        try:
            satir = self.uretimdetaylari_3.currentRow()
            sutun = self.uretimdetaylari_3.currentColumn()
            if sutun == 1:
                path = self.uretimdetaylari_3.item(satir, 8).text()
                if len(path) == 0:
                    self.preview_3.setText("PREVIEW")
                else:
                    print "sutun1" + path

                    pixmap = QPixmap(path)
                    pixmap= pixmap.scaledToHeight(600)
                    # pixmap= pixmap.scaledToWidth(400)
                    self.preview_3.setScaledContents(True)
                    self.preview_3.setPixmap(pixmap)
                    print path
                    # self.preview.setFixedSize(580, 580)
            elif sutun == 2:
                path = self.uretimdetaylari_3.item(satir, 9).text()
                print "sutun2" + path
                # pixmap = QPixmap(path)
                if len(path) == 0:
                    self.preview_3.setText("PREVIEW")
                else:
                    pixmap = QPixmap(path)
                    pixmap= pixmap.scaledToHeight(600)
                    # pixmap= pixmap.scaledToWidth(400)
                    self.preview_3.setScaledContents(True)
                    self.preview_3.setPixmap(pixmap)
                    # self.preview.setFixedSize(580, 580)
            else:
                pass

        except:
            import traceback
            traceback.print_exc()
            return

        pass


    def malzemelistesipreviewform(self):
        try:
            satir = self.malzemelistesi_3.currentRow()
            sutun = self.malzemelistesi_3.currentColumn()
            if sutun == 1:
                path = self.malzemelistesi_3.item(satir, 8).text()
                if len(path) == 0:
                    self.preview_3.setText("AKMILL KAYNAKLI RESIM BULUNAMADI  ")
                else:
                    print "sutun1" + path
                    pixmap = QPixmap(path)
                    pixmap= pixmap.scaledToHeight(600)
                    # pixmap= pixmap.scaledToWidth(400)
                    self.preview_3.setScaledContents(True)
                    self.preview_3.setPixmap(pixmap)
                    print path
                    # self.preview.setFixedSize(580, 580)
            elif sutun == 2:
                path = self.malzemelistesi_3.item(satir, 10).text()
                print "sutun2" + path
                # pixmap = QPixmap(path)
                if len(path) == 0:
                    self.preview_3.setText("MUSTERI KAYNAKLI RESIM BULUNAMADI")
                else:
                    pixmap = QPixmap(path)
                    pixmap= pixmap.scaledToHeight(600)
                    # pixmap= pixmap.scaledToWidth(400)
                    self.preview_3.setScaledContents(True)
                    self.preview_3.setPixmap(pixmap)
                    # self.preview.setFixedSize(580, 580)
            else:
                pass

        except:
            import traceback
            traceback.print_exc()
            return

        pass



    def malzemetalebipreview(self):
        try:
            satir = self.uretimdetaylari.currentRow()
            if satir == -1 :
                self.adet.setValue(0)
                return
            sutun = self.uretimdetaylari.currentColumn()
            if sutun == 0:
                path = self.uretimdetaylari.item(satir, 7).text()
                if len(path) == 0:
                    self.preview.setText("PREVIEW")
                else:
                    pixmap = QPixmap(path)
                    pixmap= pixmap.scaledToHeight(600)
                    # pixmap= pixmap.scaledToWidth(400)
                    self.preview.setScaledContents(True)
                    self.preview.setPixmap(pixmap)
                    print path
                    # self.preview.setFixedSize(580, 580)
            elif sutun == 1:
                path = self.uretimdetaylari.item(satir, 9).text()
                pixmap = QPixmap(path)
                if len(path) == 0:
                    self.preview.setText("PREVIEW")
                else:
                    pixmap = QPixmap(path)
                    pixmap= pixmap.scaledToHeight(600)
                    # pixmap= pixmap.scaledToWidth(400)
                    self.preview.setScaledContents(True)
                    self.preview.setPixmap(pixmap)
                    # self.preview.setFixedSize(580, 580)
            else:
                pass

        except:
            import traceback
            traceback.print_exc()
            return
        seciliuretimdetaysatino = self.uretimdetaylari.currentRow()
        secilisiparisdetaysatirno = self.siparisdetaylari.currentRow()
        altparcaadedi = int(self.uretimdetaylari.item(seciliuretimdetaysatino, 2).text())
        takimadedi = int(self.siparisdetaylari.item(secilisiparisdetaysatirno,4).text())
        self.adet.setValue(int(altparcaadedi*takimadedi))


        pass

    def checkedboxeslist(self):
        try:
            secililer = []
            uretimdetayidlistesi = []

            satirsayisi = self.uretimdetaylari_3.rowCount()
        except:
            traceback.print_exc()
        if satirsayisi == None or satirsayisi == 0 :
            QtWidgets.QMessageBox.warning(self, "HATA", "HERHANGİ BİR ÜRETİMDETAYI BULUNAMAMIŞTIR")
        else :
            for i in range (0,satirsayisi) :
                if self.cblist[i].isChecked() :
                    secililer.append(i)


        adet = len(secililer)
        mevcutsatirsayisi = self.malzemelistesi_3.rowCount()
        liste = []

        try:
            if mevcutsatirsayisi > 0:
                for i in secililer:
                    for a in range(0, mevcutsatirsayisi):
                        if self.malzemelistesi_3.item(a, 6).text() == self.uretimdetaylari_3.item(i, 6).text():
                            QtWidgets.QMessageBox.warning(self, "HATA",
                                                          "SEÇMİŞ OLDUĞUNUZ ÜRÜN ZATEN LİSTEYE EKLENMİŞ BULUNMAKTADIR LÜTFEN KONTROL EDİNİZ")
                            return
                    self.cursor.execute(
                        "select olcux, olcuy, olcuz, malzemetipi, siparismistokmu, malzemeagirlik, idmalzemetalepleri from malzemetalepleri where uretimdetaylandirma_uretim_detaylandirma_id="
                        "'" + self.uretimdetaylari_3.item(i, 6).text() + "'")
                    sonuclar = self.cursor.fetchall()
                    liste.append(sonuclar)
                    print liste
                    kayitsayisi = len(sonuclar)
                    if kayitsayisi == 0:
                        QtWidgets.QMessageBox.warning(self, "HATA",
                                                      "SEÇMİŞ OLDUĞUNUZ TALEPLERDEN BİR VEYA DAHA FAZLASI İÇİN MALZEME TANIMLAMASI YAPILMAMIŞTIR KONTROL EDİNİZ")
                        return
            else:
                for i in secililer:
                    self.cursor.execute("select count(*) from malzemetalepformlari where uretimdetay_id = '"+self.uretimdetaylari_3.item(i, 6).text()+"'")
                    mevcutmu = self.cursor.fetchone()[0]
                    if mevcutmu > 0:
                        qm = QtWidgets.QMessageBox
                        parcanumarasi = self.uretimdetaylari_3.item(i,1).text()
                        ret = qm.question(self, '', "DAHA ÖNCE "+parcanumarasi+ " İLE İLGİLİ KAYIT OLUŞTURULMUŞ \n YENİDEN TALEP OLUŞTURMAK İSTİYOR MUSUNUZ?", qm.Yes | qm.No)
                        if ret == qm.Yes:
                            qm.information(self, 'BİLDİRİM', "EKLEME GERÇEKLEŞTİRİLECEK")

                        else:
                            qm.warning(self, 'BİLDİRİM', "EKLEME İPTAL EDİLDİ")
                            return 



                    self.cursor.execute(
                        "select olcux, olcuy, olcuz, malzemetipi, siparismistokmu, malzemeagirlik, idmalzemetalepleri from malzemetalepleri where uretimdetaylandirma_uretim_detaylandirma_id="
                        "'" + self.uretimdetaylari_3.item(i, 6).text() + "'")
                    sonuclar = self.cursor.fetchall()
                    liste.append(sonuclar)
                    print liste
                    kayitsayisi = len(sonuclar)
                    if kayitsayisi == 0:
                        QtWidgets.QMessageBox.warning(self, "HATA",
                                                      "SEÇMİŞ OLDUĞUNUZ TALEPLERDEN BİR VEYA DAHA FAZLASI İÇİN MALZEME TANIMLAMASI YAPILMAMIŞTIR KONTROL EDİNİZ")
                        return

            self.malzemelistesi_3.setRowCount(mevcutsatirsayisi + adet)
            for satir , i in enumerate(secililer):
                    print satir, i, secililer
                    print liste


                    olcux  = str(liste[satir][0][0])
                    olcuy  = str(liste[satir][0][1])
                    olcuz  = str(liste[satir][0][2])
                    malzemetipi = str(liste[satir][0][3])
                    siparismistokmu = str(liste[satir][0][4])
                    malzemetalepid = str(liste[satir][0][6])

                    self.malzemelistesi_3.setItem((mevcutsatirsayisi+satir),1,QtWidgets.QTableWidgetItem(self.uretimdetaylari_3.item(i,1).text())) #AKMILL RESIM NUMARASI
                    self.malzemelistesi_3.setItem((mevcutsatirsayisi+satir),2,QtWidgets.QTableWidgetItem(self.uretimdetaylari_3.item(i,2).text())) #MUSTERI RESIM NUMARASI
                    if len(self.uretimdetaylari_3.item(i,2).text()) != 0 :

                        self.cursor.execute("select parca_adi from musteriteknikresimleri where musteri_teknikresim_id = '"+self.uretimdetaylari_3.item(i,2).text()+"'")
                        muparcaadi = self.cursor.fetchone()[0]
                        self.malzemelistesi_3.setItem((mevcutsatirsayisi + satir), 18,QtWidgets.QTableWidgetItem(muparcaadi))  # MUSTERI PARCA ADI

                    yeniadetisayisi = int(self.uretimdetaylari_3.item(i,3).text()) * int(self.siparisdetaylari_3.item(self.siparisdetaylari_3.currentRow(), 4).text())

                    self.malzemelistesi_3.setItem((mevcutsatirsayisi+satir),3,QtWidgets.QTableWidgetItem(str(yeniadetisayisi))) #ADET
                    #
                    self.cursor.execute("select parca_adi from akmillteknikresimleri where akmill_teknik_resim_no = '"+self.uretimdetaylari_3.item(i,1).text()+"'")

                    parcaadi = self.cursor.fetchone()[0]

                    self.malzemelistesi_3.setItem((mevcutsatirsayisi+satir),4,QtWidgets.QTableWidgetItem(self.uretimdetaylari_3.item(i,4).text()))  #OPERASON SAYISI
                    self.malzemelistesi_3.setItem((mevcutsatirsayisi+satir),5,QtWidgets.QTableWidgetItem(self.uretimdetaylari_3.item(i,5).text()))  #MALZEME CINSI

                    self.cursor.execute("select malzemeagirlik, malzemetipi,aciklamalar from malzemetalepleri where uretimdetaylandirma_uretim_detaylandirma_id = '"+self.uretimdetaylari_3.item(i,6).text()+"'")
                    sonuc = self.cursor.fetchall()
                    birimagirlik = sonuc[0][0]
                    malzemetipi  = sonuc[0][1]
                    aciklamalar = sonuc[0][2]
                    self.malzemelistesi_3.setItem((mevcutsatirsayisi+satir),6,QtWidgets.QTableWidgetItem(self.uretimdetaylari_3.item(i,6).text()))  #URETIM ID
                    self.malzemelistesi_3.setItem((mevcutsatirsayisi+satir),7,QtWidgets.QTableWidgetItem(self.uretimdetaylari_3.item(i,7).text())) #AKMILL RESIM DOSYA YOLU
                    self.malzemelistesi_3.setItem((mevcutsatirsayisi+satir),8,QtWidgets.QTableWidgetItem(self.uretimdetaylari_3.item(i,8).text()))  #AKMILL RESIM ONIZLEME DOSYAY OLU
                    self.malzemelistesi_3.setItem((mevcutsatirsayisi+satir),9,QtWidgets.QTableWidgetItem(self.uretimdetaylari_3.item(i,9).text())) #MUSTERI RESIM DOSYA YOLU
                    self.malzemelistesi_3.setItem((mevcutsatirsayisi+satir),10,QtWidgets.QTableWidgetItem(self.uretimdetaylari_3.item(i,10).text())) #MUSTERI RESIM ONIZLEME DOSYA YOLU
                    self.malzemelistesi_3.setItem((mevcutsatirsayisi+satir),11,QtWidgets.QTableWidgetItem(parcaadi)) #PARCA ADI
                    self.malzemelistesi_3.setItem((mevcutsatirsayisi+satir),12,QtWidgets.QTableWidgetItem(olcux)) # PARCA OLCUSU X
                    self.malzemelistesi_3.setItem((mevcutsatirsayisi+satir),13,QtWidgets.QTableWidgetItem(olcuy)) #PARCA OLCUSU Y
                    self.malzemelistesi_3.setItem((mevcutsatirsayisi+satir),14,QtWidgets.QTableWidgetItem(olcuz)) #PARCA OLCUSU Z
                    self.malzemelistesi_3.setItem((mevcutsatirsayisi+satir),15,QtWidgets.QTableWidgetItem(malzemetipi)) # MALZEME TIPI
                    self.malzemelistesi_3.setItem((mevcutsatirsayisi+satir),16,QtWidgets.QTableWidgetItem(siparismistokmu)) #MALZEME TEDARIK TURU SİPARİŞ VEYA STOK
                    self.malzemelistesi_3.setItem((mevcutsatirsayisi+satir),17,QtWidgets.QTableWidgetItem(malzemetalepid)) #MALZEME TEDARIK TURU SİPARİŞ VEYA STOK
                    self.malzemelistesi_3.setItem((mevcutsatirsayisi+satir),19,QtWidgets.QTableWidgetItem(str(birimagirlik))) #BIRIM AGIRLIK
                    toplamagirlik = yeniadetisayisi * birimagirlik
                    self.malzemelistesi_3.setItem((mevcutsatirsayisi+satir),20,QtWidgets.QTableWidgetItem(str(toplamagirlik))) #TOPLAM AGIRLIK
                    self.malzemelistesi_3.setItem((mevcutsatirsayisi+satir),21,QtWidgets.QTableWidgetItem(str(malzemetipi))) #MALZEME TIPI
                    self.malzemelistesi_3.setItem((mevcutsatirsayisi+satir),22,QtWidgets.QTableWidgetItem(str(aciklamalar))) #EK ACIKLAMALAR








                    uretimdetayidlistesi.append(self.uretimdetaylari_3.item(i, 6).text())
            self.malzemelistesi_3.setColumnHidden(0, True)
            self.malzemelistesi_3.setColumnHidden(2, False)
            self.malzemelistesi_3.setColumnHidden(3, False)
            self.malzemelistesi_3.setColumnHidden(4, True)
            self.malzemelistesi_3.setColumnHidden(6, False)
            self.malzemelistesi_3.setColumnHidden(7, True)
            self.malzemelistesi_3.setColumnHidden(8, True)
            self.malzemelistesi_3.setColumnHidden(9, True)
            self.malzemelistesi_3.setColumnHidden(10, True)

            try:
                yenisatirsayisi = self.uretimdetaylari_3.rowCount()
                for a in range (0,yenisatirsayisi):
                    if self.uretimdetaylari_3.item(a,6).text() in uretimdetayidlistesi :
                        self.cblist[a].setChecked(False)
                        self.uretimdetaylari_3.hideRow(a)
            except:
                import traceback
                traceback.print_exc()
        except:
            import traceback
            traceback.print_exc()


    def malzemetalepleriguncelle(self):

        satir = self.siparisdetaylari.currentRow()
        if satir == -1:
            QtWidgets.QMessageBox.warning(self, "HATA", "SİPARİŞ DETAYI SEÇİMİ YAPMANIZ GEREKMEKTEDİR")
        else:
            try:
                siparisdetayid = self.siparisdetaylari.item(satir, 5).text()
                self.cursor.execute(
                    "select * from malzemetalepleri join malzemelistesi on malzemelistesi.malzeme_id = malzemetalepleri.malzemelistesi_malzeme_id where siparisdetaylari_siparis_detay_id = '" + siparisdetayid + "'")
                malzemelisteleri = self.cursor.fetchall()
                self.malzemelistesi.setRowCount(len(malzemelisteleri))
                for satir, data in enumerate(malzemelisteleri):
                    self.malzemelistesi.setItem(satir, 0, QtWidgets.QTableWidgetItem(str(data[1])))
                    self.malzemelistesi.setItem(satir, 1, QtWidgets.QTableWidgetItem(str(data[2])))
                    self.malzemelistesi.setItem(satir, 2, QtWidgets.QTableWidgetItem(str(data[3])))
                    self.malzemelistesi.setItem(satir, 3, QtWidgets.QTableWidgetItem(str(data[4])))
                    self.malzemelistesi.setItem(satir, 4, QtWidgets.QTableWidgetItem(data[10]))
                    self.malzemelistesi.setItem(satir, 5, QtWidgets.QTableWidgetItem(data[11]))
                    self.malzemelistesi.setItem(satir, 6, QtWidgets.QTableWidgetItem(str(data[12])))
                    self.malzemelistesi.setItem(satir, 7, QtWidgets.QTableWidgetItem(str(data[8])))
                    self.malzemelistesi.setItem(satir, 8, QtWidgets.QTableWidgetItem(data[14]))
                    self.malzemelistesi.setItem(satir, 9, QtWidgets.QTableWidgetItem(str(data[0])))

                    # self.malzemelistesi.setItem(satir, 6, QtWidgets.QTableWidgetItem(str(data[6])))
                    # self.malzemelistesi.setItem(satir, 7, QtWidgets.QTableWidgetItem(str(data[7])))
                    # self.malzemelistesi.setItem(satir, 8, QtWidgets.QTableWidgetItem(str(data[8].encode('utf-8'))))
                    # self.malzemelistesi.setItem(satir, 9, QtWidgets.QTableWidgetItem(str(data[9].encode('utf-8'))))
                    # self.malzemelistesi.setItem(satir, 10, QtWidgets.QTableWidgetItem(str(data[10])))
            except:
                import traceback
                traceback.print_exc()

    def formusil(self):
        pass
        satir = self.siparistalepformlistesi.currentRow()
        if satir == -1:
            QtWidgets.QMessageBox.warning(self, "HATA", " SEÇİM YAPMANIZ GEREKMEKTEDİR")
            return
        else:
            value = self.siparistalepformlistesi.item(satir, 0).text()
            path = "//AKMILL2/Users/Akmill2/Desktop/AKmill/MRP/MalzemeTalepleri/" + value
            self.cursor.execute("select count(*) from malzemetalebigorunumu where uretimEmirNo = '" + value[3:] + "'")
            altsiparisvarmi = self.cursor.fetchone()[0]
            if altsiparisvarmi != 0:
                QtWidgets.QMessageBox.warning(self, "HATA",
                                              "ÖNCELİKLE ALT SİPARİS TALEPLERİNİ KALDIRMANIZ GEREKMEKTEDİR")
                return
            else:
                try:
                    self.cursor.execute("start transaction")
                    self.cursor.execute("delete from malzemetalepformlistesi where siparistalepno = '" + value + "'")
                    if os.path.exists(path + ".pdf"):
                        QtWidgets.QMessageBox.warning(self, "BAsarili", "DOSYA MEVCUT")
                        try:
                            os.remove(path + ".pdf")
                            os.remove(path + ".html")
                            QtWidgets.QMessageBox.information(self, "BAŞARILI",
                                                              "SİPARİŞ TALEBİNE AİT AİT PDF ve HTML DOSAYSI SİSTEMDEN KALDIRILDI")
                        except:
                            QtWidgets.QMessageBox.warning(self, "HATA",
                                                          "PDF SİSTEMDEN KALDIRILMAADI DOSYA AÇIK VEYA BŞAKA BİR PC TARAFINDAN KULLANILIYOR OLABİLİR ")
                            self.cursor.execute("rollback")
                    else:
                        QtWidgets.QMessageBox.warning(self, "HATA",
                                                      "İLGİLİ DOSYA SİLİNMİŞ VEYA HENÜZ OLUŞTURULMAMIŞ VERİİTABANINDAN SİLME İŞLEMİ YİNEDE GERÇEKLEŞTİRİLECEK")

                except:
                    QtWidgets.QMessageBox.warning(self, "HATA ", "BEKLENMEDİK BİR HATA İLE KARŞILAŞILDI")
                self.cursor.execute("commit")
                self.siparistalepformlistesigorunumu()
                pass

    def formuac(self):
        pass
        satir = self.siparistalepformlistesi.currentRow()
        value = self.siparistalepformlistesi.item(satir, 0).text()
        path = "//AKMILL2/Users/Akmill2/Desktop/AKmill/MRP/MalzemeTalepleri/" + value + ".pdf"
        subprocess.Popen(path, shell=True)
        pass

    def formlarihazirla(self):

        siparisdetaysatir = self.siparisdetaylari.currentRow()
        if siparisdetaysatir == -1:
            QtWidgets.QMessageBox.warning(self, "HATA", "SEÇİM YAPMANIZ GEREKMEKTEDİR")
            return
        else:
            uretimemirno = self.siparisdetaylari.item(siparisdetaysatir, 0).text()
            siparisdetayid = self.siparisdetaylari.item(siparisdetaysatir, 5).text()
            firmaadi = self.siparisdetaylari.item(siparisdetaysatir, 1).text()
            projeadi = self.siparisdetaylari.item(siparisdetaysatir, 2).text()
        numara = self.malzemetalepnumarasi_3.text()
        # numara = uretimemirno + '-' + siparisdetayid
        dosyayolu = "//AYAS/Users/AYAS/Desktop/MRP/URETIM/MALZEMETALEPLERI/PR-" + numara + '.html'

        if os.path.exists(dosyayolu):
            QtWidgets.QMessageBox.warning(self, "HATA",
                                          "SİPARİS TALEP FORMU DAHA ONCE HAZIRLANMIS\n DOSYANIN SİLİNMESİ GEREKMEKTEDİR")
            return
        else:
            yazilacak = """<!DOCTYPE html>
            <!-- saved from url=(0071)http://localhost:50246/akmill/SSDA.html?_ijt=ij9ub9111m8taomk4lflgpc95v -->
            <html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><style>
                html ,body {
            height : 210mm;
            width:297mm;
            }
            table, th ,td {
            border : 1px solid black;
            border-collapse : collapse;
            }
            th ,td {
            padding : 2px;
            }
            p.normal {
                font-style : normal ;
                font-size : 1px;
                font-family : "Calibri";
            }

            p.small {
            line-height:0.5;
            font-weight : bold;

            }
            </style>"""
            asil = """ <title>Title</title>
            </head>
            <body>
            <table style="width : 100%">
                <tbody><tr>
                    <th rowspan="2" colspan="3"><img src="//AYAS/Users/AYAS/Desktop/MRP/akmillogo.png" width="200" height="64"></th>
                    <th rowspan="2" colspan="7" style="white-space:pre-wrap; word-wrap:break-word"> MALZEME TALEP FORMU <p>HAMMEDDE</p></th>
                    <th rowspan="1" colspan="1">TARİH</th>
                    <th rowspan="1" colspan="1">{0}</th>
                    <th rowspan="1" colspan="1">TERMIN TARIHI</th>

                </tr>
                <tr>
                    <th rowspan="1" colspan="1">MALZEME TALEP NO</th>
                    <th rowspan="1" colspan="1"> {1}</th>
                    <th rowspan="1" colspan="1"> {2}</th>

                </tr>
                <tr>
                    <th rowspan="2" colspan="1">NO</th>
                    <th rowspan="2" colspan="1">AK-PARCA ADI</th>
                    <th rowspan="2" colspan="1">MU-PARCA ADI</th>
                    <th rowspan="1" colspan="3">ÖLÇÜLER</th>
                    <th rowspan="2" colspan="1">ADET</th>
                    <th rowspan="2" colspan="1">MALZEME CİNSİ</th>
                    <th rowspan="2" colspan="1">TEORİK AĞIRLIK</th>
                    <th rowspan="2" colspan="1">EK AÇIKLAMA</th>
                    <th rowspan="2" colspan="1">FİRMA ADI</th>
                    <th rowspan="2" colspan="1">PROJE ADI</th>
                    <th rowspan="2" colspan="1">TALEP DURUMU</th>

                </tr>
                <tr>
                    <th rowspan="1" colspan="1">Z</th>
                    <th rowspan="1" colspan="1"> X</th>
                    <th rowspan="1" colspan="1">Y</th>
                </tr>"""
            file = open(dosyayolu, 'w')
            # operatoradi = operator[0] + " " + operator[1]
            talepno = "PR-" + numara
            now = datetime.datetime.now()
            date = now.strftime("%Y-%m-%d")
            termin = self.termintarihi.text()
            mystring = asil.format(date, talepno, termin)
            file.write(yazilacak + mystring)
            file.close()
            query = """select t1.olcux, t1.olcuy, t1.olcuz, t1.adet, t1.aciklamalar, t1.siparisdetayid, t1.uretimdetayid, t1.malzemetipi, t1.siparismistokmu, t1.malzemeagirlik,
                                   t1.akresimid, t1.muresimid, akmillteknikresimleri.parca_adi as akparcaadi,  musteriteknikresimleri.parca_adi as muparcaadi, t1.malzemecinsi from
                                    (select olcux, olcuy, olcuz, malzemetalepleri.adet as adet, aciklamalar, termintarihi, malzemetalepleri.siparisdetaylari_siparis_detay_id as siparisdetayid,
                                    uretimdetaylandirma_uretim_detaylandirma_id as uretimdetayid, malzemetipi, siparismistokmu,
                                    malzemeagirlik, akmillteknikresimleri_akmill_teknikresim_id as akresimid, musteriteknikresimleri_musteri_teknikresim_id as muresimid,
                                    malzemelistesi.malzeme_adi as malzemecinsi from malzemetalepleri
                                    join uretimdetaylandirma on malzemetalepleri.uretimdetaylandirma_uretim_detaylandirma_id = uretim_detaylandirma_id
                                    join malzemelistesi on malzemelistesi.malzeme_id = malzemetalepleri.malzemelistesi_malzeme_id) as t1
                                    left join akmillteknikresimleri on akmillteknikresimleri.akmill_teknikresim_id = t1.akresimid
                                    left join musteriteknikresimleri on musteriteknikresimleri.musteri_teknikresim_id = t1.muresimid  where siparisdetayid = {0}"""
            newquery = query.format(siparisdetayid)
            try:
                self.cursor.execute(newquery)
                print newquery
                # self.cursor.execute("select * from malzemetalepleri where siparisdetaylari_siparis_detay_id  = '"+siparisdetayid+"'")
                listi = self.cursor.fetchall()
                print listi
            except:
                import traceback
                traceback.print_exc()
            a = 1
            firmaadi = self.siparisdetaylari.item(siparisdetaysatir, 1).text()
            projeadi = self.siparisdetaylari.item(siparisdetaysatir, 2).text()
            print firmaadi, projeadi
            # firmadi = listi[0][8].encode('utf-8')
            # projeadi = listi[0][9].encode('utf-8')
            try:
                print("asdasdasdasdasdasd")
                with open(dosyayolu, 'ab') as f:
                    for i in listi:

                        if i[7] == "CAP MALZEME":
                            f.write(
                                "\n<tr>"
                                "\n<td>" + str(a) + " </td>"
                                                    "\n<td>" + str(i[12]) + " </td>"  # AKMİLL PARÇA ADI
                                                                            "\n<td>" + str(
                                    i[13]) + " </td>"  # MUSTERI PARÇA ADI
                                             "\n<td>" + "-" + " </td>"  # PARÇA OLCULERI
                                                              "\n<td> &#x2205 " + str(i[0]) + "</td>"  # PARÇA OLCULERI
                                                                                              "\n<td>" + str(
                                    i[1]) + " </td>"  # PARÇA OLCULERI
                                            "\n<td>" + str(i[3]) + " </td>"  # ADET
                                                                   "\n<td>" + i[14].encode(
                                    "utf-8") + " </td>"  # MALZEME CİNSİ
                                               "\n<td>" + str(i[9]) + " </td>"  # MALZEME AGIRLIK
                                                                      "\n<td>" + str(i[4]) + " </td>"  # ACIKLAMALAR
                                                                                             "\n<td>" + firmaadi + " </td>"  # firma adi            
                                                                                                                   "\n<td>" + projeadi + " </td>"  # proje adi
                                                                                                                                         "\n<td>" + str(
                                    i[8]) + " </td>"  # termin tarihi

                                            "\n</tr>")
                            a = a + 1
                        elif i[7] == "BORU MALZEME":
                            f.write(
                                "\n<tr>"
                                "\n<td>" + str(a) + " </td>"
                                                    "\n<td>" + str(i[12]) + " </td>"  # AKMİLL PARÇA ADI
                                                                            "\n<td>" + str(
                                    i[13]) + " </td>"  # MUSTERI PARÇA ADI
                                             "\n<td> &#x2205o " + str(i[0]) + "</td>"  # #PARCA OLCULERI DIS CAP
                                                                              "\n<td> &#x2205i " + str(
                                    i[1]) + "</td>"  # PARCA OLCULERI IC CAP
                                            "\n<td>" + str(i[2]) + " </td>"  # PARCA OLCULERI UZUNLUK
                                                                   "\n<td>" + str(i[3]) + " </td>"  # ADET
                                                                                          "\n<td>" + i[14].encode(
                                    "utf-8") + " </td>"  # MALZEME CİNSİ
                                               "\n<td>" + str(i[9]) + " </td>"  # MALZEME AGIRLIK
                                                                      "\n<td>" + str(i[4]) + " </td>"  # ACIKLAMALAR
                                                                                             "\n<td>" + firmaadi + " </td>"  # firma adi            
                                                                                                                   "\n<td>" + projeadi + " </td>"  # proje adi
                                                                                                                                         "\n<td>" + str(
                                    i[8]) + " </td>"  # termin tarihi

                                            "\n</tr>")
                            a = a + 1
                        elif i[7] == "PROFIL":
                            f.write(
                                "\n<tr>"
                                "\n<td>" + str(a) + " </td>"
                                                    "\n<td>" + str(i[12]) + " </td>"  # AKMİLL PARÇA ADI
                                                                            "\n<td>" + str(
                                    i[13]) + " </td>"  # MUSTERI PARÇA ADI
                                             "\n<td>" + str(i[0]) + "</td>"  # PARCA OLCULERI
                                                                    "\n<td>" + str(i[1]) + " </td>"  # PARCA OLCULERI
                                                                                           "\n<td>" + str(
                                    i[2]) + " </td>"  # PARCA OLCULERI
                                            "\n<td>" + str(i[3]) + " </td>"  # ADET
                                                                   "\n<td>" + i[14].encode(
                                    "utf-8") + " </td>"  # MALZEME CİNSİ
                                               "\n<td>" + str(i[9]) + " </td>"  # MALZEME AGIRLIK
                                                                      "\n<td>" + str(i[4]) + " </td>"  # ACIKLAMALAR
                                                                                             "\n<td>" + firmaadi + " </td>"  # firma adi            
                                                                                                                   "\n<td>" + projeadi + " </td>"  # proje adi
                                                                                                                                         "\n<td>" + str(
                                    i[8]) + " </td>"  # termin tarihi

                                            "\n</tr>")
                            a = a + 1
                    f.write(
                        """\n</tbody></table>
                        <p class="small"><br>&#x2205 sembolü malzemenin silindirik kesitli olduğunu göstermekte olup çap ölçüsünü temsil etmektedir.</br><br> 
                        &#x2205i ve &#x2205o sembolleri malzemenin boru profilinde olduğunu göstermektedir. &#x2205o: dış çap ölçüsünü, &#x2205i: iç çap ölçüsünü temsil etmektedir.  </br><br> Bütün ölçü birimleri mm cinsindendir. </br> <br>Anlaşılmayan veya eksik olan yerlerde lütfen iletişime geçiniz... </br> 
                        <br>Tel : 0312 395 57 22</th></br></p>"""
                        "</body></html>"
                    )
                    f.close()
            except:
                import traceback
                traceback.print_exc()
            config = pdfkit.configuration(wkhtmltopdf="C:\\Program Files\\wkhtmltopdf\\bin\\wkhtmltopdf.exe")
            optionss = {'page-size': 'A4',
                        'orientation': 'Landscape',
                        'margin-top': '10mm',
                        'margin-left': '25mm',
                        'margin-right': '10mm',
                        'margin-bottom': '10mm'}
            pdfkit.from_file(dosyayolu, dosyayolu + '.pdf', options=optionss, configuration=config)
            # self.cursor.execute("insert into malzemetalepformlistesi (siparistalepno , firmadi , projeadi , tarih) values (%s , %s, %s,%s)", ('PR-'+uretimemrino, firmadi, projeadi, "tar"))
            # self.conn.commit()
            # self.siparistalepformlistesigorunumu()
            QtWidgets.QMessageBox.information(self, "BASARILI", "MALZEME TALEP FORMU BASARI ILE OLUSTURULDU")


    def formhazirla(self):
        talepedilensiparissayisi = self.malzemelistesi_3.rowCount()
        if talepedilensiparissayisi == 0 :
            QtWidgets.QMessageBox.warning(self, "HATA", "HERHANGI BİR TALEP EKLEMEDİNİZ")
            return
        if len(self.malzemetalepnumarasi_3.text()) == 0 :
            QtWidgets.QMessageBox.warning(self, "HATA","TALEP NUMARASI ALMANIZ GEREKMEKTEDIR")
            return

        siparisdetaysatir = self.siparisdetaylari_3.currentRow()
        if siparisdetaysatir == -1:
            QtWidgets.QMessageBox.warning(self, "HATA", "SİPARİŞ DETAYI SEÇİMİ YAPMANIZ GEREKMEKTEDİR")
            return
        else:
            uretimemirno = self.siparisdetaylari.item(siparisdetaysatir, 0).text()
            siparisdetayid = self.siparisdetaylari.item(siparisdetaysatir, 5).text()
            firmaadi = self.siparisdetaylari.item(siparisdetaysatir, 1).text()
            projeadi = self.siparisdetaylari.item(siparisdetaysatir, 2).text()
        numara = self.malzemetalepnumarasi_3.text()
        dosyayolu = "//AYAS/Users/AYAS/Desktop/MRP/URETIM/MALZEMETALEPLERI/" + numara + '.html'

        if os.path.exists(dosyayolu):
            QtWidgets.QMessageBox.warning(self, "HATA","SİPARİS TALEP FORMU DAHA ONCE HAZIRLANMIS\n DOSYANIN SİLİNMESİ GEREKMEKTEDİR")
            return
        else:
            yazilacak = """<!DOCTYPE html>
            <!-- saved from url=(0071)http://localhost:50246/akmill/SSDA.html?_ijt=ij9ub9111m8taomk4lflgpc95v -->
            <html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><style>
                html ,body {
            height : 210mm;
            width:297mm;
            }
            table, th ,td {
            border : 1px solid black;
            border-collapse : collapse;
            }
            th ,td {
            padding : 2px;
            }
            p.normal {
                font-style : normal ;
                font-size : 1px;
                font-family : "Calibri";
            }

            p.small {
            line-height:0.5;
            font-weight : bold;

            }
            </style>"""
            asil = """ <title>Title</title>
            </head>
            <body>
            <table style="width : 100%">
                <tbody><tr>
                    <th rowspan="2" colspan="3"><img src="//AYAS/Users/AYAS/Desktop/MRP/akmillogo.png" width="200" height="64"></th>
                    <th rowspan="2" colspan="7" style="white-space:pre-wrap; word-wrap:break-word"> MALZEME TALEP FORMU <p>HAMMEDDE</p></th>
                    <th rowspan="1" colspan="1">TARİH</th>
                    <th rowspan="1" colspan="1">{0}</th>
                    <th rowspan="1" colspan="1">TERMIN TARIHI</th>

                </tr>
                <tr>
                    <th rowspan="1" colspan="1">MALZEME TALEP NO</th>
                    <th rowspan="1" colspan="1"> {1}</th>
                    <th rowspan="1" colspan="1"> {2}</th>

                </tr>
                <tr>
                    <th rowspan="2" colspan="1">NO</th>
                    <th rowspan="2" colspan="1">AK-PARCA ADI</th>
                    <th rowspan="2" colspan="1">MU-PARCA ADI</th>
                    <th rowspan="1" colspan="3">ÖLÇÜLER</th>
                    <th rowspan="2" colspan="1">ADET</th>
                    <th rowspan="2" colspan="1">MALZEME CİNSİ</th>
                    <th rowspan="2" colspan="1">TEORİK AĞIRLIK</th>
                    <th rowspan="2" colspan="1">EK AÇIKLAMA</th>
                    <th rowspan="2" colspan="1">FİRMA ADI</th>
                    <th rowspan="2" colspan="1">PROJE ADI</th>
                    <th rowspan="2" colspan="1">TALEP DURUMU</th>

                </tr>
                <tr>
                    <th rowspan="1" colspan="1">Z</th>
                    <th rowspan="1" colspan="1"> X</th>
                    <th rowspan="1" colspan="1">Y</th>
                </tr>"""
            file = open(dosyayolu, 'w')
            talepno = numara
            now = datetime.datetime.now()
            date = now.strftime("%Y-%m-%d")
            termin = self.termintarihi_3.text()
            mystring = asil.format(date, talepno, termin)
            file.write(yazilacak + mystring)
            file.close()

            a = 1
            firmaadi = self.siparisdetaylari.item(siparisdetaysatir, 1).text()
            projeadi = self.siparisdetaylari.item(siparisdetaysatir, 2).text()
            print firmaadi, projeadi
            try:
                print("asdasdasdasdasdasd")
                with open(dosyayolu, 'ab') as f:
                    for x in range(0, talepedilensiparissayisi):
                        akmillresimnumarasi = self.malzemelistesi_3.item(x,1)
                        if akmillresimnumarasi is None :
                            akmillresimno = "----"
                        else :
                            akmillresimno = akmillresimnumarasi.text()
                        if self.malzemelistesi_3.item(x,2) is None :
                            musteriresimno = "---"
                        else :
                            musteriresimno = self.malzemelistesi_3.item(x,2).text()
                        adet = self.malzemelistesi_3.item(x,3).text()
                        if self.malzemelistesi_3.item(x,5) is None :
                            malzeme = "---"
                        else:
                            malzeme = self.malzemelistesi_3.item(x,5).text()
                        if self.malzemelistesi_3.item(x, 11) is None :
                            akmillparcaadi = "---"
                        else:
                            akmillparcaadi = self.malzemelistesi_3.item(x, 11).text()
                        if self.malzemelistesi_3.item(x,12) is None:
                            olcux = "---"
                        else:
                            olcux = self.malzemelistesi_3.item(x,12).text()
                        if self.malzemelistesi_3.item(x,13) is None:
                            olcuy = "---"
                        else:
                            olcuy = self.malzemelistesi_3.item(x,13).text()
                        if  self.malzemelistesi_3.item(x,14) is None:
                            olcuz = "---"
                        else:
                            olcuz = self.malzemelistesi_3.item(x,14).text()
                        if self.malzemelistesi_3.item(x,21) is None:
                            malzemetipi = "---"
                        else:
                            malzemetipi = self.malzemelistesi_3.item(x,21).text()
                        if self.malzemelistesi_3.item(x,16) is None:
                            siparismistokmu = "---"
                        else:
                            siparismistokmu = self.malzemelistesi_3.item(x,16).text()
                        if self.malzemelistesi_3.item(x,18) is None:
                            musteriparcaadi = "---"
                        else:
                            musteriparcaadi = self.malzemelistesi_3.item(x,18).text()
                        if self.malzemelistesi_3.item(x,22) is None:
                            aciklamalar = "---"
                        else:
                            aciklamalar = self.malzemelistesi_3.item(x,22).text()
                            print "aciklamalar"+ aciklamalar
                        birimagirlik = self.malzemelistesi_3.item(x,19).text()
                        toplamagirlik = self.malzemelistesi_3.item(x,20).text()
                        print malzemetipi
                        if malzemetipi == "CAP MALZEME":
                            f.write(
                                "\n<tr>"
                                "\n<td>" + str(a) + " </td>"
                                                    "\n<td>" + str(akmillparcaadi) + " </td>"  # AKMİLL PARÇA ADI
                                                                            "\n<td>" + str(musteriparcaadi) + " </td>"  # MUSTERI PARÇA ADI
                                             "\n<td>" + "-" + " </td>"  # PARÇA OLCULERI
                                                              "\n<td> &#x2205 " + str(olcux) + "</td>"  # PARÇA OLCULERI
                                                                                              "\n<td>" + str(
                                    olcuy) + " </td>"  # PARÇA OLCULERI
                                            "\n<td>" + str(adet) + " </td>"  # ADET
                                                                   "\n<td>" + malzeme.encode(
                                    "utf-8") + " </td>"  # MALZEME CİNSİ
                                               "\n<td>" + str(toplamagirlik) + " </td>"  # MALZEME AGIRLIK
                                                                      "\n<td>" + str(aciklamalar) + " </td>"  # ACIKLAMALAR
                                                                                             "\n<td>" + firmaadi + " </td>"  # firma adi            
                                                                                                                   "\n<td>" + projeadi + " </td>"  # proje adi
                                                                                                                                         "\n<td>" + str(
                                    siparismistokmu) + " </td>"  # termin tarihi

                                            "\n</tr>")
                            a = a + 1
                        elif malzemetipi == "BORU MALZEME":
                            f.write(
                                "\n<tr>"
                                "\n<td>" + str(a) + " </td>"
                                                    "\n<td>" + str(akmillparcaadi) + " </td>"  # AKMİLL PARÇA ADI
                                                                            "\n<td>" + str(
                                    musteriparcaadi) + " </td>"  # MUSTERI PARÇA ADI
                                             "\n<td> &#x2205o " + str(olcux) + "</td>"  # #PARCA OLCULERI DIS CAP
                                                                              "\n<td> &#x2205i " + str(
                                    olcuy) + "</td>"  # PARCA OLCULERI IC CAP
                                            "\n<td>" + str(olcuz) + " </td>"  # PARCA OLCULERI UZUNLUK
                                                                   "\n<td>" + str(adet) + " </td>"  # ADET
                                                                                          "\n<td>" + malzeme.encode(
                                    "utf-8") + " </td>"  # MALZEME CİNSİ
                                               "\n<td>" + str(toplamagirlik) + " </td>"  # MALZEME AGIRLIK
                                                                      "\n<td>" + str(aciklamalar) + " </td>"  # ACIKLAMALAR
                                                                                             "\n<td>" + firmaadi + " </td>"  # firma adi            
                                                                                                                   "\n<td>" + projeadi + " </td>"  # proje adi
                                                                                                                                         "\n<td>" + str(
                                    siparismistokmu) + " </td>"  # termin tarihi

                                            "\n</tr>")
                            a = a + 1
                        elif malzemetipi == "PROFIL":
                            f.write(
                                "\n<tr>"
                                "\n<td>" + str(a) + " </td>"
                                                    "\n<td>" + str(akmillparcaadi) + " </td>"  # AKMİLL PARÇA ADI
                                                                            "\n<td>" + str(
                                    musteriparcaadi) + " </td>"  # MUSTERI PARÇA ADI
                                             "\n<td>" + str(olcux) + "</td>"  # PARCA OLCULERI
                                                                    "\n<td>" + str(olcuy) + " </td>"  # PARCA OLCULERI
                                                                                           "\n<td>" + str(
                                    olcuz) + " </td>"  # PARCA OLCULERI
                                            "\n<td>" + str(adet) + " </td>"  # ADET
                                                                   "\n<td>" + malzeme.encode(
                                    "utf-8") + " </td>"  # MALZEME CİNSİ
                                               "\n<td>" + str(toplamagirlik) + " </td>"  # MALZEME AGIRLIK
                                                                      "\n<td>" + str(aciklamalar) + " </td>"  # ACIKLAMALAR
                                                                                             "\n<td>" + firmaadi + " </td>"  # firma adi            
                                                                                                                   "\n<td>" + projeadi + " </td>"  # proje adi
                                                                                                                                         "\n<td>" + str(
                                    siparismistokmu) + " </td>"  # termin tarihi

                                            "\n</tr>")
                            a = a + 1
                    f.write(
                        """\n</tbody></table>
                        <p class="small"><br>&#x2205 sembolü malzemenin silindirik kesitli olduğunu göstermekte olup çap ölçüsünü temsil etmektedir.</br><br> 
                        &#x2205i ve &#x2205o sembolleri malzemenin boru profilinde olduğunu göstermektedir. &#x2205o: dış çap ölçüsünü, &#x2205i: iç çap ölçüsünü temsil etmektedir.  </br><br> Bütün ölçü birimleri mm cinsindendir. </br> <br>Anlaşılmayan veya eksik olan yerlerde lütfen iletişime geçiniz... </br> 
                        <br>Tel : 0312 395 57 22</th></br></p>"""
                        "</body></html>"
                    )
                    f.close()
            except:
                import traceback
                traceback.print_exc()
            config = pdfkit.configuration(wkhtmltopdf="C:\\Program Files\\wkhtmltopdf\\bin\\wkhtmltopdf.exe")
            optionss = {'page-size': 'A4',
                        'orientation': 'Landscape',
                        'margin-top': '10mm',
                        'margin-left': '25mm',
                        'margin-right': '10mm',
                        'margin-bottom': '10mm'}
            pdfkit.from_file(dosyayolu, dosyayolu + '.pdf', options=optionss, configuration=config)
            # self.cursor.execute("insert into malzemetalepformlistesi (siparistalepno , firmadi , projeadi , tarih) values (%s , %s, %s,%s)", ('PR-'+uretimemrino, firmadi, projeadi, "tar"))
            # self.conn.commit()
            # self.siparistalepformlistesigorunumu()
            QtWidgets.QMessageBox.information(self, "BASARILI", "MALZEME TALEP FORMU BASARI ILE OLUSTURULDU")


    def malzemegirislerinilimitle(self):

        if self.borumalzeme.checkState() == 2:
            self.capmalzeme.setCheckState(0)
            self.profil.setCheckState(0)
            self.cap.setDisabled(True)
            self.uzunlukcap.setDisabled(True)
            self.kalinlik.setDisabled(True)
            self.genislik.setDisabled(True)
            self.uzunluk.setDisabled(True)
            self.capmalzeme.setDisabled(True)
            self.profil.setDisabled(True)
            self.discap.setDisabled(False)
            self.iccap.setDisabled(False)
            self.uzunlukboru.setDisabled(False)
        elif self.capmalzeme.checkState() == 2:
            self.discap.setDisabled(True)
            self.iccap.setDisabled(True)
            self.uzunlukboru.setDisabled(True)
            self.kalinlik.setDisabled(True)
            self.genislik.setDisabled(True)
            self.uzunluk.setDisabled(True)
            self.profil.setDisabled(True)
            self.borumalzeme.setDisabled(True)
            self.cap.setDisabled(False)
            self.uzunlukcap.setDisabled(False)
        elif self.profil.checkState() == 2:
            self.kalinlik.setDisabled(False)
            self.genislik.setDisabled(False)
            self.uzunluk.setDisabled(False)
            self.discap.setDisabled(True)
            self.iccap.setDisabled(True)
            self.uzunlukboru.setDisabled(True)
            self.cap.setDisabled(True)
            self.uzunlukcap.setDisabled(True)
            self.capmalzeme.setDisabled(True)
            self.borumalzeme.setDisabled(True)
        else:
            self.capmalzeme.setDisabled(False)
            self.profil.setDisabled(False)
            self.borumalzeme.setDisabled(False)

    # def projeguncelle(self):
    #     pass
    #     self.projeadi.clear()
    #     firmaadi = self.firmaadi.currentText()
    #     self.cursor.execute("select projeadi from firma_proje where firma_adi = '"+firmaadi+"'")
    #     projeler = self.cursor.fetchall()
    #     for i in projeler :
    #         self.projeadi.addItems(i)
    #     self.uretimnoguncelle()
    # def uretimnoguncelle(self):
    #     pass
    #     self.uretimno.clear()
    #     projeadi = self.projeadi.currentText()
    #     self.cursor.execute("select distinct uretimEmirNo from uretimdetaylandirma where projeadi = '"+projeadi+"'")
    #     uretimemirleri = self.cursor.fetchall()
    #     for i in uretimemirleri:
    #         self.uretimno.addItems(i)
    def malzemelistesiguncelle(self):

        self.cursor.execute("select malzeme_adi from malzemelistesi")
        malzemeler = self.cursor.fetchall()
        for i in malzemeler:
            self.malzeme.addItems(i)

    # def parcalistesiguncelle(self):
    #     pass
    #     # try :
    #     durum  = "False"
    #     self.cursor.execute("select parcaadi from uretimdetaylandirma where uretimEmirNo = '"+self.uretimno.currentText()+"' and  siparisdurumu = '"+durum+"'")
    #     parcalistesi = self.cursor.fetchall()
    #     self.parcaadi.clear()
    #     for i in parcalistesi :
    #         self.parcaadi.addItems(i)
    #     # except :
    #     #     QtWidgets.QMessageBox.warning(self, " HATA " , "BİR PROBLEMLE KARŞILAŞILDI")
    #     #     return
    def listeyeekle(self):
        try:
            uretimdetaysatir = self.uretimdetaylari.currentRow()
            if uretimdetaysatir == -1:
                QtWidgets.QMessageBox.warning(self, "HATA",
                                              "TALEP OLUŞTURMAK İSTEDİĞİNİZ URETİMDETAYINI SEÇMENİZ GEREKMEKTEDİR \n ÜRETİM DETAYLARI BÖLÜMÜNDEN SEÇİMİ GERÇEKLEŞTİRİLEBİLİRSİNİZ..")
                return
            else:
                pass
            if self.agirlikhesapla() == 1:
                QtWidgets.QMessageBox.warning(self, "HATA", "BİR HATA İLE KARŞILAIŞDI")
                import traceback
                traceback.print_exc()
                return
            # elif len(self.malzemetalepnumarasi.text()) == 0:
            #     QtWidgets.QMessageBox.warning(self, "HATA", "MALZEME TALEP NUMARASI ALMANIZ GEREKMEKTEDİR")
            #     return
            else:
                # uretimemrino = self.uretimno.currentText()
                # firmadi = self.firmaadi.currentText()
                # projeadi = self.projeadi.currentText()
                satir = self.uretimdetaylari.currentRow()
                uretimdetayid = self.uretimdetaylari.item(satir, 5).text()

                now = datetime.datetime.now()
                date = now.strftime("%Y-%m-%d")
                # ad = 'PR-' + uretimemrino
                self.cursor.execute(
                    "select count(*) from malzemetalepleri where uretimdetaylandirma_uretim_detaylandirma_id = '" + uretimdetayid + "'")
                mevcutmu = self.cursor.fetchone()[0]
                if mevcutmu != 0:
                    QtWidgets.QMessageBox.warning(self, "HATA",
                                                  "DAHA ÖNCE BU ÜRETİM DETAYI İÇİN TALEP OLUŞTURULMUŞ LÜTFEN KONTROL EDİNİZ")
                    return
                    # self.cursor.execute(
                    #     "insert into malzemetalepleri (siparistalepno , firmadi , projeadi , tarih) values (%s , %s, %s,%s)",
                    #     ('PR-' + uretimemrino, firmadi, projeadi, date))
                else:
                    try:
                        siparisdetaysatir = self.siparisdetaylari.currentRow()
                        siparisdetayid = self.siparisdetaylari.item(siparisdetaysatir, 5).text()
                        uretimdetaysatir = self.uretimdetaylari.currentRow()
                        uretimdetayid = self.uretimdetaylari.item(uretimdetaysatir, 5).text()
                        temindurumu = self.siparismistokmu.currentText()
                        self.cursor.execute(
                            "select malzeme_id from malzemelistesi where malzeme_adi= '" + self.malzeme.currentText() + "'")
                        malzemeid = self.cursor.fetchone()[0]
                        # parcaid = self.cursor.fetchone()[0]
                        ekaciklama = self.aciklamalar.text()
                        adet = int(self.adet.text())
                        termintarihi = self.termintarihi.text()
                        # malzemetipi = self.malzeme.currentText()
                        paket = self.olcux, self.olcuy, self.olcuz, adet, ekaciklama, termintarihi, siparisdetayid, uretimdetayid, malzemeid, self.malzemetipi, temindurumu, self.teorikagirlik
                        self.cursor.execute(
                            "insert into  malzemetalepleri (olcux, olcuy, olcuz, adet, aciklamalar, termintarihi,siparisdetaylari_siparis_detay_id,"
                            "uretimdetaylandirma_uretim_detaylandirma_id, malzemelistesi_malzeme_id, malzemetipi, siparismistokmu, malzemeagirlik) values (%s ,%s ,%s,%s ,%s,%s ,%s,%s ,%s,%s ,%s ,%s)",
                            (paket))
                        self.conn.commit()
                        # self.cursor.execute("update uretimdetaylari set siparisdurumu  = True where teknikresim_id = '"+str(parcaid)+"' and uretimemrino_id = '"+str(uretimnoid)+"'")
                        self.conn.commit()
                        QtWidgets.QMessageBox.information(self, "BAŞARILI", "TALEP BAŞARIYLA EKLENDİ")
                        self.malzemetalepleriguncelle()

                    except:
                        import traceback
                        traceback.print_exc()
                        QtWidgets.QMessageBox.warning(self, "hata", "BİR HATA İLE KARŞILAŞILDI")
                        return
        except:
            import traceback
            traceback.print_exc()
            # self.parcalistesiguncelle()
            # self.siparistalepformlistesigorunumu()

    def agirlikhesapla(self):


        if self.adet.text() == "0":
            QtWidgets.QMessageBox.warning(self, "hata", "ADET SAYISI '0' OLAMAZ")
            return 1
        else:

            if self.borumalzeme.checkState() == 2:
                try:
                    # adet = int(self.adet.text())
                    adet = 1
                    discap = int(self.discap.text())
                    iccap = int(self.iccap.text())
                    if iccap >= discap:
                        QtWidgets.QMessageBox.warning(self, "HATA",
                                                      "İÇ ÇAP DIŞ ÇAPA KÜÇÜK VEYA EŞİT OLAMAZ LÜTFEN KONTROL EDİNİZ")
                        return
                    else:
                        pass
                    uzunluk = int(self.uzunlukboru.text())
                    hacim = (discap ** 2 - iccap ** 2) * math.pi * uzunluk / 4
                    self.cursor.execute(
                        "select malzeme_yogunluk from malzemelistesi where malzeme_adi = '" + self.malzeme.currentText() + "'")
                    yogunluk = float(self.cursor.fetchone()[0])
                    self.teorikagirlik = yogunluk * adet * hacim / 1000000000
                    self.olcux = discap
                    self.olcuy = iccap
                    self.olcuz = uzunluk
                    self.malzemetipi = "BORU MALZEME"
                except:
                    QtWidgets.QMessageBox.warning(self, "HATA",
                                                  "LUTFEN DEGERLERI GERÇEK SAYI OLARAK GIRINIZ VE ALANLARI BOŞ BIRAKMAYINIZ")
                    return 1
            elif self.capmalzeme.checkState() == 2:
                try:
                    cap = int(self.cap.text())
                    uzunluk = int(self.uzunlukcap.text())
                    # adet = int(self.adet.text())
                    adet = 1
                    hacim = (cap ** 2 * math.pi * uzunluk / 4)
                    self.cursor.execute(
                        "select malzeme_yogunluk from malzemelistesi where malzeme_adi = '" + self.malzeme.currentText() + "'")
                    yogunluk = float(self.cursor.fetchone()[0])
                    self.teorikagirlik = yogunluk * hacim * adet / 1000000000
                    self.olcux = cap
                    self.olcuy = uzunluk
                    self.olcuz = 0
                    self.malzemetipi = "CAP MALZEME"
                except:
                    import traceback
                    traceback.print_exc()
                    QtWidgets.QMessageBox.warning(self, "HATA",
                                                  "LUTFEN DEGERLERI RAKAM OLARAK GIRINIZ VE ALANLARI BOŞ BIRAKMAYINIZ")
                    return 1
            elif self.profil.checkState() == 2:
                try:
                    kalinlik = int(self.kalinlik.text())
                    uzunluk = int(self.uzunluk.text())
                    genislik = int(self.genislik.text())
                    # adet = int(self.adet.text())
                    adet = 1
                    hacim = kalinlik * genislik * uzunluk
                    self.cursor.execute(
                        "select malzeme_yogunluk from malzemelistesi where malzeme_adi = '" + self.malzeme.currentText() + "'")
                    yogunluk = float(self.cursor.fetchone()[0])
                    self.teorikagirlik = hacim * yogunluk / 1000000000
                    self.olcux = kalinlik
                    self.olcuy = genislik
                    self.olcuz = uzunluk
                    self.malzemetipi = "PROFIL"
                except:
                    import traceback
                    traceback.print_exc()
                    QtWidgets.QMessageBox.warning(self, "HATA",
                                                  "LUTFEN DEGERLERI RAKAM OLARAK GIRINIZ VE ALANLARI BOŞ BIRAKMAYINIZ")
                    return 1
            else:
                QtWidgets.QMessageBox.warning(self, "HATA",
                                              "MALZEME TİPİNİ BELİRLEYİP GEREKLİ ÖLÇÜLERİ DOLDURMANIZ GEREKMEKTEDİR")
                return

    # def listegorunumu(self):
    #     pass
    #     satir = self.siparistalepformlistesi.currentRow()
    #     if satir == -1 :
    #         QtWidgets.QMessageBox.warning(self, "HATA" ,"HERHANGİ BİR KAYIT BULUNAMADI")
    #     else :
    #
    #         value = self.siparistalepformlistesi.item(satir, 0).text()[3:]
    #     # try :
    #         self.cursor.execute("select * from  malzemetalebigorunumu where uretimEmirNo = '"+value+"'")
    #         malzemelisteleri = self.cursor.fetchall()
    #         self.malzemelistesi.setRowCount(len(malzemelisteleri))
    #         for satir, data in enumerate(malzemelisteleri):
    #             self.malzemelistesi.setItem(satir, 0, QtWidgets.QTableWidgetItem(data[0].encode('utf-8')))
    #             self.malzemelistesi.setItem(satir, 1, QtWidgets.QTableWidgetItem(data[1].encode('utf-8')))
    #             self.malzemelistesi.setItem(satir, 2, QtWidgets.QTableWidgetItem(str(data[2].encode('utf-8'))))
    #             self.malzemelistesi.setItem(satir, 3, QtWidgets.QTableWidgetItem(str(data[3].encode('utf-8'))))
    #             self.malzemelistesi.setItem(satir, 4, QtWidgets.QTableWidgetItem(str(data[4])))
    #             self.malzemelistesi.setItem(satir, 5, QtWidgets.QTableWidgetItem(str(data[5])))
    #             self.malzemelistesi.setItem(satir, 6, QtWidgets.QTableWidgetItem(str(data[6])))
    #             self.malzemelistesi.setItem(satir, 7, QtWidgets.QTableWidgetItem(str(data[7])))
    #             self.malzemelistesi.setItem(satir, 8, QtWidgets.QTableWidgetItem(str(data[8].encode('utf-8'))))
    #             self.malzemelistesi.setItem(satir, 9, QtWidgets.QTableWidgetItem(str(data[9].encode('utf-8'))))
    #             self.malzemelistesi.setItem(satir, 10, QtWidgets.QTableWidgetItem(str(data[10])))
    #
    #     # except :
    #     #     QtWidgets.QMessageBox.warning(self, "HATA" , "BEKLENMEDIK BIR HATA İLE KARŞILAŞILDI")
    #     #     return
    #         self.stackedWidget.setCurrentIndex(1)
    # def siparistalepformlistesigorunumu(self):
    #     pass
    #     self.cursor.execute("select * from malzemetalepformlistesi")
    #     siparislisteleri  = self.cursor.fetchall()
    #     self.siparistalepformlistesi.setRowCount(len(siparislisteleri))
    #     for satir, data in enumerate(siparislisteleri):
    #         self.siparistalepformlistesi.setItem(satir, 0, QtWidgets.QTableWidgetItem(data[1]))
    #         self.siparistalepformlistesi.setItem(satir, 1, QtWidgets.QTableWidgetItem(data[2]))
    #         self.siparistalepformlistesi.setItem(satir, 2, QtWidgets.QTableWidgetItem(data[3]))
    #         self.siparistalepformlistesi.setItem(satir, 3, QtWidgets.QTableWidgetItem(data[4]))
    #
    #
    #     pass
    # def talepgorunumu(self):
    #     pass
    #     self.stackedWidget.setCurrentIndex(0)
    #     # self.cursor.execute("select ")
    def siparisformuhazirla(self):
        satir = self.siparisdetaylari.currentRow()
        if satir == -1:
            QtWidgets.QMessageBox.warning(self, "HATA", "SEÇİM YAPMANIZ GEREKMEKTEDİR")
            return
        else:
            siparisdetayid = self.siparisdetaylari.item(satir, 5).text()
            uretimemrinumarasi = self.siparisdetaylari.item(satir, 0).text()
            dosyaadi = "PO-" + uretimemrinumarasi + "-" + siparisdetayid
            output = "//AYAS/Users/AYAS/Desktop/MRP/URETIM/SIPARIS FORMLARI/" + dosyaadi + ".xls"

        try:
            if os.path.exists(output):
                QtWidgets.QMessageBox.warning(self, "HATA",
                                              "SİPARİS TALEP FORMU DAHA ONCE HAZIRLANMIS\n DOSYANIN SİLİNMESİ GEREKMEKTEDİR")
                return
            else:
                pass

            query = """select t1.olcux, t1.olcuy, t1.olcuz, t1.adet, t1.aciklamalar, t1.malzemetipi, t1.siparismistokmu, 
                                    t1.malzemecinsi, t1.siparisdetayid from
                                    (select olcux, olcuy, olcuz, malzemetalepleri.adet as adet, aciklamalar, termintarihi, malzemetipi, siparismistokmu,
                                    malzemelistesi.malzeme_adi as malzemecinsi, siparisdetaylari_siparis_detay_id as siparisdetayid from malzemetalepleri
                                    join malzemelistesi on malzemelistesi.malzeme_id = malzemetalepleri.malzemelistesi_malzeme_id where siparismistokmu = 'SİPARİŞ') as t1 where t1.siparisdetayid = {0}"""
            newquery = query.format(siparisdetayid)
            self.cursor.execute(newquery)
            datalar = self.cursor.fetchall()
            try:
                deneme = exeltablecreator.SiparisTalepFormu()
                deneme.form_olustur("MALZEME SIPARIS FORMU.xlsx", output, datalar, dosyaadi)
                QtWidgets.QMessageBox.information(self, "TAMAMLANDI", "SİPARİŞ FORM TASLAĞI HAZIRLANDI")
            except:
                import traceback
                traceback.print_exc()
            import subprocess
            subprocess.Popen(output, shell=True)
        except:
            import traceback
            traceback.print_exc()

        pass
    # def arama(self):
    #     pass
    #     aranacak = self.lineEdit.text()
    #     self.cursor.execute("select * from malzemetalebigorunumu where parcaadi like '%"+aranacak+"%' or firma_adi like  '%"+aranacak+"%' or malzemeadi like '%"+aranacak+"%'")
    #     sonuclistesi = self.cursor.fetchall()
    #     self.malzemelistesi.setRowCount(len(sonuclistesi))
    #     for satir, data in enumerate(sonuclistesi):
    #         self.malzemelistesi.setItem(satir, 0, QtWidgets.QTableWidgetItem(data[0]))
    #         self.malzemelistesi.setItem(satir, 1, QtWidgets.QTableWidgetItem(data[1]))
    #         self.malzemelistesi.setItem(satir, 2, QtWidgets.QTableWidgetItem(data[2]))
    #         self.malzemelistesi.setItem(satir, 3, QtWidgets.QTableWidgetItem(str(data[3])))
    #         self.malzemelistesi.setItem(satir, 4, QtWidgets.QTableWidgetItem(str(data[4])))
    #         self.malzemelistesi.setItem(satir, 5, QtWidgets.QTableWidgetItem(str(data[5])))
    #         self.malzemelistesi.setItem(satir, 6, QtWidgets.QTableWidgetItem(str(data[6])))
    #         self.malzemelistesi.setItem(satir, 7, QtWidgets.QTableWidgetItem(str(data[7])))
    #         self.malzemelistesi.setItem(satir, 8, QtWidgets.QTableWidgetItem(str(data[8])))
    #         self.malzemelistesi.setItem(satir, 9, QtWidgets.QTableWidgetItem(str(data[9])))
    #         self.malzemelistesi.setItem(satir, 10, QtWidgets.QTableWidgetItem(str(data[10])))
    # def getselection(self):
    #     pass
    #     try:
    #         satir = self.malzemelistesi.currentRow()
    #         if satir == -1 :
    #             return 1
    #         else:
    #             uretimno = self.malzemelistesi.item(satir, 1).text()
    #             resimno = self.malzemelistesi.item(satir ,3).text()
    #             self.cursor.execute("select id from uretimemirleri where uretimEmirNo = '"+uretimno+"'")
    #             self.uretimnoid = self.cursor.fetchone()[0]
    #             self.cursor.execute("select id from teknikresimler where teknikresimno = '"+resimno+"'")
    #             self.teknikresimnumarasiid = self.cursor.fetchone()[0]
    #     except:
    #         QtWidgets.QMessageBox.warning(self, "HATA", "BÜTÜN MALZEME TALEPLERİ SİLİNDİ")
    # def silme(self):
    #     pass
    #     if self.getselection() == 1 :
    #         QtWidgets.QMessageBox.warning(self, "HATA", "SEÇİM YAPMANIZ GEREKMEKTEDİR")
    #     else:
    #         self.cursor.execute(
    #             "delete from malzemetalebi where parcaid = '" + str(self.teknikresimnumarasiid )+ "' and uretimnoid = '" + str(self.uretimnoid )+ "'")
    #         self.cursor.execute(
    #             "update uretimdetaylari set siparisdurumu  = False where teknikresim_id = '" +str(self.teknikresimnumarasiid )+ "' and uretimemrino_id = '" + str(self.uretimnoid )+ "'")
    #         self.conn.commit()
    #         self.parcalistesiguncelle()
    #         self.siparistalepformlistesigorunumu()
    #         self.listegorunumu()
    #         # self.listegorunumu()
