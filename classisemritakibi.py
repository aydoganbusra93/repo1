#!/usr/bin/env python
# -*- coding:utf -8-*-
import isemritakibi
from mysql.connector import (connection)
from PyQt5 import QtWidgets
from PyQt5.QtGui import QPixmap

class IsemriTakibi(isemritakibi.Ui_Dialog, QtWidgets.QDialog):
    def __init__(self, parent=None):
        super(IsemriTakibi, self).__init__(parent)
        self.setupUi(self)
        self.xlikisemirleri.currentIndexChanged.connect(self.isemirlerilistesiguncelle)
        try:
            self.conn = connection.MySQLConnection(user='root', password='13579', host='localhost', database='AKMILL')
            self.cursor = self.conn.cursor()
        except:
            self.err = classwarnings.Warnings()
            msg= self.err.databaseconnectionerror()
            QtWidgets.QMessageBox.warning(self,msg[0], msg[1])
            return
        self.isemirlerilistesiguncelle()
        self.isemirlerilistesi.currentCellChanged.connect(self.isemriuretimdetaylarilistesiguncelle)
        self.uretimdetaylari.currentCellChanged.connect(self.isemritakibipreview)
        self.isemrisil.clicked.connect(self.isemrinikaldir)
        try:
            self.takimkaldir.clicked.connect(self.takimilistedenkaldir)

        except:
            import traceback
            traceback.print_exc()

    def isemrinikaldir(self):
        isemrisatir = self.isemirlerilistesi.currentRow()
        if isemrisatir == -1:
            import traceback
            traceback.print_exc()
            QtWidgets.QMessageBox.warning(self, "HATA", "SİLMEK İSTEDİĞİNİZ İŞ EMRİNİ SEÇMENİZ GEREKMEKTEDİR")
            return
        else:

            pass
        isemriid = self.isemirlerilistesi.item(isemrisatir, 0).text()

        try:
            self.cursor.execute("start transaction")
            self.cursor.execute("delete from isemirleri where isemri_id = '" + isemriid + "'")
            QtWidgets.QMessageBox.information(self, "BİLGİ", "İŞEMRİ SİLİNDİ")
        except:
            import traceback
            traceback.print_exc()
            self.cursor.execute("rollback")
            QtWidgets.QMessageBox.warning(self, "HATA", "BİR HATA İLE KARŞILAŞILDI İŞLEMLER GERİ ALINIYOR...")
        self.conn.commit()
        self.isemirlerilistesiguncelle()

    def takimilistedenkaldir(self):
        isemrisatir = self.isemirlerilistesi.currentRow()
        takimsatir = self.takimlistesi.currentRow()
        if isemrisatir == -1 or takimsatir == -1:
            QtWidgets.QMessageBox.warning(self, "HATA", "İŞ EMRİ ve TAKIM SEÇİMİ YAPMANIZ GEREKMEKTEDİR")
            return
        else:
            try:
                takimid = self.takimlistesi.item(takimsatir, 9).text()
                self.cursor.execute("start transaction")
                self.cursor.execute("delete from takimlisteleri where idtakimlisteleri = '" + takimid + "'")
                QtWidgets.QMessageBox.warning(self, "BİLGİ", "İŞEMRİNE AİT İLGİLİ TAKIM LİSTEDEN KALDIRILDI")
            except:
                QtWidgets.QMessageBox.warning(self, "HATA", "BİR HATA İLE KARŞILAŞILDI İŞLEMLER GERİ ALINIYOR")
                self.cursor.execute(rollback)
                return
        self.conn.commit()
        self.takimlistesiguncelle()

        pass

    def isemirlerilistesiguncelle(self):
        try:
            x = self.xlikisemirleri.currentText()
            self.cursor.execute("select * from isemirleri order by isemri_id desc limit " + str(x) + "")
            datalar = self.cursor.fetchall()
            self.isemirlerilistesi.setRowCount(len(datalar))
            for satir, data in enumerate(datalar):
                self.isemirlerilistesi.setItem(satir, 0, QtWidgets.QTableWidgetItem(str(data[0])))
                self.isemirlerilistesi.setItem(satir, 1, QtWidgets.QTableWidgetItem(data[1]))
                self.isemirlerilistesi.setItem(satir, 2, QtWidgets.QTableWidgetItem(str(data[2])))
                self.isemirlerilistesi.setItem(satir, 3, QtWidgets.QTableWidgetItem(str(data[3])))
                self.isemirlerilistesi.setItem(satir, 4, QtWidgets.QTableWidgetItem(data[4]))
        except:
            import traceback
            traceback.print_exc()
        self.isemirlerilistesi.setColumnHidden(0, True)
        self.isemirlerilistesi.setColumnHidden(3, True)

        pass

    def isemriuretimdetaylarilistesiguncelle(self):
        satir = self.isemirlerilistesi.currentRow()
        if satir == -1:
            return
        else:
            uretimdetayid = self.isemirlerilistesi.item(satir, 3).text()
        try:

            self.cursor.execute(
                "select akmill_teknik_resim_no , musteri_teknik_resim_no, uretimdetaylandirma.adet,  operasyon_sayisi, malzeme_adi ,uretim_detaylandirma_id, akmillteknikresimleri.dosya_yolu,"
                "akmillteknikresimleri.dosya_yolu_onizleme, musteriteknikresimleri.dosya_yolu  , musteriteknikresimleri.dosya_yolu_onizleme, malzemetalepleri.olcux, malzemetalepleri.olcuy, malzemetalepleri.olcuz from uretimdetaylandirma "
                "left join akmillteknikresimleri on akmillteknikresimleri.akmill_teknikresim_id =uretimdetaylandirma.akmillteknikresimleri_akmill_teknikresim_id "
                "left join musteriteknikresimleri on musteriteknikresimleri.musteri_teknikresim_id = uretimdetaylandirma.musteriteknikresimleri_musteri_teknikresim_id"
                " left join malzemetalepleri on malzemetalepleri.uretimdetaylandirma_uretim_detaylandirma_id = uretimdetaylandirma.uretim_detaylandirma_id "
                "left join  malzemelistesi on malzemelistesi.malzeme_id = uretimdetaylandirma.malzemelistesi_malzeme_id where uretimdetaylandirma.uretim_detaylandirma_id = '" + str(
                    uretimdetayid) + "'")

            uretimdetaylarilistesi = self.cursor.fetchall()
            self.uretimdetaylari.setRowCount(len(uretimdetaylarilistesi))
            for row, value in enumerate(uretimdetaylarilistesi):
                self.uretimdetaylari.setItem(row, 0, QtWidgets.QTableWidgetItem(value[0]))  # AKMILL RESIM NO
                self.uretimdetaylari.setItem(row, 1, QtWidgets.QTableWidgetItem(value[1]))  # MUSTERI RESIM NO
                self.uretimdetaylari.setItem(row, 2, QtWidgets.QTableWidgetItem(str(value[2])))  # ADET
                self.uretimdetaylari.setItem(row, 3, QtWidgets.QTableWidgetItem(str(value[3])))  # OP SAYISI
                self.uretimdetaylari.setItem(row, 4, QtWidgets.QTableWidgetItem(value[4]))  # MALZEME ADI
                self.uretimdetaylari.setItem(row, 5, QtWidgets.QTableWidgetItem(str(value[5])))  # URETIMDETAYID
                self.uretimdetaylari.setItem(row, 6, QtWidgets.QTableWidgetItem(value[6]))  # MALZEME ADI
                self.uretimdetaylari.setItem(row, 7, QtWidgets.QTableWidgetItem(value[7]))  # MALZEME ADI
                self.uretimdetaylari.setItem(row, 8, QtWidgets.QTableWidgetItem(value[8]))  # MALZEME ADI
                self.uretimdetaylari.setItem(row, 9, QtWidgets.QTableWidgetItem(value[9]))  # MALZEME ADI
                self.uretimdetaylari.setItem(row, 10, QtWidgets.QTableWidgetItem(str(value[10])))  # OLCUX
                self.uretimdetaylari.setItem(row, 11, QtWidgets.QTableWidgetItem(str(value[11])))  # OLCUY
                self.uretimdetaylari.setItem(row, 12, QtWidgets.QTableWidgetItem(str(value[12])))  # OLCUZ

            self.uretimdetaylari.setColumnHidden(5, False)
            self.uretimdetaylari.setColumnHidden(6, False)
            self.uretimdetaylari.setColumnHidden(7, False)
            self.uretimdetaylari.setColumnHidden(8, False)
            self.uretimdetaylari.setColumnHidden(9, False)
            self.uretimdetaylari.setColumnHidden(10, False)
            self.uretimdetaylari.setColumnHidden(11, False)
            self.uretimdetaylari.setColumnHidden(12, False)

            self.takimlistesiguncelle()


        except:
            import traceback
            traceback.print_exc()
            return

    def takimlistesiguncelle(self):
        try:
            satir = self.isemirlerilistesi.currentRow()
            if satir == -1:
                return
            else:
                isemriid = self.isemirlerilistesi.item(satir, 0).text()
                self.cursor.execute("select * from takimlisteleri where isemirleri_isemri_id = '" + isemriid + "'")
                takimlar = self.cursor.fetchall()
                self.takimlistesi.setRowCount(len(takimlar))
                for satir, data in enumerate(takimlar):
                    self.takimlistesi.setItem(satir, 0, QtWidgets.QTableWidgetItem(str(data[2])))
                    self.takimlistesi.setItem(satir, 1, QtWidgets.QTableWidgetItem(data[3]))
                    self.takimlistesi.setItem(satir, 2, QtWidgets.QTableWidgetItem(str(data[4])))
                    self.takimlistesi.setItem(satir, 3, QtWidgets.QTableWidgetItem(str(data[5])))
                    self.takimlistesi.setItem(satir, 4, QtWidgets.QTableWidgetItem(str(data[6])))
                    self.takimlistesi.setItem(satir, 5, QtWidgets.QTableWidgetItem(str(data[7])))
                    self.takimlistesi.setItem(satir, 6, QtWidgets.QTableWidgetItem(str(data[8])))
                    self.takimlistesi.setItem(satir, 7, QtWidgets.QTableWidgetItem(data[9]))
                    self.takimlistesi.setItem(satir, 8, QtWidgets.QTableWidgetItem(data[10]))
                    self.takimlistesi.setItem(satir, 9, QtWidgets.QTableWidgetItem(str(data[0])))



        except:
            import traceback
            traceback.print_exc()

        pass

    def isemritakibipreview(self):

        try:
            satir = self.uretimdetaylari.currentRow()
            sutun = self.uretimd  # etaylari.currentColumn()
            if sutun == 0:
                path = self.uretimdetaylari.item(satir, 7).text()
                if len(path) == 0:
                    self.preview.setText("PREVIEW")
                else:
                    pixmap = QPixmap(path)
                    # pixmap= pixmap.scaledToHeight(600)
                    # pixmap= pixmap.scaledToWidth(400)
                    self.preview.setScaledContents(True)
                    self.preview.setPixmap(pixmap)
                    self.preview.setFixedSize(580, 580)
            elif sutun == 1:
                path = self.uretimdetaylari.item(satir, 9).text()
                pixmap = QPixmap(path)
                if len(path) == 0:
                    self.preview.setText("PREVIEW")
                else:
                    pixmap = QPixmap(path)
                    # pixmap= pixmap.scaledToHeight(600)
                    # pixmap= pixmap.scaledToWidth(400)
                    self.preview.setScaledContents(True)
                    self.preview.setPixmap(pixmap)
                    self.preview.setFixedSize(580, 580)
            else:
                pass

        except:
            import traceback
            traceback.print_exc()
            return

        pass