#!/usr/bin/env python
# -*- coding:utf -8-*-
from mysql.connector import (connection)
from PyQt5 import QtWidgets
import isemriformu
from PyQt5.QtGui import QPixmap
import datetime
class Isemirleri(QtWidgets.QDialog, isemriformu.Ui_Dialog):
    def __init__(self, parent=None):
        super(Isemirleri, self).__init__(parent)
        self.setupUi(self)
        try:
            self.conn = connection.MySQLConnection(user='root', password='13579', host='localhost', database='AKMILL')
            self.cursor = self.conn.cursor(buffered=True)
        except:
            self.err = classwarnings.Warnings()
            msg= self.err.databaseconnectionerror()
            QtWidgets.QMessageBox.warning(self,msg[0], msg[1])
            return
        self.cursor.execute("SELECT firma_adi  FROM firmalar")
        firmalar = self.cursor.fetchall()
        for i in firmalar:
            self.firmaadi.addItems(i)
        self.projelistesiguncelle()
        self.firmaadi.currentIndexChanged.connect(self.projelistesiguncelle)
        self.projeadi.currentIndexChanged.connect(self.isemrisiparisdetaylariguncelle)
        self.siparisdetaylari.currentCellChanged.connect(self.isemriuretimdetaylariguncelle)
        self.uretimdetaylari.currentCellChanged.connect(self.isemripreview)
        self.isemrinumarasial.clicked.connect(self.setisemrinumarasi)
        self.takimtipi.currentIndexChanged.connect(self.takimparametreleri)
        self.takimparametreleri()
        self.takimekle.clicked.connect(self.takimkaydet)
        self.takimsil.clicked.connect(self.takimkaldir)
        self.isemriolustur.clicked.connect(self.isemrikaydet)
        self.uretimdetaylari.currentCellChanged.connect(self.malzemeolculerinigetir)
        self.coklu_adet.setDisabled(True)
        self.coklu_isemri.stateChanged.connect(self.cokluisemristatechanged)
        self.taslakolustur.clicked.connect(self.taslakkaydet)
        self.taslakyukle.clicked.connect(self.taslakgetir)

    def cokluisemristatechanged(self):
        durum = self.coklu_isemri.isChecked()
        if durum == True:
            self.coklu_adet.setDisabled(False)
        else:
            self.coklu_adet.setValue(0)
            self.coklu_adet.setDisabled(True)

    def malzemeolculerinigetir(self):
        satir = self.uretimdetaylari.currentRow()
        if satir == -1:
            return
        else:
            olcux = self.uretimdetaylari.item(satir, 10).text()
            olcuy = self.uretimdetaylari.item(satir, 11).text()
            olcuz = self.uretimdetaylari.item(satir, 12).text()
            self.olcux.setText(olcux)
            self.olcuy.setText(olcuy)
            self.olcuz.setText(olcuz)
    def projelistesiguncelle(self):
        self.siparisdetaylari.setRowCount(0)
        self.uretimdetaylari.setRowCount(0)
        self.projeadi.clear()
        # self.uretimemrino.clear()
        try:
            firmaadi = self.firmaadi.currentText()
            self.cursor.execute("select firma_id from firmalar where firma_adi = '" + firmaadi + "'")
            firmaid = self.cursor.fetchone()[0]
            self.cursor.execute("SELECT proje_adi from projeler where FIRMALAR_firma_id = '" + str(firmaid) + "'")
            projeler = self.cursor.fetchall()
            for i in projeler:
                self.projeadi.addItems(i)
        except:
            QtWidgets.QMessageBox.information(self, "BİR SORUNLA KARŞILAŞILDI", "TANIMLI HERHANGİ BİR "
                                                                                "PROJE VEYA FİRMA BULUNMAMAKTADIR \n LÜTFEN FİRMA VEYA PROJE TANIMLAMASINI GERÇEKLEŞTİRİNİZ")
            import traceback
            traceback.print_exc
    def isemrisiparisdetaylariguncelle(self):
        self.siparisdetaylari.setRowCount(0)
        self.uretimdetaylari.setRowCount(0)
        if len(self.projeadi.currentText()) == 0:
            return
        else:
            pass
        self.siparisdetaylari.clearContents()
        try:
            self.cursor.execute("select proje_id from projeler where proje_adi = '" + self.projeadi.currentText() + "'")
            projeid = self.cursor.fetchone()[0]
            self.cursor.execute(
                "select uretim_emir_no from uretimemirleri where PROJELER_proje_id = '" + str(projeid) + "'")
            try:
                uretimemirnumarasi = self.cursor.fetchone()[0]
            except TypeError:
                QtWidgets.QMessageBox.warning(self, "HATA",
                                              "İLGİLİ PROJE İLGİLİ HERHANGİ BİR ÜRETİM EMRİ KAYDI BULUNAMAMŞITR")
                return

            self.cursor.execute(
                "select uretim_emir_id,FIRMALAR_firma_id, PROJELER_proje_id from uretimemirleri where uretim_emir_no = '" + uretimemirnumarasi + "'")
            uretimfirmaprojeid = self.cursor.fetchall()
            uretimid = str(uretimfirmaprojeid[0][0])
            firmaid = str(uretimfirmaprojeid[0][1])
            projeid = str(uretimfirmaprojeid[0][2])
            self.cursor.execute(
                "select uretimemirleri.uretim_emir_no, firmalar.firma_adi , projeler.proje_adi, malzeme_tanimi, miktar, siparis_detay_id from siparisdetaylari  "
                "join uretimemirleri on  uretim_emir_id= '" + uretimid + "'"
                                                                         "join projeler on projeler.proje_id = '" + projeid + "' join firmalar on  firmalar.firma_id = '" + firmaid + "' where siparisdetaylari.URETIMEMIRLERI_uretim_emir_id = '" + uretimid + "'")
            # todo burada kaldin
            siparisdetaylari = self.cursor.fetchall()
            self.siparisdetaylari.setRowCount(len(siparisdetaylari))
            for row, value in enumerate(siparisdetaylari):
                self.siparisdetaylari.setItem(row, 0, QtWidgets.QTableWidgetItem(value[0]))
                self.siparisdetaylari.setItem(row, 1, QtWidgets.QTableWidgetItem(value[1]))
                self.siparisdetaylari.setItem(row, 2, QtWidgets.QTableWidgetItem(value[2]))
                self.siparisdetaylari.setItem(row, 3, QtWidgets.QTableWidgetItem(value[3]))
                self.siparisdetaylari.setItem(row, 4, QtWidgets.QTableWidgetItem(str(value[4])))
                self.siparisdetaylari.setItem(row, 5, QtWidgets.QTableWidgetItem(str(value[5])))
            self.siparisdetaylari.setColumnHidden(5, False)

        except:
            import traceback
            traceback.print_exc()

    def isemriuretimdetaylariguncelle(self):
        satir = self.siparisdetaylari.currentRow()
        if satir == -1:
            return
        else:
            siparisdetayid = self.siparisdetaylari.item(satir, 5).text()
        try:

            self.cursor.execute(
                "select akmill_teknik_resim_no , musteri_teknik_resim_no, uretimdetaylandirma.adet,  operasyon_sayisi, malzeme_adi ,uretim_detaylandirma_id, akmillteknikresimleri.dosya_yolu,"
                "akmillteknikresimleri.dosya_yolu_onizleme, musteriteknikresimleri.dosya_yolu  , musteriteknikresimleri.dosya_yolu_onizleme, olcux, olcuy, olcuz from uretimdetaylandirma "
                "left join akmillteknikresimleri on akmillteknikresimleri.akmill_teknikresim_id =uretimdetaylandirma.akmillteknikresimleri_akmill_teknikresim_id "
                " left join malzemetalepleri on malzemetalepleri.uretimdetaylandirma_uretim_detaylandirma_id = uretimdetaylandirma.uretim_detaylandirma_id "
                "left join musteriteknikresimleri on musteriteknikresimleri.musteri_teknikresim_id = uretimdetaylandirma.musteriteknikresimleri_musteri_teknikresim_id "
                "left join  malzemelistesi on malzemelistesi.malzeme_id = uretimdetaylandirma.malzemelistesi_malzeme_id where uretimdetaylandirma.siparisdetaylari_siparis_detay_id = '" + str(
                    siparisdetayid) + "'")
            uretimdetaylarilistesi = self.cursor.fetchall()
            self.uretimdetaylari.setRowCount(len(uretimdetaylarilistesi))
            for row, value in enumerate(uretimdetaylarilistesi):
                self.uretimdetaylari.setItem(row, 0, QtWidgets.QTableWidgetItem(value[0]))  # AKMILL RESIM NO
                self.uretimdetaylari.setItem(row, 1, QtWidgets.QTableWidgetItem(value[1]))  # MUSTERI RESIM NO
                self.uretimdetaylari.setItem(row, 2, QtWidgets.QTableWidgetItem(str(value[2])))  # ADET
                self.uretimdetaylari.setItem(row, 3, QtWidgets.QTableWidgetItem(str(value[3])))  # OP SAYISI
                self.uretimdetaylari.setItem(row, 4, QtWidgets.QTableWidgetItem(value[4]))  # MALZEME ADI
                self.uretimdetaylari.setItem(row, 5, QtWidgets.QTableWidgetItem(str(value[5])))  # URETIMDETAYID
                self.uretimdetaylari.setItem(row, 6, QtWidgets.QTableWidgetItem(value[6]))  # AKDSOYA
                self.uretimdetaylari.setItem(row, 7, QtWidgets.QTableWidgetItem(value[7]))  # AKDOSYA ONIZLEME
                self.uretimdetaylari.setItem(row, 8, QtWidgets.QTableWidgetItem(value[8]))  # MUSTERI DOSYA
                self.uretimdetaylari.setItem(row, 9, QtWidgets.QTableWidgetItem(value[9]))  # MUSTERI DOSYA ONIZLEME
                self.uretimdetaylari.setItem(row, 10, QtWidgets.QTableWidgetItem(str(value[10])))  # OLCUX
                self.uretimdetaylari.setItem(row, 11, QtWidgets.QTableWidgetItem(str(value[11])))  # OLCUY
                self.uretimdetaylari.setItem(row, 12, QtWidgets.QTableWidgetItem(str(value[12])))  # OLCUZ
                self.cursor.execute(
                    "select parca_adi from akmillteknikresimleri where akmill_teknik_resim_no = '" + str(
                        value[0]) + "'")
                parcaadi = self.cursor.fetchall()[0][0]
                self.uretimdetaylari.setItem(row, 13, QtWidgets.QTableWidgetItem(parcaadi))
                self.cursor.execute(
                    "select count(*) from malzemetalepleri where uretimdetaylandirma_uretim_detaylandirma_id = '" + str(
                        value[5]) + "'")
                kayitmevcutmu = self.cursor.fetchone()[0]
                print kayitmevcutmu
                if kayitmevcutmu == 0:
                    continue
                else:
                    self.cursor.execute(
                        "select adet from malzemetalepleri where uretimdetaylandirma_uretim_detaylandirma_id = '" + str(
                            value[5]) + "'")
                    malzemeadedi = self.cursor.fetchone()[0]
                    self.uretimdetaylari.setItem(row, 14, QtWidgets.QTableWidgetItem(
                        str(malzemeadedi)))  # SİPARİŞ VERİLEN MALZEME ADEDİ

            self.uretimdetaylari.setColumnHidden(5, True)
            self.uretimdetaylari.setColumnHidden(6, True)
            self.uretimdetaylari.setColumnHidden(7, True)
            self.uretimdetaylari.setColumnHidden(8, True)
            self.uretimdetaylari.setColumnHidden(9, True)
            self.uretimdetaylari.setColumnHidden(10, True)
            self.uretimdetaylari.setColumnHidden(11, True)
            self.uretimdetaylari.setColumnHidden(12, True)


        except:
            import traceback
            traceback.print_exc()
            return

        self.malzemeolculerinigetir()

        pass

    def isemripreview(self):

        try:
            satir = self.uretimdetaylari.currentRow()
            sutun = self.uretimdetaylari.currentColumn()
            if sutun == 0:
                path = self.uretimdetaylari.item(satir, 7).text()
                if len(path) == 0:
                    self.preview.setText("PREVIEW")
                else:
                    pixmap = QPixmap(path)
                    pixmap= pixmap.scaledToHeight(600)
                    # pixmap= pixmap.scaledToWidth(400)
                    self.preview.setScaledContents(True)
                    self.preview.setPixmap(pixmap)
                    print path
                    # self.preview.setFixedSize(580, 580)
            elif sutun == 1:
                path = self.uretimdetaylari.item(satir, 9).text()
                pixmap = QPixmap(path)
                if len(path) == 0:
                    self.preview.setText("PREVIEW")
                else:
                    pixmap = QPixmap(path)
                    pixmap= pixmap.scaledToHeight(600)
                    # pixmap= pixmap.scaledToWidth(400)
                    self.preview.setScaledContents(True)
                    self.preview.setPixmap(pixmap)
                    # self.preview.setFixedSize(580, 580)
            else:
                pass

        except:
            import traceback
            traceback.print_exc()
            return

        pass

    def setisemrinumarasi(self):
        satir = self.uretimdetaylari.currentRow()
        if satir == -1:
            QtWidgets.QMessageBox.warning(self, "HATA", "ÜRETİM DETAYI SEÇİMİ YAPMANIZ GEREKMEKTEDİR")
            self.isemrinumarasi.setText("İŞ EMRİ NUMARASI...")
            return
        else:
            uretimemrinumarasi = self.siparisdetaylari.item(0, 0).text()
            uretimdetayid = self.uretimdetaylari.item(satir, 5).text()
            self.cursor.execute(
                "select count(*) from isemirleri where uretimdetaylandirma_uretim_detaylandirma_id = '" + uretimdetayid + "'")
            cikartilanisemrisayisi = self.cursor.fetchone()[0]
            operasyonsayisi = int(self.uretimdetaylari.item(satir, 3).text())
            try:
                if operasyonsayisi <= cikartilanisemrisayisi:
                    QtWidgets.QMessageBox.warning(self, "HATA", "BU PARÇAYA AİT BÜTÜN İŞEMİRLERİ ÇIKARTILMIŞTIR")
                    return
                else:
                    opno = cikartilanisemrisayisi + 1
                self.cursor.execute(
                    "select firma_numaralandirma from firmalar where firma_adi = '" + self.firmaadi.currentText() + "'")
                firmanumarasi = self.cursor.fetchone()[0]
                year = datetime.datetime.now().strftime('%Y')
                self.cursor.execute("select numaralandirma from uretimdetaylandirma where uretim_detaylandirma_id = '"+str(uretimdetayid)+"'")
                parcaisnumarasi = self.cursor.fetchone()[0]



                self.isemrinumarasi.setText(
                    str("I-") + str(uretimemrinumarasi[2:]) + "-" +str(parcaisnumarasi)+ "-OP" + str(opno))
            except:
                import traceback
                traceback.print_exc()

    def takimparametreleri(self):
        if self.takimtipi.currentText() == "TARAMA":
            self.takimcapi.setDisabled(False)
            self.takimboyu.setDisabled(False)
            self.takimradyusu.setDisabled(True)
            self.takimacisi.setDisabled(True)
            self.tcakikalinlik.setDisabled(True)
            self.finismi.setDisabled(True)
            self.takimmalzemesi.setDisabled(True)
        elif self.takimtipi.currentText() == "FREZE":
            self.takimcapi.setDisabled(False)
            self.takimboyu.setDisabled(False)
            self.takimradyusu.setDisabled(False)
            self.takimacisi.setDisabled(True)
            self.tcakikalinlik.setDisabled(True)
            self.finismi.setDisabled(False)
            self.takimmalzemesi.setDisabled(False)
        elif self.takimtipi.currentText() == "MATKAP":
            self.takimcapi.setDisabled(False)
            self.takimboyu.setDisabled(False)
            self.takimradyusu.setDisabled(True)
            self.takimacisi.setDisabled(False)
            self.takimacisi.clear()
            self.takimacisi.addItem("118")
            self.tcakikalinlik.setDisabled(True)
            self.finismi.setDisabled(True)
            self.takimmalzemesi.setDisabled(False)
        elif self.takimtipi.currentText() == "RAYBA":
            self.takimcapi.setDisabled(False)
            self.takimboyu.setDisabled(False)
            self.takimradyusu.setDisabled(True)
            self.takimacisi.setDisabled(True)
            self.tcakikalinlik.setDisabled(True)
            self.finismi.setDisabled(True)
            self.takimmalzemesi.setDisabled(False)
        elif self.takimtipi.currentText() == "NC PUNTA":
            self.takimcapi.setDisabled(False)
            self.takimboyu.setDisabled(False)
            self.takimradyusu.setDisabled(True)
            self.takimacisi.setDisabled(False)
            self.takimacisi.clear()
            self.takimacisi.addItem("30")
            self.takimacisi.addItem("45")
            self.takimacisi.addItem("60")
            self.takimacisi.addItem("90")
            self.tcakikalinlik.setDisabled(True)
            self.finismi.setDisabled(True)
            self.takimmalzemesi.setDisabled(False)
        elif self.takimtipi.currentText() == "U-DRILL":
            self.takimcapi.setDisabled(False)
            self.takimboyu.setDisabled(False)
            self.takimradyusu.setDisabled(True)
            self.takimacisi.setDisabled(True)
            self.tcakikalinlik.setDisabled(True)
            self.finismi.setDisabled(True)
            self.takimmalzemesi.setDisabled(True)
        elif self.takimtipi.currentText() == "T-CAKI":
            self.takimcapi.setDisabled(False)
            self.takimboyu.setDisabled(False)
            self.takimradyusu.setDisabled(True)
            self.takimacisi.setDisabled(True)
            self.tcakikalinlik.setDisabled(False)
            self.finismi.setDisabled(True)
            self.takimmalzemesi.setDisabled(True)
        else:
            pass
        pass

    def takimkaydet(self):
        try:
            item = QtWidgets.QTableWidgetItem()
            item.setData(0, int(self.takimnumarasi.currentText()))
        except:
            QtWidgets.QMessageBox.warning(self, "HATA", "TAKIM NUMARASINI KONTROL EDİNİZ")
            return
        if self.takimtipi.currentText() == "TARAMA":
            try:
                takimcapi = float(self.takimcapi.text())
                takimboyu = float(self.takimboyu.text())
            except:
                QtWidgets.QMessageBox.warning(self, "HATA", "LÜTFEN DEĞERLERİ SAYI OLARKA GRİNİZ")
                return
            rownumber = self.takimlistesi.rowCount()
            self.takimlistesi.insertRow(rownumber)
            rownumber = self.takimlistesi.rowCount()
            rownumber = rownumber - 1
            item = QtWidgets.QTableWidgetItem()
            item1 = QtWidgets.QTableWidgetItem()
            item2 = QtWidgets.QTableWidgetItem()
            item3 = QtWidgets.QTableWidgetItem()
            item4 = QtWidgets.QTableWidgetItem()
            item5 = QtWidgets.QTableWidgetItem()
            item6 = QtWidgets.QTableWidgetItem()
            item7 = QtWidgets.QTableWidgetItem()
            item8 = QtWidgets.QTableWidgetItem()
            try:
                item.setData(0, int(self.takimnumarasi.currentText()))
            except:
                QtWidgets.QMessageBox.warning(self, "HATA", "TAKIM NUMARASINI KONTROL EDİNİZ")
                return
            # item.setText(self.takimnumarasi.currentText())
            item1.setText(self.takimtipi.currentText().encode('utf-8'))
            item2.setText(self.takimcapi.text().encode('utf-8'))
            item3.setText("0")
            item4.setText(self.takimboyu.text().encode('utf-8'))
            item5.setText("0")
            item6.setText("0")
            item7.setText("None")
            item8.setText("None")

            self.takimlistesi.setItem(rownumber, 0, item)
            self.takimlistesi.setItem(rownumber, 1, item1)
            self.takimlistesi.setItem(rownumber, 2, item2)
            self.takimlistesi.setItem(rownumber, 3, item3)
            self.takimlistesi.setItem(rownumber, 4, item4)
            self.takimlistesi.setItem(rownumber, 5, item5)
            self.takimlistesi.setItem(rownumber, 6, item6)
            self.takimlistesi.setItem(rownumber, 7, item7)
            self.takimlistesi.setItem(rownumber, 8, item8)

            self.takimnumarasi.removeItem(self.takimnumarasi.currentIndex())
        elif self.takimtipi.currentText() == "FREZE":
            try:
                takimcapi = float(self.takimcapi.text())
                takimboyu = float(self.takimboyu.text())
                takimradusu = float(self.takimradyusu.text())
            except:
                QtWidgets.QMessageBox.warning(self, "HATA", "LÜTFEN DEĞERLERİ SAYI OLARKA GRİNİZ")
                return
            try:
                rownumber = self.takimlistesi.rowCount()
                self.takimlistesi.insertRow(rownumber)
                rownumber = self.takimlistesi.rowCount()
                rownumber = rownumber - 1
                item = QtWidgets.QTableWidgetItem()  # takim no
                item1 = QtWidgets.QTableWidgetItem()  # takim tipi
                item2 = QtWidgets.QTableWidgetItem()  # takim cpai
                item3 = QtWidgets.QTableWidgetItem()  # takim radusu
                item4 = QtWidgets.QTableWidgetItem()  # takim boyu
                item5 = QtWidgets.QTableWidgetItem()  # takimacisi
                item6 = QtWidgets.QTableWidgetItem()  # takimkalinlik
                item7 = QtWidgets.QTableWidgetItem()  # takim malzemesi
                item8 = QtWidgets.QTableWidgetItem()  # finis mi kabamı
                try:
                    item.setData(0, int(self.takimnumarasi.currentText()))
                except:
                    QtWidgets.QMessageBox.warning(self, "HATA", "TAKIM NUMARASINI KONTROL EDİNİZ")
                    return
                # item.setText(self.takimnumarasi.currentText())
                item1.setText(self.takimtipi.currentText().encode('utf-8'))
                item2.setText(self.takimcapi.text().encode('utf-8'))
                item3.setText(self.takimradyusu.text().encode('utf-8'))
                item4.setText(self.takimboyu.text().encode('utf-8'))
                item5.setText("0")
                item6.setText("0")
                item7.setText(self.takimmalzemesi.currentText().encode('utf-8'))
                item8.setText("-")

                if self.finismi.isChecked():
                    item8.setText("FINISH")
                else:
                    item8.setText("KABA")
                self.takimlistesi.setItem(rownumber, 0, item)
                self.takimlistesi.setItem(rownumber, 1, item1)
                self.takimlistesi.setItem(rownumber, 2, item2)
                self.takimlistesi.setItem(rownumber, 3, item3)
                self.takimlistesi.setItem(rownumber, 4, item4)
                self.takimlistesi.setItem(rownumber, 5, item5)
                self.takimlistesi.setItem(rownumber, 6, item6)
                self.takimlistesi.setItem(rownumber, 7, item7)
                self.takimlistesi.setItem(rownumber, 8, item8)
                self.takimnumarasi.removeItem(self.takimnumarasi.currentIndex())
            except:
                import traceback
                traceback.print_exc()
        elif self.takimtipi.currentText() == "MATKAP":
            try:
                rownumber = self.takimlistesi.rowCount()
                self.takimlistesi.insertRow(rownumber)
                rownumber = self.takimlistesi.rowCount()
                rownumber = rownumber - 1
                item = QtWidgets.QTableWidgetItem()  # takim no 0
                item1 = QtWidgets.QTableWidgetItem()  # takim tipi 1
                item2 = QtWidgets.QTableWidgetItem()  # takim cpai 2
                item3 = QtWidgets.QTableWidgetItem()  # takim radusu 3
                item4 = QtWidgets.QTableWidgetItem()  # takim boyu 4
                item5 = QtWidgets.QTableWidgetItem()  # takimacisi 5
                item6 = QtWidgets.QTableWidgetItem()  # takimkalinlik 6
                item7 = QtWidgets.QTableWidgetItem()  # takim malzemesi 7
                item8 = QtWidgets.QTableWidgetItem()  # finis mi kabamı 8
                try:
                    item.setData(0, int(self.takimnumarasi.currentText()))
                except:
                    QtWidgets.QMessageBox.warning(self, "HATA", "TAKIM NUMARASINI KONTROL EDİNİZ")
                    return
                # item.setText(self.takimnumarasi.currentText())
                item1.setText(self.takimtipi.currentText().encode('utf-8'))
                item2.setText(self.takimcapi.text().encode('utf-8'))
                item3.setText("0")
                item4.setText(self.takimboyu.text())
                item5.setText(self.takimacisi.currentText().encode('utf-8'))
                item6.setText("0")
                item7.setText(self.takimmalzemesi.currentText().encode('utf-8'))
                item8.setText("None")
                self.takimlistesi.setItem(rownumber, 0, item)
                self.takimlistesi.setItem(rownumber, 1, item1)
                self.takimlistesi.setItem(rownumber, 2, item2)
                self.takimlistesi.setItem(rownumber, 3, item3)
                self.takimlistesi.setItem(rownumber, 4, item4)
                self.takimlistesi.setItem(rownumber, 5, item5)
                self.takimlistesi.setItem(rownumber, 6, item6)
                self.takimlistesi.setItem(rownumber, 7, item7)
                self.takimlistesi.setItem(rownumber, 8, item8)
                self.takimnumarasi.removeItem(self.takimnumarasi.currentIndex())
            except:
                import traceback
                traceback.print_exc()
        elif self.takimtipi.currentText() == "RAYBA":
            try:
                rownumber = self.takimlistesi.rowCount()
                self.takimlistesi.insertRow(rownumber)
                rownumber = self.takimlistesi.rowCount()
                rownumber = rownumber - 1
                item = QtWidgets.QTableWidgetItem()  # takim no 0
                item1 = QtWidgets.QTableWidgetItem()  # takim tipi 1
                item2 = QtWidgets.QTableWidgetItem()  # takim cpai 2
                item3 = QtWidgets.QTableWidgetItem()  # takim radusu 3
                item4 = QtWidgets.QTableWidgetItem()  # takim boyu 4
                item5 = QtWidgets.QTableWidgetItem()  # takimacisi 5
                item6 = QtWidgets.QTableWidgetItem()  # takimkalinlik 6
                item7 = QtWidgets.QTableWidgetItem()  # takim malzemesi 7
                item8 = QtWidgets.QTableWidgetItem()  # finis mi kabamı 8
                try:
                    item.setData(0, int(self.takimnumarasi.currentText()))
                except:
                    QtWidgets.QMessageBox.warning(self, "HATA", "TAKIM NUMARASINI KONTROL EDİNİZ")
                    return
                # item.setText(self.takimnumarasi.currentText())
                item1.setText(self.takimtipi.currentText().encode('utf-8'))
                item2.setText(self.takimcapi.text().encode('utf-8'))
                item3.setText("0")
                item4.setText(self.takimboyu.text().encode('utf-8'))
                item5.setText("0")
                item6.setText("0")
                item7.setText(self.takimmalzemesi.currentText().encode('utf-8'))
                item8.setText("None")
                self.takimlistesi.setItem(rownumber, 0, item)
                self.takimlistesi.setItem(rownumber, 1, item1)
                self.takimlistesi.setItem(rownumber, 2, item2)
                self.takimlistesi.setItem(rownumber, 3, item3)
                self.takimlistesi.setItem(rownumber, 4, item4)
                self.takimlistesi.setItem(rownumber, 5, item5)
                self.takimlistesi.setItem(rownumber, 6, item6)
                self.takimlistesi.setItem(rownumber, 7, item7)
                self.takimlistesi.setItem(rownumber, 8, item8)
                self.takimnumarasi.removeItem(self.takimnumarasi.currentIndex())
            except:
                import traceback
                traceback.print_exc()
        elif self.takimtipi.currentText() == "NC PUNTA":
            try:
                rownumber = self.takimlistesi.rowCount()
                self.takimlistesi.insertRow(rownumber)
                rownumber = self.takimlistesi.rowCount()
                rownumber = rownumber - 1
                item = QtWidgets.QTableWidgetItem()  # takim no 0
                item1 = QtWidgets.QTableWidgetItem()  # takim tipi 1
                item2 = QtWidgets.QTableWidgetItem()  # takim cpai 2
                item3 = QtWidgets.QTableWidgetItem()  # takim radusu 3
                item4 = QtWidgets.QTableWidgetItem()  # takim boyu 4
                item5 = QtWidgets.QTableWidgetItem()  # takimacisi 5
                item6 = QtWidgets.QTableWidgetItem()  # takimkalinlik 6
                item7 = QtWidgets.QTableWidgetItem()  # takim malzemesi 7
                item8 = QtWidgets.QTableWidgetItem()  # finis mi kabamı 8
                try:
                    item.setData(0, int(self.takimnumarasi.currentText()))
                except:
                    QtWidgets.QMessageBox.warning(self, "HATA", "TAKIM NUMARASINI KONTROL EDİNİZ")
                    return
                # item.setText(self.takimnumarasi.currentText())
                item1.setText(self.takimtipi.currentText().encode('utf-8'))
                item2.setText(self.takimcapi.text().encode('utf-8'))
                item3.setText("0")
                item4.setText(self.takimboyu.text().encode('utf-8'))
                item5.setText(self.takimacisi.currentText().encode('utf-8'))
                item6.setText("0")
                item7.setText(self.takimmalzemesi.currentText().encode('utf-8'))
                item8.setText("None")
                self.takimlistesi.setItem(rownumber, 0, item)
                self.takimlistesi.setItem(rownumber, 1, item1)
                self.takimlistesi.setItem(rownumber, 2, item2)
                self.takimlistesi.setItem(rownumber, 3, item3)
                self.takimlistesi.setItem(rownumber, 4, item4)
                self.takimlistesi.setItem(rownumber, 5, item5)
                self.takimlistesi.setItem(rownumber, 6, item6)
                self.takimlistesi.setItem(rownumber, 7, item7)
                self.takimlistesi.setItem(rownumber, 8, item8)
                self.takimnumarasi.removeItem(self.takimnumarasi.currentIndex())
            except:
                import traceback
                traceback.print_exc()
        elif self.takimtipi.currentText() == "U-DRILL":
            try:
                rownumber = self.takimlistesi.rowCount()
                self.takimlistesi.insertRow(rownumber)
                rownumber = self.takimlistesi.rowCount()
                rownumber = rownumber - 1
                item = QtWidgets.QTableWidgetItem()  # takim no 0
                item1 = QtWidgets.QTableWidgetItem()  # takim tipi 1
                item2 = QtWidgets.QTableWidgetItem()  # takim cpai 2
                item3 = QtWidgets.QTableWidgetItem()  # takim radusu 3
                item4 = QtWidgets.QTableWidgetItem()  # takim boyu 4
                item5 = QtWidgets.QTableWidgetItem()  # takimacisi 5
                item6 = QtWidgets.QTableWidgetItem()  # takimkalinlik 6
                item7 = QtWidgets.QTableWidgetItem()  # takim malzemesi 7
                item8 = QtWidgets.QTableWidgetItem()  # finis mi kabamı 8
                try:
                    item.setData(0, int(self.takimnumarasi.currentText()))
                except:
                    QtWidgets.QMessageBox.warning(self, "HATA", "TAKIM NUMARASINI KONTROL EDİNİZ")
                    return
                # item.setText(self.takimnumarasi.currentText())
                item1.setText(self.takimtipi.currentText().encode('utf-8'))
                item2.setText(self.takimcapi.text().encode('utf-8'))
                item3.setText("0")
                item4.setText(self.takimboyu.text().encode('utf-8'))
                item5.setText("0")
                item6.setText("0")
                item7.setText("None")
                item8.setText("None")
                self.takimlistesi.setItem(rownumber, 0, item)
                self.takimlistesi.setItem(rownumber, 1, item1)
                self.takimlistesi.setItem(rownumber, 2, item2)
                self.takimlistesi.setItem(rownumber, 3, item3)
                self.takimlistesi.setItem(rownumber, 4, item4)
                self.takimlistesi.setItem(rownumber, 5, item5)
                self.takimlistesi.setItem(rownumber, 6, item6)
                self.takimlistesi.setItem(rownumber, 7, item7)
                self.takimlistesi.setItem(rownumber, 8, item8)
                self.takimnumarasi.removeItem(self.takimnumarasi.currentIndex())
            except:
                import traceback
                traceback.print_exc()
        elif self.takimtipi.currentText() == "T-CAKI":
            try:
                rownumber = self.takimlistesi.rowCount()
                self.takimlistesi.insertRow(rownumber)
                rownumber = self.takimlistesi.rowCount()
                rownumber = rownumber - 1
                item = QtWidgets.QTableWidgetItem()  # takim no 0
                item1 = QtWidgets.QTableWidgetItem()  # takim tipi 1
                item2 = QtWidgets.QTableWidgetItem()  # takim cpai 2
                item3 = QtWidgets.QTableWidgetItem()  # takim radusu 3
                item4 = QtWidgets.QTableWidgetItem()  # takim boyu 4
                item5 = QtWidgets.QTableWidgetItem()  # takimacisi 5
                item6 = QtWidgets.QTableWidgetItem()  # takimkalinlik 6
                item7 = QtWidgets.QTableWidgetItem()  # takim malzemesi 7
                item8 = QtWidgets.QTableWidgetItem()  # finis mi kabamı 8
                try:
                    item.setData(0, int(self.takimnumarasi.currentText()))
                except:
                    QtWidgets.QMessageBox.warning(self, "HATA", "TAKIM NUMARASINI KONTROL EDİNİZ")
                    return
                # item.setText(self.takimnumarasi.currentText())
                item1.setText(self.takimtipi.currentText().encode('utf-8'))
                item2.setText(self.takimcapi.text().encode('utf-8'))
                item3.setText("0")
                item4.setText(self.takimboyu.text().encode('utf-8'))
                item5.setText("0")
                item6.setText(self.tcakikalinlik.text())
                item7.setText(self.takimmalzemesi.currentText().encode('utf-8'))
                item8.setText("None")
                self.takimlistesi.setItem(rownumber, 0, item)
                self.takimlistesi.setItem(rownumber, 1, item1)
                self.takimlistesi.setItem(rownumber, 2, item2)
                self.takimlistesi.setItem(rownumber, 3, item3)
                self.takimlistesi.setItem(rownumber, 4, item4)
                self.takimlistesi.setItem(rownumber, 5, item5)
                self.takimlistesi.setItem(rownumber, 6, item6)
                self.takimlistesi.setItem(rownumber, 7, item7)
                self.takimlistesi.setItem(rownumber, 8, item8)
                self.takimnumarasi.removeItem(self.takimnumarasi.currentIndex())
            except:
                import traceback
                traceback.print_exc()

        else:
            return

        pass

    def takimkaldir(self):
        try:
            satir = self.takimlistesi.currentRow()
            if satir == -1:
                QtWidgets.QMessageBox.warning(self, "HATA", "TAKIM SEÇİNİZ")
                return
            else:
                takimnumarasi = self.takimlistesi.item(satir, 0).text()
                self.takimnumarasi.addItem(takimnumarasi)
                self.takimlistesi.removeRow(satir)
        except:
            import traceback
            traceback.print_exc()

    def isemrikaydet(self):
        satir = self.uretimdetaylari.currentRow()
        uretimid = self.uretimdetaylari.item(satir, 5).text()
        tahminiayar = self.tahminiayarsuresi.text()
        isemrinumarasi = self.isemrinumarasi.text()
        aciklamalar = self.aciklamalar.toPlainText()
        if len(isemrinumarasi) == 0:
            QtWidgets.QMessageBox.warning(self, "HATA", "İŞ EMRİ NUMARASI ALMANIZ GEREKMEKTEDİR LÜTFEN KONTROL EDİNİZ")
            return
        else:
            self.cursor.execute("select count(*) from isemirleri where isemri_no = '" + isemrinumarasi + "'")
            mevcutmu = self.cursor.fetchone()[0]
            totalrownumber = self.takimlistesi.rowCount()
            if totalrownumber == 0:
                QtWidgets.QMessageBox.warning(self, "HATA", "GEREKLİ TAKIMLANDIRMA İŞLEMLERİNİ GERÇEKLEŞTİRMELİSİNİZ")
                return
            else:
                pass
            if mevcutmu != 0:
                QtWidgets.QMessageBox.warning(self, "HATA",
                                              "DAHA ÖNCE BU İŞ EMRİ NUMARASI İLE KAYIT ALINMIŞ \n KONTROL EDİNİZ")
                return
            else:
                pass
        try:
            zaman = datetime.datetime.now()

            self.cursor.execute("start transaction")

            if self.coklu_isemri.isChecked() == True:
                cokluadet = int(self.coklu_adet.text())
                if cokluadet == 0:
                    QtWidgets.QMessageBox.warning(self, "HATA",
                                                  "ÇOKLU İŞ EMRİ ÇIKARTILMASI İÇİN ADET '0' DAN FARKLI OLMALIDIR!!!")
                    return
                else:
                    isemrinolistesi = []
                    for i in range(cokluadet):
                        isemrinolistesi.append(isemrinumarasi + "-MAK" + str((i + 1)))

            else:
                isemrinolistesi = []
                isemrinolistesi.append(isemrinumarasi)
                pass

            for i in isemrinolistesi:
                self.cursor.execute(
                    "insert into isemirleri (isemri_no,tahmini_ayar_zamani, uretimdetaylandirma_uretim_detaylandirma_id, aciklamalar, isdurumu,tarih) values (%s, %s , %s, %s, %s, %s)",
                    (str(i), tahminiayar, int(uretimid), aciklamalar, "BEKLİYOR", zaman.strftime("%Y-%m-%d")))
                print str(i)
            isemrinumarasi1 = "%" + isemrinumarasi + "%"
            self.cursor.execute("select isemri_id from isemirleri where isemri_no like '" + isemrinumarasi1 + "'")
            isemriid = self.cursor.fetchall()
            print ("işemirleri idleri:", isemriid)
            totalrownumber = self.takimlistesi.rowCount()
            for i in range(0, totalrownumber):
                takimno = int(self.takimlistesi.item(i, 0).text())
                takimtipi = self.takimlistesi.item(i, 1).text()
                takimcapi = float(self.takimlistesi.item(i, 2).text())
                takimradusu = float(self.takimlistesi.item(i, 3).text())
                takimboyu = float(self.takimlistesi.item(i, 4).text())
                takimaci = int(self.takimlistesi.item(i, 5).text())
                takimkalinlik = float(self.takimlistesi.item(i, 6).text())
                takimmalzemesi = self.takimlistesi.item(i, 7).text()
                kabafinish = self.takimlistesi.item(i, 8).text()
                datalar = [isemriid, takimno, takimtipi, takimcapi, takimradusu, takimboyu, takimaci, takimkalinlik,
                           takimmalzemesi, kabafinish]
                for a in isemriid:
                    b = str(a[0])
                    datalar[0] = b
                    # print (type(a[0]), a[0])
                    self.cursor.execute(
                        "insert into takimlisteleri (isemirleri_isemri_id,takimno,takimtipi,takimcapi,takimradusu,takimboyu,takimaci,takimkalinlik,takimmalzemesi,kabafinish) values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                        (datalar))
            QtWidgets.QMessageBox.information(self, "BAŞARILI", "KAYITLAR BAŞARI İLE EKLENDİ")


        except:
            QtWidgets.QMessageBox.warning(self, "HATA",
                                          "BİR HATA İLE KARŞILAŞILDI İŞLEMLER GERİ ALINIYOR LÜTFEN KONTROL EDİNİİZ")
            self.cursor.execute("rollback")
            import traceback
            traceback.print_exc()
            return
        self.conn.commit()
        pass


    def taslakkaydet(self):
        try:
            takimlistesi = []

            satirsayisi = self.takimlistesi.rowCount()
            if satirsayisi == None or satirsayisi == 0:
                QtWidgets.QMessageBox.warning(self, "HATA",
                                              "HERHANGİ BİR KAYIT MEVUCT OLMADIĞI İÇİN İŞLEM YAPILAMAMAKTADIR")
                return
            else:
                pass
            for i in range(0, satirsayisi):
                satirliste = []
                takimno = self.takimlistesi.item(i, 0).text()
                satirliste.append(takimno)
                takimtipi = self.takimlistesi.item(i, 1).text().encode('utf-8')
                satirliste.append(takimtipi)
                cap = self.takimlistesi.item(i, 2).text().encode('utf-8')
                satirliste.append(cap)
                radus = self.takimlistesi.item(i, 3).text().encode('utf-8')
                satirliste.append(radus)
                boy = self.takimlistesi.item(i, 4).text().encode('utf-8')
                satirliste.append(boy)
                aci = self.takimlistesi.item(i, 5).text().encode('utf-8')
                satirliste.append(aci)
                kalinlik = self.takimlistesi.item(i, 5).text().encode('utf-8')
                satirliste.append(kalinlik)
                hasskarbur = self.takimlistesi.item(i, 7).text().encode('utf-8')
                satirliste.append(hasskarbur)
                kabafinis = self.takimlistesi.item(i, 8).text().encode('utf-8')
                satirliste.append(kabafinis)
                takimlistesi.append(satirliste)
            print takimlistesi

            kayitekrani = QtWidgets.QFileDialog.getSaveFileName(self, "Save File",
                                                                directory="\\\\AYAS\\Users\\AYAS\\Desktop\\MRP\\TAKIM TASLAKLARI",
                                                                filter="TEXT (*.txt)")
            with open(kayitekrani[0], 'w') as f:
                for i in takimlistesi:
                    f.write("\n")
                    for a in i:
                        f.write(a + ",")


        except:
            traceback.print_exc()

        pass

    def taslakgetir(self):
        yuklenecekliste = []
        try:
            dosyaadi = QtWidgets.QFileDialog.getOpenFileName(self, "DOSYA AC",
                                                             "\\\\Desktop-qc87hvj\\akmill\\MRP\\TAKIM TASLAKLARI",
                                                             "TEXT (*.txt)")
            with open(dosyaadi[0], 'r') as f:
                contents = f.readlines()
                for x in contents:
                    ayristirma = x.split(",")
                    satir = []
                    for i in ayristirma:
                        satir.append(i)
                    yuklenecekliste.append(satir)

            del yuklenecekliste[0]
            takimsayisi = len(yuklenecekliste)
            self.takimlistesi.setRowCount(takimsayisi)
            for sirano, a in enumerate(yuklenecekliste):

                self.takimlistesi.setItem(sirano, 0, QtWidgets.QTableWidgetItem((a[0])))
                self.takimlistesi.setItem(sirano, 1, QtWidgets.QTableWidgetItem((a[1])))
                # self.takimnumarasi.remove,
                toplamliste = self.takimnumarasi.count()
                for y in range(0, toplamliste):
                    print self.takimnumarasi.itemText(y)
                    print str(a[0])
                    if self.takimnumarasi.itemText(y) == str(a[0]):
                        self.takimnumarasi.removeItem(y)
                    else:
                        continue

                self.takimlistesi.setItem(sirano, 2, QtWidgets.QTableWidgetItem((a[2])))
                self.takimlistesi.setItem(sirano, 3, QtWidgets.QTableWidgetItem((a[3])))
                self.takimlistesi.setItem(sirano, 4, QtWidgets.QTableWidgetItem((a[4])))
                self.takimlistesi.setItem(sirano, 5, QtWidgets.QTableWidgetItem((a[5])))
                self.takimlistesi.setItem(sirano, 6, QtWidgets.QTableWidgetItem((a[6])))
                self.takimlistesi.setItem(sirano, 7, QtWidgets.QTableWidgetItem((a[7])))
                self.takimlistesi.setItem(sirano, 8, QtWidgets.QTableWidgetItem((a[8])))


        except:
            traceback.print_exc()
